

--USE [FHPDataMarts]
--GO



--IF OBJECT_ID('dbo.Membership_Export_Member_SOURCE', 'V') IS NOT NULL
--	DROP VIEW dbo.Membership_Export_Member_SOURCE;
--GO



--CREATE  VIEW [dbo].[Membership_Export_Member_SOURCE]

--AS

---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: June 25th 2019
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================

--	WITH LATEST_ELIGIBILITY_RANKED AS
--	(

--		-- Revisit this, since it's rather crude.   Members going inactive could suddenly jump to a new LOB upon becoming inactive.   Need dual eligibility system -- actual, and reported.
--		SELECT
--			 [MEMBER_ELIGIBILITY_ROW_ID]
--			,[MEMBER_MASTER_ROW_ID]
--			,ROW_NUMBER() OVER (PARTITION BY ME.[MEMBER_MASTER_ROW_ID] ORDER BY CASE WHEN ME.[STATUS] = 'ACTIVE' THEN 1 ELSE 2 END ASC, ME.[START_DATE] DESC, ME.[TERM_DATE] DESC) AS [RANK]
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		WHERE ME.[ROW_DELETED] = 'N'

--		--SELECT
--		--	 [MEMBER_ELIGIBILITY_ROW_ID]
--		--	,[MEMBER_MASTER_ROW_ID]
--		--	,ROW_NUMBER() OVER (PARTITION BY [MEMBER_MASTER_ROW_ID] ORDER BY CASE WHEN [STATUS] = 'ACTIVE' THEN [HISTORICAL_ROW_CREATE_DATE] ELSE CAST('01-01-1800' AS DATETIME2) END DESC, CASE WHEN [START_DATE] < GETDATE() THEN 1 ELSE 0 END DESC, LOBH.LOB_RANK ASC, [START_DATE] DESC, [HISTORICAL_ROW_CREATE_DATE] DESC, [MEMBER_ELIGIBILITY_HISTORICAL_ROW_ID] DESC) AS [RANK]
--		--FROM [FHPDataMarts].[dbo].[Member_Eligibility_Historical] MEH
--		--JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
--		--	ON MEH.[LOB_TYPE] = LOBH.[LOB_TYPE]
--		--	AND MEH.[LOB_VENDOR] = LOBH.[LOB_VENDOR]
--		--WHERE MEH.[ROW_DELETED] = 'N'
--	)
--	,LATEST_ELIGIBILITY AS
--	(
--		SELECT 
--			 [MEMBER_ELIGIBILITY_ROW_ID]
--			,[MEMBER_MASTER_ROW_ID]
--		FROM LATEST_ELIGIBILITY_RANKED
--		WHERE [RANK] = 1
--	)
--	,FHP_LOB_DETAILS AS
--	(

--		-- SUBCAPS
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD
--			ON ME.[LOB_TYPE] = LOBD.[LOB_TYPE]
--			AND ME.[LOB_VENDOR] = LOBD.[LOB_VENDOR]
--		JOIN LATEST_ELIGIBILITY LE
--			ON LE.[MEMBER_ELIGIBILITY_ROW_ID] = ME.[MEMBER_ELIGIBILITY_ROW_ID]
--		WHERE ME.[LOB_TYPE] = 'SUBCAP'
		
--		UNION ALL

--		-- ESCO's
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD
--			ON ME.[LOB_TYPE] = LOBD.[LOB_TYPE]
--			AND ME.[LOB_VENDOR] = LOBD.[LOB_VENDOR]
--			AND ME.[ESCO_ID] = LOBD.[LOB_ID]
--		JOIN LATEST_ELIGIBILITY LE
--			ON LE.[MEMBER_ELIGIBILITY_ROW_ID] = ME.[MEMBER_ELIGIBILITY_ROW_ID]
--		WHERE ME.[LOB_TYPE] = 'ESCO'

--		UNION ALL

--		-- Prelim ESCO's
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD
--			ON ME.[ESCO_ID] = LOBD.[LOB_ID]   -- NOTE:  Prelim ESCO's share ESCO ID's with regular ESCO's
--		JOIN LATEST_ELIGIBILITY LE
--			ON LE.[MEMBER_ELIGIBILITY_ROW_ID] = ME.[MEMBER_ELIGIBILITY_ROW_ID]
--		WHERE ME.[LOB_TYPE] = 'PRELIM_ESCO'
--		AND LOBD.[LOB_TYPE] = 'ESCO'

--		UNION ALL

--		-- CKD
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD
--			ON ME.[LOB_TYPE] = LOBD.[LOB_TYPE]
--			AND ME.[LOB_VENDOR] = LOBD.[LOB_VENDOR]
--		JOIN LATEST_ELIGIBILITY LE
--			ON LE.[MEMBER_ELIGIBILITY_ROW_ID] = ME.[MEMBER_ELIGIBILITY_ROW_ID]
--		WHERE ME.[LOB_TYPE] = 'CKD'

--	)
--	SELECT DISTINCT
--		 MM.[MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,NULL AS [HEALTHCLOUD_ID]
--		,MM.[SSN]
--		,MM.[HICN]
--		,MM.[CLAIM_SUBSCRIBER_ID]
--		,MM.[MBI]
--		,MM.[MEDICAID_NO]
--		,CASE WHEN MM.[MRN] LIKE '99999%' AND LEN(MM.[MRN]) = 9 THEN NULL ELSE MM.[MRN] END AS [MRN]   --Remove fake MRN's, which start with '99999'.
--		,MM.[FIRST_NAME]
--		,MM.[MIDDLE_NAME]
--		,MM.[LAST_NAME]
--		,MM.[DATE_OF_BIRTH]
--		,MM.[GENDER]
--		,MM.[RACE]
--		,NULL [CKD_STAGE] --MM.[CKD_STAGE]   --WILL BE ADDED TO MM LATER
--		,MAL.[ADDRESS_1]
--		,MAL.[ADDRESS_2]
--		,MAL.[ADDRESS_3]
--		,MAL.[CITY]
--		,MAL.[STATE]
--		,MAL.[ZIP]
--		,MAL.[COUNTY]
--		,MAL.[ISLAND]
--		,ISNULL(MAL.[COUNTRY],'USA') AS [COUNTRY]
--		,MAL.[PHONE]
--		,MAL.[ALTERNATE_PHONE]
--		,MAL.[EVENING_PHONE]
--		,MAL.[EMERGENCY_PHONE]
--		,MAL.[FAX]
--		,MAL.[EMAIL]
--		,MM.[STATUS]
--		,LE.MEMBER_ELIGIBILITY_ROW_ID AS [LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,MM.[LOB_VENDOR]
--		,MM.[LOB_TYPE]


--		,CASE
--			WHEN MM.[LOB_TYPE] IN ('SUBCAP','CKD') THEN 'PAYOR'
--			WHEN MM.[LOB_TYPE] IN ('ESCO','PRELIM_ESCO') THEN 'ESCO'
--			ELSE NULL
--		 END AS [HC_LOB]
--		,CASE
--			WHEN MM.[LOB_TYPE] IN ('SUBCAP','CKD') THEN MM.[LOB_VENDOR]
--			WHEN MM.[LOB_TYPE] IN ('ESCO','PRELIM_ESCO') AND LE.MEMBER_MASTER_ROW_ID IS NOT NULL THEN CONCAT(LOBD.[LOB_ID],' - ',LOBD.[SHORT_NAME])
--			ELSE NULL
--		 END AS [HC_GROUP]
--		,CASE
--			WHEN MM.[LOB_TYPE] = 'SUBCAP' AND MM.[LOB_VENDOR] IN ('AETNA','COVENTRY','PROSPECT','HUMANA') THEN 'ESRD MA'
--			WHEN MM.[LOB_TYPE] = 'SUBCAP' AND MM.[LOB_VENDOR] IN ('CIGNA','UNITED') THEN 'ESRD COMMERCIAL'
--			WHEN MM.[LOB_TYPE] = 'ESCO' THEN 'ESRD'
--			WHEN MM.[LOB_TYPE] = 'PRELIM_ESCO' THEN 'Prelim ESCO'
--			WHEN MM.[LOB_TYPE] = 'CKD' AND MM.[LOB_VENDOR] IN ('HUMANA') THEN 'CKD'
--			ELSE NULL
--		 END AS [HC_PROGRAM]

--		,MM.[ROW_SOURCE]
--		,MM.[ROW_SOURCE_ID]
--		,MM.[ROW_PROBLEM]
--		,MM.[ROW_PROBLEM_DATE]
--		,MM.[ROW_PROBLEM_REASON]
--		,MM.[ROW_DELETED]
--		,MM.[ROW_DELETED_DATE]
--		,MM.[ROW_DELETED_REASON]
--		,MM.[ROW_CREATE_DATE]
--		,MM.[ROW_UPDATE_DATE]
--	FROM [FHPDW].[dbo].[Member_Master] MM
--	JOIN LATEST_ELIGIBILITY LE -- This joins out any members that don't have eligibility records [7/22/2019]
--		ON MM.[MEMBER_MASTER_ROW_ID] = LE.MEMBER_MASTER_ROW_ID
--	LEFT JOIN [FHPDataMarts].[dbo].[Member_Address_Latest] MAL
--		ON MM.[MEMBER_MASTER_ROW_ID] = MAL.MEMBER_MASTER_ROW_ID
--		AND MAL.[ADDRESS_TYPE] = 'PERMANENT'
--	LEFT JOIN FHP_LOB_DETAILS LOBD
--		ON LOBD.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
--	WHERE MM.[LOB_TYPE] <> 'CSNP'  -- Do not send CSNP records
--	AND MM.[FIRST_NAME] IS NOT NULL -- Business requirement [7/22/2019]
--	AND MM.[LAST_NAME] IS NOT NULL -- Business requirement [7/22/2019]
	

--GO


--GRANT SELECT ON [dbo].[Membership_Export_Member_SOURCE] TO [FHP_DATAMARTS_READ_SPECIFIC_DATA_TEAM];
--GO
--GRANT VIEW DEFINITION ON [dbo].[Membership_Export_Member_SOURCE] TO [FHP_DATAMARTS_READ_SPECIFIC_DATA_TEAM];
--GO







--USE [FHPDataMarts]
--GO


--IF OBJECT_ID('dbo.Membership_Export_Eligibility_SOURCE', 'V') IS NOT NULL
--	DROP VIEW dbo.Membership_Export_Eligibility_SOURCE;
--GO



--CREATE  VIEW [dbo].[Membership_Export_Eligibility_SOURCE]

--AS

---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: June 25th 2019
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================

--	WITH FHP_LOB_DETAILS AS
--	(

--		-- SUBCAPS
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD
--			ON ME.[LOB_TYPE] = LOBD.[LOB_TYPE]
--			AND ME.[LOB_VENDOR] = LOBD.[LOB_VENDOR]
--		WHERE ME.[LOB_TYPE] = 'SUBCAP'
		
--		UNION ALL

--		-- ESCO's
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		LEFT JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD  -- LEFT JOIN, because ESCO_ID does not provide 100% coverage.
--			ON ME.[LOB_TYPE] = LOBD.[LOB_TYPE]
--			AND ME.[LOB_VENDOR] = LOBD.[LOB_VENDOR]
--			AND ME.[ESCO_ID] = LOBD.[LOB_ID]
--		WHERE ME.[LOB_TYPE] = 'ESCO'

--		UNION ALL

--		-- Prelim ESCO's
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		LEFT JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD  -- LEFT JOIN, because ESCO_ID does not provide 100% coverage.
--			ON ME.[ESCO_ID] = LOBD.[LOB_ID]   -- NOTE:  Prelim ESCO's share ESCO ID's with regular ESCO's
--		WHERE ME.[LOB_TYPE] = 'PRELIM_ESCO'
--		AND LOBD.[LOB_TYPE] = 'ESCO'

--		UNION ALL

--		-- CKD
--		SELECT
--			 ME.[MEMBER_MASTER_ROW_ID]
--			,ME.[MEMBER_ELIGIBILITY_ROW_ID]
--			,LOBD.*
--		FROM [FHPDW].[dbo].[Member_Eligibility] ME
--		JOIN [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] LOBD
--			ON ME.[LOB_TYPE] = LOBD.[LOB_TYPE]
--			AND ME.[LOB_VENDOR] = LOBD.[LOB_VENDOR]
--		WHERE ME.[LOB_TYPE] = 'CKD'

--	)
--	SELECT DISTINCT
--		 ME.[MEMBER_ELIGIBILITY_ROW_ID] AS [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,ME.[MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,ME.[START_DATE]
--		,ME.[TERM_DATE]
--		,ME.[TERM_REASON]
--		,ME.[STATUS]
--		,ME.[LOB_VENDOR]
--		,ME.[LOB_TYPE]

--		,CASE
--			WHEN ME.[LOB_TYPE] IN ('SUBCAP','CKD') THEN 'PAYOR'
--			WHEN ME.[LOB_TYPE] IN ('ESCO','PRELIM_ESCO') THEN 'ESCO'
--			ELSE NULL
--		 END AS [HC_LOB]
--		,CASE
--			WHEN ME.[LOB_TYPE] IN ('SUBCAP','CKD') THEN ME.[LOB_VENDOR]
--			WHEN ME.[LOB_TYPE] IN ('ESCO','PRELIM_ESCO') THEN CONCAT(LOBD.[LOB_ID],' - ',LOBD.[SHORT_NAME])
--			ELSE NULL
--		 END AS [HC_GROUP]
--		,CASE
--			WHEN ME.[LOB_TYPE] = 'SUBCAP' AND ME.[LOB_VENDOR] IN ('AETNA','COVENTRY','PROSPECT','HUMANA') THEN 'ESRD MA'
--			WHEN ME.[LOB_TYPE] = 'SUBCAP' AND ME.[LOB_VENDOR] IN ('CIGNA','UNITED') THEN 'ESRD COMMERCIAL'
--			WHEN ME.[LOB_TYPE] = 'ESCO' THEN 'ESRD'
--			WHEN ME.[LOB_TYPE] = 'PRELIM_ESCO' THEN 'Prelim ESCO'
--			WHEN ME.[LOB_TYPE] = 'CKD' AND ME.[LOB_VENDOR] IN ('HUMANA') THEN 'CKD'
--			ELSE NULL
--		 END AS [HC_PROGRAM]

--		,ME.[ROW_SOURCE]
--		,ME.[ROW_SOURCE_ID]
--		,ME.[ROW_PROBLEM]
--		,ME.[ROW_PROBLEM_DATE]
--		,ME.[ROW_PROBLEM_REASON]
--		,ME.[ROW_DELETED]
--		,ME.[ROW_DELETED_DATE]
--		,ME.[ROW_DELETED_REASON]
--		,ME.[ROW_CREATE_DATE]
--		,ME.[ROW_UPDATE_DATE]
--	FROM [FHPDW].[dbo].[Member_Eligibility] ME
--	JOIN [FHPDW].[dbo].[Membership_Export_Member] MEM  -- Only include eligiblity records for members that have already been loaded in the Export tables [7/22/2019]
--		ON MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = ME.[MEMBER_MASTER_ROW_ID]
--	LEFT JOIN FHP_LOB_DETAILS LOBD
--		ON LOBD.[MEMBER_MASTER_ROW_ID] = ME.[MEMBER_MASTER_ROW_ID]
--		AND LOBD.[MEMBER_ELIGIBILITY_ROW_ID] = ME.[MEMBER_ELIGIBILITY_ROW_ID]
--	WHERE ME.[LOB_TYPE] <> 'CSNP'  -- Do not send CSNP records


--GO


--GRANT SELECT ON [dbo].[Membership_Export_Eligibility_SOURCE] TO [FHP_DATAMARTS_READ_SPECIFIC_DATA_TEAM];
--GO
--GRANT VIEW DEFINITION ON [dbo].[Membership_Export_Eligibility_SOURCE] TO [FHP_DATAMARTS_READ_SPECIFIC_DATA_TEAM];
--GO




--USE [FHPDataMarts]
--GO


--IF OBJECT_ID('dbo.Membership_Export_Labs_SOURCE', 'V') IS NOT NULL
--	DROP VIEW dbo.Membership_Export_Labs_SOURCE;
--GO



--CREATE  VIEW [dbo].[Membership_Export_Labs_SOURCE]

--AS

---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: September 15th 2019
---- Description:	
---- ==========================================================================================
--/*

--*/
---- ==========================================================================================

	
--	SELECT DISTINCT

--		 [MEMBER_LABS_ROW_ID] [MEMBERSHIP_EXPORT_LABS_ROW_ID]
--		,[MEMBER_MASTER_ROW_ID] [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

--		,[LAB_TYPE]
--		,[LAB_RESULT]
--		,[LAB_LOINC_CODE]
--		,[LAB_SERVICE_DATE]

--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		,[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON]
--		,[ROW_DELETED]
--		,[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]

--	FROM [FHPDW].[dbo].[Member_Labs] Labs



--GO




USE [FHPDW]
GO



IF OBJECT_ID('dbo.Membership_Export_Member_MergeList', 'V') IS NOT NULL
	DROP VIEW dbo.Membership_Export_Member_MergeList;
GO



CREATE  VIEW [dbo].[Membership_Export_Member_MergeList]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: March 12th 2019
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================

SELECT DISTINCT
	 [MEMBER_MASTER_ROW_ID_PRIMARY] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID_PRIMARY]
	,[MEMBER_MASTER_ROW_ID_MERGE] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID_MERGE]
	,[ROW_PROCESSED_DATE] AS [ROW_CREATE_DATE]
	,[ROW_PROCESSED_DATE] AS [ROW_UPDATE_DATE]
FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys]
WHERE ROW_PROCESSED = 'Y'
AND ROW_DELETED = 'N'


GO



GRANT SELECT ON [dbo].[Membership_Export_Member_MergeList] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_Member_MergeList] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO


GRANT SELECT ON [FHPDW].[dbo].[Membership_Export_Member_MergeList] TO fkcIT_read_only
GO
GRANT VIEW DEFINITION ON [FHPDW].[dbo].[Membership_Export_Member_MergeList] TO fkcIT_read_only
GO


USE [FHPDataMarts]
GO

GRANT SELECT ON [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] TO fkcIT_read_only
GO







USE [FHPDW]
GO



IF OBJECT_ID('dbo.Membership_Export_Member_SplitList', 'V') IS NOT NULL
	DROP VIEW dbo.Membership_Export_Member_SplitList;
GO



CREATE  VIEW [dbo].[Membership_Export_Member_SplitList]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: March 12th 2019
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================


	SELECT DISTINCT
		 [MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID_ORIGINAL]
		,[MEMBER_SPLITMEMBER_NEW_PERMANENT_MEMBER_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID_NEW]
		,[ROW_PROCESSED_DATE] AS [ROW_CREATE_DATE]
		,[ROW_PROCESSED_DATE] AS [ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys]
	WHERE ROW_PROCESSED = 'Y'
	AND ROW_DELETED = 'N'

GO


GRANT SELECT ON [dbo].[Membership_Export_Member_SplitList] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_Member_SplitList] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO


GRANT SELECT ON [FHPDW].[dbo].[Membership_Export_Member_SplitList] TO fkcIT_read_only
GO
GRANT VIEW DEFINITION ON [FHPDW].[dbo].[Membership_Export_Member_SplitList] TO fkcIT_read_only
GO



USE [FHPDataMarts]
GO

GRANT SELECT ON [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] TO fkcIT_read_only
GO










USE [FHPDW]
GO



IF OBJECT_ID('dbo.Membership_Export_LOB_LookUp', 'V') IS NOT NULL
	DROP VIEW dbo.Membership_Export_LOB_LookUp;
GO



CREATE  VIEW [dbo].[Membership_Export_LOB_LookUp]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: March 12th 2019
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================


	SELECT DISTINCT
		 [LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_LOB_Hierarchy]
	--WHERE LOB_RANK < 99

GO


GRANT SELECT ON [dbo].[Membership_Export_LOB_LookUp] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_LOB_LookUp] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO


GRANT SELECT ON [FHPDW].[dbo].[Membership_Export_LOB_LookUp] TO fkcIT_read_only
GO

GRANT VIEW DEFINITION ON [FHPDW].[dbo].[Membership_Export_LOB_LookUp] TO fkcIT_read_only
GO


USE [FHPDataMarts]
GO

GRANT SELECT ON [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] TO fkcIT_read_only
GO

