


USE [FHPDataMarts]
GO

-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Eligibility_Source_Cigna', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Eligibility_Source_Cigna;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Eligibility_Source_Cigna] AS


	WITH AllBen_with_SSN AS
	(
		SELECT
			CAST([FHPDataMarts].[dbo].[HICN_to_SSN](AllBen.[HICN]) AS VARCHAR(11)) AS [SSN]
		   ,AllBen.*
		FROM [ESCO Staging].[dbo].[ALL_BENE_ALIGNED] AllBen
	),
	MedHOK_MAX_Internal_ID AS
	(
		SELECT
			 MAX([MHK Member Internal ID]) AS [MHK Member Internal ID]
			,[Member ID]
		FROM [MedHok Nightly].[dbo].[Member]  --38930
		GROUP BY [Member ID]
	)
	SELECT DISTINCT
		 CAST(NULLIF(LTRIM(RTRIM(MedHOK_MAX_Internal_ID.[MHK Member Internal ID])),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[SUBSCRIBER_ID])),'') AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(CASE WHEN LEN(LTRIM(RTRIM(COALESCE(Mem.[SSN],AllBen_with_SSN.[SSN],[FHPDataMarts].[dbo].[HICN_to_SSN](AllBen_with_SSN.[HICN]))))) = 11 THEN LTRIM(RTRIM(COALESCE(Mem.[SSN],AllBen_with_SSN.[SSN],[FHPDataMarts].[dbo].[HICN_to_SSN](AllBen_with_SSN.[HICN])))) ELSE NULL END AS VARCHAR(11)) AS [SSN]
		,CAST(NULLIF(LTRIM(RTRIM(AllBen_with_SSN.[HICN])),'') AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(AllBen_with_SSN.[EXT_ID_2])),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [MBI]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[PTMRN])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ESCO_ID]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[HLTHPLNSYSID])),'') AS INT) AS [HLTH_PLN_SYSID]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[HLTHPLNPRODLINE])),'') AS VARCHAR(50)) AS [HLTH_PLN_PROD_LINE]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[HLTHPLNRPTGRP])),'') AS VARCHAR(50)) AS [HLTH_PLN_RPT_GRP]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[HLTHPLNSTDCARRIERCD])),'') AS VARCHAR(50)) AS [HLTH_PLN_STD_CARRIER_CD]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[GRPNAME])),'') AS VARCHAR(50)) AS [PLAN_NAME]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[EFFECTIVEDATE])),'') AS DATE) AS [START_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[TERMDATE])),'') AS DATE) AS [TERM_DATE]
		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[MEDHOK_Ext_MEMBERELIGIBILITY].[MEMBER_EXT_ID]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[MEMBER_EXT_ID])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
		,CAST(NULLIF(LTRIM(RTRIM('CIGNA')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[MEDHOK_Ext_MEMBERELIGIBILITY] Eli
	JOIN [FHPDataMarts].[dbo].[MEDHOK_Ext_MEMBER] Mem
	ON 	Eli.[MEMBER_EXT_ID] = Mem.[SUBSCRIBER_ID]
	LEFT OUTER JOIN [FHPDataMarts].[dbo].[MEDHOK_Ext_Source_Data] SourceData   --Note:  The member data doesn't contain MRN, so we have to join back to the source.
	ON Eli.[MEMBER_EXT_ID] = SourceData.[MEDHOK_ID]
	--LEFT OUTER JOIN [MedHok Nightly].[dbo].[Member] MHN
	--ON MHN.[Member ID] = Mem.[SUBSCRIBER_ID]
	LEFT OUTER JOIN MedHOK_MAX_Internal_ID
	ON MedHOK_MAX_Internal_ID.[Member ID] = Mem.[SUBSCRIBER_ID]
	LEFT OUTER JOIN AllBen_with_SSN
	ON SourceData.[PTMRN] = AllBen_with_SSN.[MRN]   -- MRN should be a more reliable join
	--ON Mem.[SSN] = AllBen_with_SSN.[SSN]   --Note:  "Mem.[SSN]" contains empty strings and "--" records.  This shouldn't join to anything, but further source cleanup might be required.


GO

