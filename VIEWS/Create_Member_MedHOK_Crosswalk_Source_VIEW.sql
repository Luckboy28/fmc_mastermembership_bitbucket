USE [FHPDataMarts]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_MedHOK_Crosswalk_Source', 'V') IS NOT NULL
	DROP VIEW dbo.Member_MedHOK_Crosswalk_Source;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_MedHOK_Crosswalk_Source] AS
	
	-- Find all the unique pairings between MEMBER_MASTER_ROW_ID and MEDHOK_ID.
	WITH Matches AS
	(
		SELECT DISTINCT
			 MEMBER_MASTER_ROW_ID
			,MEDHOK_ID
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE MEDHOK_ID IS NOT NULL
		AND ROW_DELETED = 'N'
	)
	-- Join to MedHOK Nightly database to find matching MedHOK members.
	-- Note:  This eliminates all cases where the ID does not exist in both Master Member and MedHOK.
	,CrosswalkTable AS
	(
		SELECT
			 Matches.[MEMBER_MASTER_ROW_ID]
			,Matches.[MEDHOK_ID]
			,MedHOK.[Member ID]
		FROM [MedHok Nightly].[dbo].[Member] MedHOK
		JOIN Matches
		ON Matches.MEDHOK_ID = MedHOK.[Member ID]
		--WHERE Matches.MEMBER_MASTER_ROW_ID NOT IN (53250, 50839)
	)
	-- Rank the MedHOK eligiblity table to find the most recently used eligibility for each MedHOK member
	,MedHOK_Eligibility_RankedData AS
	(
		SELECT
			 [MHK Member Internal ID]
			,[Line of Business]
			,[Effective Date]
			,[Term Date]
			,[Updated At]
			,[MHK Eligibility Internal ID]
			,ROW_NUMBER() OVER (PARTITION BY [MHK Member Internal ID] ORDER BY
				CASE WHEN [Term Date] IS NULL
					THEN
						(CASE [Line of Business]
							WHEN 'Medicare (CSNP)' THEN 1

							WHEN 'Humana Medicare Sub-Capitation' THEN 2
							WHEN 'Aetna Medicare Sub-Capitation' THEN 2
							WHEN 'Cigna Commercial' THEN 2
							WHEN 'Coventry Medicare Sub-Capitation' THEN 2

							WHEN 'ESCO' THEN 3

							WHEN 'United Commercial Sub-Capitation' THEN 4
							WHEN 'Indianapolis ESCO Excluded Clinics' THEN 4
							WHEN 'Shared Savings Risk LOB 1' THEN 4
							ELSE 9999999
						END) 
					ELSE 9999999
					END ASC  -- For the LOB Hierarchy, move inactive LOBs to the bottom of the ranking list.
				,CAST([Effective Date] AS DATE) DESC, CAST([Updated At] AS DATE) DESC, [MHK Eligibility Internal ID] DESC) AS [RANK]
		FROM [MedHok Nightly].[dbo].[Eligibility]
		--ORDER BY [MHK Member Internal ID],[Effective Date]
	)
	-- Join back to the MedHOK member table, and limit the results to only the most recent MedHOK eligibily records
	,MedHOK_Eligibility_Latest AS
	(
		SELECT
			Member.[Member ID]
			,RD.*
		FROM MedHOK_Eligibility_RankedData RD
		JOIN [MedHok Nightly].[dbo].[Member]
		ON MEMBER.[MHK Member Internal ID] = RD.[MHK Member Internal ID]
		WHERE [RANK] = 1
	)
	-- Next, join to the CrosswalkTable CTE to compare all of the latest MedHOK eligibilities with the Master Membership.
	-- This comparison allows us to select the MedHOK member with the latest MedHOK ID.
	,Member_Master_with_MedhokID AS
	(
		SELECT
		 MEMBER_MASTER_ROW_ID
		,MEDHOK_ID
		--,ROW_NUMBER() OVER (PARTITION BY MEMBER_MASTER_ROW_ID ORDER BY CAST([Effective Date] AS DATE) DESC, CAST([Updated At] AS DATE) DESC, [MHK Eligibility Internal ID] DESC) AS [FINAL_RANK]
		,ROW_NUMBER() OVER (PARTITION BY CrosswalkTable.[MEMBER_MASTER_ROW_ID] ORDER BY
				CASE WHEN [Term Date] IS NULL
					THEN
						(CASE [Line of Business]
							WHEN 'Medicare (CSNP)' THEN 1

							WHEN 'Humana Medicare Sub-Capitation' THEN 2
							WHEN 'Aetna Medicare Sub-Capitation' THEN 2
							WHEN 'Cigna Commercial' THEN 2
							WHEN 'Coventry Medicare Sub-Capitation' THEN 2

							WHEN 'ESCO' THEN 3

							WHEN 'United Commercial Sub-Capitation' THEN 4
							WHEN 'Indianapolis ESCO Excluded Clinics' THEN 4
							WHEN 'Shared Savings Risk LOB 1' THEN 4
							ELSE 9999999
						END) 
					ELSE 9999999
					END ASC  -- For the LOB Hierarchy, move inactive LOBs to the bottom of the ranking list.
				,CAST([Effective Date] AS DATE) DESC, CAST([Updated At] AS DATE) DESC, [MHK Eligibility Internal ID] DESC) AS [FINAL_RANK]
		FROM CrosswalkTable
		JOIN MedHOK_Eligibility_Latest
		ON CrosswalkTable.[Member ID] = MedHOK_Eligibility_Latest.[Member ID]
	)
	-- This is the ranked data for members already in MedHOK
	,Final_MedHOK_Ranked_Data AS
	(
		SELECT
			 Final.MEMBER_MASTER_ROW_ID
			,Final.MEDHOK_ID
			,CASE WHEN [FINAL_RANK] = 1 AND XW.[MEMBER_MASTER_ROW_ID] IS NULL THEN 'PRIMARY' ELSE 'DUPLICATE' END AS [MEDHOK_ID_TYPE]
		FROM Member_Master_with_MedhokID Final
		LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW
		ON Final.MEMBER_MASTER_ROW_ID = XW.MEMBER_MASTER_ROW_ID
	)
	-- Catch all the members that aren't in MedHOK yet (or don't have MedHOK eligibility)
	,Historical_MedHOKs AS
	(
		SELECT DISTINCT
			 MEMBER_MASTER_ROW_ID
			,MEDHOK_ID
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE MEDHOK_ID IS NOT NULL
		AND ROW_DELETED = 'N'
	)
	-- Rank the data to avoid possible dupes.  (This should never happen)
	,Historical_MedHOKs_Ranked AS
	(
		SELECT 
			 MEMBER_MASTER_ROW_ID
			,MEDHOK_ID
			,ROW_NUMBER() OVER (PARTITION BY MEMBER_MASTER_ROW_ID ORDER BY MEMBER_MASTER_ROW_ID) AS [RANK]
		FROM Historical_MedHOKs
	)
	,Historical_MedHOKs_Final AS
	(
		SELECT
			 MEMBER_MASTER_ROW_ID
			,MEDHOK_ID
		FROM Historical_MedHOKs_Ranked
		WHERE [RANK] = 1
	)
	,Final_Unioned AS
	(
		SELECT
			 MEMBER_MASTER_ROW_ID
			,MEDHOK_ID
			,MEDHOK_ID_TYPE
		FROM Final_MedHOK_Ranked_Data

		UNION ALL

		SELECT
			 Historical_MedHOKs.MEMBER_MASTER_ROW_ID
			,Historical_MedHOKs.MEDHOK_ID
			,'PRIMARY' [MEDHOK_ID_TYPE]
		FROM Historical_MedHOKs
		LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW
		ON Historical_MedHOKs.MEDHOK_ID = XW.MEDHOK_ID
		LEFT OUTER JOIN [MedHok Nightly].[dbo].[Member] MH
		ON MH.[Member ID] = Historical_MedHOKs.MEDHOK_ID
		WHERE XW.MEDHOK_ID IS NULL
		AND Historical_MedHOKs.MEDHOK_ID NOT IN (SELECT MEDHOK_ID FROM Final_MedHOK_Ranked_Data)
	)
	SELECT
		 MEMBER_MASTER_ROW_ID
		,MEDHOK_ID
		,MEDHOK_ID_TYPE
	FROM Final_Unioned

	EXCEPT   -- Remove MEMBER_MASTER_ROW_ID / MEDHOK_ID combinations that already exist as 'PRIMARY'

	SELECT
		 MEMBER_MASTER_ROW_ID
		,MEDHOK_ID
		,'PRIMARY' AS [MEDHOK_ID_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW

	EXCEPT   -- Remove MEMBER_MASTER_ROW_ID / MEDHOK_ID combinations that already exist as 'DUPLICATE'

	SELECT
		 MEMBER_MASTER_ROW_ID
		,MEDHOK_ID
		,'DUPLICATE' AS [MEDHOK_ID_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW


GO