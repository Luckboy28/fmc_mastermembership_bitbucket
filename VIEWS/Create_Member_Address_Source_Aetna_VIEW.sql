

USE [FHPDataMarts]
GO

-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Address_Source_Aetna', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Address_Source_Aetna;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Address_Source_Aetna] AS

	-- Member's Permanent Address
	SELECT DISTINCT

		 CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[MEDHOK_ID])),'') AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[PTSSN])),'') AS VARCHAR(11)) AS [SSN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [MBI]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[PTMRN])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]

		,CAST(NULLIF(LTRIM(RTRIM('PERMANENT')),'') AS VARCHAR(50)) AS [ADDRESS_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Aetna.[PTSTADDR]))),'') AS VARCHAR(100)) AS [ADDRESS_1]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(100)) AS [ADDRESS_2]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(100)) AS [ADDRESS_3]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Aetna.[PTCITYNAME]))),'') AS VARCHAR(50)) AS [CITY]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Aetna.[PTSTATE]))),'') AS VARCHAR(50)) AS [STATE]
		,CAST(

			CASE
				WHEN
					NULLIF(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[PTZIPCD],'-',''),'~',''))),'') IS NULL
				THEN
					NULL
				ELSE
					CASE
						WHEN
							LEN(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[PTZIPCD],'-',''),'~','')))) > 5 
						THEN
							CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[PTZIPCD],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[PTZIPCD],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[PTZIPCD],'-',''),'~',''))))))
						ELSE
							LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[PTZIPCD],'-',''),'~','')))
					END
			END
		
		AS VARCHAR(10)) AS [ZIP]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(50)) AS [COUNTY]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(50)) AS [ISLAND]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(50)) AS [COUNTRY]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [EFFECTIVE_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [TERM_DATE]
		,CAST(NULLIF(LTRIM(RTRIM('ACTIVE')),'') AS VARCHAR(10)) AS [ADDRESS_STATUS]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ALTERNATE_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EVENING_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EMERGENCY_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [FAX]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EMAIL]

		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[Aetna_Source_Data]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[PTMRN])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
		,CAST(NULLIF(LTRIM(RTRIM('AETNA')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]

	FROM [FHPDataMarts].[dbo].[Aetna_Source_Data] Aetna
	WHERE Aetna.[PTMRN] IS NOT NULL

	UNION ALL

	-- 
	SELECT DISTINCT

		 CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[MEDHOK_ID])),'') AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[PTSSN])),'') AS VARCHAR(11)) AS [SSN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [MBI]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[PTMRN])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]

		,CAST(NULLIF(LTRIM(RTRIM('INSURED')),'') AS VARCHAR(50)) AS [ADDRESS_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Aetna.[INSUREDSTADDR]))),'') AS VARCHAR(100)) AS [ADDRESS_1]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(100)) AS [ADDRESS_2]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(NULL))),'') AS VARCHAR(100)) AS [ADDRESS_3]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Aetna.[INSUREDCITY]))),'') AS VARCHAR(50)) AS [CITY]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Aetna.[INSUREDSTATE]))),'') AS VARCHAR(50)) AS [STATE]
		,CAST(

			CASE
				WHEN
					NULLIF(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[INSUREDZIP],'-',''),'~',''))),'') IS NULL
				THEN
					NULL
				ELSE
					CASE
						WHEN
							LEN(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[INSUREDZIP],'-',''),'~','')))) > 5 
						THEN
							CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[INSUREDZIP],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[INSUREDZIP],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[INSUREDZIP],'-',''),'~',''))))))
						ELSE
							LTRIM(RTRIM(REPLACE(REPLACE(Aetna.[INSUREDZIP],'-',''),'~','')))
					END
			END
		
		AS VARCHAR(10)) AS [ZIP]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [COUNTY]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ISLAND]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [COUNTRY]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [EFFECTIVE_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [TERM_DATE]
		,CAST(NULLIF(LTRIM(RTRIM('ACTIVE')),'') AS VARCHAR(10)) AS [ADDRESS_STATUS]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ALTERNATE_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EVENING_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EMERGENCY_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [FAX]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EMAIL]

		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[Aetna_Source_Data]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(Aetna.[PTMRN])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
		,CAST(NULLIF(LTRIM(RTRIM('AETNA')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]

	FROM [FHPDataMarts].[dbo].[Aetna_Source_Data] Aetna
	WHERE Aetna.[PTMRN] IS NOT NULL

GO

