
USE [FHPDataMarts]
GO
-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Labs_Combined_Sources', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Labs_Combined_Sources;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Labs_Combined_Sources] AS

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM [FHPDataMarts].[dbo].[Member_Labs_Source_Humana_CKD]


GO



