

USE [FHPDataMarts]
GO



IF OBJECT_ID('dbo.Member_PatientEvents_Combined_Sources', 'V') IS NOT NULL
	DROP VIEW dbo.Member_PatientEvents_Combined_Sources;
GO



CREATE VIEW [dbo].[Member_PatientEvents_Combined_Sources]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: September 27th 2019
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================



	/***********************************************************************************

	                            Humana CKD Commorbidities

	 ***********************************************************************************/

	WITH PreCast_Data AS
	(

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,CONCAT('DIABETES:',[Diab_Type]) [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [DIABETES] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,'HYPERTENSION' [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [HYPERTENSION] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,'CARDIOVASCULAR' [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [Cardiovascular] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,'PERIPHERAL VASCULAR DISEASE' [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [PVD] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,CASE WHEN [CHF_STAGE] IS NOT NULL AND [CHF_STAGE] <> '' THEN CONCAT('CHRONIC HEART FAILURE:STAGE ',[CHF_STAGE]) ELSE 'CHRONIC HEART FAILURE' END [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [CHF] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,'METABOLIC SYNDROME' [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [Metabolic_syndrome] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,'PROTEINURIA' [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [PROTEINURIA] = 1

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'COMORBIDITY' [EVENT_TYPE]
			,CONCAT('CKD STAGE: ',[CKD_STAGE]) [EVENT_DESCRIPTION]
			,NULL [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [CKD_STAGE] <> 'UNKNOWN'
		AND [CKD_STAGE] <> ''
		AND [CKD_STAGE] IS NOT NULL

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'HUMANA_RISK_CATEGORY' [EVENT_TYPE]
			,[cat] [EVENT_DESCRIPTION]
			,HU.[ROW_CREATE_DATE] [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,'This data is based on Humana''s internal ranking algorithms.' [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [cat] <> 'UNKNOWN'
		AND [cat] <> ''
		AND [cat] IS NOT NULL

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'HUMANA_RISK_SCORE' [EVENT_TYPE]
			,[SCORE] [EVENT_DESCRIPTION]
			,HU.[ROW_CREATE_DATE] [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,'This data is based on Humana''s internal ranking algorithms.' [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [SCORE] <> 'UNKNOWN'
		AND [SCORE] <> ''
		AND [SCORE] IS NOT NULL

		UNION ALL

		SELECT DISTINCT
			 NULL [MEMBER_MASTER_ROW_ID]
			,NULL [MHK_INTERNAL_ID]
			,NULL [MEDHOK_ID]
			,NULL [SSN]
			,NULL [HICN]
			,NULL [CLAIM_SUBSCRIBER_ID]
			,NULL [MBI]
			,NULL [MEDICAID_NO]
			,NULL [MRN]
			,HU.[MBR_ID] [EXT_ID]
			,'HUMANA_CKD_MBR_ID' [EXT_ID_TYPE]
			,'VENDOR_PROGRAM' [EVENT_TYPE]
			,'Humana at Home' [EVENT_DESCRIPTION]
			,HU.[ROW_CREATE_DATE] [EVENT_START_DATE]
			,NULL [EVENT_END_DATE]
			,NULL [EVENT_NOTES]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		WHERE [HAH_IND] = 'Y'
	)
	SELECT

			 CAST([MEMBER_MASTER_ROW_ID] AS INT) [MEMBER_MASTER_ROW_ID]
			,CAST([MHK_INTERNAL_ID] AS INT) [MHK_INTERNAL_ID]
			,CAST([MEDHOK_ID] AS VARCHAR(50)) [MEDHOK_ID]
			,CAST([SSN] AS VARCHAR(11)) [SSN]
			,CAST(NULLIF(LTRIM(RTRIM(COALESCE(PreCast_Data.[HICN],[FHPDev].[dbo].HICN_Validate(Humana_MRN_XREF.[MEDICARE_ID])))),'') AS VARCHAR(12)) AS [HICN]
			,CAST([CLAIM_SUBSCRIBER_ID] AS VARCHAR(50)) [CLAIM_SUBSCRIBER_ID]
			,CAST(NULLIF(LTRIM(RTRIM(COALESCE(PreCast_Data.[MBI],[FHPDev].[dbo].MBI_Validate(Humana_MRN_XREF.[MEDICARE_ID])))),'') AS VARCHAR(11)) AS [MBI]
			,CAST([MEDICAID_NO] AS VARCHAR(50)) [MEDICAID_NO]
			,CAST(NULLIF(LTRIM(RTRIM(COALESCE(PreCast_Data.[MRN],Humana_MRN_XREF.[PT_MRN]))),'') AS VARCHAR(50)) AS [MRN]
			,CAST([EXT_ID] AS VARCHAR(50)) [EXT_ID]
			,CAST([EXT_ID_TYPE] AS VARCHAR(50)) [EXT_ID_TYPE]
			,CAST([EVENT_TYPE] AS VARCHAR(255)) [EVENT_TYPE]
			,CAST([EVENT_DESCRIPTION] AS VARCHAR(255)) AS [EVENT_DESCRIPTION]
			,CAST([EVENT_START_DATE] AS DATE) [EVENT_START_DATE]
			,CAST([EVENT_END_DATE] AS DATE) [EVENT_END_DATE]
			,CAST([EVENT_NOTES] AS VARCHAR(4000)) [EVENT_NOTES]

			,CAST([ROW_SOURCE] AS VARCHAR(500)) [ROW_SOURCE]
			,CAST([ROW_SOURCE_ID] AS VARCHAR(50)) [ROW_SOURCE_ID]

	FROM PreCast_Data
	LEFT OUTER JOIN [FHPDataMarts].[dbo].[CKD_MBR_XREF_HUMANA_IDS] Humana_MRN_XREF
		ON CAST(LTRIM(RTRIM(PreCast_Data.[EXT_ID])) AS VARCHAR(50)) = Humana_MRN_XREF.[SRC_MBR_HIPPA_ID]

GO
