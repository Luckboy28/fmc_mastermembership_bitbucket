
USE [FHPDataMarts]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Master_Source_BCBSMI', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Master_Source_BCBSMI;
GO

-- Union all sources
CREATE VIEW [dbo].[Member_Master_Source_BCBSMI] AS

	WITH MedHOK_MAX_Internal_ID AS
	(
		SELECT
			MAX([MHK Member Internal ID]) AS [MHK Member Internal ID]
			,[Member ID]
		FROM [MedHok Nightly].[dbo].[Member]
		GROUP BY [Member ID]
	),
	Member AS
	(
		SELECT
			 CAST(NULLIF(LTRIM(RTRIM(Member.[SUBSCRIBER_ID])),'') AS VARCHAR(50)) AS [MEDHOK_ID]
			,CAST(NULLIF(NULLIF(NULLIF(LTRIM(RTRIM(Member.[SSN])),''),'--'),'NULL') AS VARCHAR(11)) AS [SSN]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(12)) AS [HICN]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [MBI]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[MEDICAID_NO])),'') AS VARCHAR(50)) AS [MEDICAID_NO]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[EXT_ID_3])),'') AS VARCHAR(50)) AS [MRN]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(1)) AS [BENEFIT_STATUS]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[FIRST_NAME])),'') AS VARCHAR(50)) AS [FIRST_NAME]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[MIDDLE_NAME])),'') AS VARCHAR(50)) AS [MIDDLE_NAME]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[LAST_NAME])),'') AS VARCHAR(50)) AS [LAST_NAME]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PREFIX]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [SUFFIX]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[DATE_OF_BIRTH])),'') AS DATE) AS [DATE_OF_BIRTH]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[GENDER])),'') AS VARCHAR(1)) AS [GENDER]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [COMPANY_DESCRIPTION]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [LOB_CODE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [FAMILY_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PERSON_NUMBER]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [RACE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ETHNICITY]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PRIMARY_LANGUAGE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PRIMARY_LANGUAGE_SOURCE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [SPOKEN_LANGUAGE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [SPOKEN_LANGUAGE_SOURCE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [WRITTEN_LANGUAGE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [WRITTEN_LANGUAGE_SOURCE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [OTHER_LANGUAGE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [OTHER_LANGUAGE_SOURCE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(1)) AS [EMPLOYEE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(15)) AS [PBP_NUMBER]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CURRENT_LIS]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [IPA_GROUP_EXT_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICARE_PLAN_CODE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICARE_TYPE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [DUPLICATE_MEDICAID_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [PREGNANCY_DUE_DATE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(1)) AS [PREGNANCY_INDICATOR]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [BOARD_NUMBER]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [DEPENDENT_CODE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [LEGACY_SUBSCRIBER_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [GROUP_NUMBER]
			--,CAST(NULLIF(LTRIM(RTRIM('BCBSMI')),'') AS VARCHAR(50)) AS [SOURCE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLIENT_SPECIFIC_DATA]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(10)) AS [RELATIONSHIP_CODE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(10)) AS [TIME_ZONE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [DATE_OF_DEATH]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(1)) AS [FOSTER_CARE_FLAG]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(1)) AS [VIP]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[CLINIC_NUMBER])),'') AS VARCHAR(50)) AS [CLINIC_NUMBER]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[MODALITY])),'') AS VARCHAR(50)) AS [MODALITY]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PAYER_NAME]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PAYER_ID_TYPE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PAYER_ID]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [DIALYSIS_START_DATE]
			,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS DATE) AS [KIDNEY_TRANSPLANT_DATE]
			--,CAST(NULLIF(LTRIM(RTRIM('PROSPECT')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
			--,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]
			,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[SubCap_Source_Member].[EXT_ID_3]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
			,CAST(NULLIF(LTRIM(RTRIM(Member.[EXT_ID_3])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
			--,CAST(GETDATE() AS DATETIME2) AS [CREATE_DATE]
			--,CAST(GETDATE() AS DATETIME2) AS [UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[SubCap_Source_Member] Member
		WHERE Member.[LOB_VENDOR] = 'BCBSMI'

	)
	SELECT
		 CAST(NULLIF(LTRIM(RTRIM(MedHOK_MAX_Internal_ID.[MHK Member Internal ID])),'') AS INT) AS [MHK_INTERNAL_ID]
		,Member.[MEDHOK_ID] AS [MEDHOK_ID]
		,Member.[SSN] AS [SSN]
		,Member.[HICN] AS [HICN]
		,Member.[CLAIM_SUBSCRIBER_ID] AS [CLAIM_SUBSCRIBER_ID]
		,Member.[MBI] AS [MBI]
		,Member.[MEDICAID_NO] AS [MEDICAID_NO]
		,Member.[MRN] AS [MRN]
		,Member.[EXT_ID] AS [EXT_ID]
		,Member.[EXT_ID_TYPE] AS [EXT_ID_TYPE]
		,Member.[EXT_ID_2] AS [EXT_ID_2]
		,Member.[EXT_ID_TYPE_2] AS [EXT_ID_TYPE_2]
		,Member.[EXT_ID_3] AS [EXT_ID_3]
		,Member.[EXT_ID_TYPE_3] AS [EXT_ID_TYPE_3]
		,Member.[BENEFIT_STATUS] AS [BENEFIT_STATUS]
		,Member.[FIRST_NAME] AS [FIRST_NAME]
		,Member.[MIDDLE_NAME] AS [MIDDLE_NAME]
		,Member.[LAST_NAME] AS [LAST_NAME]
		,Member.[PREFIX] AS [PREFIX]
		,Member.[SUFFIX] AS [SUFFIX]
		,Member.[DATE_OF_BIRTH] AS [DATE_OF_BIRTH]
		,Member.[GENDER] AS [GENDER]
		,Member.[COMPANY_DESCRIPTION] AS [COMPANY_DESCRIPTION]
		,Member.[LOB_CODE] AS [LOB_CODE]
		,Member.[FAMILY_ID] AS [FAMILY_ID]
		,Member.[PERSON_NUMBER] AS [PERSON_NUMBER]
		,Member.[RACE] AS [RACE]
		,Member.[ETHNICITY] AS [ETHNICITY]
		,Member.[PRIMARY_LANGUAGE] AS [PRIMARY_LANGUAGE]
		,Member.[PRIMARY_LANGUAGE_SOURCE] AS [PRIMARY_LANGUAGE_SOURCE]
		,Member.[SPOKEN_LANGUAGE] AS [SPOKEN_LANGUAGE]
		,Member.[SPOKEN_LANGUAGE_SOURCE] AS [SPOKEN_LANGUAGE_SOURCE]
		,Member.[WRITTEN_LANGUAGE] AS [WRITTEN_LANGUAGE]
		,Member.[WRITTEN_LANGUAGE_SOURCE] AS [WRITTEN_LANGUAGE_SOURCE]
		,Member.[OTHER_LANGUAGE] AS [OTHER_LANGUAGE]
		,Member.[OTHER_LANGUAGE_SOURCE] AS [OTHER_LANGUAGE_SOURCE]
		,Member.[EMPLOYEE] AS [EMPLOYEE]
		,Member.[PBP_NUMBER] AS [PBP_NUMBER]
		,Member.[CURRENT_LIS] AS [CURRENT_LIS]
		,Member.[IPA_GROUP_EXT_ID] AS [IPA_GROUP_EXT_ID]
		,Member.[MEDICARE_PLAN_CODE] AS [MEDICARE_PLAN_CODE]
		,Member.[MEDICARE_TYPE] AS [MEDICARE_TYPE]
		,Member.[DUPLICATE_MEDICAID_ID] AS [DUPLICATE_MEDICAID_ID]
		,Member.[PREGNANCY_DUE_DATE] AS [PREGNANCY_DUE_DATE]
		,Member.[PREGNANCY_INDICATOR] AS [PREGNANCY_INDICATOR]
		,Member.[BOARD_NUMBER] AS [BOARD_NUMBER]
		,Member.[DEPENDENT_CODE] AS [DEPENDENT_CODE]
		,Member.[LEGACY_SUBSCRIBER_ID] AS [LEGACY_SUBSCRIBER_ID]
		,Member.[GROUP_NUMBER] AS [GROUP_NUMBER]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ESCO_ID]
		,Member.[CLIENT_SPECIFIC_DATA] AS [CLIENT_SPECIFIC_DATA]
		,Member.[RELATIONSHIP_CODE] AS [RELATIONSHIP_CODE]
		,Member.[TIME_ZONE] AS [TIME_ZONE]
		,Member.[DATE_OF_DEATH] AS [DATE_OF_DEATH]
		,Member.[FOSTER_CARE_FLAG] AS [FOSTER_CARE_FLAG]
		,Member.[VIP] AS [VIP]
		,Member.[CLINIC_NUMBER] AS [CLINIC_NUMBER]
		,Member.[MODALITY] AS [MODALITY]
		,Member.[PAYER_NAME] AS [PAYER_NAME]
		,Member.[PAYER_ID_TYPE] AS [PAYER_ID_TYPE]
		,Member.[PAYER_ID] AS [PAYER_ID]
		,Member.[DIALYSIS_START_DATE] AS [DIALYSIS_START_DATE]
		,Member.[KIDNEY_TRANSPLANT_DATE] AS [KIDNEY_TRANSPLANT_DATE]
		,CAST(NULLIF(LTRIM(RTRIM('BCBSMI')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]
		,Member.[ROW_SOURCE] AS [ROW_SOURCE]
		,Member.[ROW_SOURCE_ID] AS [ROW_SOURCE_ID]
		,CAST(GETDATE() AS DATETIME2) AS [CREATE_DATE]
		,CAST(GETDATE() AS DATETIME2) AS [UPDATE_DATE]
	FROM Member
	LEFT OUTER JOIN MedHOK_MAX_Internal_ID
	ON MedHOK_MAX_Internal_ID.[Member ID] = Member.[MEDHOK_ID]


GO