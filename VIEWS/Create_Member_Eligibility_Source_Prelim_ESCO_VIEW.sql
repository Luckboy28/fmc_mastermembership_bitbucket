

USE [FHPDataMarts]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Eligibility_Source_Prelim_ESCO', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Eligibility_Source_Prelim_ESCO;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Eligibility_Source_Prelim_ESCO] AS

	WITH MedHOK_MAX_Internal_ID AS
	(
		SELECT
			 MAX([MHK Member Internal ID]) AS [MHK Member Internal ID]
			,[Member ID]
		FROM [MedHok Nightly].[dbo].[Member]  --38930
		GROUP BY [Member ID]
	),
	HICN_MBI_Lookup AS
	(
		SELECT DISTINCT
			 [MEMBER_EXT_ID]
			,[MEDICARE_NO] AS [HICN_MBI]
		FROM [FHPDataMarts].[dbo].[ESCO_PA_MEMBERELIGIBILITY] Eli
	)
	SELECT DISTINCT
		 CAST(NULLIF(LTRIM(RTRIM(MedHOK_MAX_Internal_ID.[MHK Member Internal ID])),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(UPPER(NULLIF(LTRIM(RTRIM(Mem.[SUBSCRIBER_ID])),'')) AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(NULLIF(NULLIF(NULLIF(LTRIM(RTRIM(COALESCE(Mem.[SSN],[FHPDataMarts].[dbo].[HICN_to_SSN](HICN_MBI_Lookup.[HICN_MBI])))),''),'--'),'NULL') AS VARCHAR(11)) AS [SSN]
		,CAST(UPPER(NULLIF(LTRIM(RTRIM([FHPDataMarts].[dbo].[HICN_Validate](HICN_MBI_Lookup.[HICN_MBI]))),'')) AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(UPPER(NULLIF(LTRIM(RTRIM([FHPDataMarts].[dbo].[MBI_Validate](HICN_MBI_Lookup.[HICN_MBI]))),'')) AS VARCHAR(11)) AS [MBI]
		,CAST(UPPER(NULLIF(NULLIF(LTRIM(RTRIM(Mem.[MEDICAID_NO])),''),'PRELIMINARY_ESCO')) AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[EXT_ID_3])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[SOURCE])),'') AS VARCHAR(50)) AS [ESCO_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS INT) AS [HLTH_PLN_SYSID]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [HLTH_PLN_PROD_LINE]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [HLTH_PLN_RPT_GRP]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [HLTH_PLN_STD_CARRIER_CD]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PLAN_NAME]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[EFFECTIVEDATE])),'') AS DATE) AS [START_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[TERMDATE])),'') AS DATE) AS [TERM_DATE]
		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[ESCO_PA_MemberEligibility].[EXT_ID]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[EXT_ID])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
		,CAST(NULLIF(LTRIM(RTRIM('eCF')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('PRELIM_ESCO')),'') AS VARCHAR(50)) AS [LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[ESCO_PA_MemberEligibility] Eli
	JOIN [FHPDataMarts].[dbo].[ESCO_PA_MEMBER] Mem
		ON Mem.[EXT_ID] = Eli.[MEMBER_EXT_ID]
	LEFT OUTER JOIN HICN_MBI_Lookup
		ON Mem.[EXT_ID] = HICN_MBI_Lookup.[MEMBER_EXT_ID] 
	LEFT OUTER JOIN MedHOK_MAX_Internal_ID
		ON MedHOK_MAX_Internal_ID.[Member ID] = Mem.[SUBSCRIBER_ID]



GO