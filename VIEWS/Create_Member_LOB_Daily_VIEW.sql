
USE [FHPDW]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_LOB_Daily_Hierarchy', 'V') IS NOT NULL
	DROP VIEW dbo.Member_LOB_Daily_Hierarchy;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_LOB_Daily_Hierarchy] AS

	-- First, let's establish baseline data.  This will give us a record for every day of every LOB, and give us a baseline total of "0" for each LOB.

	-- Rank the member eligibility data according to the LOB hierarchy.  If a member belongs to multiple LOB's on a particular day, only their highest ranked LOB will be counted.
	WITH RANKED_DATA AS
	(
		SELECT
			 D.[Date]
			,ME.[MEMBER_MASTER_ROW_ID]
			,ME.[START_DATE]
			,ME.[TERM_DATE]
			,ME.[LOB_TYPE]
			,ME.[LOB_VENDOR]
			,ROW_NUMBER() OVER (PARTITION BY D.[Date], ME.[MEMBER_MASTER_ROW_ID] ORDER BY LOBH.[LOB_RANK] ASC) [RANK]
		FROM [FHPDataMarts].[dbo].[Date] D
		JOIN [FHPDW].[dbo].[Member_Eligibility] ME
			ON D.[Date] >= ME.[START_DATE]
			AND D.[Date] <= ISNULL(ME.[TERM_DATE],GETDATE())
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
			ON LOBH.LOB_TYPE = ME.LOB_TYPE
			AND LOBH.LOB_VENDOR = ME.LOB_VENDOR
		WHERE 1=1
		AND ME.ROW_DELETED = 'N'
		AND ME.[START_DATE] <> ISNULL(ME.[TERM_DATE],CAST('01-01-9999' AS DATE))   -- SQL doesn't like comparing against NULLs, so instead we compare it to an arbitrary non-existent date if null
	)
	SELECT
		 [Date]
		,[MEMBER_MASTER_ROW_ID]
		,[START_DATE]
		,[TERM_DATE]
		,[LOB_TYPE]
		,[LOB_VENDOR]
	FROM RANKED_DATA
	WHERE [RANK] = 1

GO



GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily_Hierarchy] TO [FHP_DW_READ_SPECIFIC];
GO

GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily_Hierarchy] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO






USE [FHPDW]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_LOB_Daily', 'V') IS NOT NULL
	DROP VIEW dbo.Member_LOB_Daily;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_LOB_Daily] AS

		SELECT
			 D.[Date]
			,ME.[MEMBER_MASTER_ROW_ID]
			,ME.[START_DATE]
			,ME.[TERM_DATE]
			,ME.[LOB_TYPE]
			,ME.[LOB_VENDOR]
			,LOBH.[LOB_RANK]
		FROM [FHPDataMarts].[dbo].[Date] D
		JOIN [FHPDW].[dbo].[Member_Eligibility] ME
			ON D.[Date] >= ME.[START_DATE]
			AND D.[Date] <= ISNULL(ME.[TERM_DATE],GETDATE())
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
			ON LOBH.LOB_TYPE = ME.LOB_TYPE
			AND LOBH.LOB_VENDOR = ME.LOB_VENDOR
		WHERE 1=1
		AND ME.ROW_DELETED = 'N'
		AND ME.[START_DATE] <> ISNULL(ME.[TERM_DATE],CAST('01-01-9999' AS DATE))   -- SQL doesn't like comparing against NULLs, so instead we compare it to an arbitrary non-existent date if null
	
GO



GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily] TO [FHP_DW_READ_SPECIFIC];
GO
GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO
