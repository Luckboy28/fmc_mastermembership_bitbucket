

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Labs_SOURCE', 'V') IS NOT NULL
	DROP VIEW dbo.Membership_Export_Labs_SOURCE;
GO



CREATE  VIEW [dbo].[Membership_Export_Labs_SOURCE]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: September 15th 2019
-- Description:	
-- ==========================================================================================
/*

*/
-- ==========================================================================================

	
	SELECT DISTINCT

		 [MEMBER_LABS_ROW_ID] [MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,[MEMBER_MASTER_ROW_ID] [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]

	FROM [FHPDW].[dbo].[Member_Labs] Labs



GO

