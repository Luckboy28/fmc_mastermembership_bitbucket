
USE [FHPDW]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_LOB_Daily_Totals_Hierarchy', 'V') IS NOT NULL
	DROP VIEW dbo.Member_LOB_Daily_Totals_Hierarchy;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_LOB_Daily_Totals_Hierarchy] AS

	-- First, let's establish baseline data.  This will give us a record for every day of every LOB, and give us a baseline total of "0" for each LOB.
	WITH BASELINE_DATA AS
	(
		SELECT 
			 D.[Date]
			,BLL.[LOB_TYPE]
			,BLL.[LOB_VENDOR]
			,BLL.[BASELINE_TOTAL]
		FROM [FHPDataMarts].[dbo].[Date] D
		CROSS APPLY
		(
			-- Baseline LOBs
			SELECT DISTINCT
				 ME.[LOB_TYPE]
				,ME.[LOB_VENDOR]
				,0 [BASELINE_TOTAL]
			FROM [FHPDW].[dbo].[Member_Eligibility] ME
			WHERE ROW_DELETED = 'N'  -- This is an internal tracking column that designates whether or not the row has been "soft deleted", meaning that the data is invalid for use.
			AND ME.[START_DATE] <> ME.[TERM_DATE]  -- If START_DATE = TERM_DATE, this represents a "backed out" eligibility.  The record cannot be deleted for compliance reasons, so it is simply terminated on the same day it was started.
		) BLL
		WHERE D.[Date] >= (SELECT MIN([START_DATE]) FROM [FHPDW].[dbo].[Member_Eligibility] WHERE ROW_DELETED = 'N')   -- Limit the date range to the earliest eligbility record
		AND D.[Date] <= GETDATE()  -- Limit the date range to the current date, so that future dates aren't considered
	)
	-- Rank the member eligibility data according to the LOB hierarchy.  If a member belongs to multiple LOB's on a particular day, only their highest ranked LOB will be counted.
	,DATASET_RANKED AS
	(
		SELECT
			 D.[Date]
			,ME.[MEMBER_MASTER_ROW_ID]
			,ME.[START_DATE]
			,ME.[TERM_DATE]
			,ME.[LOB_TYPE]
			,ME.[LOB_VENDOR]
			,ROW_NUMBER() OVER (PARTITION BY D.[Date], ME.[MEMBER_MASTER_ROW_ID] ORDER BY LOBH.[LOB_RANK] ASC) [RANK]
		FROM [FHPDataMarts].[dbo].[Date] D
		JOIN [FHPDW].[dbo].[Member_Eligibility] ME
			ON D.[Date] >= ME.[START_DATE]
			AND D.[Date] <= ISNULL(ME.[TERM_DATE],GETDATE())
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
			ON LOBH.LOB_TYPE = ME.LOB_TYPE
			AND LOBH.LOB_VENDOR = ME.LOB_VENDOR
		WHERE 1=1
		AND ME.ROW_DELETED = 'N'
		AND ME.[START_DATE] <> ISNULL(ME.[TERM_DATE],CAST('01-01-9999' AS DATE))   -- SQL doesn't like comparing against NULLs, so instead we compare it to an arbitrary non-existent date if null
	)
	-- Now we count up the total number of members, and filter out any members that lost the LOB hierarchy ranking
	,DATASET AS
	(
		SELECT
			 [DATE]
			,[LOB_TYPE]
			,[LOB_VENDOR]
			,COUNT(1) [TOTAL]
		FROM DATASET_RANKED
		WHERE [RANK] = 1
		GROUP BY 
			 [DATE]
			,[LOB_TYPE]
			,[LOB_VENDOR]
	)
	-- The last step is to take our baseline data and apply the dataset on days where there's overlap, and add up the totals
	SELECT
		 BASELINE_DATA.[DATE]
		,BASELINE_DATA.[LOB_TYPE]
		,BASELINE_DATA.[LOB_VENDOR]
		,BASELINE_DATA.[BASELINE_TOTAL] + ISNULL(DATASET.[TOTAL],0) [TOTAL]
	FROM BASELINE_DATA
	LEFT OUTER JOIN DATASET
	ON DATASET.[DATE] = BASELINE_DATA.[DATE]
	AND DATASET.[LOB_TYPE] = BASELINE_DATA.[LOB_TYPE]
	AND DATASET.[LOB_VENDOR] = BASELINE_DATA.[LOB_VENDOR]

GO



GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily_Totals_Hierarchy] TO [FHP_DW_READ_SPECIFIC];
GO
GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily_Totals_Hierarchy] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO






USE [FHPDW]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_LOB_Daily_Totals', 'V') IS NOT NULL
	DROP VIEW dbo.Member_LOB_Daily_Totals;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_LOB_Daily_Totals] AS



	-- First, let's establish baseline data.  This will give us a record for every day of every LOB, and give us a baseline total of "0" for each LOB.
	WITH BASELINE_DATA AS
	(
		SELECT 
			 D.[Date]
			,BLL.[LOB_TYPE]
			,BLL.[LOB_VENDOR]
			,BLL.[BASELINE_TOTAL]
		FROM [FHPDataMarts].[dbo].[Date] D
		CROSS APPLY
		(
			-- Baseline LOBs
			SELECT DISTINCT
				 ME.[LOB_TYPE]
				,ME.[LOB_VENDOR]
				,0 [BASELINE_TOTAL]
			FROM [FHPDW].[dbo].[Member_Eligibility] ME
			WHERE ROW_DELETED = 'N'  -- This is an internal tracking column that designates whether or not the row has been "soft deleted", meaning that the data is invalid for use.
			AND ME.[START_DATE] <> ME.[TERM_DATE]  -- If START_DATE = TERM_DATE, this represents a "backed out" eligibility.  The record cannot be deleted for compliance reasons, so it is simply terminated on the same day it was started.
		) BLL
		WHERE D.[Date] >= (SELECT MIN([START_DATE]) FROM [FHPDW].[dbo].[Member_Eligibility] WHERE ROW_DELETED = 'N')   -- Limit the date range to the earliest eligbility record
		AND D.[Date] <= GETDATE()  -- Limit the date range to the current date, so that future dates aren't considered
	)
	-- Join the Eligibility table to the Date, and return only the timeframe we need
	,DATASET_DATES AS
	(
		SELECT
			 D.[Date]
			,ME.[MEMBER_MASTER_ROW_ID]
			,ME.[START_DATE]
			,ME.[TERM_DATE]
			,ME.[LOB_TYPE]
			,ME.[LOB_VENDOR]
		FROM [FHPDataMarts].[dbo].[Date] D
		JOIN [FHPDW].[dbo].[Member_Eligibility] ME
			ON D.[Date] >= ME.[START_DATE]
			AND D.[Date] <= ISNULL(ME.[TERM_DATE],GETDATE())
		WHERE 1=1
		AND ME.ROW_DELETED = 'N'
		AND ME.[START_DATE] <> ISNULL(ME.[TERM_DATE],CAST('01-01-9999' AS DATE))   -- SQL doesn't like comparing against NULLs, so instead we compare it to an arbitrary non-existent date if null
	)
	-- Now we count up the total number of members, and filter out any members that lost the LOB hierarchy ranking
	,DATASET AS
	(
		SELECT
			 [DATE]
			,[LOB_TYPE]
			,[LOB_VENDOR]
			,COUNT(1) [TOTAL]
		FROM DATASET_DATES
		GROUP BY 
			 [DATE]
			,[LOB_TYPE]
			,[LOB_VENDOR]
	)
	-- The last step is to take our baseline data and apply the dataset on days where there's overlap, and add up the totals
	SELECT
		 BASELINE_DATA.[DATE]
		,BASELINE_DATA.[LOB_TYPE]
		,BASELINE_DATA.[LOB_VENDOR]
		,BASELINE_DATA.[BASELINE_TOTAL] + ISNULL(DATASET.[TOTAL],0) [TOTAL]
	FROM BASELINE_DATA
	LEFT OUTER JOIN DATASET
	ON DATASET.[DATE] = BASELINE_DATA.[DATE]
	AND DATASET.[LOB_TYPE] = BASELINE_DATA.[LOB_TYPE]
	AND DATASET.[LOB_VENDOR] = BASELINE_DATA.[LOB_VENDOR]

GO



GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily_Totals] TO [FHP_DW_READ_SPECIFIC];
GO
GRANT SELECT, VIEW DEFINITION ON [dbo].[Member_LOB_Daily_Totals] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO
