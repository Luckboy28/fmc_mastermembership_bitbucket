
USE [FHPDataMarts]
GO
-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Contacts_Combined_Sources', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Contacts_Combined_Sources;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Contacts_Combined_Sources] AS

	--SELECT
	--	 [MHK_INTERNAL_ID]
	--	,[MEDHOK_ID]
	--	,[SSN]
	--	,[HICN]
	--	,[CLAIM_SUBSCRIBER_ID]
	--	,[MBI]
	--	,[MEDICAID_NO]
	--	,[MRN]
	--	,[EXT_ID]
	--	,[EXT_ID_TYPE]
	--	,[EXT_ID_2]
	--	,[EXT_ID_TYPE_2]
	--	,[EXT_ID_3]
	--	,[EXT_ID_TYPE_3]

	--	,[CONTACT_TYPE]
	--	,[FIRST_NAME]
	--	,[MIDDLE_NAME]
	--	,[LAST_NAME]
	--	,[PREFIX]
	--	,[SUFFIX]
	--	,[DATE_OF_BIRTH]
	--	,[GENDER]
	--	,[RELATION_TO_MEMBER]
	--	,[PROVIDER_ID_TYPE]
	--	,[PROVIDER_ID]

	--	,[ROW_SOURCE]
	--	,[ROW_SOURCE_ID]
	--	,[LOB_VENDOR]
	--	,[LOB_TYPE]

	--FROM [dbo].[Member_Contacts_Source_IKA_CSNP]

	--UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [dbo].[Member_Contacts_Source_ESCO]

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [dbo].[Member_Contacts_Source_Aetna]

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [dbo].[Member_Contacts_Source_Cigna]
	
	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [dbo].[Member_Contacts_Source_Coventry]

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [dbo].[Member_Contacts_Source_Humana]

GO



