

USE [FHPDataMarts]
GO



IF OBJECT_ID('dbo.Membership_Export_Provider_SOURCE', 'V') IS NOT NULL
	DROP VIEW dbo.Membership_Export_Provider_SOURCE;
GO



CREATE  VIEW [dbo].[Membership_Export_Provider_SOURCE]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: October 10th 2019
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================


		WITH Provider_Sources AS
		(
			-- Humana CKD:  PCP
			SELECT DISTINCT
				-- [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]

				 CAST(NULLIF(LTRIM(RTRIM(Member.[PCP_NPI_ID])),'') AS VARCHAR(50)) [NPI]
				,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [PROVIDER_TYPE]
				,CAST(NULLIF(LTRIM(RTRIM('Primary Care Physician')),'') AS VARCHAR(50)) [SPECIALITY_1]
				,NULL [SPECIALITY_2]
				,NULL [SPECIALITY_3]
				,Member.[PCP_FIRST_NAME] [FIRST_NAME]
				,NULL [MIDDLE_NAME]
				,Member.[PCP_LAST_NAME] [LAST_NAME]
				,NULL [GENDER]
				,CAST(NULL AS DATE) [DATE_OF_BIRTH]
				,Member.[PCP_ORG_NAME] [TITLE]
				,NULL [GROUP_NAME]  --Multiple [PCP_GROUPER_NAME] made it unusable/meaningless
				,Affil.[AFFILIATION]  -- Will be updated by the mutually affiliated list

				,'ACTIVE' [STATUS]
				,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[PCP_NPI_ID]' [ROW_SOURCE]
				,Member.[PCP_NPI_ID] [ROW_SOURCE_ID]

				,Member.[ROW_UPDATE_DATE] --USED FOR RANKING LATER


				--,[ROW_PROBLEM]
				--,[ROW_PROBLEM_DATE]
				--,[ROW_PROBLEM_REASON]
				--,[ROW_DELETED]
				--,[ROW_DELETED_DATE]
				--,[ROW_DELETED_REASON]
				--,[ROW_CREATE_DATE]
				--,[ROW_UPDATE_DATE]
			FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] Member
			LEFT JOIN [FHPDW].[dbo].[Membership_Export_Provider_Affiliation] Affil
				ON Member.[PCP_NPI_ID] =  Affil.[NPI]
			LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
				ON Member.[PCP_NPI_ID] = NPIData.[NPI]
			WHERE Member.[PCP_NPI_ID] IS NOT NULL
			AND Member.[PCP_NPI_ID] <> 0
			AND Member.[PCP_NPI_ID] <> ''
			AND Member.[ROW_DELETED] = 'N'

			UNION ALL

			-- Humana CKD:  Nephrologist
			SELECT DISTINCT
				-- [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]

				 CAST(NULLIF(LTRIM(RTRIM(HCKDM.[Neph_NPI_ID])),'') AS VARCHAR(50)) [NPI]
				,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [PROVIDER_TYPE]
				,CAST(NULLIF(LTRIM(RTRIM('Nephrologist')),'') AS VARCHAR(50)) [SPECIALITY_1]
				,NULL [SPECIALITY_2]
				,NULL [SPECIALITY_3]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Name])),'') [FIRST_NAME]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider Middle Name])),'') [MIDDLE_NAME]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider Last Name])),'') [LAST_NAME]
				,CASE WHEN NPIData.[Provider Gender Code] = 'M' OR NPIData.[Provider Gender Code] = 'F' THEN NPIData.[Provider Gender Code] ELSE NULL END [GENDER]
				,CAST(NULL AS DATE) [DATE_OF_BIRTH]
				,NULL [TITLE]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider Organization Name])),'') [GROUP_NAME]
				,Affil.[AFFILIATION]  -- Will be updated by the mutually affiliated list

				,'ACTIVE' [STATUS]
				,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[Neph_NPI_ID] / [dbo].[NPIData]' [ROW_SOURCE]
				,HCKDM.[Neph_NPI_ID] [ROW_SOURCE_ID]

				,HCKDM.[ROW_UPDATE_DATE]  --USED FOR RANKING LATER

				--,[ROW_PROBLEM]
				--,[ROW_PROBLEM_DATE]
				--,[ROW_PROBLEM_REASON]
				--,[ROW_DELETED]
				--,[ROW_DELETED_DATE]
				--,[ROW_DELETED_REASON]
				--,[ROW_CREATE_DATE]
				--,[ROW_UPDATE_DATE]
			FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
			LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
				ON HCKDM.[Neph_NPI_ID] = NPIData.[NPI]
			LEFT JOIN [FHPDW].[dbo].[Membership_Export_Provider_Affiliation] Affil
				ON HCKDM.[Neph_NPI_ID] = Affil.[NPI]
			WHERE HCKDM.[Neph_NPI_ID] IS NOT NULL
			AND HCKDM.[Neph_NPI_ID] <> 0
			AND HCKDM.[Neph_NPI_ID] <> ''
			AND HCKDM.[ROW_DELETED] = 'N'


			UNION ALL

			-- Mutually Affiliated:  Nephrologist (Currently they're all Nephrologists -- in the future the affiliation table should include specialty)
			SELECT DISTINCT
				-- [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]

				 CAST(NULLIF(LTRIM(RTRIM(MEPA.[NPI])),'') AS VARCHAR(50)) [NPI]
				,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [PROVIDER_TYPE]
				,CAST(NULLIF(LTRIM(RTRIM('Nephrologist')),'') AS VARCHAR(50)) [SPECIALITY_1]
				,NULL [SPECIALITY_2]
				,NULL [SPECIALITY_3]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Name])),'') [FIRST_NAME]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider Middle Name])),'') [MIDDLE_NAME]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider Last Name])),'') [LAST_NAME]
				,CASE WHEN NPIData.[Provider Gender Code] = 'M' OR NPIData.[Provider Gender Code] = 'F' THEN NPIData.[Provider Gender Code] ELSE NULL END [GENDER]
				,CAST(NULL AS DATE) [DATE_OF_BIRTH]
				,NULL [TITLE]
				,NULLIF(LTRIM(RTRIM(NPIData.[Provider Organization Name])),'') [GROUP_NAME]
				,MEPA.[AFFILIATION]  -- Will be updated by the mutually affiliated list

				,'ACTIVE' [STATUS]
				,'[FHPDW].[dbo].[Membership_Export_Provider_Affiliation] / [dbo].[NPIData]' [ROW_SOURCE]
				,MEPA.[NPI] [ROW_SOURCE_ID]

				,MEPA.[ROW_UPDATE_DATE]  --USED FOR RANKING LATER

				--,[ROW_PROBLEM]
				--,[ROW_PROBLEM_DATE]
				--,[ROW_PROBLEM_REASON]
				--,[ROW_DELETED]
				--,[ROW_DELETED_DATE]
				--,[ROW_DELETED_REASON]
				--,[ROW_CREATE_DATE]
				--,[ROW_UPDATE_DATE]
			FROM [FHPDW].[dbo].[Membership_Export_Provider_Affiliation] MEPA
			LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
				ON MEPA.[NPI] = NPIData.[NPI]
			WHERE 1=1
			AND MEPA.[ROW_DELETED] = 'N'
			AND MEPA.[AFFILIATION] LIKE '%FKC%'
			AND LEN(REPLACE(MEPA.[AFFILIATION],'FKC','')) > 0
		)
		,Latest_NPI AS
		(
			SELECT
				*
				,ROW_NUMBER() OVER (PARTITION BY [NPI] ORDER BY [ROW_UPDATE_DATE]) [RANK]
			FROM Provider_Sources
		)
		,Latest_NPI_RANKED AS
		(
			SELECT
				*
			FROM Latest_NPI
			WHERE [RANK] = 1
		)
		,All_Specialties AS
		(
			SELECT
				 [NPI]
				,CAST([SPECIALITY_1] AS VARCHAR(50)) [SPECIALITY]
				,[ROW_UPDATE_DATE]
			FROM Provider_Sources
			WHERE [SPECIALITY_1] IS NOT NULL

			UNION ALL

			SELECT
				 [NPI]
				,CAST([SPECIALITY_2] AS VARCHAR(50)) [SPECIALITY]
				,[ROW_UPDATE_DATE]
			FROM Provider_Sources
			WHERE [SPECIALITY_2] IS NOT NULL

			UNION ALL

			SELECT
				 [NPI]
				,CAST([SPECIALITY_3] AS VARCHAR(50)) [SPECIALITY]
				,[ROW_UPDATE_DATE]
			FROM Provider_Sources
			WHERE [SPECIALITY_3] IS NOT NULL

		)
		,All_Specialties_RANKED AS
		(
			SELECT DISTINCT
				 [NPI]
				,CAST([SPECIALITY] AS VARCHAR(50)) [SPECIALITY]
				,ROW_NUMBER() OVER (PARTITION BY [NPI] ORDER BY [ROW_UPDATE_DATE] DESC) [RANK]
			FROM All_Specialties
		)
		SELECT
			-- [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
			 Latest_NPI_RANKED.[NPI]
			,Latest_NPI_RANKED.[PROVIDER_TYPE]
			,R1.[SPECIALITY] [SPECIALITY_1]
			,R2.[SPECIALITY] [SPECIALITY_2]
			,R3.[SPECIALITY] [SPECIALITY_3]
			,Latest_NPI_RANKED.[FIRST_NAME]
			,Latest_NPI_RANKED.[MIDDLE_NAME]
			,Latest_NPI_RANKED.[LAST_NAME]
			,Latest_NPI_RANKED.[GENDER]
			,Latest_NPI_RANKED.[DATE_OF_BIRTH]
			,Latest_NPI_RANKED.[TITLE]
			,Latest_NPI_RANKED.[GROUP_NAME]
			,Latest_NPI_RANKED.[AFFILIATION]
			,Latest_NPI_RANKED.[STATUS]
			,Latest_NPI_RANKED.[ROW_SOURCE]
			,Latest_NPI_RANKED.[ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM All_Specialties_RANKED R1
		LEFT OUTER JOIN All_Specialties_RANKED R2
			ON R1.[NPI] = R2.[NPI]
			AND R2.[RANK] = 2
		LEFT OUTER JOIN All_Specialties_RANKED R3
			ON R1.[NPI] = R3.[NPI]
			AND R3.[RANK] = 3
		JOIN Latest_NPI_RANKED
			ON R1.[NPI] = Latest_NPI_RANKED.[NPI]
		WHERE R1.[RANK] = 1


GO





GRANT SELECT ON [dbo].[Membership_Export_Provider_SOURCE] TO [FHP_DATAMARTs_READ_SPECIFIC_DATA_TEAM];
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_Provider_SOURCE] TO [FHP_DATAMARTs_READ_SPECIFIC_DATA_TEAM];
GO