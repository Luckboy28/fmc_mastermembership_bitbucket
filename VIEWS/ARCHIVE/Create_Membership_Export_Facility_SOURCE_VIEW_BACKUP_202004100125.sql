

USE [FHPDataMarts]
GO



--IF OBJECT_ID('dbo.Membership_Export_Facility_SOURCE', 'V') IS NOT NULL
--	DROP VIEW dbo.Membership_Export_Facility_SOURCE;
--GO



--CREATE  VIEW [dbo].[Membership_Export_Facility_SOURCE]

--AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: October 4th 2019
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================



	WITH Addresses AS
	(

		-- ==================================================================================================================================
		--
		--                                                         PHYSICAL ADDRESSES
		--
		-- ==================================================================================================================================


		-- LOB: Humana CKD
		-- ADDRESS_SOURCE: Vendor
		-- ADDRESS_TYPE: Physical
		-- PROVIDER: Primary Care Physician
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(HCKDM.[PCP_NPI_ID])),'') [NPI] --Used for reference later

			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [FACILITY_TYPE]
			,NULL [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_ADDR_LINE1])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_ADDR_LINE2])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_CITY_NAME])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_STATE_CD])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_ZIP_CD])),'') [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_PHONE_NBR])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULL [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[PCP_NPI_ID] / NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_NPI_ID])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
			,CAST(999 AS INT) AS [NPI_ADDRESS_RANKING]  -- The address from the vendor always takes bottom priority, per Richard.
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
			ON HCKDM.[PCP_NPI_ID] = NPIData.[NPI]
		WHERE HCKDM.[ROW_DELETED] = 'N'
		AND HCKDM.[PCP_ADDR_LINE1] IS NOT NULL
		AND HCKDM.[PCP_NPI_ID] IS NOT NULL


		UNION ALL

		-- LOB: Humana CKD
		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Physical
		-- PROVIDER: Primary Care Physician
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(HCKDM.[PCP_NPI_ID])),'') [NPI] --Used for reference later

			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [FACILITY_TYPE]
			,NULL [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Practice Location Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Practice Location Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address State Name])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),'') [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[PCP_NPI_ID] / NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_NPI_ID])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
			,CAST(1 AS INT) AS [NPI_ADDRESS_RANKING]  -- Addresses from the NPIData table take priority, per Richard
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		JOIN [FHPDW].[dbo].[NPIData] NPIData
			ON HCKDM.[PCP_NPI_ID] = NPIData.[NPI]
		WHERE HCKDM.[ROW_DELETED] = 'N'
		AND HCKDM.[PCP_NPI_ID] IS NOT NULL
		AND HCKDM.[PCP_NPI_ID] <> 0
		AND HCKDM.[PCP_NPI_ID] <> ''


		UNION ALL


		-- LOB: Humana CKD
		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Physical
		-- PROVIDER: Nephrologist
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(HCKDM.[Neph_NPI_ID])),'') [NPI] --Used for reference later

			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [FACILITY_TYPE]
			,NULL [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Practice Location Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Practice Location Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address State Name])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),'') [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[Neph_NPI_ID] / NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[Neph_NPI_ID])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
			,CAST(1 AS INT) AS [NPI_ADDRESS_RANKING]  -- Addresses from the NPIData table take priority, per Richard
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		JOIN [FHPDW].[dbo].[NPIData] NPIData
			ON HCKDM.[Neph_NPI_ID] = NPIData.[NPI]
		WHERE [ROW_DELETED] = 'N'
		AND HCKDM.[Neph_NPI_ID] IS NOT NULL
		AND HCKDM.[Neph_NPI_ID] <> 0
		AND HCKDM.[Neph_NPI_ID] <> ''


		
		-- ==================================================================================================================================
		--
		--                                                         MAILING ADDRESSES
		--
		-- ==================================================================================================================================



		UNION ALL

		-- LOB: Humana CKD
		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Mailing
		-- PROVIDER: Primary Care Physician
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(HCKDM.[PCP_NPI_ID])),'') [NPI] --Used for reference later

			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [FACILITY_TYPE]
			,NULL [FACILITY_NAME]
			,'MAILING' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Mailing Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Mailing Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address State Name])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),'') [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[PCP_NPI_ID] / NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[PCP_NPI_ID])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
			,CAST(1 AS INT) AS [NPI_ADDRESS_RANKING]  -- Addresses from the NPIData table take priority, per Richard
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		JOIN [FHPDW].[dbo].[NPIData] NPIData
			ON HCKDM.[PCP_NPI_ID] = NPIData.[NPI]
		WHERE HCKDM.[ROW_DELETED] = 'N'
		AND HCKDM.[PCP_NPI_ID] IS NOT NULL
		AND HCKDM.[PCP_NPI_ID] <> 0
		AND HCKDM.[PCP_NPI_ID] <> ''


		UNION ALL


		-- LOB: Humana CKD
		-- SOURCE: NPIData
		-- ADDRESS_TYPE: Mailing
		-- PROVIDER: Nephrologist
		SELECT DISTINCT
			 HCKDM.[Neph_NPI_ID] [NPI] --Used for reference later

			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [FACILITY_TYPE]
			,NULL [FACILITY_NAME]
			,'MAILING' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Mailing Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Mailing Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address State Name])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),'') [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[Neph_NPI_ID] / NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(HCKDM.[Neph_NPI_ID])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
			,CAST(1 AS INT) AS [NPI_ADDRESS_RANKING]  -- Addresses from the NPIData table take priority, per Richard
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		JOIN [FHPDW].[dbo].[NPIData] NPIData
			ON HCKDM.[Neph_NPI_ID] = NPIData.[NPI]
		WHERE [ROW_DELETED] = 'N'
		AND HCKDM.[Neph_NPI_ID] IS NOT NULL
		AND HCKDM.[Neph_NPI_ID] <> 0
		AND HCKDM.[Neph_NPI_ID] <> ''
	)
	,Addresses_Ranked AS
	(
		SELECT
			*
			,ROW_NUMBER() OVER (PARTITION BY [NPI], [ADDRESS_TYPE] ORDER BY [NPI_ADDRESS_RANKING] ASC) AS [RANK]
		FROM Addresses
	)
	SELECT DISTINCT
			 [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,[FACILITY_TYPE]
			,[FACILITY_NAME]
			,[ADDRESS_TYPE]
			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[COUNTRY]
			,REPLACE(REPLACE(REPLACE(REPLACE([PHONE],' ',''),')',''),'(',''),'-','') AS [PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[INTERNAL_FACILITY_ID]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[STATUS]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
			,[NPI_ADDRESS_RANKING]
	FROM Addresses_Ranked
	WHERE [RANK] = 1

--GO



--GRANT SELECT ON [dbo].[Membership_Export_Facility_SOURCE] TO [FHP_DATAMARTs_READ_SPECIFIC_DATA_TEAM];
--GO
--GRANT VIEW DEFINITION ON [dbo].[Membership_Export_Facility_SOURCE] TO [FHP_DATAMARTs_READ_SPECIFIC_DATA_TEAM];
--GO