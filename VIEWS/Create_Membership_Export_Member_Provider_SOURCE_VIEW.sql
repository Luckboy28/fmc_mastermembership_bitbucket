

USE [FHPDataMarts]
GO



IF OBJECT_ID('dbo.Membership_Export_Member_Provider_SOURCE', 'V') IS NOT NULL
	DROP VIEW dbo.Membership_Export_Member_Provider_SOURCE;
GO



CREATE VIEW [dbo].[Membership_Export_Member_Provider_SOURCE]

AS

-- ==========================================================================================
-- Author:		David M. Wilson
-- Update date: April 15th 2020
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================

	WITH Member_Provider_Relationships AS
	(
		-- Humana_CKD_Member
		SELECT DISTINCT
			-- [MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]
			 MMKH.[MEMBER_MASTER_ROW_ID] [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,P.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
			,'Primary Care Physician' AS [SPECIALITY_UTILIZED]
			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			ON HU.[MBR_ID] = MMKH.[EXT_ID]
			AND MMKH.EXT_ID_TYPE = 'HUMANA_CKD_MBR_ID'
		JOIN [FHPDW].[dbo].[Membership_Export_Provider] P
			ON HU.[PCP_NPI_ID] = P.[NPI]
		WHERE MMKH.ROW_DELETED = 'N'

		UNION ALL

		-- Humana_CKD_Member
		SELECT DISTINCT
			-- [MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]
			 MMKH.[MEMBER_MASTER_ROW_ID] [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,P.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
			,'Nephrologist' AS [SPECIALITY_UTILIZED]

			,'[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]' [ROW_SOURCE]
			,HU.[MBR_ID] [ROW_SOURCE_ID]

			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HU
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			ON HU.[MBR_ID] = MMKH.[EXT_ID]
			AND MMKH.EXT_ID_TYPE = 'HUMANA_CKD_MBR_ID'
		JOIN [FHPDW].[dbo].[Membership_Export_Provider] P
			ON HU.[NEPH_NPI_ID] = P.[NPI]
		WHERE MMKH.ROW_DELETED = 'N'
	)
	SELECT DISTINCT
		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,[SPECIALITY_UTILIZED]
		--,[ROW_SOURCE]
		--,[ROW_SOURCE_ID]
	FROM Member_Provider_Relationships

GO
