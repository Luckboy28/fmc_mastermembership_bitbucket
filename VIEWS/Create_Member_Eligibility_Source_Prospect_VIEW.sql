

USE [FHPDataMarts]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Eligibility_Source_Prospect', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Eligibility_Source_Prospect;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Eligibility_Source_Prospect] AS

	WITH MedHOK_MAX_Internal_ID AS
	(
		SELECT
			 MAX([MHK Member Internal ID]) AS [MHK Member Internal ID]
			,[Member ID]
		FROM [MedHok Nightly].[dbo].[Member]  --38930
		GROUP BY [Member ID]
	)
	SELECT DISTINCT
		 CAST(NULLIF(LTRIM(RTRIM(MedHOK_MAX_Internal_ID.[MHK Member Internal ID])),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[SUBSCRIBER_ID])),'') AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[SSN])),'') AS VARCHAR(11)) AS [SSN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [MBI]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[EXT_ID_3])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [ESCO_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS INT) AS [HLTH_PLN_SYSID]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [HLTH_PLN_PROD_LINE]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [HLTH_PLN_RPT_GRP]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [HLTH_PLN_STD_CARRIER_CD]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [PLAN_NAME]  -- Re-add later?   Available in the Source Data table.
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[EFFECTIVEDATE])),'') AS DATE) AS [START_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[TERMDATE])),'') AS DATE) AS [TERM_DATE]
		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[SubCap_Source_Eligibility].[ELIGIBILITY_ROW_ID]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(Eli.[ELIGIBILITY_ROW_ID])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
		,CAST(NULLIF(LTRIM(RTRIM('PROSPECT')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[SubCap_Source_Eligibility] Eli
	JOIN [FHPDataMarts].[dbo].[SubCap_Source_Member] Mem
	ON 	Eli.[MEMBER_ROW_ID] = Mem.[MEMBER_ROW_ID]
	LEFT OUTER JOIN MedHOK_MAX_Internal_ID
	ON MedHOK_MAX_Internal_ID.[Member ID] = Mem.[SUBSCRIBER_ID]
	WHERE Mem.[LOB_VENDOR] = 'Prospect'


GO
