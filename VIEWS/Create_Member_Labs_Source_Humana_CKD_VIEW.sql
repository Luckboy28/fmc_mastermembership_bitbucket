

USE [FHPDataMarts]
GO

-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Labs_Source_Humana_CKD', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Labs_Source_Humana_CKD;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Labs_Source_Humana_CKD] AS

	SELECT DISTINCT
		 CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [SSN]
		,CAST(NULLIF(LTRIM(RTRIM([FHPDev].[dbo].HICN_Validate(Humana_MRN_XREF.[MEDICARE_ID]))),'') AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(NULLIF(LTRIM(RTRIM([FHPDev].[dbo].MBI_Validate(Humana_MRN_XREF.[MEDICARE_ID]))),'') AS VARCHAR(11)) AS [MBI]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(Humana_MRN_XREF.[PT_MRN])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[MBR_ID])),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM('HUMANA_CKD_MBR_ID')),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]


		,CAST(NULLIF(LTRIM(RTRIM([LAB_DX_GROUP])),'') AS VARCHAR(150)) AS [LAB_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM([LAB_RESULTS_VALUE])),'') AS VARCHAR(150)) AS [LAB_RESULT]
		,CAST(NULLIF(LTRIM(RTRIM([LOINC_CD])),'') AS VARCHAR(50)) AS [LAB_LOINC_CODE]
		,CAST(NULLIF(LTRIM(RTRIM([SERVICE_DATE])),'') AS DATE) AS [LAB_SERVICE_DATE]


		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[Humana_CKD_Member].[MBR_ID]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(SourceData.[MBR_ID])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]

		--,CAST(NULLIF(LTRIM(RTRIM('HUMANA')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		--,CAST(NULLIF(LTRIM(RTRIM('CKD')),'') AS VARCHAR(50)) AS [LOB_TYPE]

	FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab] SourceData
	LEFT OUTER JOIN [FHPDataMarts].[dbo].[CKD_MBR_XREF_HUMANA_IDS] Humana_MRN_XREF
		ON SourceData.[MBR_ID] = Humana_MRN_XREF.[SRC_MBR_HIPPA_ID]

GO
