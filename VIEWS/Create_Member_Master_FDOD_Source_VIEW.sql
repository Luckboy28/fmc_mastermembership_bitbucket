
USE [FHPDataMarts]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Master_FDOD_Source', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Master_FDOD_Source;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Master_FDOD_Source] AS
	

	SELECT DISTINCT
		 MMFDOD.[MRN]
		,MIN(COALESCE([FDOCD_DT],[PT_FRST_EVER_DIAL_DT],[FDOD])) [DIALYSIS_START_DATE]
	FROM [FHPDataMarts].[dbo].[Member_Master_FDOD] MMFDOD
	GROUP BY MMFDOD.[MRN]

	--SELECT DISTINCT
	--	 MM.[MEMBER_MASTER_ROW_ID]
	--	,MIN(COALESCE([FDOCD_DT],[PT_FRST_EVER_DIAL_DT],[FDOD])) [DIALYSIS_START_DATE]
	--FROM [FHPDataMarts].[dbo].[Member_Master_FDOD] MMFDOD
	--JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
	--	ON MMFDOD.[MRN] = MMKH.[MRN]
	--JOIN [FHPDW].[dbo].[Member_Master] MM
	--	ON MM.[MEMBER_MASTER_ROW_ID] = MMKH.[MEMBER_MASTER_ROW_ID]
	--WHERE 1=1
	--AND MMKH.[ROW_DELETED] = 'N'
	--AND MM.[ROW_DELETED] = 'N'
	--GROUP BY MM.[MEMBER_MASTER_ROW_ID]


GO