

USE [FHPDataMarts]
GO

-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Address_Source_BCBSMI', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Address_Source_BCBSMI;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Address_Source_BCBSMI] AS

	-- Member's Permanent Address
	SELECT DISTINCT

		 CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS INT) AS [MHK_INTERNAL_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[SUBSCRIBER_ID])),'') AS VARCHAR(50)) AS [MEDHOK_ID]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[SSN])),'') AS VARCHAR(11)) AS [SSN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(12)) AS [HICN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [CLAIM_SUBSCRIBER_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(11)) AS [MBI]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [MEDICAID_NO]
		,CAST(NULLIF(LTRIM(RTRIM(Mem.[EXT_ID_3])),'') AS VARCHAR(50)) AS [MRN]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_2]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_3]
		,CAST(NULLIF(LTRIM(RTRIM(NULL)),'') AS VARCHAR(50)) AS [EXT_ID_TYPE_3]

		,CAST(NULLIF(LTRIM(RTRIM('PERMANENT')),'') AS VARCHAR(50)) AS [ADDRESS_TYPE]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[ADDRESS_1]))),'') AS VARCHAR(100)) AS [ADDRESS_1]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[ADDRESS_2]))),'') AS VARCHAR(100)) AS [ADDRESS_2]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[ADDRESS_3]))),'') AS VARCHAR(100)) AS [ADDRESS_3]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[CITY]))),'') AS VARCHAR(50)) AS [CITY]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[STATE]))),'') AS VARCHAR(50)) AS [STATE]
		,CAST(

			CASE
				WHEN
					NULLIF(LTRIM(RTRIM(REPLACE(REPLACE(Ad.[ZIP],'-',''),'~',''))),'') IS NULL
				THEN
					NULL
				ELSE
					CASE
						WHEN
							LEN(LTRIM(RTRIM(REPLACE(REPLACE(Ad.[ZIP],'-',''),'~','')))) > 5 
						THEN
							CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE(Ad.[ZIP],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE(Ad.[ZIP],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE(Ad.[ZIP],'-',''),'~',''))))))
						ELSE
							LTRIM(RTRIM(REPLACE(REPLACE(Ad.[ZIP],'-',''),'~','')))
					END
			END
		
		AS VARCHAR(10)) AS [ZIP]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[COUNTY]))),'') AS VARCHAR(50)) AS [COUNTY]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[ISLAND]))),'') AS VARCHAR(50)) AS [ISLAND]
		,CAST(NULLIF(LTRIM(RTRIM(UPPER(Ad.[COUNTRY]))),'') AS VARCHAR(50)) AS [COUNTRY]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[EFFECTIVE_DATE])),'') AS DATE) AS [EFFECTIVE_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[TERMDATE])),'') AS DATE) AS [TERM_DATE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[ADDRESS_STATUS])),'') AS VARCHAR(10)) AS [ADDRESS_STATUS]
		,CAST(NULLIF(LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(REPLACE(Ad.[PHONE],' ',''),'(',''),')',''),'-',''))),'') AS VARCHAR(50)) AS [PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[ALTERNATE_PHONE])),'') AS VARCHAR(50)) AS [ALTERNATE_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[EVENING_PHONE])),'') AS VARCHAR(50)) AS [EVENING_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[EMERGENCY_PHONE])),'') AS VARCHAR(50)) AS [EMERGENCY_PHONE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[FAX])),'') AS VARCHAR(50)) AS [FAX]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[EMAIL])),'') AS VARCHAR(50)) AS [EMAIL]

		,CAST(NULLIF(LTRIM(RTRIM('[FHPDataMarts].[dbo].[SubCap_Source_Address].[ADDRESS_ROW_ID]')),'') AS VARCHAR(500)) AS [ROW_SOURCE]
		,CAST(NULLIF(LTRIM(RTRIM(Ad.[ADDRESS_ROW_ID])),'') AS VARCHAR(50)) AS [ROW_SOURCE_ID]
		,CAST(NULLIF(LTRIM(RTRIM('BCBSMI')),'') AS VARCHAR(50)) AS [LOB_VENDOR]
		,CAST(NULLIF(LTRIM(RTRIM('SUBCAP')),'') AS VARCHAR(50)) AS [LOB_TYPE]

	FROM [FHPDataMarts].[dbo].[SubCap_Source_Address] Ad
	JOIN [FHPDataMarts].[dbo].[SubCap_Source_Member] Mem
	ON Ad.[MEMBER_ROW_ID] = Mem.[MEMBER_ROW_ID]
	WHERE Mem.[LOB_VENDOR] = 'BCBSMI'
	AND Ad.[ADDRESS_TYPE] = 'PERMANENT'


GO

