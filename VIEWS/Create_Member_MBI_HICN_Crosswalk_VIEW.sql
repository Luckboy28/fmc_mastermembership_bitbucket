
USE [FHPDataMarts]
GO


-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_MBI_HICN_Crosswalk', 'V') IS NOT NULL
	DROP VIEW dbo.Member_MBI_HICN_Crosswalk;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_MBI_HICN_Crosswalk] AS



	-- Select the MBI records from the HICN column
	WITH IKA_MBI AS
	(
		SELECT DISTINCT
			-- SUBSTRING(UPPER(HICN),1,1)
			 MemberHistoryID
			,MemberID
			,HICN
		FROM [FHPDW].[dbo].[IKA_Member_HicnHistory]
		WHERE LEN(HICN) = 11
		AND SUBSTRING(UPPER(HICN),1,1) IN ('1','2','3','4','5','6','7','8','9')
		AND SUBSTRING(UPPER(HICN),2,1) IN ('A','C','D','E','F','G','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y')
		AND SUBSTRING(UPPER(HICN),3,1) IN ('A','C','D','E','F','G','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y','0','1','2','3','4','5','6','7','8','9')
		AND SUBSTRING(UPPER(HICN),4,1) IN ('0','1','2','3','4','5','6','7','8','9')
		AND SUBSTRING(UPPER(HICN),5,1) IN ('A','C','D','E','F','G','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y')
		AND SUBSTRING(UPPER(HICN),6,1) IN ('A','C','D','E','F','G','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y','0','1','2','3','4','5','6','7','8','9')
		AND SUBSTRING(UPPER(HICN),7,1) IN ('0','1','2','3','4','5','6','7','8','9')
		AND SUBSTRING(UPPER(HICN),8,1) IN ('A','C','D','E','F','G','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y')
		AND SUBSTRING(UPPER(HICN),9,1) IN ('A','C','D','E','F','G','H','J','K','M','N','P','Q','R','T','U','V','W','X','Y')
		AND SUBSTRING(UPPER(HICN),10,1) IN ('0','1','2','3','4','5','6','7','8','9')
		AND SUBSTRING(UPPER(HICN),11,1) IN ('0','1','2','3','4','5','6','7','8','9')
	)
	-- Exclude all of the MBI records from the HICN field, to leave only HICN records (and everything else that isn't strictly an MBI)
	,IKA_HICN AS
	(	
		SELECT DISTINCT
			 MemberHistoryID
			,MemberID
			,HICN
		FROM FHPDW.dbo.IKA_Member_HicnHistory IKA

		EXCEPT 

		SELECT DISTINCT
			 MemberHistoryID
			,MemberID
			,HICN
		FROM IKA_MBI
	)
	-- Final IKA MBI/HICN Crosswalk
	,IKA_CROSSWALK AS
	(
		SELECT DISTINCT
			 IKA_MBI.HICN AS [MBI]
			,IKA_HICN.HICN AS [HICN]
		FROM IKA_MBI
		JOIN IKA_HICN
		ON IKA_MBI.MemberID = IKA_HICN.MemberID
		WHERE IKA_MBI.MemberHistoryID <> IKA_HICN.MemberHistoryID
	)
	-- Final ESCO MBI/HICN Crosswalk
	,ESCO_CROSSWALK AS
	(

		SELECT DISTINCT
			 [MBI]
			,[HICN]
		FROM [FHPDataMarts].[dbo].[ESCO_MBI_CROSSWALK] 

	)
	-- UNION all the data
	,ALL_DATA AS
	(
		SELECT 
			 [MBI]
			,[HICN]
		FROM IKA_CROSSWALK

		UNION ALL

		SELECT 
			 [MBI]
			,[HICN]
		FROM ESCO_CROSSWALK
	)
	SELECT DISTINCT
		 [MBI]
		,[HICN]
	FROM ALL_DATA


GO
