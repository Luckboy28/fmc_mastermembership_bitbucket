
USE [FHPDataMarts]
GO
-- Drop the view if it already exists.
IF OBJECT_ID('dbo.Member_Address_Combined_Sources', 'V') IS NOT NULL
	DROP VIEW dbo.Member_Address_Combined_Sources;
GO


-- Union all sources
CREATE VIEW [dbo].[Member_Address_Combined_Sources] AS

	--SELECT
	--	 [MHK_INTERNAL_ID]
	--	,[MEDHOK_ID]
	--	,[SSN]
	--	,[HICN]
	--	,[CLAIM_SUBSCRIBER_ID]
	--	,[MBI]
	--	,[MEDICAID_NO]
	--	,[MRN]
	--	,[EXT_ID]
	--	,[EXT_ID_TYPE]
	--	,[EXT_ID_2]
	--	,[EXT_ID_TYPE_2]
	--	,[EXT_ID_3]
	--	,[EXT_ID_TYPE_3]
	--	,[ADDRESS_TYPE]
	--	,[ADDRESS_1]
	--	,[ADDRESS_2]
	--	,[ADDRESS_3]
	--	,[CITY]
	--	,[STATE]
	--	,[ZIP]
	--	,[COUNTY]
	--	,[ISLAND]
	--	,[COUNTRY]
	--	,[EFFECTIVE_DATE]
	--	,[TERM_DATE]
	--	,[ADDRESS_STATUS]
	--	,[PHONE]
	--	,[ALTERNATE_PHONE]
	--	,[EVENING_PHONE]
	--	,[EMERGENCY_PHONE]
	--	,[FAX]
	--	,[EMAIL]
	--	,[ROW_SOURCE]
	--	,[ROW_SOURCE_ID]
	--	,[LOB_VENDOR]
	--	,[LOB_TYPE]
	--FROM [FHPDataMarts].[dbo].[Member_Address_Source_IKA_CSNP]
	----WHERE 
	------ Try to find enough data to make a reasonable contact.
	------ Valid addresses must have at least one address field, and a valid city/state or a zip code.
	----(
	----	(
	----		   [ADDRESS_1] IS NOT NULL
	----		OR [ADDRESS_2] IS NOT NULL
	----		OR [ADDRESS_3] IS NOT NULL
	----	)
	----	AND
	----	(
	----		(
	----				[CITY] IS NOT NULL
	----			AND [STATE] IS NOT NULL
	----		)
	----		OR [ZIP] IS NOT NULL
	----	)
	----)
	------ Any direct line of communication is considered enough to make a valid contact
	----OR [PHONE] IS NOT NULL
	----OR [ALTERNATE_PHONE] IS NOT NULL
	----OR [EVENING_PHONE] IS NOT NULL
	----OR [EMERGENCY_PHONE] IS NOT NULL
	----OR [FAX] IS NOT NULL
	----OR [EMAIL] IS NOT NULL

	--UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_ESCO]
	--WHERE 
	---- Try to find enough data to make a reasonable contact.
	---- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	--(
	--	(
	--		   [ADDRESS_1] IS NOT NULL
	--		OR [ADDRESS_2] IS NOT NULL
	--		OR [ADDRESS_3] IS NOT NULL
	--	)
	--	AND
	--	(
	--		(
	--				[CITY] IS NOT NULL
	--			AND [STATE] IS NOT NULL
	--		)
	--		OR [ZIP] IS NOT NULL
	--	)
	--)
	---- Any direct line of communication is considered enough to make a valid contact
	--OR [PHONE] IS NOT NULL
	--OR [ALTERNATE_PHONE] IS NOT NULL
	--OR [EVENING_PHONE] IS NOT NULL
	--OR [EMERGENCY_PHONE] IS NOT NULL
	--OR [FAX] IS NOT NULL
	--OR [EMAIL] IS NOT NULL

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Aetna]
	--WHERE 
	---- Try to find enough data to make a reasonable contact.
	---- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	--(
	--	(
	--		   [ADDRESS_1] IS NOT NULL
	--		OR [ADDRESS_2] IS NOT NULL
	--		OR [ADDRESS_3] IS NOT NULL
	--	)
	--	AND
	--	(
	--		(
	--				[CITY] IS NOT NULL
	--			AND [STATE] IS NOT NULL
	--		)
	--		OR [ZIP] IS NOT NULL
	--	)
	--)
	---- Any direct line of communication is considered enough to make a valid contact
	--OR [PHONE] IS NOT NULL
	--OR [ALTERNATE_PHONE] IS NOT NULL
	--OR [EVENING_PHONE] IS NOT NULL
	--OR [EMERGENCY_PHONE] IS NOT NULL
	--OR [FAX] IS NOT NULL
	--OR [EMAIL] IS NOT NULL

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Cigna]
	--WHERE 
	---- Try to find enough data to make a reasonable contact.
	---- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	--(
	--	(
	--		   [ADDRESS_1] IS NOT NULL
	--		OR [ADDRESS_2] IS NOT NULL
	--		OR [ADDRESS_3] IS NOT NULL
	--	)
	--	AND
	--	(
	--		(
	--				[CITY] IS NOT NULL
	--			AND [STATE] IS NOT NULL
	--		)
	--		OR [ZIP] IS NOT NULL
	--	)
	--)
	---- Any direct line of communication is considered enough to make a valid contact
	--OR [PHONE] IS NOT NULL
	--OR [ALTERNATE_PHONE] IS NOT NULL
	--OR [EVENING_PHONE] IS NOT NULL
	--OR [EMERGENCY_PHONE] IS NOT NULL
	--OR [FAX] IS NOT NULL
	--OR [EMAIL] IS NOT NULL
	
	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Coventry]
	--WHERE 
	---- Try to find enough data to make a reasonable contact.
	---- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	--(
	--	(
	--		   [ADDRESS_1] IS NOT NULL
	--		OR [ADDRESS_2] IS NOT NULL
	--		OR [ADDRESS_3] IS NOT NULL
	--	)
	--	AND
	--	(
	--		(
	--				[CITY] IS NOT NULL
	--			AND [STATE] IS NOT NULL
	--		)
	--		OR [ZIP] IS NOT NULL
	--	)
	--)
	---- Any direct line of communication is considered enough to make a valid contact
	--OR [PHONE] IS NOT NULL
	--OR [ALTERNATE_PHONE] IS NOT NULL
	--OR [EVENING_PHONE] IS NOT NULL
	--OR [EMERGENCY_PHONE] IS NOT NULL
	--OR [FAX] IS NOT NULL
	--OR [EMAIL] IS NOT NULL


	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Humana]
	--WHERE 
	---- Try to find enough data to make a reasonable contact.
	---- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	--(
	--	(
	--		   [ADDRESS_1] IS NOT NULL
	--		OR [ADDRESS_2] IS NOT NULL
	--		OR [ADDRESS_3] IS NOT NULL
	--	)
	--	AND
	--	(
	--		(
	--				[CITY] IS NOT NULL
	--			AND [STATE] IS NOT NULL
	--		)
	--		OR [ZIP] IS NOT NULL
	--	)
	--)
	---- Any direct line of communication is considered enough to make a valid contact
	--OR [PHONE] IS NOT NULL
	--OR [ALTERNATE_PHONE] IS NOT NULL
	--OR [EVENING_PHONE] IS NOT NULL
	--OR [EMERGENCY_PHONE] IS NOT NULL
	--OR [FAX] IS NOT NULL
	--OR [EMAIL] IS NOT NULL

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Prospect]
	--WHERE 
	---- Try to find enough data to make a reasonable contact.
	---- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	--(
	--	(
	--		   [ADDRESS_1] IS NOT NULL
	--		OR [ADDRESS_2] IS NOT NULL
	--		OR [ADDRESS_3] IS NOT NULL
	--	)
	--	AND
	--	(
	--		(
	--				[CITY] IS NOT NULL
	--			AND [STATE] IS NOT NULL
	--		)
	--		OR [ZIP] IS NOT NULL
	--	)
	--)
	---- Any direct line of communication is considered enough to make a valid contact
	--OR [PHONE] IS NOT NULL
	--OR [ALTERNATE_PHONE] IS NOT NULL
	--OR [EVENING_PHONE] IS NOT NULL
	--OR [EMERGENCY_PHONE] IS NOT NULL
	--OR [FAX] IS NOT NULL
	--OR [EMAIL] IS NOT NULL

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Prelim_ESCO]

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_United]

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_Humana_CKD]

	UNION ALL

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Source_BCBSMI]


GO
