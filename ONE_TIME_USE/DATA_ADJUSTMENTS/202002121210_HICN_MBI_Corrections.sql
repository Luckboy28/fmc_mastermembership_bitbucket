

-- *******************************************************************************************************
--                                       Remove Empty CMS Members
-- *******************************************************************************************************



--SELECT DISTINCT
--	 MMKH1.[MBI] 
--	,MMKH1.[MEMBER_MASTER_ROW_ID] [MMKH1_MEMBER_MASTER_ROW_ID]
--	,MMKH2.[HICN]
--	,MMKH2.[MEMBER_MASTER_ROW_ID] [MMKH2_MEMBER_MASTER_ROW_ID]
--INTO [FHPDev].[dbo].[Daves_MBI_HICN_Audit]
--FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1
--JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2
--	ON MMKH1.[MBI] = MMKH2.[HICN]
--WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]
--AND MMKH1.[ROW_DELETED] = 'N'
--AND MMKH2.[ROW_DELETED] = 'N'


--SELECT * FROM [FHPDev].[dbo].[Daves_MBI_HICN_Audit] 



-- Soft Delete from Member Master
UPDATE MM
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_DELETED_REASON] = 'Manual Cleanup'
FROM [FHPDW].[dbo].[Member_Master] MM
JOIN [FHPDev].[dbo].[Daves_MBI_HICN_Audit] Aud
ON MM.[MEMBER_MASTER_ROW_ID] = Aud.[MMKH2_MEMBER_MASTER_ROW_ID]  --HICNS that are actually MBI from other members
WHERE MM.[LOB_TYPE] = 'ESCO'
AND MM.[LOB_VENDOR] = 'CMS'
AND MM.[FIRST_NAME] IS NULL
AND MM.[LAST_NAME] IS NULL
AND CAST(MM.[ROW_CREATE_DATE] AS DATE) = CAST('2020-01-30' AS DATE)
AND MM.[ROW_DELETED] = 'N'
GO


-- Soft Delete from Member Master Key History
UPDATE MMKH
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_DELETED_REASON] = 'Manual Cleanup'
FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
JOIN [FHPDev].[dbo].[Daves_MBI_HICN_Audit] Aud
	ON MMKH.[MEMBER_MASTER_ROW_ID] = Aud.[MMKH2_MEMBER_MASTER_ROW_ID]  --HICNS that are actually MBI from other members
JOIN [FHPDW].[dbo].[Member_Master] MM
	ON MM.[MEMBER_MASTER_ROW_ID] = Aud.[MMKH2_MEMBER_MASTER_ROW_ID]
WHERE MM.[LOB_TYPE] = 'ESCO'
AND MM.[LOB_VENDOR] = 'CMS'
AND MM.[FIRST_NAME] IS NULL
AND MM.[LAST_NAME] IS NULL
AND CAST(MM.[ROW_CREATE_DATE] AS DATE) = CAST('2020-01-30' AS DATE)
AND MM.[ROW_DELETED] = 'N'
AND MMKH.[ROW_DELETED] = 'N'
GO




-- Soft Delete from Member Eligibility
UPDATE ME
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_DELETED_REASON] = 'Manual Cleanup'
FROM [FHPDW].[dbo].[Member_Eligibility] ME
JOIN [FHPDW].[dbo].[Member_Master] MM
	ON ME.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
JOIN [FHPDev].[dbo].[Daves_MBI_HICN_Audit] Aud
	ON MM.[MEMBER_MASTER_ROW_ID] = Aud.[MMKH2_MEMBER_MASTER_ROW_ID]  --HICNS that are actually MBI from other members
WHERE MM.[LOB_TYPE] = 'ESCO'
AND MM.[LOB_VENDOR] = 'CMS'
AND MM.[FIRST_NAME] IS NULL
AND MM.[LAST_NAME] IS NULL
AND CAST(MM.[ROW_CREATE_DATE] AS DATE) = CAST('2020-01-30' AS DATE)
AND MM.[ROW_DELETED] = 'N'
AND ME.[ROW_DELETED] = 'N'
GO



-- Soft Delete from Member Address
UPDATE MA
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_DELETED_REASON] = 'Manual Cleanup'
FROM [FHPDW].[dbo].[Member_Address] MA
JOIN [FHPDW].[dbo].[Member_Master] MM
	ON MA.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
JOIN [FHPDev].[dbo].[Daves_MBI_HICN_Audit] Aud
	ON MM.[MEMBER_MASTER_ROW_ID] = Aud.[MMKH2_MEMBER_MASTER_ROW_ID]  --HICNS that are actually MBI from other members
WHERE MM.[LOB_TYPE] = 'ESCO'
AND MM.[LOB_VENDOR] = 'CMS'
AND MM.[FIRST_NAME] IS NULL
AND MM.[LAST_NAME] IS NULL
AND CAST(MM.[ROW_CREATE_DATE] AS DATE) = CAST('2020-01-30' AS DATE)
AND MM.[ROW_DELETED] = 'N'
AND MA.[ROW_DELETED] = 'N'
GO






-- *******************************************************************************************************
--                                   Copy MBI records out of HICN column
--                                          (Same Member Only)
-- *******************************************************************************************************


-- Copy MBI data out of the HICN column, and insert back as MBI
;WITH MMKH_Additions AS
(
	SELECT DISTINCT
		 [MEMBER_MASTER_ROW_ID]
		,[HICN] AS [MBI]
	FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
	WHERE [FHPDev].[dbo].[MBI_Validate]([HICN]) IS NOT NULL
	AND [ROW_DELETED] = 'N'
	AND [HICN] IS NOT NULL

	EXCEPT

	-- Don't need to copy if the MBI already exists for this member
	SELECT DISTINCT
		 [MEMBER_MASTER_ROW_ID]
		,[MBI]
	FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
	WHERE [ROW_DELETED] = 'N'
	AND [MBI] IS NOT NULL
)
INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
(
	 [MEMBER_MASTER_ROW_ID]
	,[MBI]
	,[ROW_SOURCE]
	,[ROW_SOURCE_ID]
)
SELECT DISTINCT
	 [MEMBER_MASTER_ROW_ID]
	,[MBI]
	,'Manual Cleanup' --[ROW_SOURCE]
	,NULL --[ROW_SOURCE_ID]
FROM MMKH_Additions
GO




-- Update the KeyHistory to remove MBI from the HICN column
UPDATE [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
SET
	 [HICN] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
WHERE [FHPDev].[dbo].[MBI_Validate]([HICN]) IS NOT NULL --HICN field contains an MBI
AND [ROW_DELETED] = 'N'
GO



-- Update the Member Master table with the latest valid HICN (or a NULL) if it has an MBI in the HICN field
;WITH Latest_HICN AS
(
	SELECT DISTINCT
		 [MEMBER_MASTER_ROW_ID]
		,[HICN]
		,ROW_NUMBER() OVER (PARTITION BY [MEMBER_MASTER_ROW_ID] ORDER BY [ROW_CREATE_DATE] DESC) AS [RANK]
	FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	WHERE [HICN] IS NOT NULL
	AND [FHPDev].[dbo].[MBI_Validate]([HICN]) IS NULL  --HICN field does NOT contain an MBI
	AND [ROW_DELETED] = 'N'
)
,Latest_HICN_Ranked AS
(
	SELECT DISTINCT
		 [MEMBER_MASTER_ROW_ID]
		,[HICN]
	FROM Latest_HICN
	WHERE [RANK] = 1
)
UPDATE MM
SET
	[HICN] = Latest_HICN_Ranked.[HICN]
FROM [FHPDW].[dbo].[Member_Master] MM
LEFT JOIN Latest_HICN_Ranked
	ON Latest_HICN_Ranked.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
WHERE [FHPDev].[dbo].[MBI_Validate](MM.[HICN]) IS NOT NULL   --Only update records where there is currently an MBI in the HICN column
AND MM.[ROW_DELETED] = 'N'
GO

