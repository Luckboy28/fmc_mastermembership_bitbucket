




-- Should produce 221 row changes
UPDATE Mem
SET
	Mem.[Neph_NPI_ID] = COALESCE(Ref.[RENDERING_NEPHROLOGIST_NPI], Mem.[Neph_NPI_ID])
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] Mem
LEFT JOIN [FHPDev].[dbo].[Humana_CKD_Referrals] Ref
	ON Mem.[MBR_ID] = Ref.[IDCARD_MBR_ID]
WHERE Mem.[Neph_NPI_ID] <> COALESCE(Ref.[RENDERING_NEPHROLOGIST_NPI], Mem.[Neph_NPI_ID])
AND Mem.[Neph_NPI_ID] IS NOT NULL


