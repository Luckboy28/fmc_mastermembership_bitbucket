
USE [FHPDev]
GO


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF0_Summary_Statistics_Header_Record_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF0_Summary_Statistics_Header_Record_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF0_Summary_Statistics_Header_Record_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF0_Summary_Statistics_Header_Record]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF1_PartA_Claims_Header_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF1_PartA_Claims_Header_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF1_PartA_Claims_Header_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF1_PartA_Claims_Header]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF2_PartA_Claims_Revenue_Center_Detail_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF2_PartA_Claims_Revenue_Center_Detail_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF3_PartA_Procedure_Code_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF3_PartA_Procedure_Code_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF3_PartA_Procedure_Code_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF3_PartA_Procedure_Code]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF4_PartA_Diagnosis_Code_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF4_PartA_Diagnosis_Code_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF4_PartA_Diagnosis_Code_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF4_PartA_Diagnosis_Code]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF5_PartB_Physicians_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF5_PartB_Physicians_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF5_PartB_Physicians_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF5_PartB_Physicians]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF6_PartB_DME_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF6_PartB_DME_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF6_PartB_DME_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF6_PartB_DME]
GO




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF7_PartD_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF7_PartD_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF7_PartD_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF7_PartD]
GO





-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF8_Beneficiary_Demographics_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF8_Beneficiary_Demographics_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF8_Beneficiary_Demographics_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF8_Beneficiary_Demographics]
GO





-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF9_Beneficiary_XREF_BACKUP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF9_Beneficiary_XREF_BACKUP_201904041525;
END
GO

SELECT * INTO [FHPDev].[dbo].[CCLF9_Beneficiary_XREF_BACKUP_201904041525] FROM [FHPDW].[dbo].[CCLF9_Beneficiary_XREF]
GO




-- Verify counts

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF0_Summary_Statistics_Header_Record_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF0_Summary_Statistics_Header_Record]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF1_PartA_Claims_Header_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF1_PartA_Claims_Header]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF3_PartA_Procedure_Code_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF3_PartA_Procedure_Code]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF4_PartA_Diagnosis_Code_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF4_PartA_Diagnosis_Code]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF5_PartB_Physicians_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF5_PartB_Physicians]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF6_PartB_DME_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF6_PartB_DME]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF7_PartD_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF7_PartD]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF8_Beneficiary_Demographics_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF8_Beneficiary_Demographics]

SELECT COUNT(1) [Backup] FROM [FHPDev].[dbo].[CCLF9_Beneficiary_XREF_BACKUP_201904041525]
SELECT COUNT(1) [Live] FROM [FHPDW].[dbo].[CCLF9_Beneficiary_XREF]