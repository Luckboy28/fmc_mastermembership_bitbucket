
USE [FHPDev]
GO


-- DONE
----------------------------------- CCLF1_PartA_Claims_Header ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF1_PartA_Claims_Header_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF1_PartA_Claims_Header_DUPLICATES_201904041525;
END
GO


;WITH NormalizedData AS
(
	SELECT
		TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF1_PartA_Claims_Header] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF1_PartA_Claims_Header_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1



-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF1_PartA_Claims_Header_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF1_PartA_Claims_Header_KEEP_201904041525;
END
GO


;WITH NormalizedData AS
(
	SELECT
		TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF1_PartA_Claims_Header] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF1_PartA_Claims_Header_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [RANK]








-- DONE
----------------------------------- CCLF2_PartA_Claims_Revenue_Center_Detail ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF2_PartA_Claims_Revenue_Center_Detail_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF2_PartA_Claims_Revenue_Center_Detail_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1



-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF2_PartA_Claims_Revenue_Center_Detail_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF2_PartA_Claims_Revenue_Center_Detail_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF2_PartA_Claims_Revenue_Center_Detail_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [RANK]




-- DONE
----------------------------------- CCLF3_PartA_Procedure_Code ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF3_PartA_Procedure_Code_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF3_PartA_Procedure_Code_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_VAL_SQNC_NUM], [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF3_PartA_Procedure_Code] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF3_PartA_Procedure_Code_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1



-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF3_PartA_Procedure_Code_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF3_PartA_Procedure_Code_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_VAL_SQNC_NUM], [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF3_PartA_Procedure_Code] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF3_PartA_Procedure_Code_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_VAL_SQNC_NUM], [RANK]




----------------------------------- CCLF4_PartA_Diagnosis_Code ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF4_PartA_Diagnosis_Code_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF4_PartA_Diagnosis_Code_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_VAL_SQNC_NUM], [CLM_PROD_TYPE_CD], [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF4_PartA_Diagnosis_Code] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF4_PartA_Diagnosis_Code_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF4_PartA_Diagnosis_Code_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF4_PartA_Diagnosis_Code_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_VAL_SQNC_NUM], [CLM_PROD_TYPE_CD], [PRVDR_OSCAR_NUM], [CLM_FROM_DT], [CLM_THRU_DT], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF4_PartA_Diagnosis_Code] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF4_PartA_Diagnosis_Code_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_VAL_SQNC_NUM], [CLM_PROD_TYPE_CD], [RANK]




-- DONE
----------------------------------- CCLF5_PartB_Physicians ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF5_PartB_Physicians_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF5_PartB_Physicians_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [CLM_CNTL_NUM], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF5_PartB_Physicians] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF5_PartB_Physicians_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1



-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF5_PartB_Physicians_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF5_PartB_Physicians_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [CLM_CNTL_NUM], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF5_PartB_Physicians] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF5_PartB_Physicians_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [RANK]



-- DONE
----------------------------------- CCLF6_PartB_DME ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF6_PartB_DME_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF6_PartB_DME_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [CLM_CNTL_NUM], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF6_PartB_DME] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF6_PartB_DME_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF6_PartB_DME_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF6_PartB_DME_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [CLM_CNTL_NUM], [BENE_EQTBL_BIC_HICN_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF6_PartB_DME] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF6_PartB_DME_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_NUM], [RANK]



-- DONE
----------------------------------- CCLF7_PartD ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF7_PartD_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF7_PartD_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_FROM_DT], [PRVDR_SRVC_ID_QLFYR_CD], [CLM_SRVC_PRVDR_GNRC_ID_NUM], [CLM_DSPNSNG_STUS_CD], [CLM_LINE_RX_SRVC_RFRNC_NUM], [CLM_LINE_RX_FILL_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF7_PartD] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF7_PartD_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1





-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF7_PartD_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF7_PartD_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [CLM_LINE_FROM_DT], [PRVDR_SRVC_ID_QLFYR_CD], [CLM_SRVC_PRVDR_GNRC_ID_NUM], [CLM_DSPNSNG_STUS_CD], [CLM_LINE_RX_SRVC_RFRNC_NUM], [CLM_LINE_RX_FILL_NUM], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF7_PartD] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF7_PartD_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY RIGHT(CONCAT('00',LTRIM(RTRIM([CUR_CLM_UNIQ_ID]))),13), [RANK]



-- DONE
----------------------------------- CCLF8_Beneficiary_Demographics ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF8_Beneficiary_Demographics_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF8_Beneficiary_Demographics_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY [BENE_HIC_NUM], [BENE_RNG_BGN_DT], [BENE_RNG_END_DT], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF8_Beneficiary_Demographics] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF8_Beneficiary_Demographics_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF8_Beneficiary_Demographics_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF8_Beneficiary_Demographics_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY [BENE_HIC_NUM], [BENE_RNG_BGN_DT], [BENE_RNG_END_DT], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF8_Beneficiary_Demographics] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF8_Beneficiary_Demographics_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY [BENE_HIC_NUM],[BENE_RNG_BGN_DT],[BENE_RNG_END_DT], [RANK]



-- DONE
----------------------------------- CCLF9_Beneficiary_XREF ------------------------------------


-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF9_Beneficiary_XREF_DUPLICATES_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF9_Beneficiary_XREF_DUPLICATES_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY [CRNT_HIC_NUM], [PRVS_HIC_NUM], [PRVS_HICN_EFCTV_DT], [PRVS_HICN_OBSLT_DT], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF9_Beneficiary_XREF] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF9_Beneficiary_XREF_DUPLICATES_201904041525]
FROM NormalizedData
WHERE [RANK] > 1




-- Drop the table if it already exists
IF OBJECT_ID('dbo.CCLF9_Beneficiary_XREF_KEEP_201904041525', 'U') IS NOT NULL
BEGIN
	DROP TABLE dbo.CCLF9_Beneficiary_XREF_KEEP_201904041525;
END
GO



;WITH NormalizedData AS
(
	SELECT
		 TheData.*
		,ROW_NUMBER() OVER (PARTITION BY [CRNT_HIC_NUM], [PRVS_HIC_NUM], [PRVS_HICN_EFCTV_DT], [PRVS_HICN_OBSLT_DT], [ESCO_ID] ORDER BY [loadDate] DESC) [RANK]
	FROM [FHPDW].[dbo].[CCLF9_Beneficiary_XREF] TheData
)
SELECT * INTO [FHPDev].[dbo].[CCLF9_Beneficiary_XREF_KEEP_201904041525]
FROM NormalizedData
WHERE [RANK] = 1
ORDER BY [CRNT_HIC_NUM],[PRVS_HIC_NUM],[PRVS_HICN_EFCTV_DT],[PRVS_HICN_OBSLT_DT],[RANK]
