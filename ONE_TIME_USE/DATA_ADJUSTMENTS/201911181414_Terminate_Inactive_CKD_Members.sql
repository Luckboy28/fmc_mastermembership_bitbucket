

-- Terminate inactive CKD members
UPDATE ME 
SET
	 TERM_DATE = CAST('2019-11-13' AS DATE)
	,TERM_REASON = 'Replaced by another LOB'
	,ROW_UPDATE_DATE = GETDATE()
FROM [FHPDW].[dbo].[Member_Eligibility] ME
WHERE [LOB_TYPE] = 'CKD'
AND [LOB_VENDOR] = 'HUMANA'
AND [STATUS] = 'INACTIVE'
AND [START_DATE] = CAST('2019-11-13' AS DATE)