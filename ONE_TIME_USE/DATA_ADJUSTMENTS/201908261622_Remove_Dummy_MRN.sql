
-- Before
SELECT [MRN] FROM [FHPDW].[dbo].[Membership_Export_Member]
WHERE [MRN] LIKE '99999%' AND LEN([MRN]) = 9

-- Update
UPDATE [FHPDW].[dbo].[Membership_Export_Member]
SET [MRN] = NULL
WHERE [MRN] LIKE '99999%' AND LEN([MRN]) = 9

-- After (should be empty)
SELECT [MRN] FROM [FHPDW].[dbo].[Membership_Export_Member]
WHERE [MRN] LIKE '99999%' AND LEN([MRN]) = 9