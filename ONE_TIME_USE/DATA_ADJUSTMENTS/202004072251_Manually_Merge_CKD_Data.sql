

--------------------------------------- PATIENT EVENTS ----------------------------------------


-- Update MM Patient Events
UPDATE MPE
	SET MPE.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDW].[dbo].[Member_PatientEvents] MPE
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MPE.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Update MembershipExport Patient Events
UPDATE MEPE
	SET MEPE.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDW].[dbo].[Membership_Export_PatientEvents] MEPE
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MEPE.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Update Patient Events Historical
UPDATE MPEH
	SET MPEH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Historical] MPEH
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MPEH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Update Patient Events Staging
UPDATE MPES
	SET MPES.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Staging] MPES
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MPES.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO




--------------------------------------- LABS ----------------------------------------

-- Update MM Labs
UPDATE ML
	SET ML.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDW].[dbo].[Member_Labs] ML
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON ML.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Update MembershipExport Labs
UPDATE MEL
	SET MEL.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDW].[dbo].[Membership_Export_Labs] MEL
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MEL.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Update Labs Historical
UPDATE MLH
	SET MLH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDataMarts].[dbo].[Member_Labs_Historical] MLH
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MLH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Update Labs Staging
UPDATE MLS
	SET MLS.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDataMarts].[dbo].[Member_Labs_Staging] MLS
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MLS.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO




--------------------------------------- PROVIDER SCHEMA ----------------------------------------


-- Update Member Provider
UPDATE MEMP
	SET MEMP.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
FROM [FHPDW].[dbo].[Membership_Export_Member_Provider] MEMP
JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
ON MEMP.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
GO


-- Note:  The remaining Provider schema tables do not rely on MM ID's, so they can be left alone. 