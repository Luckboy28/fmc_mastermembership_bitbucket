




----------------------------------- ISSUE #1 -----------------------------------------

-- Remove bad HICN from 61764
UPDATE MMKH
SET
	 [HICN] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
WHERE [MEMBER_MASTER_ROW_ID] = 61764
AND [HICN] = '5TD7J76RW67'


-- Remove bad CLAIM_SUBSCRIBER_ID from 61764, and move to 88705
UPDATE MMKH
SET
	 [CLAIM_SUBSCRIBER_ID] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
WHERE [MEMBER_MASTER_ROW_ID] = 61764
AND [CLAIM_SUBSCRIBER_ID] = '100009976'


UPDATE MMKH
SET
	 [CLAIM_SUBSCRIBER_ID] = '100009976'
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
WHERE [MEMBER_MASTER_ROW_ID] = 88705
AND [MEDHOK_ID] = '100009976'
AND [CLAIM_SUBSCRIBER_ID] IS NULL


UPDATE MM
SET
	 [CLAIM_SUBSCRIBER_ID] = '100009976'
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDW].[dbo].[Member_Master] MM
WHERE [MEMBER_MASTER_ROW_ID] = 88705
AND [MEDHOK_ID] = '100009976'
AND [CLAIM_SUBSCRIBER_ID] IS NULL





----------------------------------- ISSUE #2 -----------------------------------------

-- Merge Members:  55236 -> 60245
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 60245 AND [MEMBER_MASTER_ROW_ID_MERGE] = 55236) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (60245,55236)




----------------------------------- ISSUE #3 -----------------------------------------

-- Merge Members:  55257 -> 60048
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 60048 AND [MEMBER_MASTER_ROW_ID_MERGE] = 55257) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (60048,55257)




----------------------------------- ISSUE #4 -----------------------------------------

-- Merge Members:  55262 -> 31588
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 31588 AND [MEMBER_MASTER_ROW_ID_MERGE] = 55262) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (31588,55262)




----------------------------------- ISSUE #5 -----------------------------------------

-- Merge Members:  55282 -> 20478
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 20478 AND [MEMBER_MASTER_ROW_ID_MERGE] = 55282) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (20478,55282)



----------------------------------- ISSUE #6 -----------------------------------------

-- Merge Members:  55333 -> 37589
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 37589 AND [MEMBER_MASTER_ROW_ID_MERGE] = 55333) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (37589,55333)




----------------------------------- ISSUE #7 -----------------------------------------

-- Add new records to the Affilliated table
INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider_Affiliation]
(
	-- [MEMBERSHIP_EXPORT_PROVIDER_AFFILIATION_ROW_ID]
	 [NPI]
	,[AFFILIATION]
	,[ROW_SOURCE]
	,[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	--,[ROW_UPDATE_DATE]
)
SELECT
	-- [MEMBERSHIP_EXPORT_PROVIDER_AFFILIATION_ROW_ID]
	 MEP.[NPI] --[NPI]
	,'Humana CKD' --[AFFILIATION]
	,'MANUAL LOAD' --[ROW_SOURCE]
	,NULL --[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	--,[ROW_UPDATE_DATE]
FROM [FHPDW].[dbo].[Membership_Export_Provider] MEP
LEFT JOIN [FHPDW].[dbo].[Membership_Export_Provider_Affiliation] MEPA
	ON MEP.[NPI] = MEPA.[NPI]
WHERE MEPA.[NPI] IS NULL





----------------------------------- ISSUE #8 -----------------------------------------


-- Merge Members:  133311 -> 131765
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 131765 AND [MEMBER_MASTER_ROW_ID_MERGE] = 133311) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (131765,133311)



----------------------------------- ISSUE #9 -----------------------------------------


-- Merge Members:  132219 -> 131757
IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] WHERE [MEMBER_MASTER_ROW_ID_PRIMARY] = 131757 AND [MEMBER_MASTER_ROW_ID_MERGE] = 132219) INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] ([MEMBER_MASTER_ROW_ID_PRIMARY], [MEMBER_MASTER_ROW_ID_MERGE]) VALUES (131757,132219)


