


-- Clear out members without names
DELETE MEM
FROM [FHPDW].[dbo].[Membership_Export_Member] MEM
WHERE [FIRST_NAME] IS NULL
AND [LAST_NAME] IS NULL


-- Clear out members without eligibility records
DELETE MEM
FROM [FHPDW].[dbo].[Membership_Export_Member] MEM
LEFT JOIN [FHPDW].[dbo].[Membership_Export_Eligibility] MEE
ON MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MEE.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
WHERE MEE.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] IS NULL