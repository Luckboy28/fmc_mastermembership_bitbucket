


USE [FHPDW]
GO


TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Facility]
GO

TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Provider]
GO

TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Provider_Facility]
GO
