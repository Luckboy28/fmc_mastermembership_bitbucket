




---- Backup Members
--SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Member_BACKUP_20200526]
--FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]



-- Insert New Members
INSERT INTO [FHPDataMarts].[dbo].[Humana_CKD_Member]
(
	 [MBR_ID]
	,[MBI]
	,[SCORE]
	,[Val_eGFR]
	,[Days_eGFR]
	,[current_HCCP_managed]
	,[cat]
	,[COV_EFFECTIVE]
	,[COV_CANCEL]
	,[LAST_NAME]
	,[FIRST_NAME]
	,[DATE_OF_BIRTH]
	,[Member_Race]
	,[SEX_CD]
	,[ADDRESS_LINE1]
	,[ADDRESS_LINE2]
	,[CITY]
	,[STATE_CD]
	,[ZIP_CD]
	,[HOME_PHONE_NBR]
	,[COUNTY_CD]
	,[PCP_FIRST_NAME]
	,[PCP_LAST_NAME]
	,[PCP_ADDR_LINE1]
	,[PCP_ADDR_LINE2]
	,[PCP_CITY_NAME]
	,[PCP_STATE_CD]
	,[PCP_ZIP_CD]
	,[PCP_PHONE_NBR]
	,[PCP_NPI_ID]
	,[PCP_GROUPER_NAME]
	,[PCP_ORG_NAME]
	,[PROCESS_DATE]
	,[MAJOR_LOB_CD]
	,[Age]
	,[DIABETES]
	,[Diab_Type]
	,[HYPERTENSION]
	,[Cardiovascular]
	,[PVD]
	,[CHF]
	,[CHF_STAGE]
	,[Metabolic_syndrome]
	,[PROTEINURIA]
	,[CKD_STAGE]
	,[Neph_TAX_ID]
	,[Neph_NPI_ID]
	,[Nephrologist_Name]
	,[ROW_SOURCE]
	--,[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	--,[ROW_UPDATE_DATE]
)
SELECT DISTINCT
	 TheSource.[MBR_ID]
	,TheSource.[MBI]
	,TheSource.[SCORE]
	,TheSource.[Val_eGFR]
	,TheSource.[Days_eGFR]
	,TheSource.[current_HCCP_managed]
	,TheSource.[cat]
	,TheSource.[COV_EFFECTIVE]
	,TheSource.[COV_CANCEL]
	,TheSource.[LAST_NAME]
	,TheSource.[FIRST_NAME]
	,TheSource.[DATE_OF_BIRTH]
	,TheSource.[Member_Race]
	,TheSource.[SEX_CD]
	,TheSource.[ADDRESS_LINE1]
	,TheSource.[ADDRESS_LINE2]
	,TheSource.[CITY]
	,TheSource.[STATE_CD]
	,TheSource.[ZIP_CD]
	,TheSource.[HOME_PHONE_NBR]
	,TheSource.[COUNTY_CD]
	,TheSource.[PCP_FIRST_NAME]
	,TheSource.[PCP_LAST_NAME]
	,TheSource.[PCP_ADDR_LINE1]
	,TheSource.[PCP_ADDR_LINE2]
	,TheSource.[PCP_CITY_NAME]
	,TheSource.[PCP_STATE_CD]
	,TheSource.[PCP_ZIP_CD]
	,TheSource.[PCP_PHONE_NBR]
	,TheSource.[PCP_NPI_ID]
	,TheSource.[PCP_GROUPER_NAME]
	,TheSource.[PCP_ORG_NAME]
	,TheSource.[PROCESS_DATE]
	,TheSource.[MAJOR_LOB_CD]
	,TheSource.[Age]
	,TheSource.[DIABETES]
	,TheSource.[Diab_Type]
	,TheSource.[HYPERTENSION]
	,TheSource.[Cardiovascular]
	,TheSource.[PVD]
	,TheSource.[CHF]
	,TheSource.[CHF_STAGE]
	,TheSource.[Metabolic_syndrome]
	,TheSource.[PROTEINURIA]
	,TheSource.[CKD_STAGE]
	,TheSource.[Neph_TAX_ID]
	,TheSource.[Neph_NPI_ID]
	,TheSource.[Nephrologist_Name]
	,TheSource.[ROW_SOURCE]
	--,[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	--,[ROW_UPDATE_DATE]
FROM [FHPDev].[dbo].[Humana_CKD_Member] TheSource
LEFT JOIN [FHPDataMarts].[dbo].[Humana_CKD_Member] TheDestination
ON TheSource.[MBR_ID] = TheDestination.[MBR_ID]
WHERE TheDestination.[MBR_ID] IS NULL







-- Update Existing Members
;WITH TheData AS
(
	SELECT
		 [MBR_ID]
		,[MBI]
		,[SCORE]
		,[Val_eGFR]
		,[Days_eGFR]
		,[current_HCCP_managed]
		,[cat]
		,[COV_EFFECTIVE]
		,[COV_CANCEL]
		,[LAST_NAME]
		,[FIRST_NAME]
		,[DATE_OF_BIRTH]
		,[Member_Race]
		,[SEX_CD]
		,[ADDRESS_LINE1]
		,[ADDRESS_LINE2]
		,[CITY]
		,[STATE_CD]
		,[ZIP_CD]
		,[HOME_PHONE_NBR]
		,[COUNTY_CD]
		,[PCP_FIRST_NAME]
		,[PCP_LAST_NAME]
		,[PCP_ADDR_LINE1]
		,[PCP_ADDR_LINE2]
		,[PCP_CITY_NAME]
		,[PCP_STATE_CD]
		,[PCP_ZIP_CD]
		,[PCP_PHONE_NBR]
		,[PCP_NPI_ID]
		,[PCP_GROUPER_NAME]
		,[PCP_ORG_NAME]
		,[PROCESS_DATE]
		,[MAJOR_LOB_CD]
		,[Age]
		,[DIABETES]
		,[Diab_Type]
		,[HYPERTENSION]
		,[Cardiovascular]
		,[PVD]
		,[CHF]
		,[CHF_STAGE]
		,[Metabolic_syndrome]
		,[PROTEINURIA]
		,[CKD_STAGE]
		,[Neph_TAX_ID]
		,[Neph_NPI_ID]
		,[Nephrologist_Name]
		,[ROW_SOURCE]
		--,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDev].[dbo].[Humana_CKD_Member]

	EXCEPT

	SELECT
		 [MBR_ID]
		,[MBI]
		,[SCORE]
		,[Val_eGFR]
		,[Days_eGFR]
		,[current_HCCP_managed]
		,[cat]
		,[COV_EFFECTIVE]
		,[COV_CANCEL]
		,[LAST_NAME]
		,[FIRST_NAME]
		,[DATE_OF_BIRTH]
		,[Member_Race]
		,[SEX_CD]
		,[ADDRESS_LINE1]
		,[ADDRESS_LINE2]
		,[CITY]
		,[STATE_CD]
		,[ZIP_CD]
		,[HOME_PHONE_NBR]
		,[COUNTY_CD]
		,[PCP_FIRST_NAME]
		,[PCP_LAST_NAME]
		,[PCP_ADDR_LINE1]
		,[PCP_ADDR_LINE2]
		,[PCP_CITY_NAME]
		,[PCP_STATE_CD]
		,[PCP_ZIP_CD]
		,[PCP_PHONE_NBR]
		,[PCP_NPI_ID]
		,[PCP_GROUPER_NAME]
		,[PCP_ORG_NAME]
		,[PROCESS_DATE]
		,[MAJOR_LOB_CD]
		,[Age]
		,[DIABETES]
		,[Diab_Type]
		,[HYPERTENSION]
		,[Cardiovascular]
		,[PVD]
		,[CHF]
		,[CHF_STAGE]
		,[Metabolic_syndrome]
		,[PROTEINURIA]
		,[CKD_STAGE]
		,[Neph_TAX_ID]
		,[Neph_NPI_ID]
		,[Nephrologist_Name]
		,[ROW_SOURCE]
		--,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]
)
UPDATE TheTable
SET
	-- [MBR_ID] = TheData.[MBR_ID]
	 [MBI] = TheData.[MBI]
	,[SCORE] = TheData.[SCORE]
	,[Val_eGFR] = TheData.[Val_eGFR]
	,[Days_eGFR] = TheData.[Days_eGFR]
	,[current_HCCP_managed] = TheData.[current_HCCP_managed]
	,[cat] = TheData.[cat]
	,[COV_EFFECTIVE] = TheData.[COV_EFFECTIVE]
	,[COV_CANCEL] = TheData.[COV_CANCEL]
	,[LAST_NAME] = TheData.[LAST_NAME]
	,[FIRST_NAME] = TheData.[FIRST_NAME]
	,[DATE_OF_BIRTH] = TheData.[DATE_OF_BIRTH]
	,[Member_Race] = TheData.[Member_Race]
	,[SEX_CD] = TheData.[SEX_CD]
	,[ADDRESS_LINE1] = TheData.[ADDRESS_LINE1]
	,[ADDRESS_LINE2] = TheData.[ADDRESS_LINE2]
	,[CITY] = TheData.[CITY]
	,[STATE_CD] = TheData.[STATE_CD]
	,[ZIP_CD] = TheData.[ZIP_CD]
	,[HOME_PHONE_NBR] = TheData.[HOME_PHONE_NBR]
	,[COUNTY_CD] = TheData.[COUNTY_CD]
	,[PCP_FIRST_NAME] = TheData.[PCP_FIRST_NAME]
	,[PCP_LAST_NAME] = TheData.[PCP_LAST_NAME]
	,[PCP_ADDR_LINE1] = TheData.[PCP_ADDR_LINE1]
	,[PCP_ADDR_LINE2] = TheData.[PCP_ADDR_LINE2]
	,[PCP_CITY_NAME] = TheData.[PCP_CITY_NAME]
	,[PCP_STATE_CD] = TheData.[PCP_STATE_CD]
	,[PCP_ZIP_CD] = TheData.[PCP_ZIP_CD]
	,[PCP_PHONE_NBR] = TheData.[PCP_PHONE_NBR]
	,[PCP_NPI_ID] = TheData.[PCP_NPI_ID]
	,[PCP_GROUPER_NAME] = TheData.[PCP_GROUPER_NAME]
	,[PCP_ORG_NAME] = TheData.[PCP_ORG_NAME]
	,[PROCESS_DATE] = TheData.[PROCESS_DATE]
	,[MAJOR_LOB_CD] = TheData.[MAJOR_LOB_CD]
	,[Age] = TheData.[Age]
	,[DIABETES] = TheData.[DIABETES]
	,[Diab_Type] = TheData.[Diab_Type]
	,[HYPERTENSION] = TheData.[HYPERTENSION]
	,[Cardiovascular] = TheData.[Cardiovascular]
	,[PVD] = TheData.[PVD]
	,[CHF] = TheData.[CHF]
	,[CHF_STAGE] = TheData.[CHF_STAGE]
	,[Metabolic_syndrome] = TheData.[Metabolic_syndrome]
	,[PROTEINURIA] = TheData.[PROTEINURIA]
	,[CKD_STAGE] = TheData.[CKD_STAGE]
	,[Neph_TAX_ID] = TheData.[Neph_TAX_ID]
	,[Neph_NPI_ID] = TheData.[Neph_NPI_ID]
	,[Nephrologist_Name] = TheData.[Nephrologist_Name]
	,[ROW_SOURCE] = TheData.[ROW_SOURCE]
	--,[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] TheTable
JOIN TheData
ON TheData.[MBR_ID] = TheTable.[MBR_ID]