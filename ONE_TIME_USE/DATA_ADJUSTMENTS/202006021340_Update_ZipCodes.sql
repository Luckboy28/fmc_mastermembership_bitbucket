		
		
		
UPDATE MM_ADDRESS
SET
	[ZIP] = CAST(
				CASE
					WHEN
						NULLIF(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),'') IS NULL
					THEN
						NULL
					ELSE
						CASE
							WHEN
								LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))) > 5 
							THEN
								CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))))))
							ELSE
								LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))
						END
				END
			AS VARCHAR(10))
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDW].[dbo].[Member_Address] MM_ADDRESS
WHERE [ZIP] IS NOT NULL
AND [ROW_DELETED] = 'N'
AND [ZIP] <> CAST(
				CASE
					WHEN
						NULLIF(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),'') IS NULL
					THEN
						NULL
					ELSE
						CASE
							WHEN
								LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))) > 5 
							THEN
								CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))))))
							ELSE
								LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))
						END
				END
			AS VARCHAR(10))



		
		
		
UPDATE MME_ADDRESS
SET
	[ZIP] = CAST(
				CASE
					WHEN
						NULLIF(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),'') IS NULL
					THEN
						NULL
					ELSE
						CASE
							WHEN
								LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))) > 5 
							THEN
								CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))))))
							ELSE
								LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))
						END
				END
			AS VARCHAR(10))
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDW].[dbo].[Membership_Export_Member] MME_ADDRESS
WHERE [ZIP] IS NOT NULL
AND [ROW_DELETED] = 'N'
AND [ZIP] <> CAST(
				CASE
					WHEN
						NULLIF(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),'') IS NULL
					THEN
						NULL
					ELSE
						CASE
							WHEN
								LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))) > 5 
							THEN
								CONCAT(SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),1,5),'-',SUBSTRING(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))),6,LEN(LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~',''))))))
							ELSE
								LTRIM(RTRIM(REPLACE(REPLACE([ZIP],'-',''),'~','')))
						END
				END
			AS VARCHAR(10))
			