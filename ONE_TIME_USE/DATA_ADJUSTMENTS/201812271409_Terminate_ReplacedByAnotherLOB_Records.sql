

-- Adjust TERM_REASON to prevent LOB "downgrades"

UPDATE Eli
SET
	 [TERM_REASON] = 'LOB Expired: Replaced by another LOB'
	,[STATUS] = 'INACTIVE'
FROM [FHPDW].[dbo].[Member_Eligibility] Eli
WHERE [TERM_REASON] = 'Replaced by another LOB'


