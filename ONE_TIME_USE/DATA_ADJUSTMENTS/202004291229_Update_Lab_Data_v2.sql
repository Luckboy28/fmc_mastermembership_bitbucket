


--*****************************************************************************************************
--
--                                        Fix Member/Lab Keys
--
--*****************************************************************************************************

-- [FHPDataMarts].[dbo].[Humana_CKD_Lab]
UPDATE Lab
SET
	Lab.[MBR_ID] = Ref.[IDCARD_MBR_ID]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab] Lab
LEFT JOIN [FHPDev].[dbo].[Humana_CKD_Referrals] Ref
ON Lab.[MBR_ID] = Ref.[MBR_PERS_GEN_KEY]
WHERE Lab.[MBR_ID] <> Ref.[IDCARD_MBR_ID]



-- [FHPDataMarts].[dbo].[Member_Labs_Staging]
UPDATE Lab
SET
	 Lab.[EXT_ID] = Ref.[IDCARD_MBR_ID]
	,Lab.[ROW_PROCESSED] = 'N'
	,Lab.[ROW_PROCESSED_DATE] = NULL
	,Lab.[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Member_Labs_Staging] Lab
LEFT JOIN [FHPDev].[dbo].[Humana_CKD_Referrals] Ref
ON Lab.[EXT_ID] = Ref.[MBR_PERS_GEN_KEY]
WHERE Lab.[EXT_ID] <> Ref.[IDCARD_MBR_ID]
AND [EXT_ID_TYPE] = 'HUMANA_CKD_MBR_ID'






--*****************************************************************************************************
--
--                      Clean Member Labs Staging, and flag for re-load
--
--*****************************************************************************************************


-- Trim end if :00, replace ":" with "-"
UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET
	[LAB_LOINC_CODE] = REPLACE(CASE WHEN RIGHT([LAB_LOINC_CODE],3) = ':00' THEN LEFT([LAB_LOINC_CODE],LEN([LAB_LOINC_CODE]) - 3) ELSE [LAB_LOINC_CODE] END,':','-')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%:%'


-- Manually remove zero padding
UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-00','-0')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-00'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-01','-1')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-01'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-02','-2')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-02'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-03','-3')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-03'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-04','-4')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-04'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-05','-5')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-05'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-06','-6')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-06'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-07','-7')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-07'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-08','-8')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-08'

UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-09','-9')
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-09'





--*****************************************************************************************************
--
--                                         Clean Member Labs
--
--*****************************************************************************************************


-- Trim end if :00, replace ":" with "-"
UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE(CASE WHEN RIGHT([LAB_LOINC_CODE],3) = ':00' THEN LEFT([LAB_LOINC_CODE],LEN([LAB_LOINC_CODE]) - 3) ELSE [LAB_LOINC_CODE] END,':','-')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%:%'


-- Manually remove zero padding
UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-00','-0')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-00'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-01','-1')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-01'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-02','-2')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-02'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-03','-3')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-03'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-04','-4')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-04'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-05','-5')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-05'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-06','-6')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-06'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-07','-7')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-07'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-08','-8')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-08'

UPDATE [FHPDW].[dbo].[Member_Labs]
SET [LAB_LOINC_CODE] = REPLACE([LAB_LOINC_CODE],'-09','-9')
	,[ROW_UPDATE_DATE] = GETDATE()
WHERE [LAB_LOINC_CODE] LIKE '%-09'






--*****************************************************************************************************
--
--                                    Dedupe Member Labs
--
--*****************************************************************************************************


-- Soft Delete Member Lab Dupes
;WITH RANKS AS
(
	SELECT 
		 [MEMBER_LABS_ROW_ID]
		,[MEMBER_MASTER_ROW_ID]
		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]
		,ROW_NUMBER() OVER (PARTITION BY [MEMBER_MASTER_ROW_ID], [LAB_TYPE], [LAB_RESULT], [LAB_LOINC_CODE], [LAB_SERVICE_DATE] ORDER BY [ROW_CREATE_DATE] DESC) [RANK]
	FROM [FHPDW].[dbo].[Member_Labs]
	WHERE [ROW_DELETED] = 'N'
)
UPDATE LabTable
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_REASON] = 'Duplicate Record'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDW].[dbo].[Member_Labs] LabTable
JOIN RANKS
ON LabTable.[MEMBER_LABS_ROW_ID] = RANKS.[MEMBER_LABS_ROW_ID]
WHERE RANKS.[RANK] > 1







--*****************************************************************************************************
--
--                        Move Humana Lab members back from the backup table
--
--*****************************************************************************************************


SET IDENTITY_INSERT [FHPDataMarts].[dbo].[Humana_CKD_Lab] ON
GO

INSERT INTO [FHPDataMarts].[dbo].[Humana_CKD_Lab]
(
	 [HUMANA_CKD_LAB_ROW_ID]
	,[MBR_ID]
	,[LAB_DX_GROUP]
	,[SERVICE_DATE]
	,[LOINC_CD]
	,[LAB_RESULTS_VALUE]
	,[ROW_SOURCE]
	,[ROW_SOURCE_ID]
	,[ROW_PROBLEM]
	,[ROW_PROBLEM_DATE]
	,[ROW_PROBLEM_REASON]
	,[ROW_DELETED]
	,[ROW_DELETED_DATE]
	,[ROW_DELETED_REASON]
	,[ROW_CREATE_DATE]
	,[ROW_UPDATE_DATE]
)
SELECT DISTINCT
	 TheBackup.[HUMANA_CKD_LAB_ROW_ID]
	,TheBackup.[MBR_ID]
	,TheBackup.[LAB_DX_GROUP]
	,TheBackup.[SERVICE_DATE]
	,TheBackup.[LOINC_CD]
	,TheBackup.[LAB_RESULTS_VALUE]
	,TheBackup.[ROW_SOURCE]
	,TheBackup.[ROW_SOURCE_ID]
	,TheBackup.[ROW_PROBLEM]
	,TheBackup.[ROW_PROBLEM_DATE]
	,TheBackup.[ROW_PROBLEM_REASON]
	,TheBackup.[ROW_DELETED]
	,TheBackup.[ROW_DELETED_DATE]
	,TheBackup.[ROW_DELETED_REASON]
	,TheBackup.[ROW_CREATE_DATE]
	,TheBackup.[ROW_UPDATE_DATE]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab_BACKUP_20200424] TheBackup
LEFT JOIN [FHPDataMarts].[dbo].[Humana_CKD_Lab] TheTable
	ON TheBackup.[HUMANA_CKD_LAB_ROW_ID] = TheTable.[HUMANA_CKD_LAB_ROW_ID]
WHERE TheTable.[HUMANA_CKD_LAB_ROW_ID] IS NULL --New records only
GO


SET IDENTITY_INSERT [FHPDataMarts].[dbo].[Humana_CKD_Lab] OFF
GO