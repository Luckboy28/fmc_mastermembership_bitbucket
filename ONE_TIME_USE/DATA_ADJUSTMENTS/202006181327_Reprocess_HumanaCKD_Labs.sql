


-- Reprocess Lab Data
UPDATE MLS
SET
	 [ROW_PROBLEM] = 'N'
	,[ROW_PROBLEM_DATE] = NULL
	,[ROW_PROBLEM_REASON] = NULL
	,[ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Member_Labs_Staging] MLS
WHERE MLS.[ROW_PROBLEM] = 'Y'