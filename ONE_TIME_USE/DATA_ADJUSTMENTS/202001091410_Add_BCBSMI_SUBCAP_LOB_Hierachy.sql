

-- Add the BCBSMI LOB Hierarchy
USE [FHPDataMarts]
GO


IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] WHERE [LOB_VENDOR] = 'BCBSMI' AND [LOB_TYPE] = 'SUBCAP')
BEGIN

	INSERT [dbo].[Member_LOB_Hierarchy] ([LOB_VENDOR], [LOB_TYPE], [LOB_RANK]) VALUES ('BCBSMI', 'SUBCAP', 1)

END
GO


SELECT * FROM [dbo].[Member_LOB_Hierarchy]




IF NOT EXISTS (SELECT * FROM [FHPDataMarts].[dbo].[FHP_LOB_DETAILS] WHERE [LOB_VENDOR] = 'BCBSMI' AND [LOB_TYPE] = 'SUBCAP')
BEGIN

	INSERT INTO [FHPDataMarts].[dbo].[FHP_LOB_DETAILS]
	(
		 [LOB_TYPE]
		,[LOB_VENDOR]
		,[LOB_ID]
		,[SHORT_NAME]
		,[LONG_NAME]
		,[ECC_NAME]
		,[START_DATE]
		,[END_DATE]
		,[CREATED_DATE]
		,[UPDATED_DATE]
	)
	VALUES
	(
		 'SUBCAP' --[LOB_TYPE]
		,'BCBSMI' --[LOB_VENDOR]
		,'BCBSMI' --[LOB_ID]
		,'BCBS - MI' --[SHORT_NAME]
		,'BCBSMI Medicare Sub-Capitation' --[LONG_NAME]
		,'BCBS - MI' --[ECC_NAME]
		,CAST('2020-01-01' AS DATE) --[START_DATE]
		,NULL --[END_DATE]
		,GETDATE() --[CREATED_DATE]
		,GETDATE() --[UPDATED_DATE]
	)

END



SELECT * FROM [dbo].[FHP_LOB_DETAILS]
