


-- Update past memebrs that were terminated
UPDATE Mem
SET
	 [COV_CANCEL] = Term.[Ineligible_Date]
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] Mem
JOIN [FHPDev].[dbo].[Humana_CKD_Termination] Term
ON Mem.[MBR_ID] = Term.[Healthplan_ID]
WHERE Mem.[COV_CANCEL] IS NULL 
AND Mem.[COV_EFFECTIVE] < Term.[Ineligible_Date]


