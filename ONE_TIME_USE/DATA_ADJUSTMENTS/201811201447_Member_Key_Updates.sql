-- Fix invalid staging record
UPDATE [FHPDataMarts].[dbo].[Member_Master_Staging]
SET
	 ROW_DELETED = 'Y'
	,ROW_DELETED_DATE = GETDATE()
	,ROW_DELETED_REASON = 'Invalid HICN discovered'
WHERE MEMBER_MASTER_STAGING_ROW_ID = 269119


-- Fix historical record
UPDATE [FHPDataMarts].[dbo].[Member_Master_Historical]
SET
	 ROW_DELETED = 'Y'
	,ROW_DELETED_DATE = GETDATE()
	,ROW_DELETED_REASON = 'Invalid HICN discovered'
WHERE MEMBER_MASTER_HISTORICAL_ROW_ID = 451463


-- Fix valid staging record
UPDATE [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
SET
	 ROW_PROCESSED = 'N'
	,ROW_PROCESSED_DATE = NULL
	,ROW_DELETED = 'N'
	,ROW_DELETED_DATE = NULL
	,ROW_DELETED_REASON = NULL
WHERE MEMBER_ELIGIBILITY_STAGING_ROW_ID = 158512

