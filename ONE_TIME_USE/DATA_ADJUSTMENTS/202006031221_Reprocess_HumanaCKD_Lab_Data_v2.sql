




-- Reprocess Lab Data
UPDATE Labs
SET
	 [ROW_PROCESSED] = 'N'
	,[ROW_PROCESSED_DATE] = NULL
	,[ROW_PROBLEM] = 'N'
	,[ROW_PROBLEM_DATE] = NULL
	,[ROW_PROBLEM_REASON] = NULL
FROM [FHPDataMarts].[dbo].[Member_Labs_Staging] Labs
WHERE [ROW_PROBLEM] = 'Y'
AND [ROW_DELETED] = 'N'
