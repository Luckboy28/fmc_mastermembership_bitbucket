



-- Add new records to the Affilliated table
INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider_Affiliation]
(
	-- [MEMBERSHIP_EXPORT_PROVIDER_AFFILIATION_ROW_ID]
	 [NPI]
	,[AFFILIATION]
	,[ROW_SOURCE]
	,[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	--,[ROW_UPDATE_DATE]
)
SELECT
	-- [MEMBERSHIP_EXPORT_PROVIDER_AFFILIATION_ROW_ID]
	 MEP.[NPI] --[NPI]
	,'Humana CKD' --[AFFILIATION]
	,'MANUAL LOAD' --[ROW_SOURCE]
	,NULL --[ROW_SOURCE_ID]
	--,[ROW_PROBLEM]
	--,[ROW_PROBLEM_DATE]
	--,[ROW_PROBLEM_REASON]
	--,[ROW_DELETED]
	--,[ROW_DELETED_DATE]
	--,[ROW_DELETED_REASON]
	--,[ROW_CREATE_DATE]
	--,[ROW_UPDATE_DATE]
FROM [FHPDW].[dbo].[Membership_Export_Provider] MEP
LEFT JOIN [FHPDW].[dbo].[Membership_Export_Provider_Affiliation] MEPA
	ON MEP.[NPI] = MEPA.[NPI]
WHERE MEP.[AFFILIATION] IS NULL
AND MEPA.[NPI] IS NULL




-- Copy new NPI into Member table
UPDATE Members
SET
	 Members.[Neph_NPI_ID] = Ref.[RENDERING_NEPHROLOGIST_NPI]
	,Members.[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] Members
JOIN [FHPDev].[dbo].[Humana_CKD_Referrals] Ref
	ON Members.[MBR_ID] = Ref.[IDCARD_MBR_ID]






-- Update the providers, now that the data has been updated
EXEC [FHPDataMarts].[dbo].[Membership_Export_Provider_UPSERT]


-- Update the member/provider list, now that the data has been updated
EXEC [FHPDataMarts].[dbo].[Membership_Export_Member_Provider_UPSERT]




-- Soft-Delete NPI's that don't have a name associated, if the patient has a provider of the same type which *does* have a name
;WITH SoftDeletes AS
(
	SELECT DISTINCT
		-- MEMP1.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]
		--,MEMP1.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		--,MEMP1.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		--,MEMP1.[SPECIALITY_UTILIZED]
		--,MEP1.[FIRST_NAME]
		 MEMP2.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]
		--,MEMP2.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		--,MEMP2.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		--,MEMP2.[SPECIALITY_UTILIZED]
		--,MEP2.[FIRST_NAME]
	FROM [FHPDW].[dbo].[Membership_Export_Member_Provider] MEMP1
	JOIN [FHPDW].[dbo].[Membership_Export_Member_Provider] MEMP2
		ON MEMP1.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MEMP2.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		AND MEMP1.[SPECIALITY_UTILIZED] = MEMP2.[SPECIALITY_UTILIZED]
		AND MEMP1.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID] <> MEMP2.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]
	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP1
		ON MEMP1.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] = MEP1.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP2
		ON MEMP2.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] = MEP2.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
	WHERE MEP1.[FIRST_NAME] IS NOT NULL
	AND MEP2.[FIRST_NAME] IS NULL
)
UPDATE MEMP
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_REASON] = 'Replaced by provider with name'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_UPDATE_DATE] = GETDATE()
FROM [FHPDW].[dbo].[Membership_Export_Member_Provider] MEMP
JOIN SoftDeletes
	ON SoftDeletes.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID] = MEMP.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]