

-- Terminate CSNP members.  (CSNP no longer supported by FHP)

UPDATE Eli
SET

	 [TERM_DATE] = CAST('12-31-2018' AS DATE)
	,[TERM_REASON] = 'LOB Expired'
	,[STATUS] = 'INACTIVE'

FROM [FHPDW].[dbo].[Member_Eligibility] Eli
WHERE [LOB_TYPE] = 'CSNP'
AND ([TERM_DATE] IS NULL OR [TERM_DATE] > CAST('12-31-2018' AS DATE))


