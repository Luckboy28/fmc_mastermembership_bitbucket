
  
-- Terminate 39 Humana CKD members
UPDATE ME
	SET
		 ME.[TERM_DATE] = TERM.[Ineligible_Date]
		,ME.[ROW_UPDATE_DATE] = GETDATE()

FROM [FHPDW].[dbo].[Member_Eligibility] ME
JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
	ON ME.[MEMBER_MASTER_ROW_ID] = MMKH.[MEMBER_MASTER_ROW_ID]
JOIN [FHPDev].[dbo].[Humana_CKD_Termination] TERM
	ON TERM.[Healthplan_ID] = MMKH.[EXT_ID]
WHERE 1=1
AND ME.[ROW_DELETED] = 'N'
AND MMKH.[ROW_DELETED] = 'N'
AND ME.[START_DATE] < TERM.[Ineligible_Date]
AND ME.[TERM_DATE] IS NULL
AND ME.[LOB_TYPE] = 'CKD'
AND ME.[LOB_VENDOR] = 'HUMANA'