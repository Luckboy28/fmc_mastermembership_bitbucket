




-- Make sure the [ROW_DELETED] flags correctly propagate to the Key History table
UPDATE MMKH
SET
	 [ROW_DELETED] = 'Y'
	,[ROW_DELETED_DATE] = GETDATE()
	,[ROW_DELETED_REASON] = 'Manual Cleanup'
FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
JOIN [FHPDW].[dbo].[Member_Master] MM
ON MM.[MEMBER_MASTER_ROW_ID] = MMKH.[MEMBER_MASTER_ROW_ID]
WHERE MM.[ROW_DELETED] = 'Y'
AND MMKH.[ROW_DELETED] = 'N'