






-- Backup Member
SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Member_BACKUP_20200424]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]

-- Verify backup
SELECT COUNT(1) FROM [FHPDataMarts].[dbo].[Humana_CKD_Member_BACKUP_20200424]
SELECT COUNT(1) FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]



-- Backup Lab
SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Lab_BACKUP_20200424]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab]


-- Verify backup
SELECT COUNT(1) FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab_BACKUP_20200424]
SELECT COUNT(1) FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab]