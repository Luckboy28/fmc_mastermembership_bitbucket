
USE [FHPDev]
GO


-- Drop backup if already exists
IF OBJECT_ID('dbo.Membership_Export_Member_BACKUP_20200512', 'U') IS NOT NULL 
  DROP TABLE dbo.Membership_Export_Member_BACKUP_20200512; 

-- Create backup
SELECT * INTO [FHPDev].[dbo].[Membership_Export_Member_BACKUP_20200512]
FROM [FHPDW].[dbo].[Membership_Export_Member]



-- VERIFY COUNTS MATCH
SELECT
	COUNT(1) [COUNT]
FROM [FHPDW].[dbo].[Membership_Export_Member]


-- VERIFY COUNTS MATCH
SELECT
	COUNT(1) [COUNT]
FROM [FHPDev].[dbo].[Membership_Export_Member_BACKUP_20200512]