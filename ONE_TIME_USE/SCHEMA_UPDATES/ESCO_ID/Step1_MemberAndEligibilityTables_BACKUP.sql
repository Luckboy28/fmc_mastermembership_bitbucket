

USE [FHPDev]
GO


-------------------------------------------------------------- Eligibility -------------------------------------------------------------- 

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Eligibility_BACKUP_201901281654', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Eligibility_BACKUP_201901281654;
GO


SELECT * INTO [FHPDev].[dbo].[Member_Eligibility_BACKUP_201901281654]
FROM [FHPDW].[dbo].[Member_Eligibility]





-------------------------------------------------------------- Eligibility Historical -------------------------------------------------------------- 

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Eligibility_Historical_BACKUP_201901281654', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Eligibility_Historical_BACKUP_201901281654;
GO


SELECT * INTO [FHPDev].[dbo].[Member_Eligibility_Historical_BACKUP_201901281654]
FROM [FHPDataMarts].[dbo].[Member_Eligibility_Historical]





-------------------------------------------------------------- Eligibility Staging -------------------------------------------------------------- 

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Eligibility_Staging_BACKUP_201901281654', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Eligibility_Staging_BACKUP_201901281654;
GO


SELECT * INTO [FHPDev].[dbo].[Member_Eligibility_Staging_BACKUP_201901281654]
FROM [FHPDataMarts].[dbo].[Member_Eligibility_Staging]

GO


SELECT COUNT(1) AS [Backup] FROM [FHPDev].[dbo].[Member_Eligibility_BACKUP_201901281654]
SELECT COUNT(1) AS [Live] FROM [FHPDW].[dbo].[Member_Eligibility]

SELECT COUNT(1) AS [Backup] FROM [FHPDev].[dbo].[Member_Eligibility_Historical_BACKUP_201901281654]
SELECT COUNT(1) AS [Live] FROM [FHPDataMarts].[dbo].[Member_Eligibility_Historical]

SELECT COUNT(1) AS [Backup] FROM [FHPDev].[dbo].[Member_Eligibility_Staging_BACKUP_201901281654]
SELECT COUNT(1) AS [Live] FROM [FHPDataMarts].[dbo].[Member_Eligibility_Staging]






USE [FHPDev]
GO


-------------------------------------------------------------- Master -------------------------------------------------------------- 

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master_BACKUP_201901281654', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master_BACKUP_201901281654;
GO


SELECT * INTO [FHPDev].[dbo].[Member_Master_BACKUP_201901281654]
FROM [FHPDW].[dbo].[Member_Master]



-------------------------------------------------------------- Master Historical -------------------------------------------------------------- 

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master_Historical_BACKUP_201901281654', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master_Historical_BACKUP_201901281654;
GO


SELECT * INTO [FHPDev].[dbo].[Member_Master_Historical_BACKUP_201901281654]
FROM [FHPDataMarts].[dbo].[Member_Master_Historical]





-------------------------------------------------------------- Master Staging -------------------------------------------------------------- 

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master_Staging_BACKUP_201901281654', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master_Staging_BACKUP_201901281654;
GO


SELECT * INTO [FHPDev].[dbo].[Member_Master_Staging_BACKUP_201901281654]
FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

GO


SELECT COUNT(1) AS [Backup] FROM [FHPDev].[dbo].[Member_Master_BACKUP_201901281654]
SELECT COUNT(1) AS [Live] FROM [FHPDW].[dbo].[Member_Master]

SELECT COUNT(1) AS [Backup] FROM [FHPDev].[dbo].[Member_Master_Historical_BACKUP_201901281654]
SELECT COUNT(1) AS [Live] FROM [FHPDataMarts].[dbo].[Member_Master_Historical]

SELECT COUNT(1) AS [Backup] FROM [FHPDev].[dbo].[Member_Master_Staging_BACKUP_201901281654]
SELECT COUNT(1) AS [Live] FROM [FHPDataMarts].[dbo].[Member_Master_Staging]




--SELECT COUNT(1) AS [Dev] FROM [FHPDW].[dbo].[Member_Master]
--SELECT COUNT(1) AS [Live] FROM [VH2-SQL-01].[FHPDW].[dbo].[Member_Master]

--SELECT COUNT(1) AS [Dev] FROM [FHPDataMarts].[dbo].[Member_Master_Historical]
--SELECT COUNT(1) AS [Live] FROM [VH2-SQL-01].[FHPDataMarts].[dbo].[Member_Master_Historical]

--SELECT COUNT(1) AS [Dev] FROM [FHPDataMarts].[dbo].[Member_Master_Staging]
--SELECT COUNT(1) AS [Live] FROM [VH2-SQL-01].[FHPDataMarts].[dbo].[Member_Master_Staging]