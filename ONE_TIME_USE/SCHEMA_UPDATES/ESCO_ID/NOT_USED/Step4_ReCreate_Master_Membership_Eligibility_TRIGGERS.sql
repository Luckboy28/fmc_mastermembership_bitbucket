


USE [FHPDW]
GO


-------------- Member Eligibility Historical Triggers


-- Drop the table if it already exists
IF OBJECT_ID('trg_AfterInsert_dbo_Member_Eligibility_Historical_Insert', 'TR') IS NOT NULL
	DROP TRIGGER trg_AfterInsert_dbo_Member_Eligibility_Historical_Insert;
GO


CREATE TRIGGER trg_AfterInsert_dbo_Member_Eligibility_Historical_Insert
   ON [dbo].[Member_Eligibility]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [FHPDataMarts].[dbo].[Member_Eligibility_Historical]
	(
		 [MEMBER_ELIGIBILITY_ROW_ID]
		,[MEMBER_MASTER_ROW_ID]
		,[ESCO_ID]  -- ADDED 1/30/2019
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[TERM_REASON]
		,[STATUS]

		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_ELIGIBILITY_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		 [MEMBER_ELIGIBILITY_ROW_ID]
		,[MEMBER_MASTER_ROW_ID]
		,[ESCO_ID]  -- ADDED 1/30/2019
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[TERM_REASON]
		,[STATUS]

		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_ELIGIBILITY_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	FROM Inserted

END
GO


USE [FHPDW]
GO

-- Drop the table if it already exists
IF OBJECT_ID('trg_AfterUpdate_dbo_Member_Eligibility_Historical_Insert', 'TR') IS NOT NULL
	DROP TRIGGER trg_AfterUpdate_dbo_Member_Eligibility_Historical_Insert;
GO


CREATE TRIGGER trg_AfterUpdate_dbo_Member_Eligibility_Historical_Insert
   ON [dbo].[Member_Eligibility]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [FHPDataMarts].[dbo].[Member_Eligibility_Historical]
	(
		 [MEMBER_ELIGIBILITY_ROW_ID]
		,[MEMBER_MASTER_ROW_ID]
		,[ESCO_ID]  -- ADDED 1/30/2019
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[TERM_REASON]
		,[STATUS]

		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_ELIGIBILITY_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		 [MEMBER_ELIGIBILITY_ROW_ID]
		,[MEMBER_MASTER_ROW_ID]
		,[ESCO_ID]  -- ADDED 1/30/2019
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[TERM_REASON]
		,[STATUS]

		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_ELIGIBILITY_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	FROM Inserted

END
GO



