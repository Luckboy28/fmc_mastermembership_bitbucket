


USE [FHPDW]
GO

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master;
GO


-- Comments
CREATE TABLE [dbo].[Member_Master]
(
	 [MEMBER_MASTER_ROW_ID] INT NOT NULL IDENTITY(1,1)
	,[MHK_INTERNAL_ID] INT NULL
	,[MEDHOK_ID] [VARCHAR](50) NULL
	,[SSN] [VARCHAR](11) NULL
	,[HICN] [VARCHAR](12) NULL
	,[CLAIM_SUBSCRIBER_ID] [VARCHAR](50) NULL
	,[MBI] [VARCHAR](11) NULL
	,[MEDICAID_NO] [VARCHAR](50) NULL
	,[MRN] [VARCHAR](50) NULL
	,[EXT_ID] [VARCHAR](50) NULL
	,[EXT_ID_TYPE] [VARCHAR](50) NULL
	,[EXT_ID_2] [VARCHAR](50) NULL
	,[EXT_ID_TYPE_2] [VARCHAR](50) NULL
	,[EXT_ID_3] [VARCHAR](50) NULL
	,[EXT_ID_TYPE_3] [VARCHAR](50) NULL
	,[BENEFIT_STATUS] [VARCHAR](1) NULL
	,[FIRST_NAME] [VARCHAR](50) NULL
	,[MIDDLE_NAME] [VARCHAR](50) NULL
	,[LAST_NAME] [VARCHAR](50) NULL
	,[PREFIX] [VARCHAR](50) NULL
	,[SUFFIX] [VARCHAR](50) NULL
	,[DATE_OF_BIRTH] DATE NULL
	,[GENDER] [VARCHAR](1) NULL
	,[COMPANY_DESCRIPTION] [VARCHAR](50) NULL
	,[LOB_CODE] [VARCHAR](50) NULL
	,[FAMILY_ID] [VARCHAR](50) NULL
	,[PERSON_NUMBER] [VARCHAR](50) NULL
	,[RACE] [VARCHAR](50) NULL
	,[ETHNICITY] [VARCHAR](50) NULL
	,[PRIMARY_LANGUAGE] [VARCHAR](50) NULL
	,[PRIMARY_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[SPOKEN_LANGUAGE] [VARCHAR](50) NULL
	,[SPOKEN_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[WRITTEN_LANGUAGE] [VARCHAR](50) NULL
	,[WRITTEN_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[OTHER_LANGUAGE] [VARCHAR](50) NULL
	,[OTHER_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[EMPLOYEE] [VARCHAR](1) NULL
	,[PBP_NUMBER] [VARCHAR](15) NULL
	,[CURRENT_LIS] [VARCHAR](50) NULL
	,[IPA_GROUP_EXT_ID] [VARCHAR](50) NULL
	,[MEDICARE_PLAN_CODE] [VARCHAR](50) NULL
	,[MEDICARE_TYPE] [VARCHAR](50) NULL
	,[DUPLICATE_MEDICAID_ID] [VARCHAR](50) NULL
	,[PREGNANCY_DUE_DATE] DATE NULL
	,[PREGNANCY_INDICATOR] [VARCHAR](1) NULL
	,[BOARD_NUMBER] [VARCHAR](50) NULL
	,[DEPENDENT_CODE] [VARCHAR](50) NULL
	,[LEGACY_SUBSCRIBER_ID] [VARCHAR](50) NULL
	,[GROUP_NUMBER] [VARCHAR](50) NULL
	,[SOURCE] [VARCHAR](50) NULL
	,[ESCO_ID] [VARCHAR](50) NULL
	,[CLIENT_SPECIFIC_DATA] [VARCHAR](50) NULL
	,[RELATIONSHIP_CODE] [VARCHAR](10) NULL
	,[TIME_ZONE] [VARCHAR](10) NULL
	,[DATE_OF_DEATH] DATE NULL
	,[FOSTER_CARE_FLAG] [VARCHAR](1) NULL
	,[VIP] [VARCHAR](1) NULL
	,[CLINIC_NUMBER] [VARCHAR](50) NULL
	,[MODALITY] [VARCHAR](50) NULL
	,[PAYER_NAME] [VARCHAR](50) NULL
	,[PAYER_ID_TYPE] [VARCHAR](50) NULL
	,[PAYER_ID] [VARCHAR](50) NULL
	,[DIALYSIS_START_DATE] DATE NULL
	,[KIDNEY_TRANSPLANT_DATE] DATE NULL

	,[STATUS] [VARCHAR](50) NOT NULL
	,[LOB_VENDOR] [VARCHAR](50) NOT NULL
	,[LOB_TYPE] [VARCHAR](50) NOT NULL

	,[MEMBER_MASTER_STAGING_ROW_ID] INT NULL
	,[ROW_SOURCE] [VARCHAR](500) NOT NULL
	,[ROW_SOURCE_ID] [VARCHAR](50) NULL
	,[ROW_PROBLEM] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_PROBLEM_DATE] [DATETIME2] NULL
	,[ROW_PROBLEM_REASON] [VARCHAR](500) NULL
	,[ROW_DELETED] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_DELETED_DATE] [DATETIME2] NULL
	,[ROW_DELETED_REASON] [VARCHAR](500) NULL
	,[ROW_CREATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,[ROW_UPDATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,CONSTRAINT PK_DBO_MEMBER_MASTER_ROW_ID PRIMARY KEY ([MEMBER_MASTER_ROW_ID])
	,CONSTRAINT CHK_MEMBER_MASTER_STATUS CHECK ([STATUS] IN ('ACTIVE','INACTIVE'))
	,CONSTRAINT CHK_MEMBER_MASTER_ROW_PROBLEM CHECK ([ROW_PROBLEM] IN ('Y','N'))
	,CONSTRAINT CHK_MEMBER_MASTER_ROW_DELETED CHECK ([ROW_DELETED] IN ('Y','N'))
	--,CONSTRAINT CHK_MEMBER_MASTER_MEDHOK_ID UNIQUE ([MEDHOK_ID])
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_MHK_INTERNAL_ID_UNIQUE ON [dbo].[Member_Master](MHK_INTERNAL_ID) WHERE MHK_INTERNAL_ID IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_MEDHOK_ID_UNIQUE ON [dbo].[Member_Master](MEDHOK_ID) WHERE MEDHOK_ID IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_SSN_UNIQUE ON [dbo].[Member_Master](SSN) WHERE SSN IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HICN_UNIQUE ON [dbo].[Member_Master](HICN) WHERE HICN IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_CLAIM_SUBSCRIBER_ID_UNIQUE ON [dbo].[Member_Master](CLAIM_SUBSCRIBER_ID) WHERE CLAIM_SUBSCRIBER_ID IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_MBI_UNIQUE ON [dbo].[Member_Master](MBI) WHERE MBI IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_MEDICAID_NO_UNIQUE ON [dbo].[Member_Master](MEDICAID_NO) WHERE MEDICAID_NO IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_MRN_UNIQUE ON [dbo].[Member_Master](MRN) WHERE MRN IS NOT NULL;  --Removed by request, due to the belief that it's not unique.  --Update:  Re-added by request.  -_-
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_EXT_ID1_UNIQUE ON [dbo].[Member_Master](EXT_ID,EXT_ID_TYPE) WHERE EXT_ID IS NOT NULL AND EXT_ID_TYPE IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_MEMBER_MASTER_STAGING_ROW_ID_UNIQUE ON [dbo].[Member_Master](MEMBER_MASTER_STAGING_ROW_ID) WHERE MEMBER_MASTER_STAGING_ROW_ID IS NOT NULL;






USE [FHPDataMarts]
GO

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master_Historical', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master_Historical;
GO


-- Master member historical
CREATE TABLE [dbo].[Member_Master_Historical]
(
	 [MEMBER_MASTER_HISTORICAL_ROW_ID] INT NOT NULL IDENTITY(1,1)
	,[MEMBER_MASTER_ROW_ID] INT NOT NULL
	,[MHK_INTERNAL_ID] INT NULL
	,[MEDHOK_ID] [VARCHAR](50) NULL
	,[SSN] [VARCHAR](11) NULL
	,[HICN] [VARCHAR](12) NULL
	,[CLAIM_SUBSCRIBER_ID] [VARCHAR](50) NULL
	,[MBI] [VARCHAR](11) NULL
	,[MEDICAID_NO] [VARCHAR](50) NULL
	,[MRN] [VARCHAR](50) NULL
	,[EXT_ID] [VARCHAR](50) NULL
	,[EXT_ID_TYPE] [VARCHAR](50) NULL
	,[EXT_ID_2] [VARCHAR](50) NULL
	,[EXT_ID_TYPE_2] [VARCHAR](50) NULL
	,[EXT_ID_3] [VARCHAR](50) NULL
	,[EXT_ID_TYPE_3] [VARCHAR](50) NULL
	,[BENEFIT_STATUS] [VARCHAR](1) NULL
	,[FIRST_NAME] [VARCHAR](50) NULL
	,[MIDDLE_NAME] [VARCHAR](50) NULL
	,[LAST_NAME] [VARCHAR](50) NULL
	,[PREFIX] [VARCHAR](50) NULL
	,[SUFFIX] [VARCHAR](50) NULL
	,[DATE_OF_BIRTH] DATE NULL
	,[GENDER] [VARCHAR](1) NULL
	,[COMPANY_DESCRIPTION] [VARCHAR](50) NULL
	,[LOB_CODE] [VARCHAR](50) NULL
	,[FAMILY_ID] [VARCHAR](50) NULL
	,[PERSON_NUMBER] [VARCHAR](50) NULL
	,[RACE] [VARCHAR](50) NULL
	,[ETHNICITY] [VARCHAR](50) NULL
	,[PRIMARY_LANGUAGE] [VARCHAR](50) NULL
	,[PRIMARY_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[SPOKEN_LANGUAGE] [VARCHAR](50) NULL
	,[SPOKEN_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[WRITTEN_LANGUAGE] [VARCHAR](50) NULL
	,[WRITTEN_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[OTHER_LANGUAGE] [VARCHAR](50) NULL
	,[OTHER_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[EMPLOYEE] [VARCHAR](1) NULL
	,[PBP_NUMBER] [VARCHAR](15) NULL
	,[CURRENT_LIS] [VARCHAR](50) NULL
	,[IPA_GROUP_EXT_ID] [VARCHAR](50) NULL
	,[MEDICARE_PLAN_CODE] [VARCHAR](50) NULL
	,[MEDICARE_TYPE] [VARCHAR](50) NULL
	,[DUPLICATE_MEDICAID_ID] [VARCHAR](50) NULL
	,[PREGNANCY_DUE_DATE] DATE NULL
	,[PREGNANCY_INDICATOR] [VARCHAR](1) NULL
	,[BOARD_NUMBER] [VARCHAR](50) NULL
	,[DEPENDENT_CODE] [VARCHAR](50) NULL
	,[LEGACY_SUBSCRIBER_ID] [VARCHAR](50) NULL
	,[GROUP_NUMBER] [VARCHAR](50) NULL
	,[SOURCE] [VARCHAR](50) NULL
	,[ESCO_ID] [VARCHAR](50) NULL
	,[CLIENT_SPECIFIC_DATA] [VARCHAR](50) NULL
	,[RELATIONSHIP_CODE] [VARCHAR](10) NULL
	,[TIME_ZONE] [VARCHAR](10) NULL
	,[DATE_OF_DEATH] DATE NULL
	,[FOSTER_CARE_FLAG] [VARCHAR](1) NULL
	,[VIP] [VARCHAR](1) NULL
	,[CLINIC_NUMBER] [VARCHAR](50) NULL
	,[MODALITY] [VARCHAR](50) NULL
	,[PAYER_NAME] [VARCHAR](50) NULL
	,[PAYER_ID_TYPE] [VARCHAR](50) NULL
	,[PAYER_ID] [VARCHAR](50) NULL
	,[DIALYSIS_START_DATE] DATE NULL
	,[KIDNEY_TRANSPLANT_DATE] DATE NULL

	,[STATUS] [VARCHAR](50) NOT NULL
	,[LOB_VENDOR] [VARCHAR](50) NOT NULL
	,[LOB_TYPE] [VARCHAR](50) NOT NULL

	,[MEMBER_MASTER_STAGING_ROW_ID] INT NULL
	,[ROW_SOURCE] [VARCHAR](500) NOT NULL
	,[ROW_SOURCE_ID] [VARCHAR](50) NULL

	,[ROW_PROBLEM] [VARCHAR](1) NOT NULL
	,[ROW_PROBLEM_DATE] [DATETIME2] NULL
	,[ROW_PROBLEM_REASON] [VARCHAR](500) NULL
	,[ROW_DELETED] [VARCHAR](1) NOT NULL
	,[ROW_DELETED_DATE] [DATETIME2] NULL
	,[ROW_DELETED_REASON] [VARCHAR](500) NULL
	,[ROW_CREATE_DATE] [DATETIME2] NOT NULL
	,[ROW_UPDATE_DATE] [DATETIME2] NOT NULL
	,[HISTORICAL_ROW_CREATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,CONSTRAINT PK_DBO_MEMBER_MASTER_HISTORICAL_ROW_ID PRIMARY KEY ([MEMBER_MASTER_HISTORICAL_ROW_ID])
	--,CONSTRAINT CHK_MEMBER_MASTER_MEDHOK_ID UNIQUE ([MEDHOK_ID])
) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MEMBER_MASTER_ROW_ID_UNIQUE ON [dbo].[Member_Master_Historical](MEMBER_MASTER_ROW_ID) WHERE MEMBER_MASTER_ROW_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MHK_INTERNAL_ID_UNIQUE ON [dbo].[Member_Master_Historical](MHK_INTERNAL_ID) WHERE MHK_INTERNAL_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MEDHOK_ID_UNIQUE ON [dbo].[Member_Master_Historical](MEDHOK_ID) WHERE MEDHOK_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_SSN_UNIQUE ON [dbo].[Member_Master_Historical](SSN) WHERE SSN IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_HICN_UNIQUE ON [dbo].[Member_Master_Historical](HICN) WHERE HICN IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_CLAIM_SUBSCRIBER_ID_UNIQUE ON [dbo].[Member_Master_Historical](CLAIM_SUBSCRIBER_ID) WHERE CLAIM_SUBSCRIBER_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MBI_UNIQUE ON [dbo].[Member_Master_Historical](MBI) WHERE MBI IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MEDICAID_NO_UNIQUE ON [dbo].[Member_Master_Historical](MEDICAID_NO) WHERE MEDICAID_NO IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MRN_UNIQUE ON [dbo].[Member_Master_Historical](MRN) WHERE MRN IS NOT NULL;  --Removed by request, due to the belief that it's not unique.  --Update:  Re-added by request.  -_-
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_EXT_ID1_UNIQUE ON [dbo].[Member_Master_Historical](EXT_ID,EXT_ID_TYPE) WHERE EXT_ID IS NOT NULL AND EXT_ID_TYPE IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_HISTORICAL_MEMBER_MASTER_STAGING_ROW_ID_UNIQUE ON [dbo].[Member_Master_Historical](MEMBER_MASTER_STAGING_ROW_ID) WHERE MEMBER_MASTER_STAGING_ROW_ID IS NOT NULL;






USE [FHPDataMarts]
GO

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master_Staging', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master_Staging;
GO


-- Comments
CREATE TABLE [dbo].[Member_Master_Staging]
(
	 [MEMBER_MASTER_STAGING_ROW_ID] INT NOT NULL IDENTITY(1,1)
	,[MEMBER_MASTER_ROW_ID] INT NULL
	,[MHK_INTERNAL_ID] INT NULL
	,[MEDHOK_ID] [VARCHAR](50) NULL
	,[SSN] [VARCHAR](11) NULL
	,[HICN] [VARCHAR](12) NULL
	,[CLAIM_SUBSCRIBER_ID] [VARCHAR](50) NULL
	,[MBI] [VARCHAR](11) NULL
	,[MEDICAID_NO] [VARCHAR](50) NULL
	,[MRN] [VARCHAR](50) NULL
	,[EXT_ID] [VARCHAR](50) NULL
	,[EXT_ID_TYPE] [VARCHAR](50) NULL
	,[EXT_ID_2] [VARCHAR](50) NULL
	,[EXT_ID_TYPE_2] [VARCHAR](50) NULL
	,[EXT_ID_3] [VARCHAR](50) NULL
	,[EXT_ID_TYPE_3] [VARCHAR](50) NULL
	,[BENEFIT_STATUS] [VARCHAR](1) NULL
	,[FIRST_NAME] [VARCHAR](50) NULL
	,[MIDDLE_NAME] [VARCHAR](50) NULL
	,[LAST_NAME] [VARCHAR](50) NULL
	,[PREFIX] [VARCHAR](50) NULL
	,[SUFFIX] [VARCHAR](50) NULL
	,[DATE_OF_BIRTH] DATE NULL
	,[GENDER] [VARCHAR](1) NULL
	,[COMPANY_DESCRIPTION] [VARCHAR](50) NULL
	,[LOB_CODE] [VARCHAR](50) NULL
	,[FAMILY_ID] [VARCHAR](50) NULL
	,[PERSON_NUMBER] [VARCHAR](50) NULL
	,[RACE] [VARCHAR](50) NULL
	,[ETHNICITY] [VARCHAR](50) NULL
	,[PRIMARY_LANGUAGE] [VARCHAR](50) NULL
	,[PRIMARY_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[SPOKEN_LANGUAGE] [VARCHAR](50) NULL
	,[SPOKEN_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[WRITTEN_LANGUAGE] [VARCHAR](50) NULL
	,[WRITTEN_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[OTHER_LANGUAGE] [VARCHAR](50) NULL
	,[OTHER_LANGUAGE_SOURCE] [VARCHAR](50) NULL
	,[EMPLOYEE] [VARCHAR](1) NULL
	,[PBP_NUMBER] [VARCHAR](15) NULL
	,[CURRENT_LIS] [VARCHAR](50) NULL
	,[IPA_GROUP_EXT_ID] [VARCHAR](50) NULL
	,[MEDICARE_PLAN_CODE] [VARCHAR](50) NULL
	,[MEDICARE_TYPE] [VARCHAR](50) NULL
	,[DUPLICATE_MEDICAID_ID] [VARCHAR](50) NULL
	,[PREGNANCY_DUE_DATE] DATE NULL
	,[PREGNANCY_INDICATOR] [VARCHAR](1) NULL
	,[BOARD_NUMBER] [VARCHAR](50) NULL
	,[DEPENDENT_CODE] [VARCHAR](50) NULL
	,[LEGACY_SUBSCRIBER_ID] [VARCHAR](50) NULL
	,[GROUP_NUMBER] [VARCHAR](50) NULL
	,[SOURCE] [VARCHAR](50) NULL
	,[ESCO_ID] [VARCHAR](50) NULL
	,[CLIENT_SPECIFIC_DATA] [VARCHAR](50) NULL
	,[RELATIONSHIP_CODE] [VARCHAR](10) NULL
	,[TIME_ZONE] [VARCHAR](10) NULL
	,[DATE_OF_DEATH] DATE NULL
	,[FOSTER_CARE_FLAG] [VARCHAR](1) NULL
	,[VIP] [VARCHAR](1) NULL
	,[CLINIC_NUMBER] [VARCHAR](50) NULL
	,[MODALITY] [VARCHAR](50) NULL
	,[PAYER_NAME] [VARCHAR](50) NULL
	,[PAYER_ID_TYPE] [VARCHAR](50) NULL
	,[PAYER_ID] [VARCHAR](50) NULL
	,[DIALYSIS_START_DATE] DATE NULL
	,[KIDNEY_TRANSPLANT_DATE] DATE NULL
	,[LOB_VENDOR] [VARCHAR](50) NOT NULL
	,[LOB_TYPE] [VARCHAR](50) NOT NULL
	,[ROW_SOURCE] [VARCHAR](500) NOT NULL
	,[ROW_SOURCE_ID] [VARCHAR](50) NULL
	--,[LOAD_DATE] [DATETIME2] NULL
	,[ROW_PROCESSED] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_PROCESSED_DATE] [DATETIME2] NULL
	,[ROW_PROBLEM] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_PROBLEM_DATE] [DATETIME2] NULL
	,[ROW_PROBLEM_REASON] [VARCHAR](500) NULL
	,[ROW_DELETED] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_DELETED_DATE] [DATETIME2] NULL
	,[ROW_DELETED_REASON] [VARCHAR](500) NULL
	,[ROW_CREATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,[ROW_UPDATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,CONSTRAINT PK_MEMBER_MASTER_STAGING_ROW_ID PRIMARY KEY ([MEMBER_MASTER_STAGING_ROW_ID])
	,CONSTRAINT CHK_MEMBER_MASTER_STAGING_ROW_PROBLEM CHECK ([ROW_PROBLEM] IN ('Y','N'))
	,CONSTRAINT CHK_MEMBER_MASTER_STAGING_ROW_PROCESSED CHECK ([ROW_PROCESSED] IN ('Y','N'))
	,CONSTRAINT CHK_MEMBER_MASTER_STAGING_ROW_DELETED CHECK ([ROW_DELETED] IN ('Y','N'))
) ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_MHK_INTERNAL_ID_UNIQUE ON [dbo].[Member_Master_Staging](MHK_INTERNAL_ID) WHERE MHK_INTERNAL_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_MEDHOK_ID_UNIQUE ON [dbo].[Member_Master_Staging](MEDHOK_ID) WHERE MEDHOK_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_SSN_UNIQUE ON [dbo].[Member_Master_Staging](SSN) WHERE SSN IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_HICN_UNIQUE ON [dbo].[Member_Master_Staging](HICN) WHERE HICN IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_CLAIM_SUBSCRIBER_ID_UNIQUE ON [dbo].[Member_Master_Staging](CLAIM_SUBSCRIBER_ID) WHERE CLAIM_SUBSCRIBER_ID IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_MBI_UNIQUE ON [dbo].[Member_Master_Staging](MBI) WHERE MBI IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_MEDICAID_NO_UNIQUE ON [dbo].[Member_Master_Staging](MEDICAID_NO) WHERE MEDICAID_NO IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_MRN_UNIQUE ON [dbo].[Member_Master_Staging](MRN) WHERE MRN IS NOT NULL;  --Removed by request, due to the belief that it's not unique.  --Update:  Re-added by request.  -_-
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_EXT_ID1_UNIQUE ON [dbo].[Member_Master_Staging](EXT_ID,EXT_ID_TYPE) WHERE EXT_ID IS NOT NULL AND EXT_ID_TYPE IS NOT NULL;
CREATE NONCLUSTERED INDEX IDX_DBO_MEMBER_MASTER_STAGING_MEMBER_MASTER_STAGING_ROW_ID_UNIQUE ON [dbo].[Member_Master_Staging](MEMBER_MASTER_STAGING_ROW_ID) WHERE MEMBER_MASTER_STAGING_ROW_ID IS NOT NULL;


