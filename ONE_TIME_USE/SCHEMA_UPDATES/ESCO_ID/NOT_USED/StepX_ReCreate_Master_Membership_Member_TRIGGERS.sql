

USE [FHPDW]
GO

-- Drop the table if it already exists
IF OBJECT_ID('trg_AfterInsert_dbo_Member_Master_KeyHistory_Insert', 'TR') IS NOT NULL
	DROP TRIGGER trg_AfterInsert_dbo_Member_Master_KeyHistory_Insert;
GO


CREATE TRIGGER trg_AfterInsert_dbo_Member_Master_KeyHistory_Insert
   ON [dbo].[Member_Master]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	(
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	)
	SELECT
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM Inserted

END
GO


USE [FHPDW]
GO

-- Drop the table if it already exists
IF OBJECT_ID('trg_AfterUpdate_dbo_Member_Master_KeyHistory_Insert', 'TR') IS NOT NULL
	DROP TRIGGER trg_AfterUpdate_dbo_Member_Master_KeyHistory_Insert;
GO


CREATE TRIGGER trg_AfterUpdate_dbo_Member_Master_KeyHistory_Insert
   ON [dbo].[Member_Master]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



	WITH UniqueKeyCombo AS
	(

		-- Select the unique keys
		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
		FROM Inserted

		EXCEPT

		-- Unless it already exists
		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	)
	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	(
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	)
	SELECT
		 UniqueKeyCombo.[MEMBER_MASTER_ROW_ID]
		,UniqueKeyCombo.[MHK_INTERNAL_ID]
		,UniqueKeyCombo.[MEDHOK_ID]
		,UniqueKeyCombo.[SSN]
		,UniqueKeyCombo.[HICN]
		,UniqueKeyCombo.[CLAIM_SUBSCRIBER_ID]
		,UniqueKeyCombo.[MBI]
		,UniqueKeyCombo.[MEDICAID_NO]
		,UniqueKeyCombo.[MRN]
		,UniqueKeyCombo.[EXT_ID]
		,UniqueKeyCombo.[EXT_ID_TYPE]
		,UniqueKeyCombo.[EXT_ID_2]
		,UniqueKeyCombo.[EXT_ID_TYPE_2]
		,UniqueKeyCombo.[EXT_ID_3]
		,UniqueKeyCombo.[EXT_ID_TYPE_3]

		,Inserted.[ROW_SOURCE]  --[ROW_SOURCE]
		,Inserted.[ROW_SOURCE_ID]  --[ROW_SOURCE_ID]
	FROM UniqueKeyCombo  --This is suppose to be empty, by design, if the inserted keys were not unique (and therefore add no value to this table)
	JOIN Inserted
	ON Inserted.[MEMBER_MASTER_ROW_ID] = UniqueKeyCombo.[MEMBER_MASTER_ROW_ID]  -- This is really just joining the Inserted record back on itself, if it was unique

END
GO




-------------- Member Master Historical Triggers


USE [FHPDW]
GO


-- Drop the table if it already exists
IF OBJECT_ID('trg_AfterInsert_dbo_Member_Master_Historical_Insert', 'TR') IS NOT NULL
	DROP TRIGGER trg_AfterInsert_dbo_Member_Master_Historical_Insert;
GO


CREATE TRIGGER trg_AfterInsert_dbo_Member_Master_Historical_Insert
   ON [dbo].[Member_Master]
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Historical]
	(
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[ESCO_ID]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]

		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_MASTER_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[ESCO_ID]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]

		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_MASTER_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	FROM Inserted

END
GO


USE [FHPDW]
GO

-- Drop the table if it already exists
IF OBJECT_ID('trg_AfterUpdate_dbo_Member_Master_Historical_Insert', 'TR') IS NOT NULL
	DROP TRIGGER trg_AfterUpdate_dbo_Member_Master_Historical_Insert;
GO


CREATE TRIGGER trg_AfterUpdate_dbo_Member_Master_Historical_Insert
   ON [dbo].[Member_Master]
   AFTER UPDATE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Historical]
	(
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[ESCO_ID]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]

		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_MASTER_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[ESCO_ID]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]

		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_MASTER_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	FROM Inserted

END
GO