
USE [FHPDataMarts]
GO

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Humana_CKD_Member', 'U') IS NOT NULL
	DROP TABLE dbo.Humana_CKD_Member;
GO


-- Comments
CREATE TABLE [dbo].[Humana_CKD_Member]
(
	 [HUMANA_CKD_MEMBER_ROW_ID] INT NOT NULL IDENTITY(1,1)

	,[MBR_ID] [VARCHAR](20) NOT NULL
	,[MBI] [VARCHAR](11) NULL --ADDED IN v4 [3/12/2020]
	,[SCORE] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[Val_eGFR] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[Days_eGFR] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[current_HCCP_managed] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[cat] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[COV_EFFECTIVE] [DATE] NULL  --UPDATED DATA TYPE IN v3 [1/3/2020]
	,[COV_CANCEL] [DATE] NULL  --UPDATED DATA TYPE IN v3 [1/3/2020]
	,[LAST_NAME] [VARCHAR](50) NULL
	,[FIRST_NAME] [VARCHAR](50) NULL
	,[DATE_OF_BIRTH] [DATE] NULL  --UPDATED DATA TYPE IN v3 [1/3/2020]
	,[Member_Race] [VARCHAR](50) NULL
	,[SEX_CD] [VARCHAR](10) NULL
	,[ADDRESS_LINE1] [VARCHAR](100) NULL
	,[ADDRESS_LINE2] [VARCHAR](100) NULL
	,[CITY] [VARCHAR](50) NULL
	,[STATE_CD] [VARCHAR](50) NULL
	,[ZIP_CD] [VARCHAR](10) NULL
	,[HOME_PHONE_NBR] [VARCHAR](50) NULL
	,[COUNTY_CD] [VARCHAR](10) NULL
	,[PCP_FIRST_NAME] [VARCHAR](50) NULL
	,[PCP_LAST_NAME] [VARCHAR](50) NULL
	,[PCP_ADDR_LINE1] [VARCHAR](100) NULL
	,[PCP_ADDR_LINE2] [VARCHAR](100) NULL
	,[PCP_CITY_NAME] [VARCHAR](50) NULL
	,[PCP_STATE_CD] [VARCHAR](50) NULL
	,[PCP_ZIP_CD] [VARCHAR](10) NULL
	,[PCP_PHONE_NBR] [VARCHAR](50) NULL
	,[PCP_NPI_ID] [VARCHAR](50) NULL
	,[PCP_GROUPER_NAME] [VARCHAR](100) NULL
	,[PCP_ORG_NAME] [VARCHAR](100) NULL
	,[PROCESS_DATE] [DATE] NULL  --UPDATED DATA TYPE IN v3 [1/3/2020]
	,[MAJOR_LOB_CD] [VARCHAR](50) NULL
	--,[STRATF_SCORE_AMT] [VARCHAR](50) NULL  --REMOVED IN v2 [10/7/2019]
	--,[SCORE_CAT] [VARCHAR](50) NULL  --REMOVED IN v2 [10/7/2019]
	,[Age] [VARCHAR](5) NULL
	,[DIABETES] [VARCHAR](5) NULL
	,[Diab_Type] [VARCHAR](50) NULL
	,[HYPERTENSION] [VARCHAR](5) NULL
	,[Cardiovascular] [VARCHAR](5) NULL
	,[PVD] [VARCHAR](5) NULL
	,[CHF] [VARCHAR](5) NULL
	,[CHF_STAGE] [VARCHAR](50) NULL
	,[Metabolic_syndrome] [VARCHAR](5) NULL
	,[PROTEINURIA] [VARCHAR](5) NULL
	--,[SUBSTANCE_ABUSE] [VARCHAR](5) NULL  --REMOVED IN v2 [10/7/2019]
	--,[TOBACCO_DEPENDENCE] [VARCHAR](5) NULL  --REMOVED IN v2 [10/7/2019]
	--,[DEPRESSION] [VARCHAR](5) NULL  --REMOVED IN v2 [10/7/2019]
	--,[ANXIETY] [VARCHAR](5) NULL  --REMOVED IN v2 [10/7/2019]
	--,[BIPOLAR_DISORDER] [VARCHAR](5) NULL  --REMOVED IN v2 [10/7/2019]
	--,[SCHIZOPHRENIA] [VARCHAR](5) NULL  --REMOVED IN v2 [10/7/2019]
	,[CKD_STAGE] [VARCHAR](50) NULL
	,[Neph_TAX_ID] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[Neph_NPI_ID] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[Nephrologist_Name] [VARCHAR](50) NULL  --ADDED IN v2 [10/7/2019]
	,[ROW_SOURCE] [VARCHAR](500) NOT NULL
	,[ROW_SOURCE_ID] [VARCHAR](50) NULL
	,[ROW_PROBLEM] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_PROBLEM_DATE] [DATETIME2] NULL
	,[ROW_PROBLEM_REASON] [VARCHAR](500) NULL
	,[ROW_DELETED] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_DELETED_DATE] [DATETIME2] NULL
	,[ROW_DELETED_REASON] [VARCHAR](500) NULL
	,[ROW_CREATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,[ROW_UPDATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,CONSTRAINT PK_DBO_HUMANA_CKD_MEMBER_ROW_ID PRIMARY KEY ([HUMANA_CKD_MEMBER_ROW_ID])
	,CONSTRAINT CHK_DBO_HUMANA_CKD_MEMBER_MBR_ID UNIQUE ([MBR_ID])
	,CONSTRAINT CHK_HUMANA_CKD_MEMBER_ROW_PROBLEM CHECK ([ROW_PROBLEM] IN ('Y','N'))
	,CONSTRAINT CHK_HUMANA_CKD_MEMBER_ROW_DELETED CHECK ([ROW_DELETED] IN ('Y','N'))
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX IDX_DBO_HUMANA_CKD_MEMBER_MBI_UNIQUE ON [dbo].[Humana_CKD_Member](MBI) WHERE MBI IS NOT NULL AND ROW_DELETED = 'N';
GO



USE [FHPDataMarts]
GO

GRANT SELECT ON [dbo].[Humana_CKD_Member] TO [FHP_DATAMARTS_READ_SPECIFIC];
GO
GRANT VIEW DEFINITION ON [dbo].[Humana_CKD_Member] TO [FHP_DATAMARTS_READ_SPECIFIC];
GO

GRANT SELECT ON [dbo].[Humana_CKD_Member] TO [FHP_DATAMARTS_READ_SPECIFIC_DATA_TEAM];
GO
GRANT VIEW DEFINITION ON [dbo].[Humana_CKD_Member] TO [FHP_DATAMARTS_READ_SPECIFIC_DATA_TEAM];
GO


