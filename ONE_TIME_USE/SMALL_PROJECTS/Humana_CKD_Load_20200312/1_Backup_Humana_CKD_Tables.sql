

USE [FHPDataMarts]
GO


-- Backup Tables
SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Member_BACKUP_20200312]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]

SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Lab_BACKUP_20200312]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab]