

-- Backup Tables
SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Member_BACKUP_20200102]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]

SELECT * INTO [FHPDataMarts].[dbo].[Humana_CKD_Lab_BACKUP_20200102]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Lab]



-- Remove data prior to reformatting
UPDATE [FHPDataMarts].[dbo].[Humana_CKD_Member]
SET [COV_EFFECTIVE] = NULL;

UPDATE [FHPDataMarts].[dbo].[Humana_CKD_Member]
SET [COV_CANCEL] = NULL;

UPDATE [FHPDataMarts].[dbo].[Humana_CKD_Member]
SET [DATE_OF_BIRTH] = NULL;

UPDATE [FHPDataMarts].[dbo].[Humana_CKD_Member]
SET [PROCESS_DATE] = NULL;



-- Reformate date tables
ALTER TABLE [FHPDataMarts].[dbo].[Humana_CKD_Member]
ALTER COLUMN [COV_EFFECTIVE] DATE;

ALTER TABLE [FHPDataMarts].[dbo].[Humana_CKD_Member]
ALTER COLUMN [COV_CANCEL] DATE;

ALTER TABLE [FHPDataMarts].[dbo].[Humana_CKD_Member]
ALTER COLUMN [DATE_OF_BIRTH] DATE;

ALTER TABLE [FHPDataMarts].[dbo].[Humana_CKD_Member]
ALTER COLUMN [PROCESS_DATE] DATE;



-- Copy data back in correct format
-- NOTE:  THIS IS OFFSET BY -2 DAYS, TO MATCH EXISTING DATA.
UPDATE Member
SET
	 [COV_EFFECTIVE] = CASE WHEN CAST(BackupMember.[COV_EFFECTIVE] AS INT) = CAST('2958465' AS INT) THEN NULL ELSE CAST(DATEADD(day,CAST(BackupMember.[COV_EFFECTIVE] AS INT) - 2,CAST('1900-01-01' AS DATE)) AS DATE) END
	,[COV_CANCEL] = CASE WHEN CAST(BackupMember.[COV_CANCEL] AS INT) = CAST('2958465' AS INT) THEN NULL ELSE CAST(DATEADD(day,CAST(BackupMember.[COV_CANCEL] AS INT) - 2,CAST('1900-01-01' AS DATE)) AS DATE) END
	,[DATE_OF_BIRTH] = CASE WHEN CAST(BackupMember.[DATE_OF_BIRTH] AS INT) = CAST('2958465' AS INT) THEN NULL ELSE CAST(DATEADD(day,CAST(BackupMember.[DATE_OF_BIRTH] AS INT) - 2,CAST('1900-01-01' AS DATE)) AS DATE) END
	,[PROCESS_DATE] = CASE WHEN CAST(BackupMember.[PROCESS_DATE] AS INT) = CAST('2958465' AS INT) THEN NULL ELSE CAST(DATEADD(day,CAST(BackupMember.[PROCESS_DATE] AS INT) - 2,CAST('1900-01-01' AS DATE)) AS DATE) END
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] Member
JOIN [FHPDataMarts].[dbo].[Humana_CKD_Member_BACKUP_20200102] BackupMember
ON Member.[Humana_CKD_Member_ROW_ID] = BackupMember.[Humana_CKD_Member_ROW_ID]
