--USE [FHPDW]
--GO


--IF OBJECT_ID('dbo.Membership_Export_Member_UPSERT', 'P') IS NOT NULL
--	DROP PROCEDURE dbo.Membership_Export_Member_UPSERT;
--GO


--CREATE PROCEDURE [dbo].[Membership_Export_Member_UPSERT]
--	-- No parameters
--AS
--BEGIN

--	-- ==========================================================================================
--	-- Author:		David M. Wilson
--	-- Create date: March 14th 2019
--	-- Description:	
--	-- ==========================================================================================
--	/*

--	*/
--	-- ==========================================================================================

--	DECLARE @GETDATE DATETIME2 = GETDATE();

--	-- INSERT
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_Member]
--	(
--		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,[HEALTHCLOUD_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[RACE]
--		,[CKD_STAGE]
--		,[ADDRESS_1]
--		,[ADDRESS_2]
--		,[ADDRESS_3]
--		,[CITY]
--		,[STATE]
--		,[ZIP]
--		,[COUNTY]
--		,[ISLAND]
--		,[COUNTRY]
--		,[PHONE]
--		,[ALTERNATE_PHONE]
--		,[EVENING_PHONE]
--		,[EMERGENCY_PHONE]
--		,[FAX]
--		,[EMAIL]
--		,[STATUS]
--		,[LOB_VENDOR]
--		,[LOB_TYPE]
--		,[HC_LOB]
--		,[HC_GROUP]
--		,[HC_PROGRAM]
--		,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		,[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON]
--		,[ROW_DELETED]
--		,[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,TheSource.[HEALTHCLOUD_ID]
--		,TheSource.[SSN]
--		,TheSource.[HICN]
--		,TheSource.[CLAIM_SUBSCRIBER_ID]
--		,TheSource.[MBI]
--		,TheSource.[MEDICAID_NO]
--		,TheSource.[MRN]
--		,TheSource.[FIRST_NAME]
--		,TheSource.[MIDDLE_NAME]
--		,TheSource.[LAST_NAME]
--		,TheSource.[DATE_OF_BIRTH]
--		,TheSource.[GENDER]
--		,TheSource.[RACE]
--		,TheSource.[CKD_STAGE]
--		,TheSource.[ADDRESS_1]
--		,TheSource.[ADDRESS_2]
--		,TheSource.[ADDRESS_3]
--		,TheSource.[CITY]
--		,TheSource.[STATE]
--		,TheSource.[ZIP]
--		,TheSource.[COUNTY]
--		,TheSource.[ISLAND]
--		,TheSource.[COUNTRY]
--		,TheSource.[PHONE]
--		,TheSource.[ALTERNATE_PHONE]
--		,TheSource.[EVENING_PHONE]
--		,TheSource.[EMERGENCY_PHONE]
--		,TheSource.[FAX]
--		,TheSource.[EMAIL]
--		,TheSource.[STATUS]
--		,TheSource.[LOB_VENDOR]
--		,TheSource.[LOB_TYPE]
--		,TheSource.[HC_LOB]
--		,TheSource.[HC_GROUP]
--		,TheSource.[HC_PROGRAM]
--		,TheSource.[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,TheSource.[ROW_SOURCE]
--		,TheSource.[ROW_SOURCE_ID]
--		,TheSource.[ROW_PROBLEM]
--		,TheSource.[ROW_PROBLEM_DATE]
--		,TheSource.[ROW_PROBLEM_REASON]
--		,TheSource.[ROW_DELETED]
--		,TheSource.[ROW_DELETED_DATE]
--		,TheSource.[ROW_DELETED_REASON]
--		,@GETDATE AS [ROW_CREATE_DATE]
--		,@GETDATE AS [ROW_UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Membership_Export_Member_SOURCE] TheSource
--	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Member] TheTable
--	ON TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--	WHERE TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] IS NULL  -- Only new records



--	-- UPDATE
--	UPDATE MEM
--	SET
--		 [HEALTHCLOUD_ID] = Delta.[HEALTHCLOUD_ID]
--		,[SSN] = Delta.[SSN]
--		,[HICN] = Delta.[HICN]
--		,[CLAIM_SUBSCRIBER_ID] = Delta.[CLAIM_SUBSCRIBER_ID]
--		,[MBI] = Delta.[MBI]
--		,[MEDICAID_NO] = Delta.[MEDICAID_NO]
--		,[MRN] = Delta.[MRN]
--		,[FIRST_NAME] = Delta.[FIRST_NAME]
--		,[MIDDLE_NAME] = Delta.[MIDDLE_NAME]
--		,[LAST_NAME] = Delta.[LAST_NAME]
--		,[DATE_OF_BIRTH] = Delta.[DATE_OF_BIRTH]
--		,[GENDER] = Delta.[GENDER]
--		,[RACE] = Delta.[RACE]
--		,[CKD_STAGE] = Delta.[CKD_STAGE]
--		,[ADDRESS_1] = Delta.[ADDRESS_1]
--		,[ADDRESS_2] = Delta.[ADDRESS_2]
--		,[ADDRESS_3] = Delta.[ADDRESS_3]
--		,[CITY] = Delta.[CITY]
--		,[STATE] = Delta.[STATE]
--		,[ZIP] = Delta.[ZIP]
--		,[COUNTY] = Delta.[COUNTY]
--		,[ISLAND] = Delta.[ISLAND]
--		,[COUNTRY] = Delta.[COUNTRY]
--		,[PHONE] = Delta.[PHONE]
--		,[ALTERNATE_PHONE] = Delta.[ALTERNATE_PHONE]
--		,[EVENING_PHONE] = Delta.[EVENING_PHONE]
--		,[EMERGENCY_PHONE] = Delta.[EMERGENCY_PHONE]
--		,[FAX] = Delta.[FAX]
--		,[EMAIL] = Delta.[EMAIL]
--		,[STATUS] = Delta.[STATUS]
--		,[LOB_VENDOR] = Delta.[LOB_VENDOR]
--		,[LOB_TYPE] = Delta.[LOB_TYPE]
--		,[HC_LOB] = Delta.[HC_LOB]
--		,[HC_GROUP] = Delta.[HC_GROUP]
--		,[HC_PROGRAM] = Delta.[HC_PROGRAM]
--		,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] = Delta.[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
--		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
--		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
--		,[ROW_DELETED] = Delta.[ROW_DELETED]
--		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE] = Delta.[ROW_UPDATE_DATE]
--	FROM [FHPDW].[dbo].[Membership_Export_Member] MEM
--	JOIN
--	(
--		SELECT
--			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--			,[HEALTHCLOUD_ID]
--			,[SSN]
--			,[HICN]
--			,[CLAIM_SUBSCRIBER_ID]
--			,[MBI]
--			,[MEDICAID_NO]
--			,[MRN]
--			,[FIRST_NAME]
--			,[MIDDLE_NAME]
--			,[LAST_NAME]
--			,[DATE_OF_BIRTH]
--			,[GENDER]
--			,[RACE]
--			,[CKD_STAGE]
--			,[ADDRESS_1]
--			,[ADDRESS_2]
--			,[ADDRESS_3]
--			,[CITY]
--			,[STATE]
--			,[ZIP]
--			,[COUNTY]
--			,[ISLAND]
--			,[COUNTRY]
--			,[PHONE]
--			,[ALTERNATE_PHONE]
--			,[EVENING_PHONE]
--			,[EMERGENCY_PHONE]
--			,[FAX]
--			,[EMAIL]
--			,[STATUS]
--			,[LOB_VENDOR]
--			,[LOB_TYPE]
--			,[HC_LOB]
--			,[HC_GROUP]
--			,[HC_PROGRAM]
--			,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--			,[ROW_DELETED]
--			,[ROW_DELETED_DATE]
--			,[ROW_DELETED_REASON]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDataMarts].[dbo].[Membership_Export_Member_SOURCE]
	
--		EXCEPT

--		SELECT
--			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--			,[HEALTHCLOUD_ID]
--			,[SSN]
--			,[HICN]
--			,[CLAIM_SUBSCRIBER_ID]
--			,[MBI]
--			,[MEDICAID_NO]
--			,[MRN]
--			,[FIRST_NAME]
--			,[MIDDLE_NAME]
--			,[LAST_NAME]
--			,[DATE_OF_BIRTH]
--			,[GENDER]
--			,[RACE]
--			,[CKD_STAGE]
--			,[ADDRESS_1]
--			,[ADDRESS_2]
--			,[ADDRESS_3]
--			,[CITY]
--			,[STATE]
--			,[ZIP]
--			,[COUNTY]
--			,[ISLAND]
--			,[COUNTRY]
--			,[PHONE]
--			,[ALTERNATE_PHONE]
--			,[EVENING_PHONE]
--			,[EMERGENCY_PHONE]
--			,[FAX]
--			,[EMAIL]
--			,[STATUS]
--			,[LOB_VENDOR]
--			,[LOB_TYPE]
--			,[HC_LOB]
--			,[HC_GROUP]
--			,[HC_PROGRAM]
--			,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--			,[ROW_DELETED]
--			,[ROW_DELETED_DATE]
--			,[ROW_DELETED_REASON]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Membership_Export_Member]
--	) Delta
--	ON MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


--	-- Return success
--	RETURN 0


--END
--GO












----------------------------------------------------------------------------------------- ELIGIBILITY


--USE [FHPDW]
--GO


--IF OBJECT_ID('dbo.Membership_Export_Eligibility_UPSERT', 'P') IS NOT NULL
--	DROP PROCEDURE dbo.Membership_Export_Eligibility_UPSERT;
--GO



--CREATE PROCEDURE [dbo].[Membership_Export_Eligibility_UPSERT]
--	-- No parameters
--AS
--BEGIN

--	-- ==========================================================================================
--	-- Author:		David M. Wilson
--	-- Create date: March 14th 2019
--	-- Description:	
--	-- ==========================================================================================
--	/*

--	*/
--	-- ==========================================================================================

--	DECLARE @GETDATE DATETIME2 = GETDATE();

--	-- INSERT
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_Eligibility]
--	(
--		 [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,[START_DATE]
--		,[TERM_DATE]
--		,[TERM_REASON]
--		,[STATUS]
--		,[LOB_VENDOR]
--		,[LOB_TYPE]
--		,[HC_LOB]
--		,[HC_GROUP]
--		,[HC_PROGRAM]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		,[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON]
--		,[ROW_DELETED]
--		,[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT DISTINCT
--		 TheSource.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--		,TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,TheSource.[START_DATE]
--		,TheSource.[TERM_DATE]
--		,TheSource.[TERM_REASON]
--		,TheSource.[STATUS]
--		,TheSource.[LOB_VENDOR]
--		,TheSource.[LOB_TYPE]
--		,TheSource.[HC_LOB]
--		,TheSource.[HC_GROUP]
--		,TheSource.[HC_PROGRAM]
--		,TheSource.[ROW_SOURCE]
--		,TheSource.[ROW_SOURCE_ID]
--		,TheSource.[ROW_PROBLEM]
--		,TheSource.[ROW_PROBLEM_DATE]
--		,TheSource.[ROW_PROBLEM_REASON]
--		,TheSource.[ROW_DELETED]
--		,TheSource.[ROW_DELETED_DATE]
--		,TheSource.[ROW_DELETED_REASON]
--		,@GETDATE AS [ROW_CREATE_DATE]
--		,@GETDATE AS [ROW_UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Membership_Export_Eligibility_SOURCE] TheSource
--	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Eligibility] TheTable
--	ON TheTable.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] = TheSource.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--	WHERE TheTable.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] IS NULL  -- Only new records



--	-- UPDATE
--	UPDATE MEM
--	SET
--		 [START_DATE] = Delta.[START_DATE]
--		,[TERM_DATE] = Delta.[TERM_DATE]
--		,[TERM_REASON] = Delta.[TERM_REASON]
--		,[STATUS] = Delta.[STATUS]
--		,[LOB_VENDOR] = Delta.[LOB_VENDOR]
--		,[LOB_TYPE] = Delta.[LOB_TYPE]
--		,[HC_LOB] = Delta.[HC_LOB]
--		,[HC_GROUP] = Delta.[HC_GROUP]
--		,[HC_PROGRAM] = Delta.[HC_PROGRAM]
--		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
--		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
--		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
--		,[ROW_DELETED] = Delta.[ROW_DELETED]
--		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE] = @GETDATE
--	FROM [FHPDW].[dbo].[Membership_Export_Eligibility] MEM
--	JOIN
--	(
--		SELECT
--			 [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--			,[START_DATE]
--			,[TERM_DATE]
--			,[TERM_REASON]
--			,[STATUS]
--			,[LOB_VENDOR]
--			,[LOB_TYPE]
--			,[HC_LOB]
--			,[HC_GROUP]
--			,[HC_PROGRAM]
--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--			,[ROW_DELETED]
--			,[ROW_DELETED_DATE]
--			,[ROW_DELETED_REASON]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDataMarts].[dbo].[Membership_Export_Eligibility_SOURCE]
	
--		EXCEPT

--		SELECT
--			 [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--			,[START_DATE]
--			,[TERM_DATE]
--			,[TERM_REASON]
--			,[STATUS]
--			,[LOB_VENDOR]
--			,[LOB_TYPE]
--			,[HC_LOB]
--			,[HC_GROUP]
--			,[HC_PROGRAM]
--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--			,[ROW_DELETED]
--			,[ROW_DELETED_DATE]
--			,[ROW_DELETED_REASON]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Membership_Export_Eligibility]
--	) Delta
--	ON MEM.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
--	AND MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


--	-- Return success
--	RETURN 0


--END
--GO











----------------------------------------------------------------------------------------- PATIENT EVENTS


--USE [FHPDataMarts]
--GO


--IF OBJECT_ID('dbo.Membership_Export_PatientEvents_UPSERT', 'P') IS NOT NULL
--	DROP PROCEDURE dbo.Membership_Export_PatientEvents_UPSERT;
--GO



--CREATE PROCEDURE [dbo].[Membership_Export_PatientEvents_UPSERT]
--	-- No parameters
--AS
--BEGIN

--	-- ==========================================================================================
--	-- Author:		David M. Wilson
--	-- Create date: October 2nd 2019
--	-- Description:	
--	-- ==========================================================================================
--	/*

--	*/
--	-- ==========================================================================================

--	DECLARE @GETDATE DATETIME2 = GETDATE();

--	-- INSERT
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_PatientEvents]
--	(
--		-- [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
--		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

--		,[EVENT_TYPE]
--		,[EVENT_DESCRIPTION]
--		,[EVENT_START_DATE]
--		,[EVENT_END_DATE]
--		,[EVENT_NOTES]

--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		,[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON]
--		,[ROW_DELETED]
--		,[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT DISTINCT
--		-- TheSource.[MEMBER_PATIENTEVENTS_ROW_ID] AS [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
--		 TheSource.[MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

--		,TheSource.[EVENT_TYPE]
--		,TheSource.[EVENT_DESCRIPTION]
--		,TheSource.[EVENT_START_DATE]
--		,TheSource.[EVENT_END_DATE]
--		,TheSource.[EVENT_NOTES]

--		,TheSource.[ROW_SOURCE]
--		,TheSource.[ROW_SOURCE_ID]
--		,TheSource.[ROW_PROBLEM]
--		,TheSource.[ROW_PROBLEM_DATE]
--		,TheSource.[ROW_PROBLEM_REASON]
--		,TheSource.[ROW_DELETED]
--		,TheSource.[ROW_DELETED_DATE]
--		,TheSource.[ROW_DELETED_REASON]
--		,@GETDATE AS [ROW_CREATE_DATE]
--		,@GETDATE AS [ROW_UPDATE_DATE]
--	FROM [FHPDW].[dbo].[Member_PatientEvents] TheSource
--	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_PatientEvents] TheTable
--	ON TheTable.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] = TheSource.[MEMBER_PATIENTEVENTS_ROW_ID]
--	WHERE TheTable.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] IS NULL  -- Only new records



--	-- UPDATE
--	UPDATE TheTable
--	SET
--		 [EVENT_TYPE] = Delta.[EVENT_TYPE]
--		,[EVENT_DESCRIPTION] = Delta.[EVENT_DESCRIPTION]
--		,[EVENT_START_DATE] = Delta.[EVENT_START_DATE]
--		,[EVENT_END_DATE] = Delta.[EVENT_END_DATE]
--		,[EVENT_NOTES] = Delta.[EVENT_NOTES]

--		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
--		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
--		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
--		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
--		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
--		,[ROW_DELETED] = Delta.[ROW_DELETED]
--		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
--		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE] = @GETDATE
--	FROM [FHPDW].[dbo].[Membership_Export_PatientEvents] TheTable
--	JOIN
--	(
--		SELECT
--			 [MEMBER_PATIENTEVENTS_ROW_ID] AS [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
--			,[MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

--			,[EVENT_TYPE]
--			,[EVENT_DESCRIPTION]
--			,[EVENT_START_DATE]
--			,[EVENT_END_DATE]
--			,[EVENT_NOTES]

--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--			,[ROW_DELETED]
--			,[ROW_DELETED_DATE]
--			,[ROW_DELETED_REASON]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Member_PatientEvents]
	
--		EXCEPT

--		SELECT
--			 [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
--			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

--			,[EVENT_TYPE]
--			,[EVENT_DESCRIPTION]
--			,[EVENT_START_DATE]
--			,[EVENT_END_DATE]
--			,[EVENT_NOTES]

--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--			,[ROW_DELETED]
--			,[ROW_DELETED_DATE]
--			,[ROW_DELETED_REASON]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Membership_Export_PatientEvents]
--	) Delta
--	ON TheTable.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
--	AND TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


--	-- Return success
--	RETURN 0


--END
--GO


















----------------------------------------------------------------------------------------- LABS


USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Labs_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Labs_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Labs_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: September 24th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Labs]
	(
		 [MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 TheSource.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,TheSource.[LAB_TYPE]
		,TheSource.[LAB_RESULT]
		,TheSource.[LAB_LOINC_CODE]
		,TheSource.[LAB_SERVICE_DATE]

		,TheSource.[ROW_SOURCE]
		,TheSource.[ROW_SOURCE_ID]
		,TheSource.[ROW_PROBLEM]
		,TheSource.[ROW_PROBLEM_DATE]
		,TheSource.[ROW_PROBLEM_REASON]
		,TheSource.[ROW_DELETED]
		,TheSource.[ROW_DELETED_DATE]
		,TheSource.[ROW_DELETED_REASON]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Labs_SOURCE] TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Labs] TheTable
	ON TheTable.[MEMBERSHIP_EXPORT_LABS_ROW_ID] = TheSource.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
	WHERE TheTable.[MEMBERSHIP_EXPORT_LABS_ROW_ID] IS NULL;  -- Only new records



	-- UPDATE
	UPDATE TheTable
	SET
		 [MEMBERSHIP_EXPORT_LABS_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,[LAB_TYPE] = Delta.[LAB_TYPE]
		,[LAB_RESULT] = Delta.[LAB_RESULT]
		,[LAB_LOINC_CODE] = Delta.[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE] = Delta.[LAB_SERVICE_DATE]

		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
		,[ROW_DELETED] = Delta.[ROW_DELETED]
		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = @GETDATE
	FROM [FHPDW].[dbo].[Membership_Export_Labs] TheTable
	JOIN
	(
		SELECT
			 [MEMBERSHIP_EXPORT_LABS_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

			,[LAB_TYPE]
			,[LAB_RESULT]
			,[LAB_LOINC_CODE]
			,[LAB_SERVICE_DATE]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Labs_SOURCE]
	
		EXCEPT

		SELECT
			 [MEMBERSHIP_EXPORT_LABS_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

			,[LAB_TYPE]
			,[LAB_RESULT]
			,[LAB_LOINC_CODE]
			,[LAB_SERVICE_DATE]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_Labs]
	) Delta
	ON TheTable.[MEMBERSHIP_EXPORT_LABS_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
	AND TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


	-- Return success
	RETURN 0


END
GO



----------------------------------------------------------------------------------------- PROVIDERS


--USE [FHPDataMarts]
--GO


--IF OBJECT_ID('dbo.Membership_Export_Provider_UPSERT', 'P') IS NOT NULL
--	DROP PROCEDURE dbo.Membership_Export_Provider_UPSERT;
--GO



--CREATE PROCEDURE [dbo].[Membership_Export_Provider_UPSERT]
--	-- No parameters
--AS
--BEGIN

--	-- ==========================================================================================
--	-- Author:		David M. Wilson
--	-- Create date: October 10th 2019
--	-- Description:	
--	-- ==========================================================================================
--	/*

--	*/
--	-- ==========================================================================================

--	DECLARE @GETDATE DATETIME2 = GETDATE();


--	-- INSERT
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider]
--	(
--		-- [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--		 [NPI]
--		,[SPECIALITY_1]
--		,[SPECIALITY_2]
--		,[SPECIALITY_3]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[GENDER]
--		,[DATE_OF_BIRTH]
--		,[TITLE]
--		,[GROUP_NAME]
--		,[AFFILIATION]
--		,[STATUS]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROBLEM]
--		--,[ROW_PROBLEM_DATE]
--		--,[ROW_PROBLEM_REASON]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT DISTINCT
--		-- TheSource.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--		 TheSource.[NPI]
--		,TheSource.[SPECIALITY_1]
--		,TheSource.[SPECIALITY_2]
--		,TheSource.[SPECIALITY_3]
--		,TheSource.[FIRST_NAME]
--		,TheSource.[MIDDLE_NAME]
--		,TheSource.[LAST_NAME]
--		,TheSource.[GENDER]
--		,TheSource.[DATE_OF_BIRTH]
--		,TheSource.[TITLE]
--		,TheSource.[GROUP_NAME]
--		,TheSource.[AFFILIATION]
--		,TheSource.[STATUS]
--		,TheSource.[ROW_SOURCE]
--		,TheSource.[ROW_SOURCE_ID]

--		--,TheSource.[ROW_PROBLEM]
--		--,TheSource.[ROW_PROBLEM_DATE]
--		--,TheSource.[ROW_PROBLEM_REASON]
--		--,TheSource.[ROW_DELETED]
--		--,TheSource.[ROW_DELETED_DATE]
--		--,TheSource.[ROW_DELETED_REASON]
--		,@GETDATE AS [ROW_CREATE_DATE]
--		,@GETDATE AS [ROW_UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Membership_Export_Provider_SOURCE] TheSource
--	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Provider] TheTable
--	ON TheTable.[NPI] = TheSource.[NPI]
--	WHERE TheTable.[NPI] IS NULL;  -- Only new NPI records


--	-- UPDATE
--	UPDATE TheTable
--	SET
--		 [NPI] = Delta.[NPI]
--		,[SPECIALITY_1] = Delta.[SPECIALITY_1]
--		,[SPECIALITY_2] = Delta.[SPECIALITY_2]
--		,[SPECIALITY_3] = Delta.[SPECIALITY_3]
--		,[FIRST_NAME] = Delta.[FIRST_NAME]
--		,[MIDDLE_NAME] = Delta.[MIDDLE_NAME]
--		,[LAST_NAME] = Delta.[LAST_NAME]
--		,[GENDER] = Delta.[GENDER]
--		,[DATE_OF_BIRTH] = Delta.[DATE_OF_BIRTH]
--		,[TITLE] = Delta.[TITLE]
--		,[GROUP_NAME] = Delta.[GROUP_NAME]
--		,[AFFILIATION] = Delta.[AFFILIATION]
--		,[STATUS] = Delta.[STATUS]

--		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
--		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
--		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE] = @GETDATE

--	FROM [FHPDW].[dbo].[Membership_Export_Provider] TheTable
--	JOIN
--	(
--		SELECT
--			 [NPI]
--			,[SPECIALITY_1]
--			,[SPECIALITY_2]
--			,[SPECIALITY_3]
--			,[FIRST_NAME]
--			,[MIDDLE_NAME]
--			,[LAST_NAME]
--			,[GENDER]
--			,[DATE_OF_BIRTH]
--			,[TITLE]
--			,[GROUP_NAME]
--			,[AFFILIATION]
--			,[STATUS]

--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDataMarts].[dbo].[Membership_Export_Provider_SOURCE]

--		EXCEPT

--		SELECT
--			 [NPI]
--			,[SPECIALITY_1]
--			,[SPECIALITY_2]
--			,[SPECIALITY_3]
--			,[FIRST_NAME]
--			,[MIDDLE_NAME]
--			,[LAST_NAME]
--			,[GENDER]
--			,[DATE_OF_BIRTH]
--			,[TITLE]
--			,[GROUP_NAME]
--			,[AFFILIATION]
--			,[STATUS]

--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Membership_Export_Provider]
--	) Delta
--	ON TheTable.[NPI] = Delta.[NPI]


--	-- Return success
--	RETURN 0


--END
--GO







----------------------------------------------------------------------------------------- MEMBER PROVIDERS


--USE [FHPDataMarts]
--GO


--IF OBJECT_ID('dbo.Membership_Export_Member_Provider_UPSERT', 'P') IS NOT NULL
--	DROP PROCEDURE dbo.Membership_Export_Member_Provider_UPSERT;
--GO



--CREATE PROCEDURE [dbo].[Membership_Export_Member_Provider_UPSERT]
--	-- No parameters
--AS
--BEGIN

--	-- ==========================================================================================
--	-- Author:		David M. Wilson
--	-- Create date: September 25th 2019
--	-- Description:	
--	-- ==========================================================================================
--	/*

--	*/
--	-- ==========================================================================================

--	DECLARE @GETDATE DATETIME2 = GETDATE();

--	-- INSERT

--	;WITH NewRecords AS
--	(

--		SELECT DISTINCT

--			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--			,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]

--		FROM [FHPDataMarts].[dbo].[Membership_Export_Member_Provider_SOURCE]
	
--		EXCEPT
		
--		SELECT DISTINCT

--			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--			,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]

--		FROM [FHPDW].[dbo].[Membership_Export_Member_Provider] TheTable

--	)
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_Member_Provider]
--	(
--		-- [MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]

--		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]

--		--,[ROW_PROBLEM]
--		--,[ROW_PROBLEM_DATE]
--		--,[ROW_PROBLEM_REASON]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]

--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT DISTINCT
--		-- TheSource.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]

--		 NewRecords.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
--		,NewRecords.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--		,'[FHPDataMarts].[dbo].[Membership_Export_Member_Provider_SOURCE]' [ROW_SOURCE]
--		,NULL [ROW_SOURCE_ID]

--		----,TheSource.[ROW_PROBLEM]
--		----,TheSource.[ROW_PROBLEM_DATE]
--		----,TheSource.[ROW_PROBLEM_REASON]
--		----,TheSource.[ROW_DELETED]
--		----,TheSource.[ROW_DELETED_DATE]
--		----,TheSource.[ROW_DELETED_REASON]

--		,@GETDATE AS [ROW_CREATE_DATE]
--		,@GETDATE AS [ROW_UPDATE_DATE]
--	FROM NewRecords

--	-- NOTE:  There is no update statement.  Records are simply added if they represent a unique key-pair.


--	-- Return success
--	RETURN 0


--END
--GO









----------------------------------------------------------------------------------------- Provider Facilities


USE [FHPDW]
GO


IF OBJECT_ID('dbo.Membership_Export_Provider_Facilities_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Provider_Facilities_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Provider_Facilities_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: September 4th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Provider_Facilities]



	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT

	IF OBJECT_ID('tempdb..#TheSource') IS NOT NULL DROP TABLE #TheSource

	CREATE TABLE #TheSource
	(
		 [NPI] VARCHAR(50) NOT NULL
		,[FACILITY_ID] VARCHAR(50) NOT NULL
		,[ROLE] VARCHAR(500) NULL
		,[START_DATE] DATE NULL
		,[END_DATE] DATE NULL
	)


	INSERT INTO #TheSource
	(
		 [NPI]
		,[FACILITY_ID]
		,[ROLE]
		,[START_DATE]
		,[END_DATE]
	)
	SELECT DISTINCT
		 [HCP_NPI] AS [NPI]
		,[FAC_ID] AS [FACILITY_ID]
		,[HCP_DL_ROLE_NM] AS [ROLE]
		,CAST([MBR_EFF_DT] AS DATE) AS [START_DATE]
		,CASE WHEN CAST([MBR_EFF_DT] AS DATE) = CAST('9999-12-31' AS DATE) THEN NULL ELSE CAST([MBR_EFF_DT] AS DATE) END AS [END_DATE]
	FROM OPENQUERY([KCNGX_PROVIDERS],
		'

		SELECT
			 HCP.HCP_NPI
			--,HCP.HCP_FRST_NM
			--,HCP.HCP_LAST_NM
			,FD.FAC_ID
			,FM.HCP_DL_ROLE_NM
			,FM.MBR_EFF_DT
			,FM.MBR_EXP_DT
		FROM PERS_DIAL.FAC_MEMBER FM
		JOIN PERS_DIAL.HCP HCP
			ON FM.HCP_ID = HCP.HCP_ID
		JOIN ORG_DIAL.FAC_DIAL FD
			ON FM.FAC_DIAL_ID = FD.FAC_DIAL_ID
		WHERE HCP.HCP_STS_CD = ''ACTV''
		--AND FM.MBR_EXP_DT = ''9999-12-31'' /*Remove this if you want to see the history of where HCPs were affiliated*/
		AND HCP.HCP_NPI IS NOT NULL
		AND FD.FAC_ID IS NOT NULL
		ORDER BY
			 HCP.HCP_ID
			,FD.FAC_ID
			,FM.MBR_EFF_DT
			,FM.MBR_EXP_DT
	
		')


	INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider_Facilities]
	(
		 [NPI]
		,[FACILITY_ID]
		,[ROLE]
		,[START_DATE]
		,[END_DATE]
		,[ROW_SOURCE]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 TheSource.[NPI]
		,TheSource.[FACILITY_ID]
		,TheSource.[ROLE]
		,TheSource.[START_DATE]
		,TheSource.[END_DATE]
		,'Membership_Export_Provider_Facilities_UPSERT' --[ROW_SOURCE]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM #TheSource TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Provider_Facilities] TheTable
	ON TheSource.[NPI] = TheTable.[NPI]
	AND TheSource.[FACILITY_ID] = TheTable.[FACILITY_ID]
	WHERE TheTable.[NPI] IS NULL  -- Only new records
	OR TheTable.[FACILITY_ID] IS NULL;  -- Only new records


	-- UPDATE
	UPDATE TheTable
	SET
		
		 [NPI] = Delta.[NPI]
		,[FACILITY_ID] = Delta.[FACILITY_ID]
		,[ROLE] = Delta.[ROLE]
		,[START_DATE] = Delta.[START_DATE]
		,[END_DATE] = Delta.[END_DATE]

		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = @GETDATE
	FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities] TheTable
	JOIN
	(
		SELECT
			 [NPI]
			,[FACILITY_ID]
			,[ROLE]
			,[START_DATE]
			,[END_DATE]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM #TheSource TheSource
	
		EXCEPT

		SELECT
			 [NPI]
			,[FACILITY_ID]
			,[ROLE]
			,[START_DATE]
			,[END_DATE]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities]
	) Delta
	ON TheTable.[NPI] = Delta.[NPI]
	AND TheTable.[FACILITY_ID] = Delta.[FACILITY_ID];


	-- Return success
	RETURN 0


END
GO

