


USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Eligibility_PreLoad_Logic', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Eligibility_PreLoad_Logic;
GO


CREATE PROCEDURE [dbo].[Member_Eligibility_PreLoad_Logic]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: Janurary 31st, 2018
-- Description:	This procedure executes any logic needed before the eligibility load runs.
-- ==========================================================================================
/*
*/
-- ==========================================================================================


	-- Expire all valid active records that have lapsed naturally.
	-- Note:  This only evalutes active records with an existing TERM_DATE.  If a member needs to be assigned a term date, that will be handled by the Upsert or Recalculate Status stored procedures.
	UPDATE Eli
	SET
		 Eli.[TERM_REASON] = CASE WHEN Eli.[TERM_REASON] = 'Replaced by another LOB' THEN Eli.[TERM_REASON] ELSE 'LOB Expired' END
		,Eli.[STATUS] = 'INACTIVE'
	FROM [FHPDW].[dbo].[Member_Eligibility] Eli
	WHERE 1=1
	AND Eli.[TERM_DATE] IS NOT NULL
	AND Eli.[TERM_DATE] < GETDATE()
	AND Eli.[ROW_DELETED] = 'N'   -- Only operate on active data, to avoid confusion
	AND Eli.[STATUS] = 'ACTIVE'



END
GO



