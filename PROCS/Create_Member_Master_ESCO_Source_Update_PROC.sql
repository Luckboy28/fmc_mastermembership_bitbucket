USE [FHPDataMarts]
GO


-------------------- NOT DEPLOYED, OLD CODE

IF OBJECT_ID('dbo.Member_Master_ESCO_Source_Update', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_ESCO_Source_Update;
GO


CREATE PROCEDURE [dbo].[Member_Master_ESCO_Source_Update]
-- No parameters
AS
BEGIN

	-- Declare a crosswalk table variable to help when joining between ALL_BENE_ALIGNED and Master Membership.
	DECLARE @AllBenXW TABLE
	(
		 [MRN_PK] VARCHAR(50) NOT NULL
		,[MEMBER_MASTER_ROW_ID] INT NOT NULL
	)

	;WITH Keys_Unioned AS
	(
		SELECT
			 AllBen.[MRN] AS [MRN_PK]
			,MMKH.[MEMBER_MASTER_ROW_ID]
		FROM [ESCO Staging].[dbo].[ALL_BENE_ALIGNED] AllBen
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
		ON AllBen.HICN = MMKH.HICN
		WHERE AllBen.HICN IS NOT NULL
		AND MMKH.HICN IS NOT NULL
		AND MMKH.ROW_DELETED = 'N'

		UNION ALL

		SELECT
			 AllBen.[MRN] AS [MRN_PK]
			,MMKH.[MEMBER_MASTER_ROW_ID]
		FROM [ESCO Staging].[dbo].[ALL_BENE_ALIGNED] AllBen
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
		ON AllBen.MEDHOK_ID = MMKH.MEDHOK_ID
		WHERE AllBen.MEDHOK_ID IS NOT NULL
		AND MMKH.MEDHOK_ID IS NOT NULL
		AND MMKH.ROW_DELETED = 'N'

		UNION ALL

		SELECT
			 AllBen.[MRN] AS [MRN_PK]
			,MMKH.[MEMBER_MASTER_ROW_ID]
		FROM [ESCO Staging].[dbo].[ALL_BENE_ALIGNED] AllBen
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
		ON AllBen.MBI = MMKH.MBI
		WHERE AllBen.MBI IS NOT NULL
		AND MMKH.MBI IS NOT NULL
		AND MMKH.ROW_DELETED = 'N'

		UNION ALL

		SELECT
			 AllBen.[MRN] AS [MRN_PK]
			,MMKH.[MEMBER_MASTER_ROW_ID]
		FROM [ESCO Staging].[dbo].[ALL_BENE_ALIGNED] AllBen
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
		ON AllBen.MRN = MMKH.MRN
		WHERE AllBen.MRN IS NOT NULL
		AND MMKH.MRN IS NOT NULL
		AND MMKH.ROW_DELETED = 'N'
	)
	,Keys_Unique AS
	(
		SELECT DISTINCT
			 [MRN_PK]
			,[MEMBER_MASTER_ROW_ID]
		FROM Keys_Unioned
	)
	INSERT INTO @AllBenXW
	(
		 [MRN_PK]
		,[MEMBER_MASTER_ROW_ID]
	)
	SELECT
		 [MRN_PK]
		,[MEMBER_MASTER_ROW_ID]
	FROM Keys_Unique;




	-- 1 = True = Unique,    0 = False = Not Unique
	DECLARE @UNIQUE_DATA AS INT = 1;


	;WITH Uniqueness_Check AS
	(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY [MRN_PK] ORDER BY [MEMBER_MASTER_ROW_ID]) AS [RANK]
		FROM @AllBenXW
	)
	SELECT TOP 1
		@UNIQUE_DATA = CASE WHEN [RANK] > 1 THEN 0 ELSE 1 END
	FROM Uniqueness_Check
	WHERE [RANK] > 1
	ORDER BY [RANK] DESC


	IF @UNIQUE_DATA <> 1
	BEGIN

		-- INVALID KEY, EXIT WITH FAILURE
		RAISERROR (15600,-1,-1, 'dbo.Member_Master_ESCO_Source_Update')
		RETURN 1

	END



	-- Data integrity is verified, proceed with source table updates.
	UPDATE AllBen
	SET
		 [ACTIVE_INACTIVE_FLAG] = CASE WHEN MM.[STATUS] = 'ACTIVE' AND MM.[LOB_TYPE] = 'ESCO' THEN 'ACTIVE' ELSE 'INACTIVE' END
		,[TRUE_ESCO] = CASE WHEN MM.[STATUS] = 'ACTIVE' AND MM.[LOB_TYPE] = 'ESCO' THEN 'YES' ELSE 'NO' END
		,[IS_ACTIVE_CSNP] = CASE WHEN MM.[STATUS] = 'ACTIVE' AND MM.[LOB_TYPE] = 'CSNP' THEN 'YES' ELSE 'NO' END
		,[TERM_REASON] = CASE 
			WHEN MM.[STATUS] = 'INACTIVE' AND MM.[LOB_TYPE] = 'SUBCAP' THEN 
				CASE WHEN [TERM_REASON] = 'Historical Member Re-Add' THEN UPPER([TERM_REASON])
					WHEN [TERM_REASON] LIKE '%Ineligible%' THEN UPPER([TERM_REASON])
					WHEN [TERM_REASON] LIKE '%Suppressed%' THEN UPPER([TERM_REASON])
					WHEN [TERM_REASON] LIKE '%Excluded Clinic%' THEN UPPER([TERM_REASON])
					WHEN [TERM_REASON] = 'Discharge Death' THEN UPPER([TERM_REASON])
					WHEN [TERM_REASON] = 'Discharge Transplant' THEN UPPER([TERM_REASON])
					WHEN [TERM_REASON] = 'Active CSNP' THEN UPPER([TERM_REASON])
					ELSE CONCAT('SUBCAP: ',[LOB_VENDOR])
				END
			WHEN MM.[STATUS] = 'ACTIVE' AND MM.[LOB_TYPE] = 'CSNP' THEN 'ACTIVE CSNP'
			ELSE [TERM_REASON]
		 END
	FROM [ESCO Staging].[dbo].[ALL_BENE_ALIGNED] AllBen
	JOIN @AllBenXW AllBenXW
	ON AllBenXW.MRN_PK = AllBen.MRN
	JOIN [FHPDW].[dbo].[Member_Master] MM
	ON AllBenXW.MEMBER_MASTER_ROW_ID = MM.MEMBER_MASTER_ROW_ID
	WHERE MM.ROW_DELETED = 'N'
	AND MM.MEMBER_MASTER_ROW_ID IS NOT NULL


END
GO

