

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_ESCO_ID_Update', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_ESCO_ID_Update;
GO


CREATE PROCEDURE [dbo].[Member_Master_ESCO_ID_Update]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: Feburary 11th 2019
-- Description:	
-- ==========================================================================================
/*

	

*/
-- ==========================================================================================


	-- Update the Member Master record, if needed.
	;WITH LatestRecords AS
	(
		SELECT
			 ELI.[MEMBER_MASTER_ROW_ID]
			,ELI.[MEMBER_ELIGIBILITY_ROW_ID]
			,ELI.[ESCO_ID]
			,ELI.[STATUS]
			,ELI.[LOB_TYPE]
			,ELI.[LOB_VENDOR]
			,ROW_NUMBER() OVER (PARTITION BY ELI.[MEMBER_MASTER_ROW_ID] ORDER BY CASE WHEN ELI.[STATUS] = 'ACTIVE' THEN 1 ELSE 2 END ASC, ELI.[START_DATE] DESC, ELI.[TERM_DATE] DESC) AS [RANK]   -- This simply puts the "Active" record on top, or the latest START_DATE or TERM_DATE if there is no active record.
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
		WHERE 1=1
		AND ELI.ROW_DELETED = 'N'
		--AND ELI.[LOB_TYPE] IN ('ESCO','PRELIM_ESCO')
		--AND ELI.[ESCO_ID] IS NOT NULL           --- DO WE WANT NULLS?
	)
	UPDATE MemberMaster
		SET
			[ESCO_ID] = CASE WHEN LR.[STATUS] = 'ACTIVE' THEN LR.[ESCO_ID] ELSE NULL END
	FROM [FHPDW].[dbo].[Member_Master] MemberMaster
	JOIN LatestRecords LR
	ON LR.MEMBER_MASTER_ROW_ID = MemberMaster.MEMBER_MASTER_ROW_ID
	WHERE 
	(
		ISNULL(MemberMaster.[ESCO_ID],'<NULL>') <> ISNULL(CASE WHEN LR.[STATUS] = 'ACTIVE' THEN LR.[ESCO_ID] ELSE NULL END,'<NULL>')   -- Only update if they're different
	)
	AND
	(
		LR.[RANK] = 1
	)

END
GO


