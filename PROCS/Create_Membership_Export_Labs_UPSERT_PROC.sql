

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Labs_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Labs_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Labs_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: September 24th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Labs]
	(
		 [MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 TheSource.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,TheSource.[LAB_TYPE]
		,TheSource.[LAB_RESULT]
		,TheSource.[LAB_LOINC_CODE]
		,TheSource.[LAB_SERVICE_DATE]

		,TheSource.[ROW_SOURCE]
		,TheSource.[ROW_SOURCE_ID]
		,TheSource.[ROW_PROBLEM]
		,TheSource.[ROW_PROBLEM_DATE]
		,TheSource.[ROW_PROBLEM_REASON]
		,TheSource.[ROW_DELETED]
		,TheSource.[ROW_DELETED_DATE]
		,TheSource.[ROW_DELETED_REASON]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Labs_SOURCE] TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Labs] TheTable
	ON TheTable.[MEMBERSHIP_EXPORT_LABS_ROW_ID] = TheSource.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
	WHERE TheTable.[MEMBERSHIP_EXPORT_LABS_ROW_ID] IS NULL;  -- Only new records



	-- UPDATE
	UPDATE TheTable
	SET
		 [MEMBERSHIP_EXPORT_LABS_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
		,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,[LAB_TYPE] = Delta.[LAB_TYPE]
		,[LAB_RESULT] = Delta.[LAB_RESULT]
		,[LAB_LOINC_CODE] = Delta.[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE] = Delta.[LAB_SERVICE_DATE]

		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
		,[ROW_DELETED] = Delta.[ROW_DELETED]
		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = @GETDATE
	FROM [FHPDW].[dbo].[Membership_Export_Labs] TheTable
	JOIN
	(
		SELECT
			 [MEMBERSHIP_EXPORT_LABS_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

			,[LAB_TYPE]
			,[LAB_RESULT]
			,[LAB_LOINC_CODE]
			,[LAB_SERVICE_DATE]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Labs_SOURCE]
	
		EXCEPT

		SELECT
			 [MEMBERSHIP_EXPORT_LABS_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

			,[LAB_TYPE]
			,[LAB_RESULT]
			,[LAB_LOINC_CODE]
			,[LAB_SERVICE_DATE]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_Labs]
	) Delta
	ON TheTable.[MEMBERSHIP_EXPORT_LABS_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_LABS_ROW_ID]
	AND TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


	-- Return success
	RETURN 0


END
GO
