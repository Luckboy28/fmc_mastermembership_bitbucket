

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Provider_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Provider_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Provider_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: October 10th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();


	-- INSERT
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider]
	(
		-- [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		 [NPI]
		,[PROVIDER_TYPE]
		,[SPECIALITY_1]
		,[SPECIALITY_2]
		,[SPECIALITY_3]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[GENDER]
		,[DATE_OF_BIRTH]
		,[TITLE]
		,[GROUP_NAME]
		,[AFFILIATION]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		-- TheSource.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		 TheSource.[NPI]
		,TheSource.[PROVIDER_TYPE]
		,TheSource.[SPECIALITY_1]
		,TheSource.[SPECIALITY_2]
		,TheSource.[SPECIALITY_3]
		,TheSource.[FIRST_NAME]
		,TheSource.[MIDDLE_NAME]
		,TheSource.[LAST_NAME]
		,TheSource.[GENDER]
		,TheSource.[DATE_OF_BIRTH]
		,TheSource.[TITLE]
		,TheSource.[GROUP_NAME]
		,TheSource.[AFFILIATION]
		,TheSource.[STATUS]
		,TheSource.[ROW_SOURCE]
		,TheSource.[ROW_SOURCE_ID]

		--,TheSource.[ROW_PROBLEM]
		--,TheSource.[ROW_PROBLEM_DATE]
		--,TheSource.[ROW_PROBLEM_REASON]
		--,TheSource.[ROW_DELETED]
		--,TheSource.[ROW_DELETED_DATE]
		--,TheSource.[ROW_DELETED_REASON]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Provider_SOURCE] TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Provider] TheTable
	ON TheTable.[NPI] = TheSource.[NPI]
	WHERE TheTable.[NPI] IS NULL;  -- Only new NPI records


	-- UPDATE
	UPDATE TheTable
	SET
		 [NPI] = Delta.[NPI]
		,[PROVIDER_TYPE] = Delta.[PROVIDER_TYPE]
		,[SPECIALITY_1] = Delta.[SPECIALITY_1]
		,[SPECIALITY_2] = Delta.[SPECIALITY_2]
		,[SPECIALITY_3] = Delta.[SPECIALITY_3]
		,[FIRST_NAME] = Delta.[FIRST_NAME]
		,[MIDDLE_NAME] = Delta.[MIDDLE_NAME]
		,[LAST_NAME] = Delta.[LAST_NAME]
		,[GENDER] = Delta.[GENDER]
		,[DATE_OF_BIRTH] = Delta.[DATE_OF_BIRTH]
		,[TITLE] = Delta.[TITLE]
		,[GROUP_NAME] = Delta.[GROUP_NAME]
		,[AFFILIATION] = Delta.[AFFILIATION]
		,[STATUS] = Delta.[STATUS]

		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = @GETDATE

	FROM [FHPDW].[dbo].[Membership_Export_Provider] TheTable
	JOIN
	(
		SELECT
			 [NPI]
			,[PROVIDER_TYPE]
			,[SPECIALITY_1]
			,[SPECIALITY_2]
			,[SPECIALITY_3]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[GENDER]
			,[DATE_OF_BIRTH]
			,[TITLE]
			,[GROUP_NAME]
			,[AFFILIATION]
			,[STATUS]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Provider_SOURCE]

		EXCEPT

		SELECT
			 [NPI]
			,[PROVIDER_TYPE]
			,[SPECIALITY_1]
			,[SPECIALITY_2]
			,[SPECIALITY_3]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[GENDER]
			,[DATE_OF_BIRTH]
			,[TITLE]
			,[GROUP_NAME]
			,[AFFILIATION]
			,[STATUS]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_Provider]
	) Delta
	ON TheTable.[NPI] = Delta.[NPI]


	-- Return success
	RETURN 0


END
GO

