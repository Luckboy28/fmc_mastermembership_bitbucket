
USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_PatientEvents_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_PatientEvents_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_PatientEvents_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: Octomber 1st 2019
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_PatientEvents_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			-- [MEMBER_MASTER_ROW_ID]
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EVENT_TYPE]
			,[EVENT_DESCRIPTION]
			,[EVENT_START_DATE]
			,[EVENT_END_DATE]
			,[EVENT_NOTES]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Combined_Sources]

		INTERSECT

		SELECT
			-- [MEMBER_MASTER_ROW_ID]
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EVENT_TYPE]
			,[EVENT_DESCRIPTION]
			,[EVENT_START_DATE]
			,[EVENT_END_DATE]
			,[EVENT_NOTES]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_PatientEvents_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EVENT_TYPE]
		,[EVENT_DESCRIPTION]
		,[EVENT_START_DATE]
		,[EVENT_END_DATE]
		,[EVENT_NOTES]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROCESSED]
		--,[ROW_PROCESSED_DATE]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EVENT_TYPE]
		,[EVENT_DESCRIPTION]
		,[EVENT_START_DATE]
		,[EVENT_END_DATE]
		,[EVENT_NOTES]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROCESSED]
		--,[ROW_PROCESSED_DATE]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EVENT_TYPE]
		,[EVENT_DESCRIPTION]
		,[EVENT_START_DATE]
		,[EVENT_END_DATE]
		,[EVENT_NOTES]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROCESSED]
		--,[ROW_PROCESSED_DATE]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Staging]

END
GO


