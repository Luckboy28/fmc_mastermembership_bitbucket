

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Labs_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Labs_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Labs_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: September 24th 2019
-- Update date: 
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[LAB_TYPE]
			,[LAB_RESULT]
			,[LAB_LOINC_CODE]
			,[LAB_SERVICE_DATE]
		FROM [FHPDataMarts].[dbo].[Member_Labs_Combined_Sources]

		INTERSECT

		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[LAB_TYPE]
			,[LAB_RESULT]
			,[LAB_LOINC_CODE]
			,[LAB_SERVICE_DATE]
		FROM [FHPDataMarts].[dbo].[Member_Labs_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_Labs_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM [FHPDataMarts].[dbo].[Member_Labs_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM [FHPDataMarts].[dbo].[Member_Labs_Staging]

END
GO

