USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Labs_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Labs_UPSERT;
GO


CREATE PROCEDURE [dbo].[Member_Labs_UPSERT]

	@MEMBER_LABS_STAGING_ROW_ID int NULL,

	@MHK_INTERNAL_ID int NULL,
	@MEDHOK_ID varchar(50) NULL,
	@SSN varchar(11) NULL,
	@HICN varchar(12) NULL,
	@CLAIM_SUBSCRIBER_ID varchar(50) NULL,
	@MBI varchar(11) NULL,
	@MEDICAID_NO varchar(50) NULL,
	@MRN varchar(50) NULL,
	@EXT_ID varchar(50) NULL,
	@EXT_ID_TYPE varchar(50) NULL,
	@EXT_ID_2 varchar(50) NULL,
	@EXT_ID_TYPE_2 varchar(50) NULL,
	@EXT_ID_3 varchar(50) NULL,
	@EXT_ID_TYPE_3 varchar(50) NULL,

	@LAB_TYPE varchar(150) NULL,
	@LAB_RESULT varchar(150) NULL,
	@LAB_LOINC_CODE varchar(50) NULL,
	@LAB_SERVICE_DATE date NULL,

	@ROW_SOURCE varchar(500) NULL,
	@ROW_SOURCE_ID varchar(50) NULL

AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: December 14th 2017
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================


IF @MEMBER_LABS_STAGING_ROW_ID IS NULL OR @ROW_SOURCE IS NULL
BEGIN
	-- INVALID KEY, EXIT WITH FAILURE
	RAISERROR (15600,-1,-1, 'dbo.Member_Labs_UPSERT')
	RETURN 1
END


-- Find the member
DECLARE @MEMBER_MASTER_ROW_COUNT AS INT = 0;
DECLARE @MEMBER_MASTER_ROW_ID AS INT = 0;



-- Used for gathering stats on key lookups.
DECLARE @KEY_SEARCH_RESULTS AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)


DECLARE @MEMBER_MASTER_UPDATED_ROW_ID AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)



/**********************************************************************************************************************

	Check all possible key collisions, and count the number of rows that this would effect on the master table.

	0 rows:  None of the current keys matched.  This member Labs record is orphaned until a matching member
			 can be located.

	1 row:  At least one or more of the keys matched, with none mismatching.  This means a valid member was found
			for the Lab result. 

	2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
					 table, meaning that an insert cannot happen because this Labs record cannot be uniquely
					 matched to a member.  This staging row should be flagged as being in error, and a human should
					 investigate why the same keys are being used in more than once place.
	

**********************************************************************************************************************/
;WITH AllKeys AS
(
	-- Historical
	SELECT
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	WHERE ROW_DELETED = 'N'
)
INSERT INTO @KEY_SEARCH_RESULTS
(
	[MEMBER_MASTER_ROW_ID]
)
SELECT DISTINCT
	[MEMBER_MASTER_ROW_ID]
FROM AllKeys
WHERE 1=2
OR @MHK_INTERNAL_ID = AllKeys.[MHK_INTERNAL_ID]
OR @MEDHOK_ID = AllKeys.[MEDHOK_ID]
OR @SSN = AllKeys.[SSN]
OR @HICN = AllKeys.[HICN]
OR @CLAIM_SUBSCRIBER_ID = AllKeys.[CLAIM_SUBSCRIBER_ID]
OR @MBI = AllKeys.[MBI]
OR @MEDICAID_NO = AllKeys.[MEDICAID_NO]
OR @MRN = AllKeys.[MRN]
OR CASE WHEN @EXT_ID IS NOT NULL AND @EXT_ID_TYPE IS NOT NULL THEN CONCAT(@EXT_ID,':',@EXT_ID_TYPE) ELSE NULL END = AllKeys.[EXT_ID_AND_TYPE]


-- Get number of matching results
SELECT @MEMBER_MASTER_ROW_COUNT = COUNT(1) FROM @KEY_SEARCH_RESULTS;

-- Get the record, if it's unique
IF @MEMBER_MASTER_ROW_COUNT = 1
BEGIN
	SELECT @MEMBER_MASTER_ROW_ID = [MEMBER_MASTER_ROW_ID] FROM @KEY_SEARCH_RESULTS
END



-- 0 rows:  None of the current keys matched.  This is an orphan record.
IF @MEMBER_MASTER_ROW_COUNT = 0
BEGIN

	UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
	SET
		[ROW_PROBLEM] = 'Y',
		[ROW_PROBLEM_REASON] = 'No matching member found.',
		[ROW_PROBLEM_DATE] = GETDATE()
	WHERE [MEMBER_LABS_STAGING_ROW_ID] = @MEMBER_LABS_STAGING_ROW_ID;


	-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
	RETURN 0

END





-- 1 row:  The Labs record uniquely matches to a single member.  We can insert it now.
IF @MEMBER_MASTER_ROW_COUNT = 1
BEGIN

	INSERT INTO [FHPDW].[dbo].[Member_Labs]
	(
		-- [MEMBER_LABS_ROW_ID]

		 [MEMBER_MASTER_ROW_ID]

		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]

		,[MEMBER_LABS_STAGING_ROW_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT	
		@MEMBER_MASTER_ROW_ID,  --@MEMBER_MASTER_ROW_ID   (from lookup)

		@LAB_TYPE,
		@LAB_RESULT,
		@LAB_LOINC_CODE,
		@LAB_SERVICE_DATE,

		@MEMBER_LABS_STAGING_ROW_ID,

		@ROW_SOURCE,  --[ROW_SOURCE]
		@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
		  --[ROW_PROCESSED]
		  --[ROW_PROCESSED_DATE]
		  --[ROW_DELETED]
		  --[ROW_DELETED_DATE]
		  --[ROW_DELETED_REASON]
		  --[ROW_CREATE_DATE]
		  --[ROW_UPDATE_DATE]
	
	EXCEPT

	-- Prevent duplicate
	SELECT
		
		 [MEMBER_MASTER_ROW_ID]

		,[LAB_TYPE]
		,[LAB_RESULT]
		,[LAB_LOINC_CODE]
		,[LAB_SERVICE_DATE]

		,@MEMBER_LABS_STAGING_ROW_ID

		,@ROW_SOURCE
		,@ROW_SOURCE_ID
	FROM [FHPDataMarts].[dbo].[Member_Labs_Staging];




	-- Update the staging table.
	UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
	SET
		[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID,
		[ROW_PROCESSED] = 'Y',
		[ROW_PROCESSED_DATE] = GETDATE()
	WHERE [MEMBER_LABS_STAGING_ROW_ID] = @MEMBER_LABS_STAGING_ROW_ID;


	-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
	RETURN 0

END


/*
	2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
					 table, meaning that an insert cannot happen because this Labs record cannot be uniquely
					 matched to a member.  This staging row should be flagged as being in error, and a human should
					 investigate why the same keys are being used in more than once place.
*/
IF @MEMBER_MASTER_ROW_COUNT > 1
BEGIN

	UPDATE [FHPDataMarts].[dbo].[Member_Labs_Staging]
	SET
		[ROW_PROBLEM] = 'Y',
		[ROW_PROBLEM_REASON] = 'The keys on this record match more than 1 record on the member master table.  Requires human analysis and intervention.',
		[ROW_PROBLEM_DATE] = GETDATE()
	WHERE [MEMBER_LABS_STAGING_ROW_ID] = @MEMBER_LABS_STAGING_ROW_ID

END


-- Return success
RETURN 0




END
GO


