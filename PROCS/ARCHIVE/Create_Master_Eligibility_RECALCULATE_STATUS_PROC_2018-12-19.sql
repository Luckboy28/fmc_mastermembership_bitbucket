
USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Eligibility_RECALCULATE_STATUS', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Eligibility_RECALCULATE_STATUS;
GO


CREATE PROCEDURE [dbo].[Member_Eligibility_RECALCULATE_STATUS]
	@MEMBER_MASTER_ROW_ID INT NULL
AS
BEGIN

	/**************************************************************************************************************************************

		Recalculate TERM_DATE, TERM_REASON, and STATUS based on all of the member's eligibility records.

	**************************************************************************************************************************************/


	---------------------------------------------------------------------------------------------------------
	-- STATUS
	---------------------------------------------------------------------------------------------------------

	;WITH STATUS_ActiveRecords AS
	(
		SELECT
				[MEMBER_ELIGIBILITY_ROW_ID]
			,[START_DATE]
			,[TERM_DATE]
			--,[TERM_REASON]
			,ELI.[LOB_VENDOR]
			,ELI.[LOB_TYPE]
			,[MEMBER_MASTER_ROW_ID]
			,ROW_NUMBER() OVER (ORDER BY LOBH.[LOB_RANK] ASC, [START_DATE] DESC, ISNULL([TERM_DATE],CAST('9999-12-31' AS DATETIME2)) DESC) [Rank]   -- NOTE:  For sorting reasons, NULL is assigned the latest date that SQL supports, because it has higher priority over normal dates.
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
		ON ELI.[LOB_TYPE] = LOBH.[LOB_TYPE]
		AND ELI.[LOB_VENDOR] = LOBH.[LOB_VENDOR]
		WHERE ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		AND [START_DATE] < GETDATE() AND ([TERM_DATE] IS NULL OR [TERM_DATE] > GETDATE() OR [TERM_REASON] = 'Replaced by another LOB')
	)

	---------------------------------------------------------------------------------------------------------
	-- TERM DATE: TERMINATED BY ANOTHER LOB
	---------------------------------------------------------------------------------------------------------

	-- Start the process of determining the TERM_DATE by gathering the relavent data.
	,TERM_DATE_LOB_REPLACEMENT_TheData AS
	(
		SELECT 
				[MEMBER_ELIGIBILITY_ROW_ID]
			,[START_DATE]
			,[TERM_DATE]
			--,[TERM_REASON]
			--,ELI.[LOB_VENDOR]
			--,ELI.[LOB_TYPE]
			,LOBH.[LOB_RANK]
			,ELI.[ROW_CREATE_DATE]
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
		ON ELI.[LOB_TYPE] = LOBH.[LOB_TYPE]
		AND ELI.[LOB_VENDOR] = LOBH.[LOB_VENDOR]
		WHERE 1=1
		AND [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		AND ELI.[ROW_DELETED] = 'N'
		--AND [START_DATE] < GETDATE()
		--AND ([TERM_DATE] >= GETDATE() OR [TERM_REASON] = 'Replaced by another LOB')

	)
	-- Cross apply the data to itself, so that we can find data relationships between rows
	,TERM_DATE_LOB_REPLACEMENT_CrossAppliedData AS
	(
		SELECT * FROM TERM_DATE_LOB_REPLACEMENT_TheData A
		CROSS APPLY
		(
			SELECT 
					B.[MEMBER_ELIGIBILITY_ROW_ID] AS [MEMBER_ELIGIBILITY_ROW_ID_REPLACING_RECORD]
				,B.[ROW_CREATE_DATE] AS [ROW_CREATE_DATE_REPLACING_RECORD]
				,B.[LOB_RANK] AS [LOB_RANK_REPLACING_RECORD]
				,DATEADD(day,-1,B.[START_DATE]) AS [START_DATE_REPLACING_RECORD]
				,DATEDIFF(day,A.[START_DATE],B.[START_DATE]) [DateDiff]
			FROM TERM_DATE_LOB_REPLACEMENT_TheData B
			WHERE A.[MEMBER_ELIGIBILITY_ROW_ID] <> B.[MEMBER_ELIGIBILITY_ROW_ID]
			AND B.[LOB_RANK] <= A.[LOB_RANK]
			AND A.[TERM_DATE] IS NULL
		) TERM_DATE_LOB_REPLACEMENT_TheDataAlias
	)
	-- Rank the cross apply, so that we can later extract only the top ranked rows -- resulting in the data we want
	,TERM_DATE_LOB_REPLACEMENT_CrossAppliedDataRanked AS
	(
		SELECT
			CAD.*
			,ROW_NUMBER() OVER (PARTITION BY [MEMBER_ELIGIBILITY_ROW_ID] ORDER BY CAD.[DateDiff] ASC, CAD.[LOB_RANK_REPLACING_RECORD] ASC, CAD.[ROW_CREATE_DATE_REPLACING_RECORD] ASC) AS [Rank]
		FROM TERM_DATE_LOB_REPLACEMENT_CrossAppliedData CAD
		WHERE [DateDiff] >= 0
	)
	-- Grab the top ranked rows only.
	,TERM_DATE_LOB_REPLACEMENT AS
	(
		SELECT 
				[MEMBER_ELIGIBILITY_ROW_ID]
			,[TERM_DATE]
			,[START_DATE_REPLACING_RECORD] AS [TERM_DATE_LOB_REPLACEMENT]  -- The start date (minus 1 day) of the new LOB becomes the old LOB's term date.
		FROM TERM_DATE_LOB_REPLACEMENT_CrossAppliedDataRanked
		WHERE [Rank] = 1
	)
	--
	-- Prep the final data for updating the Eligibility table
	--
	,FinalData AS
	(
		SELECT
				ELI.[MEMBER_ELIGIBILITY_ROW_ID]
			,CASE 
				WHEN STATUS_ActiveRecords.[Rank] = 1
					THEN NULL
				ELSE
					CASE 
						WHEN TERM_DATE_LOB_REPLACEMENT.[TERM_DATE_LOB_REPLACEMENT] IS NOT NULL
							THEN TERM_DATE_LOB_REPLACEMENT.[TERM_DATE_LOB_REPLACEMENT]
						ELSE ELI.[TERM_DATE]
					END
				END AS [TERM_DATE]
			,CASE 
				WHEN STATUS_ActiveRecords.[Rank] = 1
					THEN NULL
				ELSE
					CASE
						WHEN ELI.[TERM_DATE] IS NOT NULL AND ELI.[TERM_DATE] <= GETDATE() 
							THEN 'LOB Expired'
						ELSE
							CASE
								WHEN TERM_DATE_LOB_REPLACEMENT.[TERM_DATE_LOB_REPLACEMENT] IS NOT NULL
									THEN 'Replaced by another LOB'
								ELSE
									NULL
							END
					END
				END AS [TERM_REASON]
			,CASE WHEN STATUS_ActiveRecords.[Rank] = 1 THEN 'ACTIVE' ELSE 'INACTIVE' END [STATUS]   -- Note, the Active record will be 1, and the others will be NULL.
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
		LEFT OUTER JOIN TERM_DATE_LOB_REPLACEMENT
		ON TERM_DATE_LOB_REPLACEMENT.[MEMBER_ELIGIBILITY_ROW_ID] = ELI.[MEMBER_ELIGIBILITY_ROW_ID]
		LEFT OUTER JOIN STATUS_ActiveRecords
		ON STATUS_ActiveRecords.[MEMBER_ELIGIBILITY_ROW_ID] = Eli.[MEMBER_ELIGIBILITY_ROW_ID]
		WHERE ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		AND ELI.[ROW_DELETED] = 'N'

		EXCEPT  -- This allows us to only update records where there are changes, instead of every record on every load
			
		SELECT
				[MEMBER_ELIGIBILITY_ROW_ID]
				,[TERM_DATE]
				,[TERM_REASON]
				,[STATUS]
		FROM [FHPDW].[dbo].[Member_Eligibility]
		WHERE [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		AND [ROW_DELETED] = 'N'
	)
	-- Perform the update
	UPDATE Eli_Production
	SET
			Eli_Production.[TERM_DATE] = FinalData.[TERM_DATE]
		,Eli_Production.[TERM_REASON] = FinalData.[TERM_REASON]
		,Eli_Production.[STATUS] = FinalData.[STATUS]
	FROM [FHPDW].[dbo].[Member_Eligibility] Eli_Production
	JOIN FinalData
	ON FinalData.[MEMBER_ELIGIBILITY_ROW_ID] = Eli_Production.[MEMBER_ELIGIBILITY_ROW_ID]
	WHERE Eli_Production.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID   -- Overly causious WHERE statement. Verifying the correct member.


END
GO