

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Provider_Facility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Provider_Facility_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Provider_Facility_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: October 14th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================


	DECLARE @GETDATE DATETIME2 = GETDATE();



	-- PACK TEMP TABLE 

	IF OBJECT_ID('tempdb..#NPI_Facility') IS NOT NULL DROP TABLE #NPI_Facility

	CREATE TABLE #NPI_Facility
	(
		 [NPI] VARCHAR(50) NOT NULL
		,[FACILITY_ID] VARCHAR(50) NOT NULL
		,[ROLE] VARCHAR(500) NULL
		,[START_DATE] DATE NULL
		,[END_DATE] DATE NULL
	)


	INSERT INTO #NPI_Facility
	(
		 [NPI]
		,[FACILITY_ID]
		,[ROLE]
		,[START_DATE]
		,[END_DATE]
	)
	SELECT DISTINCT
		 [HCP_NPI] AS [NPI]
		,[FAC_ID] AS [FACILITY_ID]
		,[HCP_DL_ROLE_NM] AS [ROLE]
		,CAST([MBR_EFF_DT] AS DATE) AS [START_DATE]
		,CASE WHEN CAST([MBR_EFF_DT] AS DATE) = CAST('9999-12-31' AS DATE) THEN NULL ELSE CAST([MBR_EFF_DT] AS DATE) END AS [END_DATE]
	FROM OPENQUERY([KCNGX_PROVIDERS],
		'

		SELECT
			 HCP.HCP_NPI
			--,HCP.HCP_FRST_NM
			--,HCP.HCP_LAST_NM
			,FD.FAC_ID
			,FM.HCP_DL_ROLE_NM
			,FM.MBR_EFF_DT
			,FM.MBR_EXP_DT
		FROM PERS_DIAL.FAC_MEMBER FM
		JOIN PERS_DIAL.HCP HCP
			ON FM.HCP_ID = HCP.HCP_ID
		JOIN ORG_DIAL.FAC_DIAL FD
			ON FM.FAC_DIAL_ID = FD.FAC_DIAL_ID
		WHERE HCP.HCP_STS_CD = ''ACTV''
		--AND FM.MBR_EXP_DT = ''9999-12-31'' /*Remove this if you want to see the history of where HCPs were affiliated*/
		AND HCP.HCP_NPI IS NOT NULL
		AND FD.FAC_ID IS NOT NULL
		ORDER BY
			 HCP.HCP_ID
			,FD.FAC_ID
			,FM.MBR_EFF_DT
			,FM.MBR_EXP_DT
	
		')



	--Insert known FKC clinics for all of the providers that we currently have
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider_Facility]
	(
		 [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		,[ROLE]
		,[START_DATE]
		,[END_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT 
		 MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,MEF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		,NULL [ROLE]
		,NULL [START_DATE]
		,NULL [END_DATE]
		,'[FHPDataMarts].[dbo].[Membership_Export_Provider_Facility_UPSERT]' [ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM #NPI_Facility NPIF
	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP --Get [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		ON MEP.[NPI] = NPIF.[NPI]
		AND MEP.[ROW_DELETED] = 'N'
	JOIN [FHPDW].[dbo].[Membership_Export_Facility] MEF --Get [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		ON MEF.[INTERNAL_FACILITY_ID] = NPIF.[FACILITY_ID]
		AND MEF.[ROW_DELETED] = 'N'
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Provider_Facility] MEPF
		ON MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] = MEPF.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		AND MEF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID] = MEPF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		AND MEPF.[ROW_DELETED] = 'N'
	WHERE (MEPF.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] IS NULL OR MEPF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID] IS NULL)  -- Only new records





	
	--Insert facilities that do not exactly match our known FMC facilities
	----------------------------------------------------------------------------------------------------------
	-- NOTE:  This will likely cause duplicate entries when the addresses do not exactly match.
	--        This is a known risk, which could be mitigated with address standardization services (USPS, etc)
	----------------------------------------------------------------------------------------------------------
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider_Facility]
	(
		 [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		,[ROLE]
		,[START_DATE]
		,[END_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,MEF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		,NULL [ROLE]
		,NULL [START_DATE]
		,NULL [END_DATE]
		,'[FHPDataMarts].[dbo].[Membership_Export_Provider_Facility_UPSERT]' [ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE] MEFS
	JOIN [FHPDW].[dbo].[Membership_Export_Facility] MEF --Get [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		ON CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[ADDRESS_1])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[ADDRESS_1])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[ADDRESS_2])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[ADDRESS_2])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[ADDRESS_3])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[ADDRESS_3])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[CITY])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[CITY])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[STATE])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[STATE])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[ZIP])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[ZIP])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[COUNTY])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[COUNTY])),''),'<NULL>') AS VARCHAR(500))
		AND CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEFS.[COUNTRY])),''),'<NULL>') AS VARCHAR(500)) = CAST(ISNULL(NULLIF(LTRIM(RTRIM(MEF.[COUNTRY])),''),'<NULL>') AS VARCHAR(500))
		AND MEF.[ROW_DELETED] = 'N'
	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP --Get [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		ON MEP.[NPI] = MEFS.[NPI]
		AND MEP.[ROW_DELETED] = 'N'
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Provider_Facility] MEPF
		ON MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] = MEPF.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		AND MEF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID] = MEPF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		AND MEPF.[ROW_DELETED] = 'N'
	WHERE (MEPF.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] IS NULL OR MEPF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID] IS NULL)  -- Only new records 


	-- Return success
	RETURN 0


END
GO
























--	--Insert known FKC clinics for all of the providers that we currently have
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
--	(
--		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
--		 [FACILITY_TYPE]
--		,[FACILITY_NAME]
--		,[ADDRESS_TYPE]
--		,[ADDRESS_1]
--		,[ADDRESS_2]
--		,[ADDRESS_3]
--		,[CITY]
--		,[STATE]
--		,[ZIP]
--		,[COUNTY]
--		,[COUNTRY]
--		,[PHONE]
--		,[ALTERNATE_PHONE]
--		,[EVENING_PHONE]
--		,[EMERGENCY_PHONE]
--		,[FAX]
--		,[EMAIL]
--		,[INTERNAL_FACILITY_ID]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[STATUS]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROBLEM]
--		--,[ROW_PROBLEM_DATE]
--		--,[ROW_PROBLEM_REASON]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT DISTINCT
--		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
--		 'FMC Facility' [FACILITY_TYPE]
--		,FIS.[FACILITY_NAME] [FACILITY_NAME]
--		,NULL [ADDRESS_TYPE]
--		,FIS.[FACILITY_ADDRESS1] [ADDRESS_1]
--		,FIS.[FACILITY_ADDRESS2] [ADDRESS_2]
--		,NULL [ADDRESS_3]
--		,FIS.[FACILITY_CITY] [CITY]
--		,FIS.[FACILITY_STATE] [STATE]
--		,FIS.[FACILITY_ZIP_CODE] [ZIP]
--		,NULL [COUNTY]
--		,'USA' [COUNTRY]
--		,FIS.[PHONE] [PHONE]
--		,NULL [ALTERNATE_PHONE]
--		,NULL [EVENING_PHONE]
--		,NULL [EMERGENCY_PHONE]
--		,NULL [FAX]
--		,NULL [EMAIL]
--		,FIS.[FACILITY_ID] [INTERNAL_FACILITY_ID]
--		,NULL [EXT_ID]
--		,NULL [EXT_ID_TYPE]
--		,'ACTIVE' [STATUS]
--		,'Membership_Export_Facility_UPSERT' [ROW_SOURCE]
--		,NULL [ROW_SOURCE_ID]

--		--,[ROW_PROBLEM]
--		--,[ROW_PROBLEM_DATE]
--		--,[ROW_PROBLEM_REASON]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]

--		,@GETDATE AS [ROW_CREATE_DATE]
--		,@GETDATE AS [ROW_UPDATE_DATE]
--	FROM #NPI_Facility --Get all NPI / FHP Facility ID relationships
--	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP --Limit to only the Providers (NPI) that are already loaded
--		ON #NPI_Facility.[NPI] = MEP.[NPI]
--	JOIN [FHP_ESCO].[ESCO].[FACILITY_INFORMATION_SOURCE] FIS --Get the addresses for the FHP facilities
--		ON #NPI_Facility.[FACILITY_ID] = FIS.[FACILITY_ID]
--	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Facility] MEF
--		ON #NPI_Facility.[FACILITY_ID] = MEF.[INTERNAL_FACILITY_ID]
--	WHERE FIS.[FACILITY_STATUS] = 'ACTIVE'
--	AND MEF.[INTERNAL_FACILITY_ID] IS NULL --ONLY NEW RECORDS
	




--	--Insert facilities that do not exactly match our known FMC facilities
--	----------------------------------------------------------------------------------------------------------
--	-- NOTE:  This will likely cause duplicate entries when the addresses do not exactly match.
--	--        This is a known risk, which could be mitigated with address standardization services (USPS, etc)
--	----------------------------------------------------------------------------------------------------------
--	;WITH NewData AS
--	(

--		SELECT
--			-- [FACILITY_TYPE]
--			--,[FACILITY_NAME]
--			--,[ADDRESS_TYPE]
--			 [ADDRESS_1]
--			,[ADDRESS_2]
--			,[ADDRESS_3]
--			,[CITY]
--			,[STATE]
--			,[ZIP]
--			,[COUNTY]
--			,[COUNTRY]
--			--,[PHONE]
--			--,[ALTERNATE_PHONE]
--			--,[EVENING_PHONE]
--			--,[EMERGENCY_PHONE]
--			--,[FAX]
--			--,[EMAIL]
--			--,[INTERNAL_FACILITY_ID]
--			--,[EXT_ID]
--			--,[EXT_ID_TYPE]
--			--,[STATUS]
--			--,[ROW_SOURCE]
--			--,[ROW_SOURCE_ID]
--		FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE]  --1247

--		EXCEPT

--		SELECT
--			-- [FACILITY_TYPE]
--			--,[FACILITY_NAME]
--			--,[ADDRESS_TYPE]
--			 [ADDRESS_1]
--			,[ADDRESS_2]
--			,[ADDRESS_3]
--			,[CITY]
--			,[STATE]
--			,[ZIP]
--			,[COUNTY]
--			,[COUNTRY]
--			--,[PHONE]
--			--,[ALTERNATE_PHONE]
--			--,[EVENING_PHONE]
--			--,[EMERGENCY_PHONE]
--			--,[FAX]
--			--,[EMAIL]
--			--,[INTERNAL_FACILITY_ID]
--			--,[EXT_ID]
--			--,[EXT_ID_TYPE]
--			--,[STATUS]
--			--,[ROW_SOURCE]
--			--,[ROW_SOURCE_ID]
--		FROM [FHPDW].[dbo].[Membership_Export_Facility]  --1247
--	)
--	INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
--	(
--		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
--		 [FACILITY_TYPE]
--		,[FACILITY_NAME]
--		,[ADDRESS_TYPE]
--		,[ADDRESS_1]
--		,[ADDRESS_2]
--		,[ADDRESS_3]
--		,[CITY]
--		,[STATE]
--		,[ZIP]
--		,[COUNTY]
--		,[COUNTRY]
--		,[PHONE]
--		,[ALTERNATE_PHONE]
--		,[EVENING_PHONE]
--		,[EMERGENCY_PHONE]
--		,[FAX]
--		,[EMAIL]
--		,[INTERNAL_FACILITY_ID]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[STATUS]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROBLEM]
--		--,[ROW_PROBLEM_DATE]
--		--,[ROW_PROBLEM_REASON]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		,[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
--		 NULL [FACILITY_TYPE]
--		,NULL [FACILITY_NAME]
--		,NULL [ADDRESS_TYPE]
--		,[ADDRESS_1]
--		,[ADDRESS_2]
--		,[ADDRESS_3]
--		,[CITY]
--		,[STATE]
--		,[ZIP]
--		,[COUNTY]
--		,[COUNTRY]
--		,NULL [PHONE]
--		,NULL [ALTERNATE_PHONE]
--		,NULL [EVENING_PHONE]
--		,NULL [EMERGENCY_PHONE]
--		,NULL [FAX]
--		,NULL [EMAIL]
--		,NULL [INTERNAL_FACILITY_ID]
--		,NULL [EXT_ID]
--		,NULL [EXT_ID_TYPE]
--		,'ACTIVE' [STATUS]
--		,'[FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE]' [ROW_SOURCE]
--		,NULL [ROW_SOURCE_ID]
--		--,[ROW_PROBLEM]
--		--,[ROW_PROBLEM_DATE]
--		--,[ROW_PROBLEM_REASON]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		,@GETDATE [ROW_CREATE_DATE]
--		,@GETDATE [ROW_UPDATE_DATE]
--	FROM NewData



----	-- UPDATE
----	UPDATE TheTable
----	SET
		
----		 [NPI] = Delta.[NPI]
----		,[FACILITY_ID] = Delta.[FACILITY_ID]
----		,[ROLE] = Delta.[ROLE]
----		,[START_DATE] = Delta.[START_DATE]
----		,[END_DATE] = Delta.[END_DATE]

----		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
----		,[ROW_UPDATE_DATE] = @GETDATE
----	FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities] TheTable
----	JOIN
----	(
----		SELECT
----			 [NPI]
----			,[FACILITY_ID]
----			,[ROLE]
----			,[START_DATE]
----			,[END_DATE]
----			--,[ROW_CREATE_DATE]
----			,@GETDATE AS [ROW_UPDATE_DATE]
----		FROM #TheSource TheSource
	
----		EXCEPT

----		SELECT
----			 [NPI]
----			,[FACILITY_ID]
----			,[ROLE]
----			,[START_DATE]
----			,[END_DATE]
----			--,[ROW_CREATE_DATE]
----			,@GETDATE AS [ROW_UPDATE_DATE]
----		FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities]
----	) Delta
----	ON TheTable.[NPI] = Delta.[NPI]
----	AND TheTable.[FACILITY_ID] = Delta.[FACILITY_ID];


--	-- Return success
--	RETURN 0


--END
--GO

