

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Facility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Facility_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Facility_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: October 14th 2019
	-- Description:	
	-- ==========================================================================================
	/*
		NOTE:  Need to add UPDATE logic once business rules are available.
	*/
	-- ==========================================================================================


	DECLARE @GETDATE DATETIME2 = GETDATE();



	-- PACK TEMP TABLE 
	IF OBJECT_ID('tempdb..#NPI_Facility') IS NOT NULL DROP TABLE #NPI_Facility

	CREATE TABLE #NPI_Facility
	(
		 [NPI] VARCHAR(50) NOT NULL
		,[FACILITY_ID] VARCHAR(50) NOT NULL
		,[ROLE] VARCHAR(500) NULL
		,[START_DATE] DATE NULL
		,[END_DATE] DATE NULL
	)


	INSERT INTO #NPI_Facility
	(
		 [NPI]
		,[FACILITY_ID]
		,[ROLE]
		,[START_DATE]
		,[END_DATE]
	)
	SELECT DISTINCT
		 [HCP_NPI] AS [NPI]
		,[FAC_ID] AS [FACILITY_ID]
		,[HCP_DL_ROLE_NM] AS [ROLE]
		,CAST([MBR_EFF_DT] AS DATE) AS [START_DATE]
		,CASE WHEN CAST([MBR_EXP_DT] AS DATE) = CAST('9999-12-31' AS DATE) THEN NULL ELSE CAST([MBR_EXP_DT] AS DATE) END AS [END_DATE]
	FROM OPENQUERY([KCNGX_PROVIDERS],
		'

		SELECT
			 HCP.HCP_NPI
			--,HCP.HCP_FRST_NM
			--,HCP.HCP_LAST_NM
			,FD.FAC_ID
			,FM.HCP_DL_ROLE_NM
			,FM.MBR_EFF_DT
			,FM.MBR_EXP_DT
		FROM PERS_DIAL.FAC_MEMBER FM
		JOIN PERS_DIAL.HCP HCP
			ON FM.HCP_ID = HCP.HCP_ID
		JOIN ORG_DIAL.FAC_DIAL FD
			ON FM.FAC_DIAL_ID = FD.FAC_DIAL_ID
		WHERE HCP.HCP_STS_CD = ''ACTV''
		--AND FM.MBR_EXP_DT = ''9999-12-31'' /*Remove this if you want to see the history of where HCPs were affiliated*/
		AND HCP.HCP_NPI IS NOT NULL
		AND FD.FAC_ID IS NOT NULL
		ORDER BY
			 HCP.HCP_ID
			,FD.FAC_ID
			,FM.MBR_EFF_DT
			,FM.MBR_EXP_DT
	
		')


	----Insert known FKC clinics for all of the providers that we currently have
	--INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
	--(
	--	-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
	--	 [FACILITY_TYPE]
	--	,[FACILITY_NAME]
	--	,[ADDRESS_TYPE]
	--	,[ADDRESS_1]
	--	,[ADDRESS_2]
	--	,[ADDRESS_3]
	--	,[CITY]
	--	,[STATE]
	--	,[ZIP]
	--	,[COUNTY]
	--	,[COUNTRY]
	--	,[PHONE]
	--	,[ALTERNATE_PHONE]
	--	,[EVENING_PHONE]
	--	,[EMERGENCY_PHONE]
	--	,[FAX]
	--	,[EMAIL]
	--	,[INTERNAL_FACILITY_ID]
	--	,[EXT_ID]
	--	,[EXT_ID_TYPE]
	--	,[STATUS]
	--	,[ROW_SOURCE]
	--	,[ROW_SOURCE_ID]
	--	--,[ROW_PROBLEM]
	--	--,[ROW_PROBLEM_DATE]
	--	--,[ROW_PROBLEM_REASON]
	--	--,[ROW_DELETED]
	--	--,[ROW_DELETED_DATE]
	--	--,[ROW_DELETED_REASON]
	--	,[ROW_CREATE_DATE]
	--	,[ROW_UPDATE_DATE]
	--)
	SELECT DISTINCT
		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		 MEFS.[FACILITY_TYPE]
		,MEFS.[FACILITY_NAME]
		,MEFS.[ADDRESS_TYPE]
		,MEFS.[ADDRESS_1]
		,MEFS.[ADDRESS_2]
		,MEFS.[ADDRESS_3]
		,MEFS.[CITY]
		,MEFS.[STATE]
		,MEFS.[ZIP]
		,MEFS.[COUNTY]
		,MEFS.[COUNTRY]
		,MEFS.[PHONE]
		,MEFS.[ALTERNATE_PHONE]
		,MEFS.[EVENING_PHONE]
		,MEFS.[EMERGENCY_PHONE]
		,MEFS.[FAX]
		,MEFS.[EMAIL]
		,#NPI_Facility.[FACILITY_ID] AS [INTERNAL_FACILITY_ID]
		,MEFS.[EXT_ID]
		,MEFS.[EXT_ID_TYPE]
		,'ACTIVE' [STATUS]
		,'Membership_Export_Facility_UPSERT' [ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]

		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]

		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]

	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE] MEFS
	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP --Limit to only the Providers (NPI) that are already loaded
		--ON #NPI_Facility.[NPI] = MEP.[NPI]
		ON MEFS.[NPI] = MEP.[NPI]
	JOIN [FHPDW].[dbo].[Membership_Export_Provider_Facility] MEPF
		ON MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] = MEPF.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
	LEFT JOIN #NPI_Facility --Get all NPI / FHP Facility ID relationships
		ON #NPI_Facility.[NPI] = MEFS.[NPI]


		--AND #NPI_Facility.[END_DATE] > GETDATE()  --Make sure it's still active before including our own internal clinic ID [OMITTED UNLESS BUSINESS REQUESTS IT]
	--LEFT JOIN [FHP_ESCO].[ESCO].[FACILITY_INFORMATION_SOURCE] FIS -- Lots of good data here, but not currently used
	--	ON #NPI_Facility.[FACILITY_ID] = FIS.[FACILITY_ID]


	LEFT JOIN [FHPDW].[dbo].[Membership_Export_Facility] MEF  --Add new Facility Types
		ON MEPF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID] = MEF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		AND MEFS.[FACILITY_TYPE] = MEF.[FACILITY_TYPE]
		AND MEF.[ROW_DELETED] = 'N'
	WHERE 1=2
		--OR MEF.[NPI] IS NULL --Limit inserts to only NPI and FACILITY_TYPE's that are not already found in the table
		OR MEF.[FACILITY_TYPE] IS NULL  --Limit inserts to only NPI and FACILITY_TYPE's that are not already found in the table

		--AND FIS.[FACILITY_STATUS] = 'ACTIVE'
		--AND MEF.[INTERNAL_FACILITY_ID] IS NULL --ONLY NEW RECORDS



		SELECT * FROM [FHPDW].[dbo].[Membership_Export_Facility]

		SELECT * FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE] 


	--Insert facilities that do not exactly match our known FMC facilities
	----------------------------------------------------------------------------------------------------------
	-- NOTE:  This will likely cause duplicate entries when the addresses do not exactly match.
	--        This is a known risk, which could be mitigated with address standardization services (USPS, etc)
	----------------------------------------------------------------------------------------------------------
	;WITH NewData AS
	(

		SELECT
			-- [FACILITY_TYPE]
			--,[FACILITY_NAME]
			--,[ADDRESS_TYPE]
			 [ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[COUNTRY]
			--,[PHONE]
			--,[ALTERNATE_PHONE]
			--,[EVENING_PHONE]
			--,[EMERGENCY_PHONE]
			--,[FAX]
			--,[EMAIL]
			--,[INTERNAL_FACILITY_ID]
			--,[EXT_ID]
			--,[EXT_ID_TYPE]
			--,[STATUS]
			--,[ROW_SOURCE]
			--,[ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE]  --1247

		EXCEPT

		SELECT
			-- [FACILITY_TYPE]
			--,[FACILITY_NAME]
			--,[ADDRESS_TYPE]
			 [ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[COUNTRY]
			--,[PHONE]
			--,[ALTERNATE_PHONE]
			--,[EVENING_PHONE]
			--,[EMERGENCY_PHONE]
			--,[FAX]
			--,[EMAIL]
			--,[INTERNAL_FACILITY_ID]
			--,[EXT_ID]
			--,[EXT_ID_TYPE]
			--,[STATUS]
			--,[ROW_SOURCE]
			--,[ROW_SOURCE_ID]
		FROM [FHPDW].[dbo].[Membership_Export_Facility]  --1247
	)
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
	(
		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		 [FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		 NULL [FACILITY_TYPE]
		,NULL [FACILITY_NAME]
		,NULL [ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,NULL [PHONE]
		,NULL [ALTERNATE_PHONE]
		,NULL [EVENING_PHONE]
		,NULL [EMERGENCY_PHONE]
		,NULL [FAX]
		,NULL [EMAIL]
		,NULL [INTERNAL_FACILITY_ID]
		,NULL [EXT_ID]
		,NULL [EXT_ID_TYPE]
		,'ACTIVE' [STATUS]
		,'[FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE]' [ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		,@GETDATE [ROW_CREATE_DATE]
		,@GETDATE [ROW_UPDATE_DATE]
	FROM NewData



--	-- UPDATE
--	UPDATE TheTable
--	SET
		
--		 [NPI] = Delta.[NPI]
--		,[FACILITY_ID] = Delta.[FACILITY_ID]
--		,[ROLE] = Delta.[ROLE]
--		,[START_DATE] = Delta.[START_DATE]
--		,[END_DATE] = Delta.[END_DATE]

--		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE] = @GETDATE
--	FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities] TheTable
--	JOIN
--	(
--		SELECT
--			 [NPI]
--			,[FACILITY_ID]
--			,[ROLE]
--			,[START_DATE]
--			,[END_DATE]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM #TheSource TheSource
	
--		EXCEPT

--		SELECT
--			 [NPI]
--			,[FACILITY_ID]
--			,[ROLE]
--			,[START_DATE]
--			,[END_DATE]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities]
--	) Delta
--	ON TheTable.[NPI] = Delta.[NPI]
--	AND TheTable.[FACILITY_ID] = Delta.[FACILITY_ID];


	-- Return success
	RETURN 0


END
GO

