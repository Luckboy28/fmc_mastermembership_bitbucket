

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Facility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Facility_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Facility_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: October 14th 2019
	-- Description:	
	-- ==========================================================================================
	/*
		NOTE:  Need to add UPDATE logic once business rules are available.
	*/
	-- ==========================================================================================


	DECLARE @GETDATE DATETIME2 = GETDATE();


	-----------------------------------STOPPED RE-WRITE HERE



	--Insert facilities that do not exactly match our known FMC facilities
	----------------------------------------------------------------------------------------------------------
	-- NOTE:  This will likely cause duplicate entries when the addresses do not exactly match.
	--        This is a known risk, which could be mitigated with address standardization services (USPS, etc)
	----------------------------------------------------------------------------------------------------------
	;WITH NewData AS
	(

		SELECT
			-- [FACILITY_TYPE]
			--,[FACILITY_NAME]
			--,[ADDRESS_TYPE]
			 [ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[COUNTRY]
			--,[PHONE]
			--,[ALTERNATE_PHONE]
			--,[EVENING_PHONE]
			--,[EMERGENCY_PHONE]
			--,[FAX]
			--,[EMAIL]
			--,[INTERNAL_FACILITY_ID]
			--,[EXT_ID]
			--,[EXT_ID_TYPE]
			--,[STATUS]
			--,[ROW_SOURCE]
			--,[ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE]  --1247

		EXCEPT

		SELECT
			-- [FACILITY_TYPE]
			--,[FACILITY_NAME]
			--,[ADDRESS_TYPE]
			 [ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[COUNTRY]
			--,[PHONE]
			--,[ALTERNATE_PHONE]
			--,[EVENING_PHONE]
			--,[EMERGENCY_PHONE]
			--,[FAX]
			--,[EMAIL]
			--,[INTERNAL_FACILITY_ID]
			--,[EXT_ID]
			--,[EXT_ID_TYPE]
			--,[STATUS]
			--,[ROW_SOURCE]
			--,[ROW_SOURCE_ID]
		FROM [FHPDW].[dbo].[Membership_Export_Facility]  --1247
	)
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
	(
		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		 [FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
		 NULL [FACILITY_TYPE]
		,NULL [FACILITY_NAME]
		,NULL [ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,NULL [PHONE]
		,NULL [ALTERNATE_PHONE]
		,NULL [EVENING_PHONE]
		,NULL [EMERGENCY_PHONE]
		,NULL [FAX]
		,NULL [EMAIL]
		,NULL [INTERNAL_FACILITY_ID]
		,NULL [EXT_ID]
		,NULL [EXT_ID_TYPE]
		,'ACTIVE' [STATUS]
		,'[FHPDataMarts].[dbo].[Membership_Export_Facility_SOURCE]' [ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		,@GETDATE [ROW_CREATE_DATE]
		,@GETDATE [ROW_UPDATE_DATE]
	FROM NewData



--	-- UPDATE
--	UPDATE TheTable
--	SET
		
--		 [NPI] = Delta.[NPI]
--		,[FACILITY_ID] = Delta.[FACILITY_ID]
--		,[ROLE] = Delta.[ROLE]
--		,[START_DATE] = Delta.[START_DATE]
--		,[END_DATE] = Delta.[END_DATE]

--		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
--		,[ROW_UPDATE_DATE] = @GETDATE
--	FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities] TheTable
--	JOIN
--	(
--		SELECT
--			 [NPI]
--			,[FACILITY_ID]
--			,[ROLE]
--			,[START_DATE]
--			,[END_DATE]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM #TheSource TheSource
	
--		EXCEPT

--		SELECT
--			 [NPI]
--			,[FACILITY_ID]
--			,[ROLE]
--			,[START_DATE]
--			,[END_DATE]
--			--,[ROW_CREATE_DATE]
--			,@GETDATE AS [ROW_UPDATE_DATE]
--		FROM [FHPDW].[dbo].[Membership_Export_Provider_Facilities]
--	) Delta
--	ON TheTable.[NPI] = Delta.[NPI]
--	AND TheTable.[FACILITY_ID] = Delta.[FACILITY_ID];


	-- Return success
	RETURN 0


END
GO

