USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Eligibility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Eligibility_UPSERT;
GO


CREATE PROCEDURE [dbo].[Member_Eligibility_UPSERT]

	@MEMBER_ELIGIBILITY_STAGING_ROW_ID INT NULL,
	@MHK_INTERNAL_ID INT NULL,
	@MEDHOK_ID VARCHAR(50) NULL,
	@SSN VARCHAR(11) NULL,
	@HICN VARCHAR(12) NULL,
	@CLAIM_SUBSCRIBER_ID VARCHAR(50) NULL,
	@MBI VARCHAR(11) NULL,
	@MEDICAID_NO VARCHAR(50) NULL,
	@MRN VARCHAR(50) NULL,
	@EXT_ID VARCHAR(50) NULL,
	@EXT_ID_TYPE VARCHAR(50) NULL,
	@EXT_ID_2 VARCHAR(50) NULL,
	@EXT_ID_TYPE_2 VARCHAR(50) NULL,
	@EXT_ID_3 VARCHAR(50) NULL,
	@EXT_ID_TYPE_3 VARCHAR(50) NULL,
	@ESCO_ID VARCHAR(50) NULL,
	@HLTH_PLN_SYSID INT NULL,
	@HLTH_PLN_PROD_LINE VARCHAR(50) NULL,
	@HLTH_PLN_RPT_GRP VARCHAR(50) NULL,
	@HLTH_PLN_STD_CARRIER_CD VARCHAR(50) NULL,
	@PLAN_NAME VARCHAR(50) NULL,
	@START_DATE DATE NULL,
	@TERM_DATE DATE NULL,
	@LOB_VENDOR VARCHAR(50) NULL,
	@LOB_TYPE VARCHAR(50) NULL,
	@ROW_SOURCE VARCHAR(500) NULL,
	@ROW_SOURCE_ID VARCHAR(50) NULL
AS
BEGIN


	/*
	===================================================================================================================
	Author:		  David M. Wilson
	Create date:  September 15th 2017
	Description:  This stored procedure takes records from the Member_Eligibility_Staging
	              table (via SSIS) and attempts to upsert it into the Member Eligibilty table. 
	===================================================================================================================

	Additional Notes:  N/A

	===================================================================================================================
	*/


	-- Raise a stored procedure error if required fields are missing
	IF @MEMBER_ELIGIBILITY_STAGING_ROW_ID IS NULL OR @ROW_SOURCE IS NULL OR @LOB_VENDOR IS NULL OR @LOB_TYPE IS NULL
	BEGIN
		-- INVALID KEY, EXIT WITH FAILURE
		RAISERROR (15600,-1,-1, 'dbo.Member_Eligibility_UPSERT')
		RETURN 1
	END





	/**********************************************************************************************************************

		Create some variables for later use.

	**********************************************************************************************************************/


	-- Find the member
	DECLARE @MEMBER_MASTER_ROW_COUNT AS INT = NULL;
	DECLARE @MEMBER_MASTER_ROW_ID AS INT = NULL;


	-- Find the eligibility plan
	DECLARE @MEMBER_ELIGIBILITY_ROW_COUNT AS INT = NULL;
	DECLARE @MEMBER_ELIGIBILITY_ROW_ID AS INT = NULL;


	-- Find the current member type
	DECLARE @CURRENT_MEMBER_LOB_VENDOR AS VARCHAR(20) = NULL;
	DECLARE @CURRENT_MEMBER_LOB_TYPE AS VARCHAR(20) = NULL;

	DECLARE @CURRENT_MEMBER_LOB_RANK AS INT = NULL;
	DECLARE @LOB_RANK AS INT = NULL;


	-- Used for gathering stats on key lookups.  Used for both Member and Eligibility lookups.
	DECLARE @KEY_SEARCH_RESULTS AS TABLE
	(
		[MEMBER_MASTER_ROW_ID] INT
	)

	-- Used for checking to see if an update was performed
	DECLARE @EXISTING_ELIGIBILTY_UPDATE AS TABLE
	(
		[MEMBER_ELIGIBILITY_ROW_ID] INT
	)

	-- Used for deciding if a record can be updated or inserted
	DECLARE @EXISTING_MEMBER_ELIGIBILITY_ROW_ID AS INT



	/**********************************************************************************************************************

		Do initial data verfication checks.  If any of these checks fail, the originating row is flagged as a
		"problem row" and this procedure will gracefully abort.

	**********************************************************************************************************************/


	-- In order to be inserted, a record must have a start date, and it must not be later than the term date.
	-- Flag the record if this bad condition is met.
	IF (@START_DATE IS NULL) OR ((@TERM_DATE IS NOT NULL) AND (@START_DATE > @TERM_DATE))
	BEGIN

		-- If insert could not happy, update the record with the error message.
		UPDATE [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
		SET
			[ROW_PROBLEM] = 'Y',
			[ROW_PROBLEM_REASON] = 'START_DATE must exist and it cannot be later than TERM_DATE.',
			[ROW_PROBLEM_DATE] = GETDATE()
		WHERE [MEMBER_ELIGIBILITY_STAGING_ROW_ID] = @MEMBER_ELIGIBILITY_STAGING_ROW_ID

		-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
		RETURN 0

	END





	--/**********************************************************************************************************************

	--	Before we begin, expire any current eligibility records that have lapsed (termination date < today).
		
	--	Note:  The presence of a termination date does not mean that the record is terminated.

	--**********************************************************************************************************************/

	--	UPDATE [FHPDW].[dbo].[Member_Eligibility]
	--	SET
	--			[TERM_REASON] = 'LOB Expired'
	--		,[STATUS] = 'INACTIVE'
	--	WHERE [TERM_DATE] <= GETDATE()
	--	AND [TERM_REASON] <> 'Replaced by another LOB'
	--	--AND [STATUS] = 'ACTIVE'





	/****************************************************************************************************************************************************************
	
		See how many members we can match to.
			0: Insert dummy record with just keys.  This should no longer happen, now that keys are pre-loaded.  Might remove in the future.
			1: Is the new type rank <= current type rank?   For example, CSNP is rank 1, and beats an ESCO at rank 3. Smaller rank number = higher priority.
				New Rank Lower (higher priority):  Begin the process of adding/updating the record based on dates.
				New Rank Higher (lower priority):  Flag the stating record as being processed and unusable.
				New Rank same as Current:  Update the current records based on dates.

	****************************************************************************************************************************************************************/


	;WITH AllKeys AS
	(
		-- Historical
		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE ROW_DELETED = 'N'

		UNION ALL

		-- Current
		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
		FROM [FHPDW].[dbo].[Member_Master]
		WHERE ROW_DELETED = 'N'
	)
	INSERT INTO @KEY_SEARCH_RESULTS
	(
		[MEMBER_MASTER_ROW_ID]
	)
	SELECT DISTINCT
		[MEMBER_MASTER_ROW_ID]
	FROM AllKeys
	WHERE 1=2
	OR @MHK_INTERNAL_ID = AllKeys.[MHK_INTERNAL_ID]
	OR @MEDHOK_ID = AllKeys.[MEDHOK_ID]
	OR @SSN = AllKeys.[SSN]
	OR @HICN = AllKeys.[HICN]
	OR @CLAIM_SUBSCRIBER_ID = AllKeys.[CLAIM_SUBSCRIBER_ID]
	OR @MBI = AllKeys.[MBI]
	OR @MEDICAID_NO = AllKeys.[MEDICAID_NO]
	OR @MRN = AllKeys.[MRN]
	OR CASE WHEN @EXT_ID IS NOT NULL AND @EXT_ID_TYPE IS NOT NULL THEN CONCAT(@EXT_ID,':',@EXT_ID_TYPE) ELSE NULL END = AllKeys.[EXT_ID_AND_TYPE]


	-- Get number of matching results
	SELECT @MEMBER_MASTER_ROW_COUNT = COUNT(1) FROM @KEY_SEARCH_RESULTS;


	-- Get the record, if it's unique
	IF @MEMBER_MASTER_ROW_COUNT = 1
	BEGIN
		SELECT @MEMBER_MASTER_ROW_ID = [MEMBER_MASTER_ROW_ID] FROM @KEY_SEARCH_RESULTS
	END



	-- No matching member rows: This is an orphan record.
	-- Insert placeholder into the Member table.
	IF @MEMBER_MASTER_ROW_COUNT = 0
	BEGIN

		-- Create table variable to hold the primary key for the record we're about to create, after it's inserted.
		DECLARE @MEMBER_MASTER_INSERTED AS TABLE
		(
			[MEMBER_MASTER_ROW_ID] INT NOT NULL
		)

		INSERT INTO [FHPDW].[dbo].[Member_Master]
		(
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]

			,[STATUS]
			,[LOB_VENDOR]
			,[LOB_TYPE]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
		)
		OUTPUT INSERTED.[MEMBER_MASTER_ROW_ID] INTO @MEMBER_MASTER_INSERTED
		VALUES
		(
			 @MHK_INTERNAL_ID
			,@MEDHOK_ID
			,@SSN
			,@HICN
			,@CLAIM_SUBSCRIBER_ID
			,@MBI
			,@MEDICAID_NO
			,@MRN
			,@EXT_ID
			,@EXT_ID_TYPE

			,CASE WHEN (@TERM_DATE IS NULL) OR (@TERM_DATE > GETDATE()) THEN 'ACTIVE' ELSE 'INACTIVE' END  --[STATUS]
			,@LOB_VENDOR
			,@LOB_TYPE

			,@ROW_SOURCE  --[ROW_SOURCE[
			,@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
			,'Y'  --[ROW_PROBLEM]
			,GETDATE()  --[ROW_PROBLEM_DATE]
			,'Placeholder. Member data not yet loaded.'  --[ROW_PROBLEM_REASON]
		)


		-- Grab the row ID of the member table that was just inserted
		SELECT TOP 1 @MEMBER_MASTER_ROW_ID = MEMBER_MASTER_ROW_ID FROM @MEMBER_MASTER_INSERTED;




		-- Insert a record into [Member_Master_KeyHistory] if none is found.  This is done now, so that the following [Member_MedHOK_Crosswalk] updates
		-- can take place.  [Member_MedHOK_Crosswalk] fetches MEMBER_MASTER_ROW_ID from [Member_Master_KeyHistory].
		IF (@MEDHOK_ID IS NOT NULL) AND (NOT EXISTS (SELECT [MEDHOK_ID] FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] WHERE [MEDHOK_ID] = @MEDHOK_ID))
		BEGIN

			WITH UniqueKeyCombo AS
			(
				-- Select the unique keys
				SELECT
					 @MEMBER_MASTER_ROW_ID AS [MEMBER_MASTER_ROW_ID]
					,@MHK_INTERNAL_ID AS [MHK_INTERNAL_ID]
					,@MEDHOK_ID AS [MEDHOK_ID]
					,@SSN AS [SSN]
					,@HICN AS [HICN]
					,@CLAIM_SUBSCRIBER_ID AS [CLAIM_SUBSCRIBER_ID]
					,@MBI AS [MBI]
					,@MEDICAID_NO AS [MEDICAID_NO]
					,@MRN AS [MRN]
					,@EXT_ID AS [EXT_ID]
					,@EXT_ID_TYPE AS [EXT_ID_TYPE]
					,@EXT_ID_2 AS [EXT_ID_2]
					,@EXT_ID_TYPE_2 AS [EXT_ID_TYPE_2]
					,@EXT_ID_3 AS [EXT_ID_3]
					,@EXT_ID_TYPE_3 AS [EXT_ID_TYPE_3]

				EXCEPT

				-- Unless it already exists
				SELECT
					 [MEMBER_MASTER_ROW_ID]
					,[MHK_INTERNAL_ID]
					,[MEDHOK_ID]
					,[SSN]
					,[HICN]
					,[CLAIM_SUBSCRIBER_ID]
					,[MBI]
					,[MEDICAID_NO]
					,[MRN]
					,[EXT_ID]
					,[EXT_ID_TYPE]
					,[EXT_ID_2]
					,[EXT_ID_TYPE_2]
					,[EXT_ID_3]
					,[EXT_ID_TYPE_3]
				FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
			)
			INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]

				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
			)
			SELECT
				 UniqueKeyCombo.[MEMBER_MASTER_ROW_ID]
				,UniqueKeyCombo.[MHK_INTERNAL_ID]
				,UniqueKeyCombo.[MEDHOK_ID]
				,UniqueKeyCombo.[SSN]
				,UniqueKeyCombo.[HICN]
				,UniqueKeyCombo.[CLAIM_SUBSCRIBER_ID]
				,UniqueKeyCombo.[MBI]
				,UniqueKeyCombo.[MEDICAID_NO]
				,UniqueKeyCombo.[MRN]
				,UniqueKeyCombo.[EXT_ID]
				,UniqueKeyCombo.[EXT_ID_TYPE]
				,UniqueKeyCombo.[EXT_ID_2]
				,UniqueKeyCombo.[EXT_ID_TYPE_2]
				,UniqueKeyCombo.[EXT_ID_3]
				,UniqueKeyCombo.[EXT_ID_TYPE_3]

				,'Eligibility UPSERT Proc: MEMBER_ELIGIBILITY_STAGING_ROW_ID'  --[ROW_SOURCE]
				,@MEMBER_ELIGIBILITY_STAGING_ROW_ID  --[ROW_SOURCE_ID]
			FROM UniqueKeyCombo  --This is suppose to be empty, by design, if the inserted keys were not unique (and therefore add no value to this table)

		END



		-- Insert into the MedHOK Crosswalk if not found
		IF (@MEDHOK_ID IS NOT NULL) AND (NOT EXISTS (SELECT [MEDHOK_ID] FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] WHERE [MEDHOK_ID] = @MEDHOK_ID))
		BEGIN

			-- Insert the "Primary" records
			INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MEDHOK_ID]
				,[MEDHOK_ID_TYPE]
			)
			SELECT
				 SRC.[MEMBER_MASTER_ROW_ID]
				,SRC.[MEDHOK_ID]
				,SRC.[MEDHOK_ID_TYPE]
			FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
			LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
			ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
			WHERE SRC.[MEDHOK_ID] IS NOT NULL
			AND TBL.[MEDHOK_ID] IS NULL
			AND SRC.[MEDHOK_ID_TYPE] = 'PRIMARY';


			-- Insert the "Duplicate" records
			INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MEDHOK_ID]
				,[MEDHOK_ID_TYPE]
			)
			SELECT
				 SRC.[MEMBER_MASTER_ROW_ID]
				,SRC.[MEDHOK_ID]
				,'DUPLICATE'  -- This is hard-coded, to ensure that only one "Primary" record is received
			FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
			LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
			ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
			WHERE SRC.[MEDHOK_ID] IS NOT NULL
			AND TBL.[MEDHOK_ID] IS NULL

		END

	END




	/**************************************************************************************************************************************

		 This eligibility record can be uniquely associated with a single member, either because the member existed and the keys uniquely matched, or because the member didn't exist
		 and the a placeholder was just created (which generated a new MEMBER_MASTER_ROW_ID).

	 **************************************************************************************************************************************/
	IF @MEMBER_MASTER_ROW_COUNT <= 1
	BEGIN


		/*
			Determine of an update or insert is nessassary.

			If an existing eligibility record matches the new record on the following fields, perform an update.  Otherwise, insert.
				* LOB_VENDOR
				* LOB_TYPE
				* START_DATE
		*/


		SELECT DISTINCT TOP 1  --Records are inserted one at a time, so there should only ever be one record with these qualities
			@EXISTING_MEMBER_ELIGIBILITY_ROW_ID = Eli.[MEMBER_ELIGIBILITY_ROW_ID]
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
		WHERE ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		AND ELI.[LOB_VENDOR] = @LOB_VENDOR
		AND ELI.[LOB_TYPE] = @LOB_TYPE
		AND ELI.[START_DATE] = @START_DATE


		-- If the existing eligibility row is not found, then no update is possible.  Insert record.
		IF @EXISTING_MEMBER_ELIGIBILITY_ROW_ID IS NULL
		BEGIN

			INSERT INTO [FHPDW].[dbo].[Member_Eligibility]
			(
				-- [MEMBER_ELIGIBILITY_ROW_ID]
				 [MEMBER_MASTER_ROW_ID]
				,[ESCO_ID]
				,[HLTH_PLN_SYSID]
				,[HLTH_PLN_PROD_LINE]
				,[HLTH_PLN_RPT_GRP]
				,[HLTH_PLN_STD_CARRIER_CD]
				,[PLAN_NAME]
				,[START_DATE]
				,[TERM_DATE]
				,[TERM_REASON]
				,[STATUS]

				,[LOB_VENDOR]
				,[LOB_TYPE]
				,[MEMBER_ELIGIBILITY_STAGING_ROW_ID]

				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				--,[ROW_PROCESSED]
				--,[ROW_PROCESSED_DATE]
				--,[ROW_DELETED]
				--,[ROW_DELETED_DATE]
				--,[ROW_DELETED_REASON]
				--,[ROW_CREATE_DATE]
				--,[ROW_UPDATE_DATE]
			)
			VALUES
			(	
				@MEMBER_MASTER_ROW_ID,  --@MEMBER_MASTER_ROW_ID   (from lookup, or insert)
				@ESCO_ID,  --[ESCO_ID]
				@HLTH_PLN_SYSID,  --[HLTH_PLN_SYSID]
				@HLTH_PLN_PROD_LINE,  --[HLTH_PLN_PROD_LINE]
				@HLTH_PLN_RPT_GRP,  --[HLTH_PLN_RPT_GRP]
				@HLTH_PLN_STD_CARRIER_CD,  --[HLTH_PLN_STD_CARRIER_CD]
				@PLAN_NAME,  --[PLAN_NAME]
				@START_DATE,  --[START_DATE]
				@TERM_DATE,  --@TERM_DATE,  --[TERM_DATE]
				NULL,  --[TERM_REASON]  --This gets recacluated later, this is just a placeholder.   
				'INACTIVE', --[STATUS]  --This gets recacluated later, this is just a placeholder.   
				@LOB_VENDOR,  --[LOB_VENDOR]
				@LOB_TYPE,  --[LOB_TYPE]
				@MEMBER_ELIGIBILITY_STAGING_ROW_ID,  --[MEMBER_ELIGIBILITY_STAGING_ROW_ID]

				@ROW_SOURCE,  --[ROW_SOURCE]
				@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
					--[ROW_PROBLEM]
					--[ROW_PROBLEM_DATE]
					--[ROW_PROBLEM_REASON]
					--[ROW_DELETED]
					--[ROW_DELETED_DATE]
					--[ROW_DELETED_REASON]
					--[ROW_CREATE_DATE]
					--[ROW_UPDATE_DATE]
			)

		END


		-- If an existing eligibility row is found, then an update is possible.  Update record.
		IF @EXISTING_MEMBER_ELIGIBILITY_ROW_ID IS NOT NULL
		BEGIN

			UPDATE ELI
				SET
					 [ESCO_ID] = @ESCO_ID
					,[HLTH_PLN_SYSID] = @HLTH_PLN_SYSID
					,[HLTH_PLN_PROD_LINE] = @HLTH_PLN_PROD_LINE
					,[HLTH_PLN_RPT_GRP] = @HLTH_PLN_RPT_GRP
					,[HLTH_PLN_STD_CARRIER_CD] = @HLTH_PLN_STD_CARRIER_CD
					,[PLAN_NAME] = @PLAN_NAME

					,[TERM_DATE] = @TERM_DATE
					,[TERM_REASON] = NULL   -- Recalculated later, NULL by default.
					--,[STATUS] = 'INACTIVE'   -- Recalculated later, "Inactive" by default.
					,[MEMBER_ELIGIBILITY_STAGING_ROW_ID] = @MEMBER_ELIGIBILITY_STAGING_ROW_ID
			OUTPUT INSERTED.[MEMBER_ELIGIBILITY_ROW_ID] INTO @EXISTING_ELIGIBILTY_UPDATE   -- Capture the ROW_ID.  Will be used to determine if an update was made, and therefore if a insert is needed. (?)
			FROM [FHPDW].[dbo].[Member_Eligibility] ELI
			WHERE ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
			AND ELI.[LOB_VENDOR] = @LOB_VENDOR

			AND ELI.[LOB_TYPE] = @LOB_TYPE
			AND ELI.[START_DATE] = @START_DATE

		END



	/**************************************************************************************************************************************

		Recalculate TERM_DATE, TERM_REASON, and STATUS based on all of the member's eligibility records.

	**************************************************************************************************************************************/


			---------------------------------------------------------------------------------------------------------
			-- STATUS
			---------------------------------------------------------------------------------------------------------

			;WITH STATUS_ActiveRecords AS
			(
				SELECT
					 [MEMBER_ELIGIBILITY_ROW_ID]
					,[START_DATE]
					,[TERM_DATE]
					--,[TERM_REASON]
					,ELI.[LOB_VENDOR]
					,ELI.[LOB_TYPE]
					,[MEMBER_MASTER_ROW_ID]
					,ROW_NUMBER() OVER (ORDER BY LOBH.[LOB_RANK] ASC, [START_DATE] DESC, ISNULL([TERM_DATE],CAST('9999-12-31' AS DATETIME2)) DESC) [Rank]   -- NOTE:  For sorting reasons, NULL is assigned the latest date that SQL supports, because it has higher priority over normal dates.
				FROM [FHPDW].[dbo].[Member_Eligibility] ELI
				JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
				ON ELI.[LOB_TYPE] = LOBH.[LOB_TYPE]
				AND ELI.[LOB_VENDOR] = LOBH.[LOB_VENDOR]
				WHERE ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
				AND [START_DATE] < GETDATE() AND ([TERM_DATE] IS NULL OR [TERM_DATE] > GETDATE() OR [TERM_REASON] = 'Replaced by another LOB')
			)

			---------------------------------------------------------------------------------------------------------
			-- TERM DATE: TERMINATED BY ANOTHER LOB
			---------------------------------------------------------------------------------------------------------

			-- Start the process of determining the TERM_DATE by gathering the relavent data.
			,TERM_DATE_LOB_REPLACEMENT_TheData AS
			(
				SELECT 
					 [MEMBER_ELIGIBILITY_ROW_ID]
					,[START_DATE]
					,[TERM_DATE]
					--,[TERM_REASON]
					--,ELI.[LOB_VENDOR]
					--,ELI.[LOB_TYPE]
					,LOBH.[LOB_RANK]
					,ELI.[ROW_CREATE_DATE]
				FROM [FHPDW].[dbo].[Member_Eligibility] ELI
				JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
				ON ELI.[LOB_TYPE] = LOBH.[LOB_TYPE]
				AND ELI.[LOB_VENDOR] = LOBH.[LOB_VENDOR]
				WHERE 1=1
				AND [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
				AND ELI.[ROW_DELETED] = 'N'
				--AND [START_DATE] < GETDATE()
				--AND ([TERM_DATE] >= GETDATE() OR [TERM_REASON] = 'Replaced by another LOB')

			)
			-- Cross apply the data to itself, so that we can find data relationships between rows
			,TERM_DATE_LOB_REPLACEMENT_CrossAppliedData AS
			(
				SELECT * FROM TERM_DATE_LOB_REPLACEMENT_TheData A
				CROSS APPLY
				(
					SELECT 
						 B.[MEMBER_ELIGIBILITY_ROW_ID] AS [MEMBER_ELIGIBILITY_ROW_ID_REPLACING_RECORD]
						,B.[ROW_CREATE_DATE] AS [ROW_CREATE_DATE_REPLACING_RECORD]
						,B.[LOB_RANK] AS [LOB_RANK_REPLACING_RECORD]
						,DATEADD(day,-1,B.[START_DATE]) AS [START_DATE_REPLACING_RECORD]
						,DATEDIFF(day,A.[START_DATE],B.[START_DATE]) [DateDiff]
					FROM TERM_DATE_LOB_REPLACEMENT_TheData B
					WHERE A.[MEMBER_ELIGIBILITY_ROW_ID] <> B.[MEMBER_ELIGIBILITY_ROW_ID]
					AND B.[LOB_RANK] <= A.[LOB_RANK]
					AND A.[TERM_DATE] IS NULL
				) TERM_DATE_LOB_REPLACEMENT_TheDataAlias
			)
			-- Rank the cross apply, so that we can later extract only the top ranked rows -- resulting in the data we want
			,TERM_DATE_LOB_REPLACEMENT_CrossAppliedDataRanked AS
			(
				SELECT
					CAD.*
					,ROW_NUMBER() OVER (PARTITION BY [MEMBER_ELIGIBILITY_ROW_ID] ORDER BY CAD.[DateDiff] ASC, CAD.[LOB_RANK_REPLACING_RECORD] ASC, CAD.[ROW_CREATE_DATE_REPLACING_RECORD] ASC) AS [Rank]
				FROM TERM_DATE_LOB_REPLACEMENT_CrossAppliedData CAD
				WHERE [DateDiff] >= 0
			)
			-- Grab the top ranked rows only.
			,TERM_DATE_LOB_REPLACEMENT AS
			(
				SELECT 
					 [MEMBER_ELIGIBILITY_ROW_ID]
					,[TERM_DATE]
					,[START_DATE_REPLACING_RECORD] AS [TERM_DATE_LOB_REPLACEMENT]  -- The start date (minus 1 day) of the new LOB becomes the old LOB's term date.
				FROM TERM_DATE_LOB_REPLACEMENT_CrossAppliedDataRanked
				WHERE [Rank] = 1
			)
			--
			-- Prep the final data for updating the Eligibility table
			--
			,FinalData AS
			(
				SELECT
					 ELI.[MEMBER_ELIGIBILITY_ROW_ID]
					,CASE 
						WHEN STATUS_ActiveRecords.[Rank] = 1
							THEN NULL
						ELSE
							CASE 
								WHEN TERM_DATE_LOB_REPLACEMENT.[TERM_DATE_LOB_REPLACEMENT] IS NOT NULL
									THEN TERM_DATE_LOB_REPLACEMENT.[TERM_DATE_LOB_REPLACEMENT]
								ELSE ELI.[TERM_DATE]
							END
					 END AS [TERM_DATE]
					,CASE 
						WHEN STATUS_ActiveRecords.[Rank] = 1
							THEN NULL
						ELSE
							CASE
								WHEN ELI.[TERM_DATE] IS NOT NULL AND ELI.[TERM_DATE] <= GETDATE() 
									THEN 'LOB Expired'
								ELSE
									CASE
										WHEN TERM_DATE_LOB_REPLACEMENT.[TERM_DATE_LOB_REPLACEMENT] IS NOT NULL
											THEN 'Replaced by another LOB'
										ELSE
											NULL
									END
							END
					 END AS [TERM_REASON]
					,CASE WHEN STATUS_ActiveRecords.[Rank] = 1 THEN 'ACTIVE' ELSE 'INACTIVE' END [STATUS]   -- Note, the Active record will be 1, and the others will be NULL.
				FROM [FHPDW].[dbo].[Member_Eligibility] ELI
				LEFT OUTER JOIN TERM_DATE_LOB_REPLACEMENT
				ON TERM_DATE_LOB_REPLACEMENT.[MEMBER_ELIGIBILITY_ROW_ID] = ELI.[MEMBER_ELIGIBILITY_ROW_ID]
				LEFT OUTER JOIN STATUS_ActiveRecords
				ON STATUS_ActiveRecords.[MEMBER_ELIGIBILITY_ROW_ID] = Eli.[MEMBER_ELIGIBILITY_ROW_ID]
				WHERE ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
				AND ELI.[ROW_DELETED] = 'N'

				EXCEPT  -- This allows us to only update records where there are changes, instead of every record on every load
			
				SELECT
					  [MEMBER_ELIGIBILITY_ROW_ID]
					 ,[TERM_DATE]
					 ,[TERM_REASON]
					 ,[STATUS]
				FROM [FHPDW].[dbo].[Member_Eligibility]
				WHERE [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
				AND [ROW_DELETED] = 'N'
			)
			-- Perform the update
			UPDATE Eli_Production
			SET
				 Eli_Production.[TERM_DATE] = FinalData.[TERM_DATE]
				,Eli_Production.[TERM_REASON] = FinalData.[TERM_REASON]
				,Eli_Production.[STATUS] = FinalData.[STATUS]
			FROM [FHPDW].[dbo].[Member_Eligibility] Eli_Production
			JOIN FinalData
			ON FinalData.[MEMBER_ELIGIBILITY_ROW_ID] = Eli_Production.[MEMBER_ELIGIBILITY_ROW_ID]
			WHERE Eli_Production.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID   -- Overly causious WHERE statement. Verifying the correct member.


		-- Flag the current record as having been processed
		UPDATE [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
		SET
			[ROW_PROCESSED] = 'Y',
			[ROW_PROCESSED_DATE] = GETDATE()
		WHERE [MEMBER_ELIGIBILITY_STAGING_ROW_ID] = @MEMBER_ELIGIBILITY_STAGING_ROW_ID;

	END





	/**************************************************************************************************************************************
		2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
						 table, meaning that an insert cannot happen because this eligibility record cannot be uniquely
						 matched to a member.  This staging row should be flagged as being in error, and a human should
						 investigate why the same keys are being used in more than once place.
	**************************************************************************************************************************************/
	IF @MEMBER_MASTER_ROW_COUNT > 1
	BEGIN

		UPDATE [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
		SET
			[ROW_PROBLEM] = 'Y',
			[ROW_PROBLEM_REASON] = 'The keys on this record match more than 1 record on the member master table.  Requires human analysis and intervention.',
			[ROW_PROBLEM_DATE] = GETDATE()
		WHERE [MEMBER_ELIGIBILITY_STAGING_ROW_ID] = @MEMBER_ELIGIBILITY_STAGING_ROW_ID

	END

END

