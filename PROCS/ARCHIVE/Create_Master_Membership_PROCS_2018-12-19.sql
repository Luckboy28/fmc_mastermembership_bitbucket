USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_Staging_LOAD_IKA_CSNP', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD_IKA_CSNP;
GO


--CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD_IKA_CSNP]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: May 19th 2017
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================



--	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
--	(
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROCESSED]
--		--,[ROW_PROCESSED_DATE]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE]
--		--,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[CREATE_DATE]
--		--,[UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Source_IKA_CSNP]

--	EXCEPT

--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

--END
--GO





IF OBJECT_ID('dbo.Member_Master_Staging_LOAD_ESCO', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD_ESCO;
GO


--CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD_ESCO]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: May 19th 2017
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================


--	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
--	(
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROCESSED]
--		--,[ROW_PROCESSED_DATE]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE]
--		--,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[CREATE_DATE]
--		--,[UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Source_ESCO]

--	EXCEPT

--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

--END
--GO








IF OBJECT_ID('dbo.Member_Master_Staging_LOAD_Aetna', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD_Aetna;
GO


--CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD_Aetna]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: May 19th 2017
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================


--	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
--	(
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROCESSED]
--		--,[ROW_PROCESSED_DATE]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE]
--		--,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[CREATE_DATE]
--		--,[UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Source_Aetna]

--	EXCEPT

--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

--END
--GO








IF OBJECT_ID('dbo.Member_Master_Staging_LOAD_Cigna', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD_Cigna;
GO


--CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD_Cigna]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: May 19th 2017
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================


--	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
--	(
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROCESSED]
--		--,[ROW_PROCESSED_DATE]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE]
--		--,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[CREATE_DATE]
--		--,[UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Source_Cigna]

--	EXCEPT

--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

--END
--GO









IF OBJECT_ID('dbo.Member_Master_Staging_LOAD_Coventry', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD_Coventry;
GO


--CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD_Coventry]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: May 19th 2017
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================


--	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
--	(
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROCESSED]
--		--,[ROW_PROCESSED_DATE]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE]
--		--,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[CREATE_DATE]
--		--,[UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Source_Coventry]

--	EXCEPT

--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

--END
--GO






IF OBJECT_ID('dbo.Member_Master_Staging_LOAD_Humana', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD_Humana;
GO


--CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD_Humana]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: May 19th 2017
---- Description:	
---- ==========================================================================================
--/*
--*/
---- ==========================================================================================


--	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
--	(
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[ROW_PROCESSED]
--		--,[ROW_PROCESSED_DATE]
--		--,[ROW_DELETED]
--		--,[ROW_DELETED_DATE]
--		--,[ROW_DELETED_REASON]
--		--,[ROW_CREATE_DATE]
--		--,[ROW_UPDATE_DATE]
--	)
--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--		--,[CREATE_DATE]
--		--,[UPDATE_DATE]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Source_Humana]

--	EXCEPT

--	SELECT
--		 [MHK_INTERNAL_ID]
--		,[MEDHOK_ID]
--		,[SSN]
--		,[HICN]
--		,[CLAIM_SUBSCRIBER_ID]
--		,[MBI]
--		,[MEDICAID_NO]
--		,[MRN]
--		,[EXT_ID]
--		,[EXT_ID_TYPE]
--		,[EXT_ID_2]
--		,[EXT_ID_TYPE_2]
--		,[EXT_ID_3]
--		,[EXT_ID_TYPE_3]
--		,[BENEFIT_STATUS]
--		,[FIRST_NAME]
--		,[MIDDLE_NAME]
--		,[LAST_NAME]
--		,[PREFIX]
--		,[SUFFIX]
--		,[DATE_OF_BIRTH]
--		,[GENDER]
--		,[COMPANY_DESCRIPTION]
--		,[LOB_CODE]
--		,[FAMILY_ID]
--		,[PERSON_NUMBER]
--		,[RACE]
--		,[ETHNICITY]
--		,[PRIMARY_LANGUAGE]
--		,[PRIMARY_LANGUAGE_SOURCE]
--		,[SPOKEN_LANGUAGE]
--		,[SPOKEN_LANGUAGE_SOURCE]
--		,[WRITTEN_LANGUAGE]
--		,[WRITTEN_LANGUAGE_SOURCE]
--		,[OTHER_LANGUAGE]
--		,[OTHER_LANGUAGE_SOURCE]
--		,[EMPLOYEE]
--		,[PBP_NUMBER]
--		,[CURRENT_LIS]
--		,[IPA_GROUP_EXT_ID]
--		,[MEDICARE_PLAN_CODE]
--		,[MEDICARE_TYPE]
--		,[DUPLICATE_MEDICAID_ID]
--		,[PREGNANCY_DUE_DATE]
--		,[PREGNANCY_INDICATOR]
--		,[BOARD_NUMBER]
--		,[DEPENDENT_CODE]
--		,[LEGACY_SUBSCRIBER_ID]
--		,[GROUP_NUMBER]
--		,[SOURCE]
--		,[CLIENT_SPECIFIC_DATA]
--		,[RELATIONSHIP_CODE]
--		,[TIME_ZONE]
--		,[DATE_OF_DEATH]
--		,[FOSTER_CARE_FLAG]
--		,[VIP]
--		,[CLINIC_NUMBER]
--		,[MODALITY]
--		,[PAYER_NAME]
--		,[PAYER_ID_TYPE]
--		,[PAYER_ID]
--		,[DIALYSIS_START_DATE]
--		,[KIDNEY_TRANSPLANT_DATE]
--		,[ROW_SOURCE]
--		,[ROW_SOURCE_ID]
--	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

--END
--GO












IF OBJECT_ID('dbo.Member_Master_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Master_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: April 25th 2017
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_Master_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[BENEFIT_STATUS]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[COMPANY_DESCRIPTION]
			,[LOB_CODE]
			,[FAMILY_ID]
			,[PERSON_NUMBER]
			,[RACE]
			,[ETHNICITY]
			,[PRIMARY_LANGUAGE]
			,[PRIMARY_LANGUAGE_SOURCE]
			,[SPOKEN_LANGUAGE]
			,[SPOKEN_LANGUAGE_SOURCE]
			,[WRITTEN_LANGUAGE]
			,[WRITTEN_LANGUAGE_SOURCE]
			,[OTHER_LANGUAGE]
			,[OTHER_LANGUAGE_SOURCE]
			,[EMPLOYEE]
			,[PBP_NUMBER]
			,[CURRENT_LIS]
			,[IPA_GROUP_EXT_ID]
			,[MEDICARE_PLAN_CODE]
			,[MEDICARE_TYPE]
			,[DUPLICATE_MEDICAID_ID]
			,[PREGNANCY_DUE_DATE]
			,[PREGNANCY_INDICATOR]
			,[BOARD_NUMBER]
			,[DEPENDENT_CODE]
			,[LEGACY_SUBSCRIBER_ID]
			,[GROUP_NUMBER]
			,[SOURCE]
			,[CLIENT_SPECIFIC_DATA]
			,[RELATIONSHIP_CODE]
			,[TIME_ZONE]
			,[DATE_OF_DEATH]
			,[FOSTER_CARE_FLAG]
			,[VIP]
			,[CLINIC_NUMBER]
			,[MODALITY]
			,[PAYER_NAME]
			,[PAYER_ID_TYPE]
			,[PAYER_ID]
			,[DIALYSIS_START_DATE]
			,[KIDNEY_TRANSPLANT_DATE]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[CREATE_DATE]
			--,[UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources]

		INTERSECT

		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[BENEFIT_STATUS]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[COMPANY_DESCRIPTION]
			,[LOB_CODE]
			,[FAMILY_ID]
			,[PERSON_NUMBER]
			,[RACE]
			,[ETHNICITY]
			,[PRIMARY_LANGUAGE]
			,[PRIMARY_LANGUAGE_SOURCE]
			,[SPOKEN_LANGUAGE]
			,[SPOKEN_LANGUAGE_SOURCE]
			,[WRITTEN_LANGUAGE]
			,[WRITTEN_LANGUAGE_SOURCE]
			,[OTHER_LANGUAGE]
			,[OTHER_LANGUAGE_SOURCE]
			,[EMPLOYEE]
			,[PBP_NUMBER]
			,[CURRENT_LIS]
			,[IPA_GROUP_EXT_ID]
			,[MEDICARE_PLAN_CODE]
			,[MEDICARE_TYPE]
			,[DUPLICATE_MEDICAID_ID]
			,[PREGNANCY_DUE_DATE]
			,[PREGNANCY_INDICATOR]
			,[BOARD_NUMBER]
			,[DEPENDENT_CODE]
			,[LEGACY_SUBSCRIBER_ID]
			,[GROUP_NUMBER]
			,[SOURCE]
			,[CLIENT_SPECIFIC_DATA]
			,[RELATIONSHIP_CODE]
			,[TIME_ZONE]
			,[DATE_OF_DEATH]
			,[FOSTER_CARE_FLAG]
			,[VIP]
			,[CLINIC_NUMBER]
			,[MODALITY]
			,[PAYER_NAME]
			,[PAYER_ID_TYPE]
			,[PAYER_ID]
			,[DIALYSIS_START_DATE]
			,[KIDNEY_TRANSPLANT_DATE]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]
		,[LOB_VENDOR]
		,[LOB_TYPE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROCESSED]
		--,[ROW_PROCESSED_DATE]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]
		,[LOB_VENDOR]
		,[LOB_TYPE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[CREATE_DATE]
		--,[UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]
		,[LOB_VENDOR]
		,[LOB_TYPE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM [FHPDataMarts].[dbo].[Member_Master_Staging]

END
GO







USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_Staging_DataValidation', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_Staging_DataValidation;
GO


CREATE PROCEDURE [dbo].[Member_Master_Staging_DataValidation]
	-- No parameters needed
AS
BEGIN

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: April 25th 2017
-- Description:	
-- ==========================================================================================


	-- Validate SSN
	UPDATE [dbo].[Member_Master_Staging]
	SET
		 ROW_PROBLEM = 'Y'
		,ROW_PROBLEM_REASON = 'Invalid SSN'
	WHERE
	(
		LEN([SSN]) <> 11
		OR ISNUMERIC(SUBSTRING([SSN],1,3)) <> 1
		OR SUBSTRING([SSN],4,1) <> '-'
		OR ISNUMERIC(SUBSTRING([SSN],5,2)) <> 1
		OR SUBSTRING([SSN],7,1) <> '-'
		OR ISNUMERIC(SUBSTRING([SSN],8,4)) <> 1
	)
	AND [SSN] IS NOT NULL;


	-- Validate HICN
	UPDATE [dbo].[Member_Master_Staging]
	SET
		 ROW_PROBLEM = 'Y'
		,ROW_PROBLEM_REASON = 'Invalid HICN'
	WHERE 
	(
		   LEN([HICN]) < 10
		--OR ISNUMERIC(SUBSTRING([HICN],1,9)) <> 1       -- Note:  Some HICN's contain a starting letter or a bracket, these HICN's should be allowed until a fix can be found.
	)
	AND [HICN] IS NOT NULL;




END
GO





USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Eligibility_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Eligibility_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Eligibility_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: April 25th 2017
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[HLTH_PLN_SYSID]
			,[HLTH_PLN_PROD_LINE]
			,[HLTH_PLN_RPT_GRP]
			,[HLTH_PLN_STD_CARRIER_CD]
			,[PLAN_NAME]
			,[START_DATE]
			,[TERM_DATE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources]

		INTERSECT

		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[HLTH_PLN_SYSID]
			,[HLTH_PLN_PROD_LINE]
			,[HLTH_PLN_RPT_GRP]
			,[HLTH_PLN_STD_CARRIER_CD]
			,[PLAN_NAME]
			,[START_DATE]
			,[TERM_DATE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Eligibility_Staging]

END
GO






USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_HIC_XREF_Update', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_HIC_XREF_Update;
GO


CREATE PROCEDURE [dbo].[Member_Master_HIC_XREF_Update]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: April 25th 2017
-- Description:	
-- ==========================================================================================
/*

*/
-- ==========================================================================================


	-- Update the Member Master table with the latest HICN's.

	UPDATE MM
		SET [HICN] = LTRIM(RTRIM(XREF.[CURRENT_HICN]))
	FROM [FHPDW].[dbo].[Member_Master] MM
	JOIN [FHPDataMarts].[dbo].[Member_HICN_XREF] XREF
	ON MM.[HICN] = XREF.[PREVIOUS_HICN]
	WHERE XREF.[CURRENT_HICN] NOT IN (SELECT HICN FROM [FHPDW].[dbo].[Member_Master] WHERE HICN <> XREF.[PREVIOUS_HICN]);  -- Prevents updating HICN if there is a conflict.  Error should be flagged elsewhere.


END
GO










IF OBJECT_ID('dbo.Member_Address_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Address_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Address_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: December 14th 2017
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_Address_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ADDRESS_TYPE]
			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[EFFECTIVE_DATE]
			,[TERM_DATE]
			,[ADDRESS_STATUS]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources]

		INTERSECT

		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ADDRESS_TYPE]
			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[EFFECTIVE_DATE]
			,[TERM_DATE]
			,[ADDRESS_STATUS]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Address_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_Address_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[EFFECTIVE_DATE]
		,[TERM_DATE]
		,[ADDRESS_STATUS]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Address_Staging];
	



	UPDATE [FHPDataMarts].[dbo].[Member_Address_Staging]
	SET
		 ROW_PROBLEM = 'Y'
		,ROW_PROBLEM_REASON = 'Not enough data to create a valid address/contact record.'
		,ROW_PROBLEM_DATE = GETDATE()
	WHERE 
	-- Try to find enough data to make a reasonable address.
	-- Valid addresses must have at least one address field, and a valid city/state or a zip code.
	(
		(
			    [ADDRESS_1] IS NULL
			AND [ADDRESS_2] IS NULL
			AND [ADDRESS_3] IS NULL
		)
		OR
		(
			(
					[CITY] IS NULL
				OR [STATE] IS NULL
			)
			AND [ZIP] IS NULL
		)
	)
	-- Any direct line of communication is considered enough to make a valid address/contact record.
	AND [PHONE] IS NULL
	AND [ALTERNATE_PHONE] IS NULL
	AND [EVENING_PHONE] IS NULL
	AND [EMERGENCY_PHONE] IS NULL
	AND [FAX] IS NULL
	AND [EMAIL] IS NULL

END
GO









IF OBJECT_ID('dbo.Member_Contacts_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Contacts_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Contacts_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: April 25th 2017
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_Contacts_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]

			,[CONTACT_TYPE]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[RELATION_TO_MEMBER]
			,[PROVIDER_ID_TYPE]
			,[PROVIDER_ID]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources]

		INTERSECT

		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]

			,[CONTACT_TYPE]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[RELATION_TO_MEMBER]
			,[PROVIDER_ID_TYPE]
			,[PROVIDER_ID]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Contacts_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_Contacts_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Contacts_Staging]

END
GO









USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_LOAD_HISTORICAL_KEYS', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_LOAD_HISTORICAL_KEYS;
GO

-- THIS PROC IS NO LONGER USED.  Eligibility rows are now loaded before member rows instead.



--CREATE PROCEDURE [dbo].[Member_Master_LOAD_HISTORICAL_KEYS]
--	@MHK_INTERNAL_ID int NULL,
--	@MEDHOK_ID varchar(50) NULL,
--	@SSN varchar(11) NULL,
--	@HICN varchar(12) NULL,
--	@CLAIM_SUBSCRIBER_ID varchar(50) NULL,
--	@MBI varchar(11) NULL,
--	@MEDICAID_NO varchar(50) NULL,
--	@MRN varchar(50) NULL,
--	@EXT_ID varchar(50) NULL,
--	@EXT_ID_TYPE varchar(50) NULL,
--	@EXT_ID_2 varchar(50) NULL,
--	@EXT_ID_TYPE_2 varchar(50) NULL,
--	@EXT_ID_3 varchar(50) NULL,
--	@EXT_ID_TYPE_3 varchar(50) NULL,
--	@ROW_SOURCE varchar(500) NULL,
--	@ROW_SOURCE_ID varchar(50) NULL
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: October 2nd 2017
---- Description:	
---- ==========================================================================================
--/*

--	UPDATE:  THIS PROC IS NO LONGER USED.  Eligibility rows are now loaded before member rows
--	instead.

--	This stored proc serves as the loader into the [dbo].[Member_Master_KeyHistory] table.

--	Whenever a new set of member keys are discovered (from the staging tables, or elsewhere,
--	this proc will attempt to uniquely match it to an existing member.  If no matches are
--	found, a completely empty member record will be created.

--	This allows us to gather every possible set of member keys before the data is loaded
--	into the Master_Member or Master_Eligibility tables.  This is nessassry because the data
--	in those tables (including the keys) must represent the data from the highest LOB
--	hierarchy, etc, not just the mostly recently loaded.

--*/
---- ==========================================================================================



--	-- Find the member
--	DECLARE @MEMBER_MASTER_ROW_COUNT AS INT = NULL;
--	DECLARE @MEMBER_MASTER_ROW_ID AS INT = NULL;


--	-- Used for gathering stats on key lookups.  Used for both Member and Eligibility lookups.
--	DECLARE @KEY_SEARCH_RESULTS AS TABLE
--	(
--		[MEMBER_MASTER_ROW_ID] INT
--	)

	
--	-- Find key matches
--	;WITH AllKeys AS
--	(
--		-- Historical
--		SELECT
--			 [MEMBER_MASTER_ROW_ID]
--			,[MHK_INTERNAL_ID]
--			,[MEDHOK_ID]
--			,[SSN]
--			,[HICN]
--			,[CLAIM_SUBSCRIBER_ID]
--			,[MBI]
--			,[MEDICAID_NO]
--			,[MRN]
--			,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
--		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
--		WHERE ROW_DELETED = 'N'

--		UNION ALL

--		-- Current
--		SELECT
--			 [MEMBER_MASTER_ROW_ID]
--			,[MHK_INTERNAL_ID]
--			,[MEDHOK_ID]
--			,[SSN]
--			,[HICN]
--			,[CLAIM_SUBSCRIBER_ID]
--			,[MBI]
--			,[MEDICAID_NO]
--			,[MRN]
--			,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
--		FROM [FHPDW].[dbo].[Member_Master]
--	)
--	INSERT INTO @KEY_SEARCH_RESULTS
--	(
--		[MEMBER_MASTER_ROW_ID]
--	)
--	SELECT DISTINCT
--		[MEMBER_MASTER_ROW_ID]
--	FROM AllKeys
--	WHERE 1=2
--	OR @MHK_INTERNAL_ID = AllKeys.[MHK_INTERNAL_ID]
--	OR @MEDHOK_ID = AllKeys.[MEDHOK_ID]
--	OR @SSN = AllKeys.[SSN]
--	OR @HICN = AllKeys.[HICN]
--	OR @CLAIM_SUBSCRIBER_ID = AllKeys.[CLAIM_SUBSCRIBER_ID]
--	OR @MBI = AllKeys.[MBI]
--	OR @MEDICAID_NO = AllKeys.[MEDICAID_NO]
--	OR @MRN = AllKeys.[MRN]
--	OR CASE WHEN @EXT_ID IS NOT NULL AND @EXT_ID_TYPE IS NOT NULL THEN CONCAT(@EXT_ID,':',@EXT_ID_TYPE) ELSE NULL END = AllKeys.[EXT_ID_AND_TYPE]


--	-- Get number of matching results
--	SELECT @MEMBER_MASTER_ROW_COUNT = COUNT(1) FROM @KEY_SEARCH_RESULTS;

--	-- Get the record, if it's unique
--	IF @MEMBER_MASTER_ROW_COUNT = 1
--	BEGIN
--		SELECT @MEMBER_MASTER_ROW_ID = [MEMBER_MASTER_ROW_ID] FROM @KEY_SEARCH_RESULTS
--	END



--	-- 0 rows:  None of the current keys matched.
--	IF @MEMBER_MASTER_ROW_COUNT = 0
--	BEGIN

--		-- Create table variable to hold the primary key for the record we're about to create, after it's inserted.
--		DECLARE @MEMBER_MASTER_INSERTED AS TABLE
--		(
--			[MEMBER_MASTER_ROW_ID] INT NOT NULL
--		)

--		INSERT INTO [FHPDW].[dbo].[Member_Master]
--		(
--			 [MHK_INTERNAL_ID]
--			,[MEDHOK_ID]
--			,[SSN]
--			,[HICN]
--			,[CLAIM_SUBSCRIBER_ID]
--			,[MBI]
--			,[MEDICAID_NO]
--			,[MRN]
--			,[EXT_ID]
--			,[EXT_ID_TYPE]

--			,[STATUS]
--			,[LOB_VENDOR]
--			,[LOB_TYPE]

--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--			,[ROW_PROBLEM]
--			,[ROW_PROBLEM_DATE]
--			,[ROW_PROBLEM_REASON]
--		)
--		OUTPUT INSERTED.[MEMBER_MASTER_ROW_ID] INTO @MEMBER_MASTER_INSERTED
--		VALUES
--		(
--			 NULL --@MHK_INTERNAL_ID
--			,NULL --@MEDHOK_ID
--			,NULL --@SSN
--			,NULL --@HICN
--			,NULL --@CLAIM_SUBSCRIBER_ID
--			,NULL --@MBI
--			,NULL --@MEDICAID_NO
--			,NULL --@MRN
--			,NULL --@EXT_ID
--			,NULL --@EXT_ID_TYPE

--			,'INACTIVE'
--			,'UNKNOWN' --@LOB_VENDOR
--			,'UNKNOWN' --@LOB_TYPE

--			,@ROW_SOURCE  --[ROW_SOURCE[
--			,@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
--			,'Y'  --[ROW_PROBLEM]
--			,GETDATE()  --[ROW_PROBLEM_DATE]
--			,'Placeholder. Member data not yet loaded.'  --[ROW_PROBLEM_REASON]
--		)


--		-- Grab the row ID of the member table that was just inserted
--		SELECT @MEMBER_MASTER_ROW_ID = MEMBER_MASTER_ROW_ID FROM @MEMBER_MASTER_INSERTED;

--	END



--	-- If not matching to more than one row (error), then insert into the KeyHistory table.
--	IF @MEMBER_MASTER_ROW_COUNT <= 1
--	BEGIN

--		WITH UniqueKeyCombo AS
--		(

--			-- Select the unique keys
--			SELECT
--				-- @MEMBER_MASTER_ROW_ID AS [MEMBER_MASTER_ROW_ID]
--				 @MHK_INTERNAL_ID AS [MHK_INTERNAL_ID]
--				,@MEDHOK_ID AS [MEDHOK_ID]
--				,@SSN AS [SSN]
--				,@HICN AS [HICN]
--				,@CLAIM_SUBSCRIBER_ID AS [CLAIM_SUBSCRIBER_ID]
--				,@MBI AS [MBI]
--				,@MEDICAID_NO AS [MEDICAID_NO]
--				,@MRN AS [MRN]
--				,@EXT_ID AS [EXT_ID]
--				,@EXT_ID_TYPE AS [EXT_ID_TYPE]
--				,@EXT_ID_2 AS [EXT_ID_2]
--				,@EXT_ID_TYPE_2 AS [EXT_ID_TYPE_2]
--				,@EXT_ID_3 AS [EXT_ID_3]
--				,@EXT_ID_TYPE_3 AS [EXT_ID_TYPE_3]

--			EXCEPT

--			-- Unless it already exists
--			SELECT
--				-- [MEMBER_MASTER_ROW_ID]
--				 [MHK_INTERNAL_ID]
--				,[MEDHOK_ID]
--				,[SSN]
--				,[HICN]
--				,[CLAIM_SUBSCRIBER_ID]
--				,[MBI]
--				,[MEDICAID_NO]
--				,[MRN]
--				,[EXT_ID]
--				,[EXT_ID_TYPE]
--				,[EXT_ID_2]
--				,[EXT_ID_TYPE_2]
--				,[EXT_ID_3]
--				,[EXT_ID_TYPE_3]
--			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
--		)
--		INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
--		(
--			 [MEMBER_MASTER_ROW_ID]
--			,[MHK_INTERNAL_ID]
--			,[MEDHOK_ID]
--			,[SSN]
--			,[HICN]
--			,[CLAIM_SUBSCRIBER_ID]
--			,[MBI]
--			,[MEDICAID_NO]
--			,[MRN]
--			,[EXT_ID]
--			,[EXT_ID_TYPE]
--			,[EXT_ID_2]
--			,[EXT_ID_TYPE_2]
--			,[EXT_ID_3]
--			,[EXT_ID_TYPE_3]

--			,[ROW_SOURCE]
--			,[ROW_SOURCE_ID]
--		)
--		SELECT
--			 @MEMBER_MASTER_ROW_ID
--			,UniqueKeyCombo.[MHK_INTERNAL_ID]
--			,UniqueKeyCombo.[MEDHOK_ID]
--			,UniqueKeyCombo.[SSN]
--			,UniqueKeyCombo.[HICN]
--			,UniqueKeyCombo.[CLAIM_SUBSCRIBER_ID]
--			,UniqueKeyCombo.[MBI]
--			,UniqueKeyCombo.[MEDICAID_NO]
--			,UniqueKeyCombo.[MRN]
--			,UniqueKeyCombo.[EXT_ID]
--			,UniqueKeyCombo.[EXT_ID_TYPE]
--			,UniqueKeyCombo.[EXT_ID_2]
--			,UniqueKeyCombo.[EXT_ID_TYPE_2]
--			,UniqueKeyCombo.[EXT_ID_3]
--			,UniqueKeyCombo.[EXT_ID_TYPE_3]

--			,@ROW_SOURCE  --[ROW_SOURCE]
--			,@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
--		FROM UniqueKeyCombo

--	END

--END
--GO



USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_StatusUpdate', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_StatusUpdate;
GO


CREATE PROCEDURE [dbo].[Member_Master_StatusUpdate]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: December 26th 2017
-- Description:	
-- ==========================================================================================
/*

	This stored procedure updates the Master Member table with the member's latest LOB_TYPE,
	LOB_VENDOR, and STATUS with their highest priority record from the Member_Eligibility
	table.

*/
-- ==========================================================================================


	-- Update the Member Master record, if needed.
	;WITH LatestRecords AS
	(
		SELECT
			 ELI.[STATUS]
			,ELI.[LOB_VENDOR]
			,ELI.[LOB_TYPE]
			,ELI.[MEMBER_MASTER_ROW_ID]
			,ROW_NUMBER() OVER (PARTITION BY ELI.[MEMBER_MASTER_ROW_ID] ORDER BY CASE WHEN ELI.[STATUS] = 'ACTIVE' THEN 1 ELSE 2 END ASC, ELI.[START_DATE] DESC, ELI.[TERM_DATE] DESC) AS [RANK]   -- This simply puts the "Active" record on top, or the latest START_DATE or TERM_DATE if there is no active record.
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
	)
	UPDATE MemberMaster
		SET
			[STATUS] = LR.[STATUS],
			[LOB_VENDOR] = LR.[LOB_VENDOR],
			[LOB_TYPE] = LR.[LOB_TYPE],
			[ROW_PROBLEM] = CASE WHEN [ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN 'N' ELSE [ROW_PROBLEM] END,
			[ROW_PROBLEM_REASON] = CASE WHEN [ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE [ROW_PROBLEM_REASON] END,
			[ROW_PROBLEM_DATE] = CASE WHEN [ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE [ROW_PROBLEM_DATE] END
	FROM [FHPDW].[dbo].[Member_Master] MemberMaster
	JOIN LatestRecords LR
	ON LR.MEMBER_MASTER_ROW_ID = MemberMaster.MEMBER_MASTER_ROW_ID
	WHERE 
	(
		MemberMaster.[STATUS] <> LR.[STATUS]
		OR MemberMaster.[LOB_VENDOR] <> LR.[LOB_VENDOR]
		OR MemberMaster.[LOB_TYPE] <> LR.[LOB_TYPE]
		OR MemberMaster.[ROW_PROBLEM] <> CASE WHEN MemberMaster.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN 'N' ELSE MemberMaster.[ROW_PROBLEM] END
		OR MemberMaster.[ROW_PROBLEM_REASON] <> CASE WHEN MemberMaster.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE MemberMaster.[ROW_PROBLEM_REASON] END
		OR MemberMaster.[ROW_PROBLEM_DATE] <> CASE WHEN MemberMaster.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE MemberMaster.[ROW_PROBLEM_DATE] END
	)
	AND
	(
		LR.[RANK] = 1
	)

END
GO








USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Eligibility_PreLoad_Logic', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Eligibility_PreLoad_Logic;
GO


CREATE PROCEDURE [dbo].[Member_Eligibility_PreLoad_Logic]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: Janurary 31st, 2018
-- Description:	This procedure executes any logic needed before the eligibility load runs.
-- ==========================================================================================
/*
*/
-- ==========================================================================================


	-- Expire all valid records that have lapsed naturally.
	UPDATE [FHPDW].[dbo].[Member_Eligibility]
	SET
		 [TERM_REASON] = 'LOB Expired'
		,[STATUS] = 'INACTIVE'
	WHERE [TERM_DATE] <= GETDATE()
	AND [TERM_REASON] <> 'Replaced by another LOB'
	AND [TERM_REASON] <> 'LOB Expired'
	AND [ROW_DELETED] = 'N'






END
GO

















--USE [FHPDataMarts]
--GO


--IF OBJECT_ID('dbo.SomeProc', 'P') IS NOT NULL
--	DROP PROCEDURE dbo.SomeProc;
--GO


--CREATE PROCEDURE [dbo].[SomeProc]
--	-- No parameters needed
--AS
--BEGIN


---- ==========================================================================================
---- Author:		David M. Wilson
---- Create date: April 25th 2017
---- Description:	
---- ==========================================================================================
--/*


--*/
---- ==========================================================================================


--END
--GO