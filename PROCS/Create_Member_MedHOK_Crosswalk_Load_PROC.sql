
USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_MedHOK_Crosswalk_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_MedHOK_Crosswalk_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_MedHOK_Crosswalk_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: May 15th 2018
-- Description:	
-- ==========================================================================================
/*
	This procedure inserts all of the newly discovered MedHOK ID's into a lookup table.

	Since the lookup table can only contain a single "Primary" record per member, and all
	other records for that member must be listed as "Duplicate", we will attack this problem
	with two insert statements.

	The first insert will insert only the "Primary" records where the Member Master ID does
	not yet exist.

	The second insert will insert all Medhok ID's that don't currently exist, but list them
	as duplicates.

	This handles the case where MedHOK's eligibliity data changes, resulting in the 
	[FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] view identifying a different
	record as being "Primary".   This method ensures that the first "Primary" to be created
	will stay as a unique Primary in the table.

*/
-- ==========================================================================================

	
	-- Insert the "Primary" records
	INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
	(
		 [MEMBER_MASTER_ROW_ID]
		,[MEDHOK_ID]
		,[MEDHOK_ID_TYPE]
	)
	SELECT
		 SRC.[MEMBER_MASTER_ROW_ID]
		,SRC.[MEDHOK_ID]
		,SRC.[MEDHOK_ID_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
	LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
	ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
	AND SRC.[MEMBER_MASTER_ROW_ID] = TBL.[MEMBER_MASTER_ROW_ID]  -- Added to prevent multiple "PRIMARY" on the same Member ID
	WHERE SRC.[MEDHOK_ID] IS NOT NULL
	AND TBL.[MEDHOK_ID] IS NULL
	AND TBL.[MEMBER_MASTER_ROW_ID] IS NULL  -- Added to prevent multiple "PRIMARY" on the same Member ID
	AND SRC.[MEDHOK_ID_TYPE] = 'PRIMARY';


	-- Insert the "Duplicate" records
	INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
	(
		 [MEMBER_MASTER_ROW_ID]
		,[MEDHOK_ID]
		,[MEDHOK_ID_TYPE]
	)
	SELECT
		 SRC.[MEMBER_MASTER_ROW_ID]
		,SRC.[MEDHOK_ID]
		,'DUPLICATE'  -- This is hard-coded, to ensure that only one "Primary" record is received
	FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
	LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
	ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
	WHERE SRC.[MEDHOK_ID] IS NOT NULL
	AND TBL.[MEDHOK_ID] IS NULL;



	-- Update the Member Master table with the primary MedHOK ID, if needed.
	UPDATE MM
	SET
		MM.[MEDHOK_ID] = XW.[MEDHOK_ID]
	FROM [FHPDW].[dbo].[Member_Master] MM
	JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW
	ON XW.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
	WHERE XW.[MEDHOK_ID_TYPE] = 'PRIMARY'
	AND XW.[MEDHOK_ID] <> MM.[MEDHOK_ID];   -- Only update if needed.




END
GO


