

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_FDOD_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_FDOD_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Master_FDOD_LOAD]
	-- No parameters needed
AS
BEGIN

-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: 
-- Update date: 
-- Description:	
-- ==========================================================================================



	-- FDOCD_DT
	IF OBJECT_ID('tempdb..#FDOCD_DT') IS NOT NULL
		DROP TABLE #FDOCD_DT


	CREATE TABLE #FDOCD_DT
	(
		 [MRN] [VARCHAR](50) NOT NULL
		,[FDOCD_DT] [DATE] NOT NULL
	)
	GO


	-- Step 2
	;WITH FDOCD_DT_RANKED AS
	(
		SELECT DISTINCT
			 LTRIM(RTRIM(CAST([MRN] AS VARCHAR(50)))) [MRN]
			,CAST([FDOCD_DT] AS DATE) [FDOCD_DT]
			,ROW_NUMBER() OVER (PARTITION BY LTRIM(RTRIM(CAST([MRN] AS VARCHAR(50)))) ORDER BY CAST([FDOCD_DT] AS DATE) ASC) [RANK]
		FROM OPENQUERY([KCNGX],'

				WITH Patient_Details AS
					(
						SELECT DISTINCT  
							P.MRN,
							P.PT_PHI_ID 
						FROM PERS_DIAL_PHI.PT_PHI P
						WHERE 1 = 1
					)
					SELECT DISTINCT
						P.MRN,
						P.PT_PHI_ID,
						D.FDOCD_DT,
						D.CHIEF_ICD_CAT_CD,
						D.CHIEF_ICD_NM,
						D.CHIEF_ICD_CD
					FROM Patient_Details P 
						LEFT JOIN CL_DIAL.PT_AKI_ESRD_HIST D
							ON P.PT_PHI_ID = D.PT_PHI_ID
						WHERE 1 = 1
							AND D.CHIEF_ICD_CAT_CD = ''ESRD''

		')
		WHERE 1=1
		AND NULLIF(LTRIM(RTRIM([MRN])),'') IS NOT NULL
		AND NULLIF(LTRIM(RTRIM([FDOCD_DT])),'') IS NOT NULL
	)
	INSERT INTO #FDOCD_DT
	(
		 [MRN]
		,[FDOCD_DT]
	)
	SELECT
		 [MRN]
		,[FDOCD_DT]
	FROM FDOCD_DT_RANKED
	WHERE [RANK] = 1







	-- PT_FRST_EVER_DIAL_DT
	IF OBJECT_ID('tempdb..#PT_FRST_EVER_DIAL_DT') IS NOT NULL
		DROP TABLE #PT_FRST_EVER_DIAL_DT


	CREATE TABLE #PT_FRST_EVER_DIAL_DT
	(
		 [MRN] [VARCHAR](50) NOT NULL
		,[PT_FRST_EVER_DIAL_DT] [DATE] NOT NULL
	)
	GO


	;WITH PT_FRST_EVER_DIAL_DT_RANKED AS
	(
		-- Step 2b
		SELECT DISTINCT
			 LTRIM(RTRIM(CAST([PT_MRN] AS VARCHAR(50)))) [MRN]
			,CAST([PT_FRST_EVER_DIAL_DT] AS DATE) [PT_FRST_EVER_DIAL_DT]
			,ROW_NUMBER() OVER (PARTITION BY LTRIM(RTRIM(CAST([PT_MRN] AS VARCHAR(50)))) ORDER BY CAST([PT_FRST_EVER_DIAL_DT] AS DATE) DESC) [RANK]
		FROM OPENQUERY([KCNGX],'

				SELECT DISTINCT
					PT_MRN,
					PT_FRST_EVER_DIAL_DT 
				FROM FN_DIAL.PT_INS_SNAPSHOT

		')
		WHERE 1=1
		AND NULLIF(LTRIM(RTRIM([PT_MRN])),'') IS NOT NULL
		AND NULLIF(LTRIM(RTRIM([PT_FRST_EVER_DIAL_DT])),'') IS NOT NULL
	)
	INSERT INTO #PT_FRST_EVER_DIAL_DT
	(
		 [MRN]
		,[PT_FRST_EVER_DIAL_DT]
	)
	SELECT DISTINCT
		 [MRN]
		,[PT_FRST_EVER_DIAL_DT]
	FROM PT_FRST_EVER_DIAL_DT_RANKED
	WHERE [RANK] = 1










	-- FDOD
	IF OBJECT_ID('tempdb..#FDOD') IS NOT NULL
		DROP TABLE #FDOD


	CREATE TABLE #FDOD
	(
		 [MRN] [VARCHAR](50) NOT NULL
		,[FDOD] [DATE] NOT NULL
	)
	GO



	;WITH FDOD_RANKED AS
	(
		-- Step 2c
		SELECT DISTINCT
			 LTRIM(RTRIM(CAST([MRN] AS VARCHAR(50)))) [MRN]
			,CAST([FDOD] AS DATE) [FDOD]
			,ROW_NUMBER() OVER (PARTITION BY LTRIM(RTRIM(CAST([MRN] AS VARCHAR(50)))) ORDER BY CAST([FDOD] AS DATE) ASC) [RANK]
		FROM OPENQUERY([DATWEXA],'

					WITH HEMO_DATE AS
					(
						SELECT
							PATIENT_ID,
							MIN(HEMO_POST_DATE) AS Min_TXT_Date 
						FROM KC.HEMO_TREATMENT_POST
							GROUP BY
								PATIENT_ID
					)
					,
					HOME_DATE AS 
					(
						SELECT 
							PATIENT_ID,
							MIN(HOME_TREAT_COUNT_START_DATE) AS Min_TXT_Date
						FROM KC.HOME_TREATMENT_COUNT
							GROUP BY 
								PATIENT_ID
					)
					,
					Combined_TXT_DATE AS 
					(
						SELECT * FROM  HEMO_DATE
						UNION ALL
						SELECT * FROM HOME_DATE
					)
					SELECT 
						PATIENT_ID AS MRN,
						MIN(Min_TXT_Date) AS FDOD
					FROM Combined_TXT_DATE
						GROUP BY
							PATIENT_ID

		')
		WHERE 1=1
		AND NULLIF(LTRIM(RTRIM([MRN])),'') IS NOT NULL
		AND NULLIF(LTRIM(RTRIM([FDOD])),'') IS NOT NULL
	)
	INSERT INTO #FDOD
	(
		 [MRN]
		,[FDOD]
	)
	SELECT DISTINCT
		 [MRN]
		,[FDOD]
	FROM FDOD_RANKED
	WHERE [RANK] = 1




	TRUNCATE TABLE [FHPDataMarts].[dbo].[Member_Master_FDOD]






	-- Upsert all sources into the final table


	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_FDOD]
	(
		 [MRN]
		,[FDOCD_DT]
		--,[PT_FRST_EVER_DIAL_DT]
		--,[FDOD]
	)
	SELECT DISTINCT
		 SourceTable.[MRN]
		,SourceTable.[FDOCD_DT]
	FROM #FDOCD_DT SourceTable
	LEFT JOIN [FHPDataMarts].[dbo].[Member_Master_FDOD] DestTable
	ON SourceTable.[MRN] = DestTable.[MRN]
	WHERE DestTable.[MRN] IS NULL --Only new records


	UPDATE DestTable
	SET
		DestTable.[FDOCD_DT] = SourceTable.[FDOCD_DT]
		--,[PT_FRST_EVER_DIAL_DT]
		--,[FDOD]
	FROM [FHPDataMarts].[dbo].[Member_Master_FDOD] DestTable
	JOIN #FDOCD_DT SourceTable
		ON SourceTable.[MRN] = DestTable.[MRN]




	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_FDOD]
	(
		 SourceTable.[MRN]
		--,[FDOCD_DT]
		,SourceTable.[PT_FRST_EVER_DIAL_DT]
		--,[FDOD]
	)
	SELECT DISTINCT
		 SourceTable.[MRN]
		,SourceTable.[PT_FRST_EVER_DIAL_DT]
	FROM #PT_FRST_EVER_DIAL_DT SourceTable
	LEFT JOIN [FHPDataMarts].[dbo].[Member_Master_FDOD] DestTable
	ON SourceTable.[MRN] = DestTable.[MRN]
	WHERE DestTable.[MRN] IS NULL --Only new records


	UPDATE DestTable
	SET
		[PT_FRST_EVER_DIAL_DT] = SourceTable.[PT_FRST_EVER_DIAL_DT]
		--,[PT_FRST_EVER_DIAL_DT]
		--,[FDOD]
	FROM [FHPDataMarts].[dbo].[Member_Master_FDOD] DestTable
	JOIN #PT_FRST_EVER_DIAL_DT SourceTable
		ON SourceTable.[MRN] = DestTable.[MRN]





	INSERT INTO [FHPDataMarts].[dbo].[Member_Master_FDOD]
	(
		 [MRN]
		--,[FDOCD_DT]
		--,[PT_FRST_EVER_DIAL_DT]
		,[FDOD]
	)
	SELECT DISTINCT
		 SourceTable.[MRN]
		,SourceTable.[FDOD]
	FROM #FDOD SourceTable
	LEFT JOIN [FHPDataMarts].[dbo].[Member_Master_FDOD] DestTable
	ON SourceTable.[MRN] = DestTable.[MRN]
	WHERE DestTable.[MRN] IS NULL --Only new records


	UPDATE DestTable
	SET
		[FDOD] = SourceTable.[FDOD]
		--,[PT_FRST_EVER_DIAL_DT]
		--,[FDOD]
	FROM [FHPDataMarts].[dbo].[Member_Master_FDOD] DestTable
	JOIN #FDOD SourceTable
		ON SourceTable.[MRN] = DestTable.[MRN]









END
GO

