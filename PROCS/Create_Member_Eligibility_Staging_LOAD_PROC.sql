

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Eligibility_Staging_LOAD', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Eligibility_Staging_LOAD;
GO


CREATE PROCEDURE [dbo].[Member_Eligibility_Staging_LOAD]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: April 25th 2017
-- Update date: Janurary 31st 2019
-- Description:	
-- ==========================================================================================


	UPDATE [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
		SET [ROW_UPDATE_DATE] = GETDATE()
	WHERE EXISTS
	(
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ESCO_ID]
			,[HLTH_PLN_SYSID]
			,[HLTH_PLN_PROD_LINE]
			,[HLTH_PLN_RPT_GRP]
			,[HLTH_PLN_STD_CARRIER_CD]
			,[PLAN_NAME]
			,[START_DATE]
			,[TERM_DATE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources]

		INTERSECT

		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ESCO_ID]
			,[HLTH_PLN_SYSID]
			,[HLTH_PLN_PROD_LINE]
			,[HLTH_PLN_RPT_GRP]
			,[HLTH_PLN_STD_CARRIER_CD]
			,[PLAN_NAME]
			,[START_DATE]
			,[TERM_DATE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
	)

	INSERT INTO [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ESCO_ID]
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	)
	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ESCO_ID]
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources]

	EXCEPT

	SELECT
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[ESCO_ID]
		,[HLTH_PLN_SYSID]
		,[HLTH_PLN_PROD_LINE]
		,[HLTH_PLN_RPT_GRP]
		,[HLTH_PLN_STD_CARRIER_CD]
		,[PLAN_NAME]
		,[START_DATE]
		,[TERM_DATE]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[LOB_VENDOR]
		,[LOB_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Eligibility_Staging]

END
GO

