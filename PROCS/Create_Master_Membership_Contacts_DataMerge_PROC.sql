USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Contacts_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Contacts_UPSERT;
GO


CREATE PROCEDURE [dbo].[Member_Contacts_UPSERT]

	@MEMBER_CONTACTS_STAGING_ROW_ID int NULL,
	@MHK_INTERNAL_ID int NULL,
	@MEDHOK_ID varchar(50) NULL,
	@SSN varchar(11) NULL,
	@HICN varchar(12) NULL,
	@CLAIM_SUBSCRIBER_ID varchar(50) NULL,
	@MBI varchar(11) NULL,
	@MEDICAID_NO varchar(50) NULL,
	@MRN varchar(50) NULL,
	@EXT_ID varchar(50) NULL,
	@EXT_ID_TYPE varchar(50) NULL,
	@EXT_ID_2 varchar(50) NULL,
	@EXT_ID_TYPE_2 varchar(50) NULL,
	@EXT_ID_3 varchar(50) NULL,
	@EXT_ID_TYPE_3 varchar(50) NULL,

	@CONTACT_TYPE varchar(50) NULL,
	@FIRST_NAME varchar(50) NULL,
	@MIDDLE_NAME varchar(50) NULL,
	@LAST_NAME varchar(50) NULL,
	@PREFIX varchar(10) NULL,
	@SUFFIX varchar(10) NULL,
	@DATE_OF_BIRTH date NULL,
	@GENDER varchar(10) NULL,
	@RELATION_TO_MEMBER varchar(50) NULL,
	@PROVIDER_ID_TYPE varchar(50) NULL,
	@PROVIDER_ID varchar(50) NULL,

	@LOB_VENDOR varchar(50) NULL,
	@LOB_TYPE varchar(50) NULL,

	@ROW_SOURCE varchar(500) NULL,
	@ROW_SOURCE_ID varchar(50) NULL

AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: December 14th 2017
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================


IF @MEMBER_CONTACTS_STAGING_ROW_ID IS NULL OR @ROW_SOURCE IS NULL OR @LOB_VENDOR IS NULL OR @LOB_TYPE IS NULL
BEGIN
	-- INVALID KEY, EXIT WITH FAILURE
	RAISERROR (15600,-1,-1, 'dbo.Member_Contacts_UPSERT')
	RETURN 1
END


-- Find the member
DECLARE @MEMBER_MASTER_ROW_COUNT AS INT = 0;
DECLARE @MEMBER_MASTER_ROW_ID AS INT = 0;



-- Used for gathering stats on key lookups.
DECLARE @KEY_SEARCH_RESULTS AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)


DECLARE @MEMBER_MASTER_UPDATED_ROW_ID AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)



/**********************************************************************************************************************

	Check all possible key collisions, and count the number of rows that this would effect on the master table.

	0 rows:  None of the current keys matched.  This member Contacts record is orphaned until a matching member
			 can be located.

	1 row:  At least one or more of the keys matched, with none mismatching.  This means a valid member was found
			for the Contact. 

	2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
					 table, meaning that an insert cannot happen because this Contacts record cannot be uniquely
					 matched to a member.  This staging row should be flagged as being in error, and a human should
					 investigate why the same keys are being used in more than once place.
	

**********************************************************************************************************************/
;WITH AllKeys AS
(
	-- Historical
	SELECT
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	WHERE ROW_DELETED = 'N'
)
INSERT INTO @KEY_SEARCH_RESULTS
(
	[MEMBER_MASTER_ROW_ID]
)
SELECT DISTINCT
	[MEMBER_MASTER_ROW_ID]
FROM AllKeys
WHERE 1=2
OR @MHK_INTERNAL_ID = AllKeys.[MHK_INTERNAL_ID]
OR @MEDHOK_ID = AllKeys.[MEDHOK_ID]
OR @SSN = AllKeys.[SSN]
OR @HICN = AllKeys.[HICN]
OR @CLAIM_SUBSCRIBER_ID = AllKeys.[CLAIM_SUBSCRIBER_ID]
OR @MBI = AllKeys.[MBI]
OR @MEDICAID_NO = AllKeys.[MEDICAID_NO]
OR @MRN = AllKeys.[MRN]
OR CASE WHEN @EXT_ID IS NOT NULL AND @EXT_ID_TYPE IS NOT NULL THEN CONCAT(@EXT_ID,':',@EXT_ID_TYPE) ELSE NULL END = AllKeys.[EXT_ID_AND_TYPE]


-- Get number of matching results
SELECT @MEMBER_MASTER_ROW_COUNT = COUNT(1) FROM @KEY_SEARCH_RESULTS;

-- Get the record, if it's unique
IF @MEMBER_MASTER_ROW_COUNT = 1
BEGIN
	SELECT @MEMBER_MASTER_ROW_ID = [MEMBER_MASTER_ROW_ID] FROM @KEY_SEARCH_RESULTS
END



-- 0 rows:  None of the current keys matched.  This is an orphan record.
IF @MEMBER_MASTER_ROW_COUNT = 0
BEGIN

	UPDATE [FHPDataMarts].[dbo].[Member_Contacts_Staging]
	SET
		[ROW_PROBLEM] = 'Y',
		[ROW_PROBLEM_REASON] = 'No matching member found.',
		[ROW_PROBLEM_DATE] = GETDATE()
	WHERE [MEMBER_CONTACTS_STAGING_ROW_ID] = @MEMBER_CONTACTS_STAGING_ROW_ID;


	-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
	RETURN 0

END





-- 1 row:  The Contact record uniquely matches to a single member.  We can insert it now.
IF @MEMBER_MASTER_ROW_COUNT = 1
BEGIN

	INSERT INTO [FHPDW].[dbo].[Member_Contacts]
	(
		-- [MEMBER_CONTACTS_ROW_ID]
		 [MEMBER_MASTER_ROW_ID]

		,[CONTACT_TYPE]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RELATION_TO_MEMBER]
		,[PROVIDER_ID_TYPE]
		,[PROVIDER_ID]

		,[LOB_VENDOR]
		,[LOB_TYPE]
		,[MEMBER_CONTACTS_STAGING_ROW_ID]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	VALUES
	(	
		@MEMBER_MASTER_ROW_ID,  --@MEMBER_MASTER_ROW_ID   (from lookup)

		@CONTACT_TYPE,
		@FIRST_NAME,
		@MIDDLE_NAME,
		@LAST_NAME,
		@PREFIX,
		@SUFFIX,
		@DATE_OF_BIRTH,
		@GENDER,
		@RELATION_TO_MEMBER,
		@PROVIDER_ID_TYPE,
		@PROVIDER_ID,

		@LOB_VENDOR,  --[LOB_VENDOR]
		@LOB_TYPE,  --[LOB_TYPE]
		@MEMBER_CONTACTS_STAGING_ROW_ID,

		@ROW_SOURCE,  --[ROW_SOURCE]
		@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
		  --[ROW_PROCESSED]
		  --[ROW_PROCESSED_DATE]
		  --[ROW_DELETED]
		  --[ROW_DELETED_DATE]
		  --[ROW_DELETED_REASON]
		  --[ROW_CREATE_DATE]
		  --[ROW_UPDATE_DATE]
	);


	-- Update the staging table.
	UPDATE [FHPDataMarts].[dbo].[Member_Contacts_Staging]
	SET
		[ROW_PROCESSED] = 'Y',
		[ROW_PROCESSED_DATE] = GETDATE()
	WHERE [MEMBER_CONTACTS_STAGING_ROW_ID] = @MEMBER_CONTACTS_STAGING_ROW_ID;


	-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
	RETURN 0

END


/*
	2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
					 table, meaning that an insert cannot happen because this Contacts record cannot be uniquely
					 matched to a member.  This staging row should be flagged as being in error, and a human should
					 investigate why the same keys are being used in more than once place.
*/
IF @MEMBER_MASTER_ROW_COUNT > 1
BEGIN

	UPDATE [FHPDataMarts].[dbo].[Member_Contacts_Staging]
	SET
		[ROW_PROBLEM] = 'Y',
		[ROW_PROBLEM_REASON] = 'The keys on this record match more than 1 record on the member master table.  Requires human analysis and intervention.',
		[ROW_PROBLEM_DATE] = GETDATE()
	WHERE [MEMBER_CONTACTS_STAGING_ROW_ID] = @MEMBER_CONTACTS_STAGING_ROW_ID

END


-- Return success
RETURN 0




END
GO


