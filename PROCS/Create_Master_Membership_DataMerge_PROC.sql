USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_UPSERT;
GO


CREATE PROCEDURE [dbo].[Member_Master_UPSERT]

	@MEMBER_MASTER_STAGING_ROW_ID int NULL,
	@MHK_INTERNAL_ID int NULL,
	@MEDHOK_ID varchar(50) NULL,
	@SSN varchar(11) NULL,
	@HICN varchar(12) NULL,
	@CLAIM_SUBSCRIBER_ID varchar(50) NULL,
	@MBI varchar(11) NULL,
	@MEDICAID_NO varchar(50) NULL,
	@MRN varchar(50) NULL,
	@EXT_ID varchar(50) NULL,
	@EXT_ID_TYPE varchar(50) NULL,
	@EXT_ID_2 varchar(50) NULL,
	@EXT_ID_TYPE_2 varchar(50) NULL,
	@EXT_ID_3 varchar(50) NULL,
	@EXT_ID_TYPE_3 varchar(50) NULL,
	@BENEFIT_STATUS varchar(1) NULL,
	@FIRST_NAME varchar(50) NULL,
	@MIDDLE_NAME varchar(50) NULL,
	@LAST_NAME varchar(50) NULL,
	@PREFIX varchar(50) NULL,
	@SUFFIX varchar(50) NULL,
	@DATE_OF_BIRTH date NULL,
	@GENDER varchar(1) NULL,
	@COMPANY_DESCRIPTION varchar(50) NULL,
	@LOB_CODE varchar(50) NULL,
	@FAMILY_ID varchar(50) NULL,
	@PERSON_NUMBER varchar(50) NULL,
	@RACE varchar(50) NULL,
	@ETHNICITY varchar(50) NULL,
	@PRIMARY_LANGUAGE varchar(50) NULL,
	@PRIMARY_LANGUAGE_SOURCE varchar(50) NULL,
	@SPOKEN_LANGUAGE varchar(50) NULL,
	@SPOKEN_LANGUAGE_SOURCE varchar(50) NULL,
	@WRITTEN_LANGUAGE varchar(50) NULL,
	@WRITTEN_LANGUAGE_SOURCE varchar(50) NULL,
	@OTHER_LANGUAGE varchar(50) NULL,
	@OTHER_LANGUAGE_SOURCE varchar(50) NULL,
	@EMPLOYEE varchar(1) NULL,
	@PBP_NUMBER varchar(15) NULL,
	@CURRENT_LIS varchar(50) NULL,
	@IPA_GROUP_EXT_ID varchar(50) NULL,
	@MEDICARE_PLAN_CODE varchar(50) NULL,
	@MEDICARE_TYPE varchar(50) NULL,
	@DUPLICATE_MEDICAID_ID varchar(50) NULL,
	@PREGNANCY_DUE_DATE date NULL,
	@PREGNANCY_INDICATOR varchar(1) NULL,
	@BOARD_NUMBER varchar(50) NULL,
	@DEPENDENT_CODE varchar(50) NULL,
	@LEGACY_SUBSCRIBER_ID varchar(50) NULL,
	@GROUP_NUMBER varchar(50) NULL,
	@SOURCE varchar(50) NULL,
	@ESCO_ID varchar(50) NULL,
	@CLIENT_SPECIFIC_DATA varchar(50) NULL,
	@RELATIONSHIP_CODE varchar(10) NULL,
	@TIME_ZONE varchar(10) NULL,
	@DATE_OF_DEATH date NULL,
	@FOSTER_CARE_FLAG varchar(1) NULL,
	@VIP varchar(1) NULL,
	@CLINIC_NUMBER varchar(50) NULL,
	@MODALITY varchar(50) NULL,
	@PAYER_NAME varchar(50) NULL,
	@PAYER_ID_TYPE varchar(50) NULL,
	@PAYER_ID varchar(50) NULL,
	@DIALYSIS_START_DATE date NULL,
	@KIDNEY_TRANSPLANT_DATE date NULL,
	@LOB_VENDOR varchar(50) NULL,
	@LOB_TYPE varchar(50) NULL,
	@ROW_SOURCE varchar(500) NULL,
	@ROW_SOURCE_ID varchar(50) NULL
	--@ROW_CREATE_DATE datetime2(7) NULL,
	--@ROW_UPDATE_DATE datetime2(7) NULL

AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: September 26th 2017
-- Description:	
-- ==========================================================================================
/*
*/
-- ==========================================================================================


IF @MEMBER_MASTER_STAGING_ROW_ID IS NULL OR @ROW_SOURCE IS NULL
BEGIN
	-- INVALID KEY, EXIT WITH FAILURE
	RAISERROR (15600,-1,-1, 'dbo.Member_Master_UPSERT')
	RETURN 1
END


-- Find the member
DECLARE @MEMBER_MASTER_ROW_COUNT AS INT = 0;
DECLARE @MEMBER_MASTER_ROW_ID AS INT = 0;

DECLARE @UPDATE_ALLOWED AS INT = 1;


-- Used for gathering stats on key lookups.  Used for both Member and Eligibility lookups.
DECLARE @KEY_SEARCH_RESULTS AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)




DECLARE @MEMBER_MASTER_INSERTED_ROW_ID AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)


DECLARE @MEMBER_MASTER_UPDATED_ROW_ID AS TABLE
(
	[MEMBER_MASTER_ROW_ID] INT
)




/**********************************************************************************************************************

	Check all possible key collisions, and count the number of rows that this would effect on the master table.

	0 rows:  None of the current keys matched.  This is a new member, requiring an insert.

	1 row:  At least one or more of the keys matched, with none mismatching.  This is a valid update to a member.

	2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
					 table, meaning that an insert cannot happen because it would cause a duplication of one or more
					 keys.  This staging row should be flagged as being in error, and a human should investigate why
					 the same keys are being used in more than once place.
	

**********************************************************************************************************************/


;WITH AllKeys AS
(
	-- Historical
	SELECT
		 [MEMBER_MASTER_ROW_ID]
		,[MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
	FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
	WHERE ROW_DELETED = 'N'
)
INSERT INTO @KEY_SEARCH_RESULTS
(
	[MEMBER_MASTER_ROW_ID]
)
SELECT DISTINCT
	[MEMBER_MASTER_ROW_ID]
FROM AllKeys
WHERE 1=2
OR @MHK_INTERNAL_ID = AllKeys.[MHK_INTERNAL_ID]
OR @MEDHOK_ID = AllKeys.[MEDHOK_ID]
OR @SSN = AllKeys.[SSN]
OR @HICN = AllKeys.[HICN]
OR @CLAIM_SUBSCRIBER_ID = AllKeys.[CLAIM_SUBSCRIBER_ID]
OR @MBI = AllKeys.[MBI]
OR @MEDICAID_NO = AllKeys.[MEDICAID_NO]
OR @MRN = AllKeys.[MRN]
OR CASE WHEN @EXT_ID IS NOT NULL AND @EXT_ID_TYPE IS NOT NULL THEN CONCAT(@EXT_ID,':',@EXT_ID_TYPE) ELSE NULL END = AllKeys.[EXT_ID_AND_TYPE]


-- Get number of matching results
SELECT @MEMBER_MASTER_ROW_COUNT = COUNT(1) FROM @KEY_SEARCH_RESULTS;

-- Get the record, if it's unique
IF @MEMBER_MASTER_ROW_COUNT = 1
BEGIN
	SELECT @MEMBER_MASTER_ROW_ID = [MEMBER_MASTER_ROW_ID] FROM @KEY_SEARCH_RESULTS
END






-- 0 rows:  None of the current keys matched.  This is a new member, requiring an insert.
IF @MEMBER_MASTER_ROW_COUNT = 0
BEGIN

	-- Insert the new member
	INSERT INTO [FHPDW].[dbo].[Member_Master]
	(
		 [MHK_INTERNAL_ID]
		,[MEDHOK_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[EXT_ID_2]
		,[EXT_ID_TYPE_2]
		,[EXT_ID_3]
		,[EXT_ID_TYPE_3]
		,[BENEFIT_STATUS]
		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[PREFIX]
		,[SUFFIX]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[COMPANY_DESCRIPTION]
		,[LOB_CODE]
		,[FAMILY_ID]
		,[PERSON_NUMBER]
		,[RACE]
		,[ETHNICITY]
		,[PRIMARY_LANGUAGE]
		,[PRIMARY_LANGUAGE_SOURCE]
		,[SPOKEN_LANGUAGE]
		,[SPOKEN_LANGUAGE_SOURCE]
		,[WRITTEN_LANGUAGE]
		,[WRITTEN_LANGUAGE_SOURCE]
		,[OTHER_LANGUAGE]
		,[OTHER_LANGUAGE_SOURCE]
		,[EMPLOYEE]
		,[PBP_NUMBER]
		,[CURRENT_LIS]
		,[IPA_GROUP_EXT_ID]
		,[MEDICARE_PLAN_CODE]
		,[MEDICARE_TYPE]
		,[DUPLICATE_MEDICAID_ID]
		,[PREGNANCY_DUE_DATE]
		,[PREGNANCY_INDICATOR]
		,[BOARD_NUMBER]
		,[DEPENDENT_CODE]
		,[LEGACY_SUBSCRIBER_ID]
		,[GROUP_NUMBER]
		,[SOURCE]
		,[ESCO_ID]
		,[CLIENT_SPECIFIC_DATA]
		,[RELATIONSHIP_CODE]
		,[TIME_ZONE]
		,[DATE_OF_DEATH]
		,[FOSTER_CARE_FLAG]
		,[VIP]
		,[CLINIC_NUMBER]
		,[MODALITY]
		,[PAYER_NAME]
		,[PAYER_ID_TYPE]
		,[PAYER_ID]
		,[DIALYSIS_START_DATE]
		,[KIDNEY_TRANSPLANT_DATE]

		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]

		,[MEMBER_MASTER_STAGING_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	OUTPUT INSERTED.[MEMBER_MASTER_ROW_ID] INTO @MEMBER_MASTER_INSERTED_ROW_ID
	SELECT
		 @MHK_INTERNAL_ID
		,@MEDHOK_ID
		,@SSN
		,@HICN
		,@CLAIM_SUBSCRIBER_ID
		,@MBI
		,@MEDICAID_NO
		,@MRN
		,@EXT_ID
		,@EXT_ID_TYPE
		,@EXT_ID_2
		,@EXT_ID_TYPE_2
		,@EXT_ID_3
		,@EXT_ID_TYPE_3
		,@BENEFIT_STATUS
		,@FIRST_NAME
		,@MIDDLE_NAME
		,@LAST_NAME
		,@PREFIX
		,@SUFFIX
		,@DATE_OF_BIRTH
		,@GENDER
		,@COMPANY_DESCRIPTION
		,@LOB_CODE
		,@FAMILY_ID
		,@PERSON_NUMBER
		,@RACE
		,@ETHNICITY
		,@PRIMARY_LANGUAGE
		,@PRIMARY_LANGUAGE_SOURCE
		,@SPOKEN_LANGUAGE
		,@SPOKEN_LANGUAGE_SOURCE
		,@WRITTEN_LANGUAGE
		,@WRITTEN_LANGUAGE_SOURCE
		,@OTHER_LANGUAGE
		,@OTHER_LANGUAGE_SOURCE
		,@EMPLOYEE
		,@PBP_NUMBER
		,@CURRENT_LIS
		,@IPA_GROUP_EXT_ID
		,@MEDICARE_PLAN_CODE
		,@MEDICARE_TYPE
		,@DUPLICATE_MEDICAID_ID
		,@PREGNANCY_DUE_DATE
		,@PREGNANCY_INDICATOR
		,@BOARD_NUMBER
		,@DEPENDENT_CODE
		,@LEGACY_SUBSCRIBER_ID
		,@GROUP_NUMBER
		,@SOURCE
		,@ESCO_ID
		,@CLIENT_SPECIFIC_DATA
		,@RELATIONSHIP_CODE
		,@TIME_ZONE
		,@DATE_OF_DEATH
		,@FOSTER_CARE_FLAG
		,@VIP
		,@CLINIC_NUMBER
		,@MODALITY
		,@PAYER_NAME
		,@PAYER_ID_TYPE
		,@PAYER_ID
		,@DIALYSIS_START_DATE
		,@KIDNEY_TRANSPLANT_DATE

		,'INACTIVE'  --[STATUS]
		,@LOB_VENDOR
		,@LOB_TYPE
		
		,@MEMBER_MASTER_STAGING_ROW_ID
		,@ROW_SOURCE
		,@ROW_SOURCE_ID

		,GETDATE()  --@ROW_CREATE_DATE
		,GETDATE(); --@ROW_UPDATE_DATE


	-- Grab the inserted [MEMBER_MASTER_ROW_ID] so we can use it when inserting into the KeyHistory
	DECLARE @MEMBER_MASTER_ROW_ID_INSERTED_VALUE INT;

	SELECT TOP 1 @MEMBER_MASTER_ROW_ID_INSERTED_VALUE = [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED_ROW_ID;


	-- Insert a record into [Member_Master_KeyHistory] if none is found.  This is done now, so that the following [Member_MedHOK_Crosswalk] updates
	-- can take place.  [Member_MedHOK_Crosswalk] fetches MEMBER_MASTER_ROW_ID from [Member_Master_KeyHistory].
	IF (@MEDHOK_ID IS NOT NULL) AND (NOT EXISTS (SELECT [MEDHOK_ID] FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] WHERE [MEDHOK_ID] = @MEDHOK_ID))
	BEGIN

		WITH UniqueKeyCombo AS
		(
			-- Select the unique keys
			SELECT
				 @MEMBER_MASTER_ROW_ID_INSERTED_VALUE AS [MEMBER_MASTER_ROW_ID]  -- From the insert
				,@MHK_INTERNAL_ID AS [MHK_INTERNAL_ID]
				,@MEDHOK_ID AS [MEDHOK_ID]
				,@SSN AS [SSN]
				,@HICN AS [HICN]
				,@CLAIM_SUBSCRIBER_ID AS [CLAIM_SUBSCRIBER_ID]
				,@MBI AS [MBI]
				,@MEDICAID_NO AS [MEDICAID_NO]
				,@MRN AS [MRN]
				,@EXT_ID AS [EXT_ID]
				,@EXT_ID_TYPE AS [EXT_ID_TYPE]
				,@EXT_ID_2 AS [EXT_ID_2]
				,@EXT_ID_TYPE_2 AS [EXT_ID_TYPE_2]
				,@EXT_ID_3 AS [EXT_ID_3]
				,@EXT_ID_TYPE_3 AS [EXT_ID_TYPE_3]

			EXCEPT

			-- Unless it already exists
			SELECT
				 [MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		)
		INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		(
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
		)
		SELECT
			 UniqueKeyCombo.[MEMBER_MASTER_ROW_ID]
			,UniqueKeyCombo.[MHK_INTERNAL_ID]
			,UniqueKeyCombo.[MEDHOK_ID]
			,UniqueKeyCombo.[SSN]
			,UniqueKeyCombo.[HICN]
			,UniqueKeyCombo.[CLAIM_SUBSCRIBER_ID]
			,UniqueKeyCombo.[MBI]
			,UniqueKeyCombo.[MEDICAID_NO]
			,UniqueKeyCombo.[MRN]
			,UniqueKeyCombo.[EXT_ID]
			,UniqueKeyCombo.[EXT_ID_TYPE]
			,UniqueKeyCombo.[EXT_ID_2]
			,UniqueKeyCombo.[EXT_ID_TYPE_2]
			,UniqueKeyCombo.[EXT_ID_3]
			,UniqueKeyCombo.[EXT_ID_TYPE_3]

			,'Member UPSERT Proc: MEMBER_MASTER_STAGING_ROW_ID'  --[ROW_SOURCE]
			,@MEMBER_MASTER_STAGING_ROW_ID  --[ROW_SOURCE_ID]
		FROM UniqueKeyCombo  --This is suppose to be empty, by design, if the inserted keys were not unique (and therefore add no value to this table)

	END



	-- Insert into the MedHOK Crosswalk if not found
	IF (@MEDHOK_ID IS NOT NULL) AND (NOT EXISTS (SELECT [MEDHOK_ID] FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] WHERE [MEDHOK_ID] = @MEDHOK_ID))
	BEGIN

		-- Insert the "Primary" records
		INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
		(
			 [MEMBER_MASTER_ROW_ID]
			,[MEDHOK_ID]
			,[MEDHOK_ID_TYPE]
		)
		SELECT
			 SRC.[MEMBER_MASTER_ROW_ID]
			,SRC.[MEDHOK_ID]
			,SRC.[MEDHOK_ID_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
		LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
		ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
		WHERE SRC.[MEDHOK_ID] IS NOT NULL
		AND TBL.[MEDHOK_ID] IS NULL
		AND SRC.[MEDHOK_ID_TYPE] = 'PRIMARY';


		-- Insert the "Duplicate" records
		INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
		(
				[MEMBER_MASTER_ROW_ID]
			,[MEDHOK_ID]
			,[MEDHOK_ID_TYPE]
		)
		SELECT
				SRC.[MEMBER_MASTER_ROW_ID]
			,SRC.[MEDHOK_ID]
			,'DUPLICATE'  -- This is hard-coded, to ensure that only one "Primary" record is received
		FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
		LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
		ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
		WHERE SRC.[MEDHOK_ID] IS NOT NULL
		AND TBL.[MEDHOK_ID] IS NULL

	END



	UPDATE [FHPDataMarts].[dbo].[Member_Master_Staging]
	SET
		[ROW_PROCESSED] = 'Y',
		[ROW_PROCESSED_DATE] = GETDATE()
	WHERE [MEMBER_MASTER_STAGING_ROW_ID] = @MEMBER_MASTER_STAGING_ROW_ID;


	-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
	RETURN 0

END



-- 1 row:  At least one or more of the keys matched, with none mismatching.  This is a valid UPDATE to a member.
IF @MEMBER_MASTER_ROW_COUNT = 1
BEGIN

		-- Insert a record into [Member_Master_KeyHistory] if none is found.  This is done now, so that the following [Member_MedHOK_Crosswalk] updates
		-- can take place.  [Member_MedHOK_Crosswalk] fetches MEMBER_MASTER_ROW_ID from [Member_Master_KeyHistory].
		IF (@MEDHOK_ID IS NOT NULL) AND (NOT EXISTS (SELECT [MEDHOK_ID] FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] WHERE [MEDHOK_ID] = @MEDHOK_ID))
		BEGIN

			WITH UniqueKeyCombo AS
			(
				-- Select the unique keys
				SELECT
					 @MEMBER_MASTER_ROW_ID AS [MEMBER_MASTER_ROW_ID]
					,@MHK_INTERNAL_ID AS [MHK_INTERNAL_ID]
					,@MEDHOK_ID AS [MEDHOK_ID]
					,@SSN AS [SSN]
					,@HICN AS [HICN]
					,@CLAIM_SUBSCRIBER_ID AS [CLAIM_SUBSCRIBER_ID]
					,@MBI AS [MBI]
					,@MEDICAID_NO AS [MEDICAID_NO]
					,@MRN AS [MRN]
					,@EXT_ID AS [EXT_ID]
					,@EXT_ID_TYPE AS [EXT_ID_TYPE]
					,@EXT_ID_2 AS [EXT_ID_2]
					,@EXT_ID_TYPE_2 AS [EXT_ID_TYPE_2]
					,@EXT_ID_3 AS [EXT_ID_3]
					,@EXT_ID_TYPE_3 AS [EXT_ID_TYPE_3]

				EXCEPT

				-- Unless it already exists
				SELECT
					 [MEMBER_MASTER_ROW_ID]
					,[MHK_INTERNAL_ID]
					,[MEDHOK_ID]
					,[SSN]
					,[HICN]
					,[CLAIM_SUBSCRIBER_ID]
					,[MBI]
					,[MEDICAID_NO]
					,[MRN]
					,[EXT_ID]
					,[EXT_ID_TYPE]
					,[EXT_ID_2]
					,[EXT_ID_TYPE_2]
					,[EXT_ID_3]
					,[EXT_ID_TYPE_3]
				FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
			)
			INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]

				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
			)
			SELECT
				 UniqueKeyCombo.[MEMBER_MASTER_ROW_ID]
				,UniqueKeyCombo.[MHK_INTERNAL_ID]
				,UniqueKeyCombo.[MEDHOK_ID]
				,UniqueKeyCombo.[SSN]
				,UniqueKeyCombo.[HICN]
				,UniqueKeyCombo.[CLAIM_SUBSCRIBER_ID]
				,UniqueKeyCombo.[MBI]
				,UniqueKeyCombo.[MEDICAID_NO]
				,UniqueKeyCombo.[MRN]
				,UniqueKeyCombo.[EXT_ID]
				,UniqueKeyCombo.[EXT_ID_TYPE]
				,UniqueKeyCombo.[EXT_ID_2]
				,UniqueKeyCombo.[EXT_ID_TYPE_2]
				,UniqueKeyCombo.[EXT_ID_3]
				,UniqueKeyCombo.[EXT_ID_TYPE_3]

				,'Member UPSERT Proc: MEMBER_MASTER_STAGING_ROW_ID'  --[ROW_SOURCE]
				,@MEMBER_MASTER_STAGING_ROW_ID  --[ROW_SOURCE_ID]
			FROM UniqueKeyCombo  --This is suppose to be empty, by design, if the inserted keys were not unique (and therefore add no value to this table)

		END



		-- Insert into the MedHOK Crosswalk if not found
		IF (@MEDHOK_ID IS NOT NULL) AND (NOT EXISTS (SELECT [MEDHOK_ID] FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] WHERE [MEDHOK_ID] = @MEDHOK_ID))
		BEGIN

			-- Insert the "Primary" records
			INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MEDHOK_ID]
				,[MEDHOK_ID_TYPE]
			)
			SELECT
				 SRC.[MEMBER_MASTER_ROW_ID]
				,SRC.[MEDHOK_ID]
				,SRC.[MEDHOK_ID_TYPE]
			FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
			LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
			ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
			WHERE SRC.[MEDHOK_ID] IS NOT NULL
			AND TBL.[MEDHOK_ID] IS NULL
			AND SRC.[MEDHOK_ID_TYPE] = 'PRIMARY';


			-- Insert the "Duplicate" records
			INSERT INTO [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MEDHOK_ID]
				,[MEDHOK_ID_TYPE]
			)
			SELECT
				 SRC.[MEMBER_MASTER_ROW_ID]
				,SRC.[MEDHOK_ID]
				,'DUPLICATE'  -- This is hard-coded, to ensure that only one "Primary" record is received
			FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk_Source] SRC
			LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] TBL
			ON SRC.[MEDHOK_ID] = TBL.[MEDHOK_ID]
			WHERE SRC.[MEDHOK_ID] IS NOT NULL
			AND TBL.[MEDHOK_ID] IS NULL

		END



		-- Find the rank of the incoming record
		DECLARE @LOB_RANK INT;

		SELECT TOP 1
			@LOB_RANK = LOB.[LOB_RANK]
		FROM [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOB
		WHERE LOB.[LOB_TYPE] = @LOB_TYPE
		AND LOB.[LOB_VENDOR] = @LOB_VENDOR
		ORDER BY LOB.LOB_RANK ASC


		UPDATE MM
			SET
				 [MHK_INTERNAL_ID] = COALESCE(@MHK_INTERNAL_ID,[MHK_INTERNAL_ID])
				,[MEDHOK_ID] = COALESCE(XW.[MEDHOK_ID], MM.[MEDHOK_ID], @MEDHOK_ID)   -- Uses Crosswalk Table
				,[SSN] = COALESCE(@SSN, [SSN])
				,[HICN] = COALESCE(@HICN, [HICN])
				,[CLAIM_SUBSCRIBER_ID] = COALESCE(@CLAIM_SUBSCRIBER_ID, [CLAIM_SUBSCRIBER_ID])
				,[MBI] = COALESCE(@MBI, [MBI])
				,[MEDICAID_NO] = COALESCE(@MEDICAID_NO, [MEDICAID_NO])
				,[MRN] = COALESCE(@MRN, [MRN])
				,[EXT_ID] = COALESCE(@EXT_ID, [EXT_ID])
				,[EXT_ID_TYPE] = COALESCE(@EXT_ID_TYPE, [EXT_ID_TYPE])
				,[EXT_ID_2] = COALESCE(@EXT_ID_2, [EXT_ID_2])
				,[EXT_ID_TYPE_2] = COALESCE(@EXT_ID_TYPE_2, [EXT_ID_TYPE_2])
				,[EXT_ID_3] = COALESCE(@EXT_ID_3, [EXT_ID_3])
				,[EXT_ID_TYPE_3] = COALESCE(@EXT_ID_TYPE_3, [EXT_ID_TYPE_3])
				,[BENEFIT_STATUS] = COALESCE(@BENEFIT_STATUS, [BENEFIT_STATUS])
				,[FIRST_NAME] = COALESCE(@FIRST_NAME, [FIRST_NAME])
				,[MIDDLE_NAME] = COALESCE(@MIDDLE_NAME, [MIDDLE_NAME])
				,[LAST_NAME] = COALESCE(@LAST_NAME, [LAST_NAME])
				,[PREFIX] = COALESCE(@PREFIX, [PREFIX])
				,[SUFFIX] = COALESCE(@SUFFIX, [SUFFIX])
				,[DATE_OF_BIRTH] = COALESCE(@DATE_OF_BIRTH, [DATE_OF_BIRTH])
				,[GENDER] = COALESCE(@GENDER, [GENDER])
				,[COMPANY_DESCRIPTION] = COALESCE(@COMPANY_DESCRIPTION, [COMPANY_DESCRIPTION])
				,[LOB_CODE] = COALESCE(@LOB_CODE, [LOB_CODE])
				,[FAMILY_ID] = COALESCE(@FAMILY_ID, [FAMILY_ID])
				,[PERSON_NUMBER] = COALESCE(@PERSON_NUMBER, [PERSON_NUMBER])
				,[RACE] = COALESCE(@RACE, [RACE])
				,[ETHNICITY] = COALESCE(@ETHNICITY, [ETHNICITY])
				,[PRIMARY_LANGUAGE] = COALESCE(@PRIMARY_LANGUAGE, [PRIMARY_LANGUAGE])
				,[PRIMARY_LANGUAGE_SOURCE] = COALESCE(@PRIMARY_LANGUAGE_SOURCE, [PRIMARY_LANGUAGE_SOURCE])
				,[SPOKEN_LANGUAGE] = COALESCE(@SPOKEN_LANGUAGE, [SPOKEN_LANGUAGE])
				,[SPOKEN_LANGUAGE_SOURCE] = COALESCE(@SPOKEN_LANGUAGE_SOURCE, [SPOKEN_LANGUAGE_SOURCE])
				,[WRITTEN_LANGUAGE] = COALESCE(@WRITTEN_LANGUAGE, [WRITTEN_LANGUAGE])
				,[WRITTEN_LANGUAGE_SOURCE] = COALESCE(@WRITTEN_LANGUAGE_SOURCE, [WRITTEN_LANGUAGE_SOURCE])
				,[OTHER_LANGUAGE] = COALESCE(@OTHER_LANGUAGE, [OTHER_LANGUAGE])
				,[OTHER_LANGUAGE_SOURCE] = COALESCE(@OTHER_LANGUAGE_SOURCE, [OTHER_LANGUAGE_SOURCE])
				,[EMPLOYEE] = COALESCE(@EMPLOYEE, [EMPLOYEE])
				,[PBP_NUMBER] = COALESCE(@PBP_NUMBER, [PBP_NUMBER])
				,[CURRENT_LIS] = COALESCE(@CURRENT_LIS, [CURRENT_LIS])
				,[IPA_GROUP_EXT_ID] = COALESCE(@IPA_GROUP_EXT_ID, [IPA_GROUP_EXT_ID])
				,[MEDICARE_PLAN_CODE] = COALESCE(@MEDICARE_PLAN_CODE, [MEDICARE_PLAN_CODE])
				,[MEDICARE_TYPE] = COALESCE(@MEDICARE_TYPE, [MEDICARE_TYPE])
				,[DUPLICATE_MEDICAID_ID] = COALESCE(@DUPLICATE_MEDICAID_ID, [DUPLICATE_MEDICAID_ID])
				,[PREGNANCY_DUE_DATE] = COALESCE(@PREGNANCY_DUE_DATE, [PREGNANCY_DUE_DATE])
				,[PREGNANCY_INDICATOR] = COALESCE(@PREGNANCY_INDICATOR, [PREGNANCY_INDICATOR])
				,[BOARD_NUMBER] = COALESCE(@BOARD_NUMBER, [BOARD_NUMBER])
				,[DEPENDENT_CODE] = COALESCE(@DEPENDENT_CODE, [DEPENDENT_CODE])
				,[LEGACY_SUBSCRIBER_ID] = COALESCE(@LEGACY_SUBSCRIBER_ID, [LEGACY_SUBSCRIBER_ID])
				,[GROUP_NUMBER] = COALESCE(@GROUP_NUMBER, [GROUP_NUMBER])
				,[SOURCE] = COALESCE(@SOURCE, [SOURCE])
				,[ESCO_ID] = COALESCE(@ESCO_ID, [ESCO_ID])
				,[CLIENT_SPECIFIC_DATA] = COALESCE(@CLIENT_SPECIFIC_DATA, [CLIENT_SPECIFIC_DATA])
				,[RELATIONSHIP_CODE] = COALESCE(@RELATIONSHIP_CODE, [RELATIONSHIP_CODE])
				,[TIME_ZONE] = COALESCE(@TIME_ZONE, [TIME_ZONE])
				,[DATE_OF_DEATH] = COALESCE(@DATE_OF_DEATH, [DATE_OF_DEATH])
				,[FOSTER_CARE_FLAG] = COALESCE(@FOSTER_CARE_FLAG, [FOSTER_CARE_FLAG])
				,[VIP] = COALESCE(@VIP, [VIP])
				,[CLINIC_NUMBER] = COALESCE(@CLINIC_NUMBER, [CLINIC_NUMBER])
				,[MODALITY] = COALESCE(@MODALITY, [MODALITY])
				,[PAYER_NAME] = COALESCE(@PAYER_NAME, [PAYER_NAME])
				,[PAYER_ID_TYPE] = COALESCE(@PAYER_ID_TYPE, [PAYER_ID_TYPE])
				,[PAYER_ID] = COALESCE(@PAYER_ID, [PAYER_ID])
				,[DIALYSIS_START_DATE] = COALESCE(@DIALYSIS_START_DATE, [DIALYSIS_START_DATE])
				,[KIDNEY_TRANSPLANT_DATE] = COALESCE(@KIDNEY_TRANSPLANT_DATE, [KIDNEY_TRANSPLANT_DATE])

				--,[LOB_VENDOR]   -- These two are omitted because they will be updated later by the eligibility table insert procedure, if an update is required.
				--,[LOB_TYPE]   -- These two are omitted because they will be updated later by the eligibility table insert procedure, if an update is required.
				,[MEMBER_MASTER_STAGING_ROW_ID] = @MEMBER_MASTER_STAGING_ROW_ID

				,[ROW_SOURCE] = COALESCE(@ROW_SOURCE, [ROW_SOURCE])
				,[ROW_SOURCE_ID] = COALESCE(@ROW_SOURCE_ID, [ROW_SOURCE_ID])

				--,[ROW_PROBLEM] = CASE WHEN [ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN 'N' ELSE [ROW_PROBLEM] END
				--,[ROW_PROBLEM_REASON] = CASE WHEN [ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE [ROW_PROBLEM_REASON] END
				--,[ROW_PROBLEM_DATE] = CASE WHEN [ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE [ROW_PROBLEM_DATE] END

				--,[ROW_CREATE_DATE] = COALESCE(@ROW_CREATE_DATE, [ROW_CREATE_DATE])
				,[ROW_UPDATE_DATE] = GETDATE()
			OUTPUT INSERTED.[MEMBER_MASTER_ROW_ID] INTO @MEMBER_MASTER_UPDATED_ROW_ID
			FROM [FHPDW].[dbo].[Member_Master] MM
			JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOB
			ON MM.[LOB_VENDOR] = LOB.[LOB_VENDOR]
			AND MM.[LOB_TYPE] = LOB.[LOB_TYPE]
			LEFT OUTER JOIN [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW
			ON XW.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
			AND XW.[MEDHOK_ID_TYPE] = 'PRIMARY'
			WHERE MM.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
			AND LOB.[LOB_RANK] >= @LOB_RANK


			-- Update the staging table based on whether or not a record was able to be loaded.
			IF NOT EXISTS (SELECT [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_UPDATED_ROW_ID)
			BEGIN
				-- Update the staging table.
				UPDATE [FHPDataMarts].[dbo].[Member_Master_Staging]
				SET
					[ROW_PROCESSED] = 'Y',
					[ROW_PROCESSED_DATE] = GETDATE(),
					[ROW_PROBLEM] = 'Y',
					[ROW_PROBLEM_REASON] = 'Could not update member record. The current record takes priority.',
					[ROW_PROBLEM_DATE] = GETDATE()
				WHERE [MEMBER_MASTER_STAGING_ROW_ID] = @MEMBER_MASTER_STAGING_ROW_ID;



				-- Insert a record into the "Key History" table, if it's unique to the table.
				-- This allows future records to tie into this user's key history, and join in a new record, even if if they differ from the "active" keys on the main member table.
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
				(
					 [MEMBER_MASTER_ROW_ID]
					,[MHK_INTERNAL_ID]
					,[MEDHOK_ID]
					,[SSN]
					,[HICN]
					,[CLAIM_SUBSCRIBER_ID]
					,[MBI]
					,[MEDICAID_NO]
					,[MRN]
					,[EXT_ID]
					,[EXT_ID_TYPE]
					,[EXT_ID_2]
					,[EXT_ID_TYPE_2]
					,[EXT_ID_3]
					,[EXT_ID_TYPE_3]
				)
				SELECT
					 @MEMBER_MASTER_ROW_ID-- [MEMBER_MASTER_ROW_ID]
					,@MHK_INTERNAL_ID  --,[MHK_INTERNAL_ID]
					,@MEDHOK_ID  --,[MEDHOK_ID]
					,@SSN  --,[SSN]
					,@HICN  --,[HICN]
					,@CLAIM_SUBSCRIBER_ID  --,[CLAIM_SUBSCRIBER_ID]
					,@MBI  --,[MBI]
					,@MEDICAID_NO  --,[MEDICAID_NO]
					,@MRN  --,[MRN]
					,@EXT_ID  --,[EXT_ID]
					,@EXT_ID_TYPE  --,[EXT_ID_TYPE]
					,@EXT_ID_2  --,[EXT_ID_2]
					,@EXT_ID_TYPE_2  --,[EXT_ID_TYPE_2]
					,@EXT_ID_3  --,[EXT_ID_3]
					,@EXT_ID_TYPE_3--,[EXT_ID_TYPE_3]

				EXCEPT

				SELECT
					 [MEMBER_MASTER_ROW_ID]
					,[MHK_INTERNAL_ID]
					,[MEDHOK_ID]
					,[SSN]
					,[HICN]
					,[CLAIM_SUBSCRIBER_ID]
					,[MBI]
					,[MEDICAID_NO]
					,[MRN]
					,[EXT_ID]
					,[EXT_ID_TYPE]
					,[EXT_ID_2]
					,[EXT_ID_TYPE_2]
					,[EXT_ID_3]
					,[EXT_ID_TYPE_3]
				FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]

			END
			ELSE
			BEGIN
				-- Update the staging table.
				UPDATE [FHPDataMarts].[dbo].[Member_Master_Staging]
				SET
					[ROW_PROCESSED] = 'Y',
					[ROW_PROCESSED_DATE] = GETDATE()
				WHERE [MEMBER_MASTER_STAGING_ROW_ID] = @MEMBER_MASTER_STAGING_ROW_ID;
			END

			-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
			RETURN 0

END


/*
	2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
					 table, meaning that an insert cannot happen because it would cause a duplication of one or more
					 keys.  This staging row should be flagged as being in error, and a human should investigate why
					 the same keys are being used in more than once place.
*/
IF @MEMBER_MASTER_ROW_COUNT > 1
BEGIN

	UPDATE [FHPDataMarts].[dbo].[Member_Master_Staging]
	SET
		[ROW_PROBLEM] = 'Y',
		[ROW_PROBLEM_REASON] = 'The keys on this record match more than 1 record on the master table.  Requires human analysis and intervention.',
		[ROW_PROBLEM_DATE] = GETDATE()
	WHERE [MEMBER_MASTER_STAGING_ROW_ID] = @MEMBER_MASTER_STAGING_ROW_ID

END


-- Return success
RETURN 0


END;