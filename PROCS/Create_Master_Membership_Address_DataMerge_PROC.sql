USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Address_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Address_UPSERT;
GO


CREATE PROCEDURE [dbo].[Member_Address_UPSERT]

	@MEMBER_ADDRESS_STAGING_ROW_ID int NULL,
	@MEMBER_CONTACT_ROW_ID int NULL,  -- CURRENTLY UNUSED.  May eventually be used to tie a member's contacts to their own addresses.
	@MHK_INTERNAL_ID int NULL,
	@MEDHOK_ID varchar(50) NULL,
	@SSN varchar(11) NULL,
	@HICN varchar(12) NULL,
	@CLAIM_SUBSCRIBER_ID varchar(50) NULL,
	@MBI varchar(11) NULL,
	@MEDICAID_NO varchar(50) NULL,
	@MRN varchar(50) NULL,
	@EXT_ID varchar(50) NULL,
	@EXT_ID_TYPE varchar(50) NULL,
	@EXT_ID_2 varchar(50) NULL,
	@EXT_ID_TYPE_2 varchar(50) NULL,
	@EXT_ID_3 varchar(50) NULL,
	@EXT_ID_TYPE_3 varchar(50) NULL,

	@ADDRESS_TYPE VARCHAR(100) NULL,
	@ADDRESS_1 VARCHAR(100) NULL,
	@ADDRESS_2 VARCHAR(100) NULL,
	@ADDRESS_3 VARCHAR(100) NULL,
	@CITY VARCHAR(50) NULL,
	@STATE VARCHAR(50) NULL,
	@ZIP VARCHAR(10) NULL,
	@COUNTY VARCHAR(50) NULL,
	@ISLAND VARCHAR(50) NULL,
	@COUNTRY VARCHAR(50) NULL,
	@EFFECTIVE_DATE DATE NULL,
	@TERM_DATE DATE NULL,
	@ADDRESS_STATUS VARCHAR(10) NULL,
	@PHONE VARCHAR(50) NULL,
	@ALTERNATE_PHONE VARCHAR(50) NULL,
	@EVENING_PHONE VARCHAR(50) NULL,
	@EMERGENCY_PHONE VARCHAR(50) NULL,
	@FAX VARCHAR(50) NULL,
	@EMAIL VARCHAR(50) NULL,

	@LOB_VENDOR varchar(50) NULL,
	@LOB_TYPE varchar(50) NULL,

	@ROW_SOURCE varchar(500) NULL,
	@ROW_SOURCE_ID varchar(50) NULL


AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: December 14th 2017
	-- Description:	
	-- ==========================================================================================
	/*
		This stored procedure takes the new incoming address records, attempts to uniquely
		match them to a member, and then inserts them.

		If the member cannot be uniquely found, the staging record will be flaged as a problem
		row.

		Note:  Addresses are not merged, deduplicated, or updated.  It's a simple insert-only.
			   Other processes/views should be used to determine which address record is the
			   most valid for a given person/situation, and selected from this table based on
			   those external criteria.
	*/
	-- ==========================================================================================


	IF @MEMBER_ADDRESS_STAGING_ROW_ID IS NULL OR @ROW_SOURCE IS NULL OR @LOB_VENDOR IS NULL OR @LOB_TYPE IS NULL
	BEGIN
		-- INVALID KEY, EXIT WITH FAILURE
		RAISERROR (15600,-1,-1, 'dbo.Member_Address_UPSERT')
		RETURN 1
	END


	-- Find the member
	DECLARE @MEMBER_MASTER_ROW_COUNT AS INT = 0;
	DECLARE @MEMBER_MASTER_ROW_ID AS INT = 0;




	-- Used for gathering stats on key lookups.
	DECLARE @KEY_SEARCH_RESULTS AS TABLE
	(
		[MEMBER_MASTER_ROW_ID] INT
	)


	DECLARE @MEMBER_MASTER_UPDATED_ROW_ID AS TABLE
	(
		[MEMBER_MASTER_ROW_ID] INT
	)





	/**********************************************************************************************************************

		Check all possible key collisions, and count the number of rows that this would effect on the master table.

		0 rows:  None of the current keys matched.  This member Address record is orphaned until a matching member
				 can be located.

		1 row:  At least one or more of the keys matched, with none mismatching.  This means a valid member was found
				for the Address, 

		2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
						 table, meaning that an insert cannot happen because this Address record cannot be uniquely
						 matched to a member.  This staging row should be flagged as being in error, and a human should
						 investigate why the same keys are being used in more than once place.
	

	**********************************************************************************************************************/
	;WITH AllKeys AS
	(
		-- Historical
		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,CASE WHEN [EXT_ID] IS NOT NULL AND [EXT_ID_TYPE] IS NOT NULL THEN CONCAT([EXT_ID],':',[EXT_ID_TYPE]) ELSE NULL END AS [EXT_ID_AND_TYPE]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE ROW_DELETED = 'N'
	)
	INSERT INTO @KEY_SEARCH_RESULTS
	(
		[MEMBER_MASTER_ROW_ID]
	)
	SELECT DISTINCT
		[MEMBER_MASTER_ROW_ID]
	FROM AllKeys
	WHERE 1=2
	OR @MHK_INTERNAL_ID = AllKeys.[MHK_INTERNAL_ID]
	OR @MEDHOK_ID = AllKeys.[MEDHOK_ID]
	OR @SSN = AllKeys.[SSN]
	OR @HICN = AllKeys.[HICN]
	OR @CLAIM_SUBSCRIBER_ID = AllKeys.[CLAIM_SUBSCRIBER_ID]
	OR @MBI = AllKeys.[MBI]
	OR @MEDICAID_NO = AllKeys.[MEDICAID_NO]
	OR @MRN = AllKeys.[MRN]
	OR CASE WHEN @EXT_ID IS NOT NULL AND @EXT_ID_TYPE IS NOT NULL THEN CONCAT(@EXT_ID,':',@EXT_ID_TYPE) ELSE NULL END = AllKeys.[EXT_ID_AND_TYPE]


	-- Get number of matching results
	SELECT @MEMBER_MASTER_ROW_COUNT = COUNT(1) FROM @KEY_SEARCH_RESULTS;

	-- Get the record, if it's unique
	IF @MEMBER_MASTER_ROW_COUNT = 1
	BEGIN
		SELECT @MEMBER_MASTER_ROW_ID = [MEMBER_MASTER_ROW_ID] FROM @KEY_SEARCH_RESULTS
	END




	-- 0 rows:  None of the current keys matched.  This is an orphan record.
	IF @MEMBER_MASTER_ROW_COUNT = 0
	BEGIN

		UPDATE [FHPDataMarts].[dbo].[Member_Address_Staging]
		SET
			[ROW_PROBLEM] = 'Y',
			[ROW_PROBLEM_REASON] = 'No matching member found.',
			[ROW_PROBLEM_DATE] = GETDATE()
		WHERE [MEMBER_ADDRESS_STAGING_ROW_ID] = @MEMBER_ADDRESS_STAGING_ROW_ID;


		-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
		RETURN 0

	END


	-- 1 row:  At least one or more of the keys matched, with none mismatching.  This Address record can not be uniquely associated with this member.
	IF @MEMBER_MASTER_ROW_COUNT = 1
	BEGIN

		INSERT INTO [FHPDW].[dbo].[Member_Address]
		(
			-- [MEMBER_ADDRESS_ROW_ID]
			 [MEMBER_MASTER_ROW_ID]
			,[MEMBER_CONTACT_ROW_ID]
			,[ADDRESS_TYPE]
			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[EFFECTIVE_DATE]
			,[TERM_DATE]
			,[ADDRESS_STATUS]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[MEMBER_ADDRESS_STAGING_ROW_ID]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[ROW_PROCESSED]
			--,[ROW_PROCESSED_DATE]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		)
		VALUES
		(	
			@MEMBER_MASTER_ROW_ID,  --@MEMBER_MASTER_ROW_ID   (from lookup)
			@MEMBER_CONTACT_ROW_ID,

			@ADDRESS_TYPE,
			@ADDRESS_1,
			@ADDRESS_2,
			@ADDRESS_3,
			@CITY,
			@STATE,
			@ZIP,
			@COUNTY,
			@ISLAND,
			@COUNTRY,
			@EFFECTIVE_DATE,
			@TERM_DATE,
			@ADDRESS_STATUS,
			@PHONE,
			@ALTERNATE_PHONE,
			@EVENING_PHONE,
			@EMERGENCY_PHONE,
			@FAX,
			@EMAIL,

			@LOB_VENDOR,  --[LOB_VENDOR]
			@LOB_TYPE,  --[LOB_TYPE]
			@MEMBER_ADDRESS_STAGING_ROW_ID,

			@ROW_SOURCE,  --[ROW_SOURCE]
			@ROW_SOURCE_ID  --[ROW_SOURCE_ID]
			  --[ROW_PROCESSED]
			  --[ROW_PROCESSED_DATE]
			  --[ROW_DELETED]
			  --[ROW_DELETED_DATE]
			  --[ROW_DELETED_REASON]
			  --[ROW_CREATE_DATE]
			  --[ROW_UPDATE_DATE]
		);


		-- Update the staging table.
		UPDATE [FHPDataMarts].[dbo].[Member_Address_Staging]
		SET
			[ROW_PROCESSED] = 'Y',
			[ROW_PROCESSED_DATE] = GETDATE()
		WHERE [MEMBER_ADDRESS_STAGING_ROW_ID] = @MEMBER_ADDRESS_STAGING_ROW_ID;


		-- STOP THE STORED PROCEDURE AND REPORT SUCCESS
		RETURN 0

	END


	/*
		2 or more rows:  One or more of the keys from the staging tables matched to multiple rows on the member master
						 table, meaning that an insert cannot happen because this Address record cannot be uniquely
						 matched to a member.  This staging row should be flagged as being in error, and a human should
						 investigate why the same keys are being used in more than once place.
	*/
	IF @MEMBER_MASTER_ROW_COUNT > 1
	BEGIN

		UPDATE [FHPDataMarts].[dbo].[Member_Address_Staging]
		SET
			[ROW_PROBLEM] = 'Y',
			[ROW_PROBLEM_REASON] = 'The keys on this record match more than 1 record on the member master table.  Requires human analysis and intervention.',
			[ROW_PROBLEM_DATE] = GETDATE()
		WHERE [MEMBER_ADDRESS_STAGING_ROW_ID] = @MEMBER_ADDRESS_STAGING_ROW_ID

	END


	-- Return success
	RETURN 0




END
GO


