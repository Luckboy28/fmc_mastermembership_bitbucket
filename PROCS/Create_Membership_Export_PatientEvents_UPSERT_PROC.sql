

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_PatientEvents_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_PatientEvents_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_PatientEvents_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: October 2nd 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT
	INSERT INTO [FHPDW].[dbo].[Membership_Export_PatientEvents]
	(
		-- [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,[EVENT_TYPE]
		,[EVENT_DESCRIPTION]
		,[EVENT_START_DATE]
		,[EVENT_END_DATE]
		,[EVENT_NOTES]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		-- TheSource.[MEMBER_PATIENTEVENTS_ROW_ID] AS [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
		 TheSource.[MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

		,TheSource.[EVENT_TYPE]
		,TheSource.[EVENT_DESCRIPTION]
		,TheSource.[EVENT_START_DATE]
		,TheSource.[EVENT_END_DATE]
		,TheSource.[EVENT_NOTES]

		,TheSource.[ROW_SOURCE]
		,TheSource.[ROW_SOURCE_ID]
		,TheSource.[ROW_PROBLEM]
		,TheSource.[ROW_PROBLEM_DATE]
		,TheSource.[ROW_PROBLEM_REASON]
		,TheSource.[ROW_DELETED]
		,TheSource.[ROW_DELETED_DATE]
		,TheSource.[ROW_DELETED_REASON]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM [FHPDW].[dbo].[Member_PatientEvents] TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_PatientEvents] TheTable
	ON TheTable.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] = TheSource.[MEMBER_PATIENTEVENTS_ROW_ID]
	WHERE TheTable.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] IS NULL  -- Only new records



	-- UPDATE
	UPDATE TheTable
	SET
		 [EVENT_TYPE] = Delta.[EVENT_TYPE]
		,[EVENT_DESCRIPTION] = Delta.[EVENT_DESCRIPTION]
		,[EVENT_START_DATE] = Delta.[EVENT_START_DATE]
		,[EVENT_END_DATE] = Delta.[EVENT_END_DATE]
		,[EVENT_NOTES] = Delta.[EVENT_NOTES]

		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
		,[ROW_DELETED] = Delta.[ROW_DELETED]
		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = @GETDATE
	FROM [FHPDW].[dbo].[Membership_Export_PatientEvents] TheTable
	JOIN
	(
		SELECT
			 [MEMBER_PATIENTEVENTS_ROW_ID] AS [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
			,[MEMBER_MASTER_ROW_ID] AS [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

			,[EVENT_TYPE]
			,[EVENT_DESCRIPTION]
			,[EVENT_START_DATE]
			,[EVENT_END_DATE]
			,[EVENT_NOTES]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Member_PatientEvents]
	
		EXCEPT

		SELECT
			 [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]

			,[EVENT_TYPE]
			,[EVENT_DESCRIPTION]
			,[EVENT_START_DATE]
			,[EVENT_END_DATE]
			,[EVENT_NOTES]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_PatientEvents]
	) Delta
	ON TheTable.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
	AND TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


	-- Return success
	RETURN 0


END
GO




