USE [FHPDW]
GO


IF OBJECT_ID('dbo.Membership_Export_Member_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Member_UPSERT;
GO



USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Member_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Member_UPSERT;
GO


CREATE PROCEDURE [dbo].[Membership_Export_Member_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: March 14th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Member]
	(
		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,[HEALTHCLOUD_ID]
		,[SSN]
		,[HICN]
		,[CLAIM_SUBSCRIBER_ID]
		,[MBI]
		,[MEDICAID_NO]
		,[MRN]
		,[EXT_ID]
		,[EXT_ID_TYPE]

		,[FIRST_NAME]
		,[MIDDLE_NAME]
		,[LAST_NAME]
		,[DATE_OF_BIRTH]
		,[GENDER]
		,[RACE]

		,[DIALYSIS_START_DATE]
		,[CKD_STAGE]

		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[ISLAND]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]
		,[HC_LOB]
		,[HC_GROUP]
		,[HC_PROGRAM]
		,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT
		 TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,TheSource.[HEALTHCLOUD_ID]
		,TheSource.[SSN]
		,TheSource.[HICN]
		,TheSource.[CLAIM_SUBSCRIBER_ID]
		,TheSource.[MBI]
		,TheSource.[MEDICAID_NO]
		,TheSource.[MRN]
		,TheSource.[EXT_ID]
		,TheSource.[EXT_ID_TYPE]

		,TheSource.[FIRST_NAME]
		,TheSource.[MIDDLE_NAME]
		,TheSource.[LAST_NAME]
		,TheSource.[DATE_OF_BIRTH]
		,TheSource.[GENDER]
		,TheSource.[RACE]

		,TheSource.[DIALYSIS_START_DATE]
		,TheSource.[CKD_STAGE]

		,TheSource.[ADDRESS_1]
		,TheSource.[ADDRESS_2]
		,TheSource.[ADDRESS_3]
		,TheSource.[CITY]
		,TheSource.[STATE]
		,TheSource.[ZIP]
		,TheSource.[COUNTY]
		,TheSource.[ISLAND]
		,TheSource.[COUNTRY]
		,TheSource.[PHONE]
		,TheSource.[ALTERNATE_PHONE]
		,TheSource.[EVENING_PHONE]
		,TheSource.[EMERGENCY_PHONE]
		,TheSource.[FAX]
		,TheSource.[EMAIL]
		,TheSource.[STATUS]
		,TheSource.[LOB_VENDOR]
		,TheSource.[LOB_TYPE]
		,TheSource.[HC_LOB]
		,TheSource.[HC_GROUP]
		,TheSource.[HC_PROGRAM]
		,TheSource.[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
		,TheSource.[ROW_SOURCE]
		,TheSource.[ROW_SOURCE_ID]
		,TheSource.[ROW_PROBLEM]
		,TheSource.[ROW_PROBLEM_DATE]
		,TheSource.[ROW_PROBLEM_REASON]
		,TheSource.[ROW_DELETED]
		,TheSource.[ROW_DELETED_DATE]
		,TheSource.[ROW_DELETED_REASON]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Member_SOURCE] TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Member] TheTable
	ON TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
	WHERE TheTable.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] IS NULL  -- Only new records



	-- UPDATE
	UPDATE MEM
	SET
		 [HEALTHCLOUD_ID] = Delta.[HEALTHCLOUD_ID]
		,[SSN] = Delta.[SSN]
		,[HICN] = Delta.[HICN]
		,[CLAIM_SUBSCRIBER_ID] = Delta.[CLAIM_SUBSCRIBER_ID]
		,[MBI] = Delta.[MBI]
		,[MEDICAID_NO] = Delta.[MEDICAID_NO]
		,[MRN] = Delta.[MRN]
		,[EXT_ID] = Delta.[EXT_ID]
		,[EXT_ID_TYPE] = Delta.[EXT_ID_TYPE]

		,[FIRST_NAME] = Delta.[FIRST_NAME]
		,[MIDDLE_NAME] = Delta.[MIDDLE_NAME]
		,[LAST_NAME] = Delta.[LAST_NAME]
		,[DATE_OF_BIRTH] = Delta.[DATE_OF_BIRTH]
		,[GENDER] = Delta.[GENDER]
		,[RACE] = Delta.[RACE]

		,[DIALYSIS_START_DATE] = Delta.[DIALYSIS_START_DATE]
		,[CKD_STAGE] = Delta.[CKD_STAGE]

		,[ADDRESS_1] = Delta.[ADDRESS_1]
		,[ADDRESS_2] = Delta.[ADDRESS_2]
		,[ADDRESS_3] = Delta.[ADDRESS_3]
		,[CITY] = Delta.[CITY]
		,[STATE] = Delta.[STATE]
		,[ZIP] = Delta.[ZIP]
		,[COUNTY] = Delta.[COUNTY]
		,[ISLAND] = Delta.[ISLAND]
		,[COUNTRY] = Delta.[COUNTRY]
		,[PHONE] = Delta.[PHONE]
		,[ALTERNATE_PHONE] = Delta.[ALTERNATE_PHONE]
		,[EVENING_PHONE] = Delta.[EVENING_PHONE]
		,[EMERGENCY_PHONE] = Delta.[EMERGENCY_PHONE]
		,[FAX] = Delta.[FAX]
		,[EMAIL] = Delta.[EMAIL]
		,[STATUS] = Delta.[STATUS]
		,[LOB_VENDOR] = Delta.[LOB_VENDOR]
		,[LOB_TYPE] = Delta.[LOB_TYPE]
		,[HC_LOB] = Delta.[HC_LOB]
		,[HC_GROUP] = Delta.[HC_GROUP]
		,[HC_PROGRAM] = Delta.[HC_PROGRAM]
		,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] = Delta.[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
		,[ROW_DELETED] = Delta.[ROW_DELETED]
		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = Delta.[ROW_UPDATE_DATE]
	FROM [FHPDW].[dbo].[Membership_Export_Member] MEM
	JOIN
	(
		SELECT
			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,[HEALTHCLOUD_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]

			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[RACE]

			,[DIALYSIS_START_DATE]
			,[CKD_STAGE]

			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[STATUS]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[HC_LOB]
			,[HC_GROUP]
			,[HC_PROGRAM]
			,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Member_SOURCE]
	
		EXCEPT

		SELECT
			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,[HEALTHCLOUD_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]

			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[RACE]

			,[DIALYSIS_START_DATE]
			,[CKD_STAGE]

			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[STATUS]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[HC_LOB]
			,[HC_GROUP]
			,[HC_PROGRAM]
			,[LATEST_MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_Member]
	) Delta
	ON MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


	-- Return success
	RETURN 0


END
GO



