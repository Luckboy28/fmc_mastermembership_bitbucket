USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Utility_MergeMembers', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Utility_MergeMembers;
GO


CREATE PROCEDURE [dbo].[Member_Utility_MergeMembers]
-- No parameters
AS
BEGIN


	/********************************************************************************************************************************************************************

		Before starting, make sure the "Primary" and "Merge" keys don't overlap other members (It never should, but good to check)

	*********************************************************************************************************************************************************************/


	-- Make a table variable to hold the results of the data overlap check.  This table should be empty if all keys are correctly seperated from each other.
	DECLARE @Data_Integrity_Check AS TABLE
	(
		 MEMBER_MASTER_ROW_ID_1 INT NOT NULL
		,MEMBER_MASTER_ROW_ID_2 INT NOT NULL
	);


	-- Empty the table, just to be cautious
	DELETE FROM @Data_Integrity_Check;


	-- Gather together all of the matching MEMBER_MASTER_ROW_ID's
	WITH Data_Integrity_Check_Query AS
	(
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID] AS [MEMBER_MASTER_ROW_ID_1]
			,MMKH2.[MEMBER_MASTER_ROW_ID] AS [MEMBER_MASTER_ROW_ID_2]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[MHK_INTERNAL_ID] = MMKH2.[MHK_INTERNAL_ID]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[MHK_INTERNAL_ID] IS NOT NULL	
		AND MMKH2.[MHK_INTERNAL_ID] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[MEDHOK_ID] = MMKH2.[MEDHOK_ID]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[MEDHOK_ID] IS NOT NULL	
		AND MMKH2.[MEDHOK_ID] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[SSN] = MMKH2.[SSN]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[SSN] IS NOT NULL	
		AND MMKH2.[SSN] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[HICN] = MMKH2.[HICN]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[HICN] IS NOT NULL	
		AND MMKH2.[HICN] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[CLAIM_SUBSCRIBER_ID] = MMKH2.[CLAIM_SUBSCRIBER_ID]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
		AND MMKH2.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[MBI] = MMKH2.[MBI]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[MBI] IS NOT NULL	
		AND MMKH2.[MBI] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[MEDICAID_NO] = MMKH2.[MEDICAID_NO]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[MEDICAID_NO] IS NOT NULL	
		AND MMKH2.[MEDICAID_NO] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON MMKH1.[MRN] = MMKH2.[MRN]	
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[MRN] IS NOT NULL	
		AND MMKH2.[MRN] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	
		UNION ALL	
	
		SELECT	
			 MMKH1.[MEMBER_MASTER_ROW_ID]
			,MMKH2.[MEMBER_MASTER_ROW_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH1	
		JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH2	
		ON CONCAT(MMKH1.[EXT_ID],':',MMKH1.[EXT_ID_TYPE]) = CONCAT(MMKH2.[EXT_ID],':',MMKH2.[EXT_ID_TYPE])
		WHERE MMKH1.[MEMBER_MASTER_ROW_ID] <> MMKH2.[MEMBER_MASTER_ROW_ID]	
		AND MMKH1.[EXT_ID] IS NOT NULL	
		AND MMKH2.[EXT_ID] IS NOT NULL	
		AND MMKH1.[EXT_ID_TYPE] IS NOT NULL	
		AND MMKH2.[EXT_ID_TYPE] IS NOT NULL	
		AND MMKH1.[ROW_DELETED] = 'N'	
		AND MMKH2.[ROW_DELETED] = 'N'	
	)
	INSERT INTO @Data_Integrity_Check (MEMBER_MASTER_ROW_ID_1,MEMBER_MASTER_ROW_ID_2)
	SELECT DISTINCT
		 MEMBER_MASTER_ROW_ID_1
		,MEMBER_MASTER_ROW_ID_2
	FROM Data_Integrity_Check_Query;




	-- Stop the PROC and return an error if any keys are found to be overlapping
	IF (SELECT COUNT(1) FROM @Data_Integrity_Check) > 0
	BEGIN

		-- INVALID STATE, EXIT WITH FAILURE
		RAISERROR (15600,-1,-1, '[FHPDataMarts].[dbo].[Member_Utility_MergeMembers]:  FAILED DATA INTEGRITY CHECK')
		RETURN 1

	END




	/********************************************************************************************************************************************************************

		Now we can begin with our main MEMBER_CURSOR to loop through the records in [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] and merge members.

	*********************************************************************************************************************************************************************/

	DECLARE @MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID_CURSOR_CURRENT AS INT;
	DECLARE @MEMBER_MASTER_ROW_ID_PRIMARY_CURSOR_CURRENT AS INT;
	DECLARE @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT AS INT;

	DECLARE MEMBER_CURSOR CURSOR FOR
		SELECT DISTINCT
			 MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID
			,MEMBER_MASTER_ROW_ID_PRIMARY
			,MEMBER_MASTER_ROW_ID_MERGE
		FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys]
		WHERE [ROW_PROCESSED] = 'N'
		AND [ROW_DELETED] = 'N'


	OPEN MEMBER_CURSOR
	FETCH NEXT FROM MEMBER_CURSOR INTO @MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID_CURSOR_CURRENT, @MEMBER_MASTER_ROW_ID_PRIMARY_CURSOR_CURRENT, @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


	-- Begin main cursor through "Primary" and "Merge" MEMBER_MASTER_ROW_ID combinations
	WHILE @@FETCH_STATUS = 0
	BEGIN

		
		/********************************************************************************************************************************************************************

			Make sure that the Primary and Merge members are not already deleted

		*********************************************************************************************************************************************************************/

		IF EXISTS
		(
			SELECT
				MEMBER_MASTER_ROW_ID
			FROM [FHPDW].[dbo].[Member_Master]
			WHERE ROW_DELETED = 'Y'
			AND
			(
				MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_PRIMARY_CURSOR_CURRENT
				OR
				MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT
			)
		)
		BEGIN

			UPDATE TheTarget
			SET
				 TheTarget.[ROW_PROBLEM] = 'Y'
				,TheTarget.[ROW_PROBLEM_DATE] = GETDATE()
				,TheTarget.[ROW_PROBLEM_REASON] = 'MERGE FAILED:  One or both of the supplied members is already deleted.'
			FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] TheTarget
			WHERE MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID = @MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID_CURSOR_CURRENT


			-- Grab the next member for the following loop, if there is one
			FETCH NEXT FROM MEMBER_CURSOR INTO @MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID_CURSOR_CURRENT, @MEMBER_MASTER_ROW_ID_PRIMARY_CURSOR_CURRENT, @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT

			-- Stop the current iteration of the loop, and continue to the next iteration
			CONTINUE

		END




		/********************************************************************************************************************************************************************

			Soft delete the MERGED member

		*********************************************************************************************************************************************************************/

		-- [FHPDW].[dbo].[Member_Master]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDW].[dbo].[Member_Master] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDW].[dbo].[Member_Eligibility]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDW].[dbo].[Member_Eligibility] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDW].[dbo].[Member_Address]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDW].[dbo].[Member_Address] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDW].[dbo].[Member_Contacts]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDW].[dbo].[Member_Contacts] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDataMarts].[dbo].[Member_Address_Historical]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDataMarts].[dbo].[Member_Address_Historical] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT



		-- [FHPDataMarts].[dbo].[Member_Contacts_Historical]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDataMarts].[dbo].[Member_Contacts_Historical] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDataMarts].[dbo].[Member_Eligibility_Historical]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDataMarts].[dbo].[Member_Eligibility_Historical] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDataMarts].[dbo].[Member_Master_Historical]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDataMarts].[dbo].[Member_Master_Historical] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDataMarts].[dbo].[Member_Master_Historical]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
			,ROW_PROBLEM = 'Y'
			,ROW_PROBLEM_DATE = GETDATE()
			,ROW_PROBLEM_REASON = 'Deleted.  Member merged due to incorrect split.'
		FROM [FHPDataMarts].[dbo].[Member_Master_Historical] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		-- [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
		DELETE FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
		WHERE [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT


		
		/********************************************************************************************************************************************************************

			Now that the member has been merged, we need to force reload any data in the staging tables that match up to the keys found in the 
			[FHPDataMarts].[dbo].[Member_Master_KeyHistory] table for that merged member.

		*********************************************************************************************************************************************************************/

		-- Members
		;WITH All_Matching_Staged_Members AS
		(
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MHK_INTERNAL_ID] = MMKH.[MHK_INTERNAL_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MHK_INTERNAL_ID] IS NOT NULL	
			AND MMKH.[MHK_INTERNAL_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDHOK_ID] = MMKH.[MEDHOK_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDHOK_ID] IS NOT NULL	
			AND MMKH.[MEDHOK_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[SSN] = MMKH.[SSN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[SSN] IS NOT NULL	
			AND MMKH.[SSN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[HICN] = MMKH.[HICN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[HICN] IS NOT NULL	
			AND MMKH.[HICN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[CLAIM_SUBSCRIBER_ID] = MMKH.[CLAIM_SUBSCRIBER_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
			AND MMKH.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MBI] = MMKH.[MBI]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MBI] IS NOT NULL	
			AND MMKH.[MBI] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDICAID_NO] = MMKH.[MEDICAID_NO]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDICAID_NO] IS NOT NULL	
			AND MMKH.[MEDICAID_NO] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MRN] = MMKH.[MRN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MRN] IS NOT NULL	
			AND MMKH.[MRN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CONCAT(CS.[EXT_ID],':',CS.[EXT_ID_TYPE]) = CONCAT(MMKH.[EXT_ID],':',MMKH.[EXT_ID_TYPE])	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[EXT_ID] IS NOT NULL	
			AND MMKH.[EXT_ID] IS NOT NULL
			AND CS.[EXT_ID_TYPE] IS NOT NULL	
			AND MMKH.[EXT_ID_TYPE] IS NOT NULL		
		)
		INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
		(
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[BENEFIT_STATUS]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[COMPANY_DESCRIPTION]
			,[LOB_CODE]
			,[FAMILY_ID]
			,[PERSON_NUMBER]
			,[RACE]
			,[ETHNICITY]
			,[PRIMARY_LANGUAGE]
			,[PRIMARY_LANGUAGE_SOURCE]
			,[SPOKEN_LANGUAGE]
			,[SPOKEN_LANGUAGE_SOURCE]
			,[WRITTEN_LANGUAGE]
			,[WRITTEN_LANGUAGE_SOURCE]
			,[OTHER_LANGUAGE]
			,[OTHER_LANGUAGE_SOURCE]
			,[EMPLOYEE]
			,[PBP_NUMBER]
			,[CURRENT_LIS]
			,[IPA_GROUP_EXT_ID]
			,[MEDICARE_PLAN_CODE]
			,[MEDICARE_TYPE]
			,[DUPLICATE_MEDICAID_ID]
			,[PREGNANCY_DUE_DATE]
			,[PREGNANCY_INDICATOR]
			,[BOARD_NUMBER]
			,[DEPENDENT_CODE]
			,[LEGACY_SUBSCRIBER_ID]
			,[GROUP_NUMBER]
			,[SOURCE]
			,[ESCO_ID]
			,[CLIENT_SPECIFIC_DATA]
			,[RELATIONSHIP_CODE]
			,[TIME_ZONE]
			,[DATE_OF_DEATH]
			,[FOSTER_CARE_FLAG]
			,[VIP]
			,[CLINIC_NUMBER]
			,[MODALITY]
			,[PAYER_NAME]
			,[PAYER_ID_TYPE]
			,[PAYER_ID]
			,[DIALYSIS_START_DATE]
			,[KIDNEY_TRANSPLANT_DATE]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[ROW_PROCESSED]
			--,[ROW_PROCESSED_DATE]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		)
		SELECT DISTINCT
			 NULL  --[MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[BENEFIT_STATUS]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[COMPANY_DESCRIPTION]
			,[LOB_CODE]
			,[FAMILY_ID]
			,[PERSON_NUMBER]
			,[RACE]
			,[ETHNICITY]
			,[PRIMARY_LANGUAGE]
			,[PRIMARY_LANGUAGE_SOURCE]
			,[SPOKEN_LANGUAGE]
			,[SPOKEN_LANGUAGE_SOURCE]
			,[WRITTEN_LANGUAGE]
			,[WRITTEN_LANGUAGE_SOURCE]
			,[OTHER_LANGUAGE]
			,[OTHER_LANGUAGE_SOURCE]
			,[EMPLOYEE]
			,[PBP_NUMBER]
			,[CURRENT_LIS]
			,[IPA_GROUP_EXT_ID]
			,[MEDICARE_PLAN_CODE]
			,[MEDICARE_TYPE]
			,[DUPLICATE_MEDICAID_ID]
			,[PREGNANCY_DUE_DATE]
			,[PREGNANCY_INDICATOR]
			,[BOARD_NUMBER]
			,[DEPENDENT_CODE]
			,[LEGACY_SUBSCRIBER_ID]
			,[GROUP_NUMBER]
			,[SOURCE]
			,[ESCO_ID]
			,[CLIENT_SPECIFIC_DATA]
			,[RELATIONSHIP_CODE]
			,[TIME_ZONE]
			,[DATE_OF_DEATH]
			,[FOSTER_CARE_FLAG]
			,[VIP]
			,[CLINIC_NUMBER]
			,[MODALITY]
			,[PAYER_NAME]
			,[PAYER_ID_TYPE]
			,[PAYER_ID]
			,[DIALYSIS_START_DATE]
			,[KIDNEY_TRANSPLANT_DATE]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			--,[CREATE_DATE]
			--,[UPDATE_DATE]
		FROM All_Matching_Staged_Members;



		-- Eligibility
		;WITH All_Matching_Staged_Eligibility AS
		(
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MHK_INTERNAL_ID] = MMKH.[MHK_INTERNAL_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MHK_INTERNAL_ID] IS NOT NULL	
			AND MMKH.[MHK_INTERNAL_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDHOK_ID] = MMKH.[MEDHOK_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDHOK_ID] IS NOT NULL	
			AND MMKH.[MEDHOK_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[SSN] = MMKH.[SSN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[SSN] IS NOT NULL	
			AND MMKH.[SSN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[HICN] = MMKH.[HICN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[HICN] IS NOT NULL	
			AND MMKH.[HICN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[CLAIM_SUBSCRIBER_ID] = MMKH.[CLAIM_SUBSCRIBER_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
			AND MMKH.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MBI] = MMKH.[MBI]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MBI] IS NOT NULL	
			AND MMKH.[MBI] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDICAID_NO] = MMKH.[MEDICAID_NO]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDICAID_NO] IS NOT NULL	
			AND MMKH.[MEDICAID_NO] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MRN] = MMKH.[MRN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MRN] IS NOT NULL	
			AND MMKH.[MRN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CONCAT(CS.[EXT_ID],':',CS.[EXT_ID_TYPE]) = CONCAT(MMKH.[EXT_ID],':',MMKH.[EXT_ID_TYPE])	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[EXT_ID] IS NOT NULL	
			AND MMKH.[EXT_ID] IS NOT NULL
			AND CS.[EXT_ID_TYPE] IS NOT NULL	
			AND MMKH.[EXT_ID_TYPE] IS NOT NULL		
		)
		INSERT INTO [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
		(
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ESCO_ID]
			,[HLTH_PLN_SYSID]
			,[HLTH_PLN_PROD_LINE]
			,[HLTH_PLN_RPT_GRP]
			,[HLTH_PLN_STD_CARRIER_CD]
			,[PLAN_NAME]
			,[START_DATE]
			,[TERM_DATE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		)
		SELECT DISTINCT
			 NULL  --[MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ESCO_ID]
			,[HLTH_PLN_SYSID]
			,[HLTH_PLN_PROD_LINE]
			,[HLTH_PLN_RPT_GRP]
			,[HLTH_PLN_STD_CARRIER_CD]
			,[PLAN_NAME]
			,[START_DATE]
			,[TERM_DATE]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM All_Matching_Staged_Eligibility;


			
		-- Address
		;WITH All_Matching_Staged_Address AS
		(
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MHK_INTERNAL_ID] = MMKH.[MHK_INTERNAL_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MHK_INTERNAL_ID] IS NOT NULL	
			AND MMKH.[MHK_INTERNAL_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDHOK_ID] = MMKH.[MEDHOK_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDHOK_ID] IS NOT NULL	
			AND MMKH.[MEDHOK_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[SSN] = MMKH.[SSN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[SSN] IS NOT NULL	
			AND MMKH.[SSN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[HICN] = MMKH.[HICN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[HICN] IS NOT NULL	
			AND MMKH.[HICN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[CLAIM_SUBSCRIBER_ID] = MMKH.[CLAIM_SUBSCRIBER_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
			AND MMKH.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MBI] = MMKH.[MBI]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MBI] IS NOT NULL	
			AND MMKH.[MBI] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDICAID_NO] = MMKH.[MEDICAID_NO]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDICAID_NO] IS NOT NULL	
			AND MMKH.[MEDICAID_NO] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MRN] = MMKH.[MRN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MRN] IS NOT NULL	
			AND MMKH.[MRN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CONCAT(CS.[EXT_ID],':',CS.[EXT_ID_TYPE]) = CONCAT(MMKH.[EXT_ID],':',MMKH.[EXT_ID_TYPE])	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[EXT_ID] IS NOT NULL	
			AND MMKH.[EXT_ID] IS NOT NULL
			AND CS.[EXT_ID_TYPE] IS NOT NULL	
			AND MMKH.[EXT_ID_TYPE] IS NOT NULL	
		)
		INSERT INTO [FHPDataMarts].[dbo].[Member_Address_Staging]
		(
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ADDRESS_TYPE]
			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[EFFECTIVE_DATE]
			,[TERM_DATE]
			,[ADDRESS_STATUS]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		)
		SELECT DISTINCT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ADDRESS_TYPE]
			,[ADDRESS_1]
			,[ADDRESS_2]
			,[ADDRESS_3]
			,[CITY]
			,[STATE]
			,[ZIP]
			,[COUNTY]
			,[ISLAND]
			,[COUNTRY]
			,[EFFECTIVE_DATE]
			,[TERM_DATE]
			,[ADDRESS_STATUS]
			,[PHONE]
			,[ALTERNATE_PHONE]
			,[EVENING_PHONE]
			,[EMERGENCY_PHONE]
			,[FAX]
			,[EMAIL]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM All_Matching_Staged_Address;


		-- Contacts
		;WITH All_Matching_Staged_Contacts AS
		(
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MHK_INTERNAL_ID] = MMKH.[MHK_INTERNAL_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MHK_INTERNAL_ID] IS NOT NULL	
			AND MMKH.[MHK_INTERNAL_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDHOK_ID] = MMKH.[MEDHOK_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDHOK_ID] IS NOT NULL	
			AND MMKH.[MEDHOK_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[SSN] = MMKH.[SSN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[SSN] IS NOT NULL	
			AND MMKH.[SSN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[HICN] = MMKH.[HICN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[HICN] IS NOT NULL	
			AND MMKH.[HICN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[CLAIM_SUBSCRIBER_ID] = MMKH.[CLAIM_SUBSCRIBER_ID]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
			AND MMKH.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MBI] = MMKH.[MBI]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MBI] IS NOT NULL	
			AND MMKH.[MBI] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MEDICAID_NO] = MMKH.[MEDICAID_NO]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MEDICAID_NO] IS NOT NULL	
			AND MMKH.[MEDICAID_NO] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CS.[MRN] = MMKH.[MRN]	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[MRN] IS NOT NULL	
			AND MMKH.[MRN] IS NOT NULL	
	
			UNION ALL	
	
			SELECT	
				CS.*
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] CS	
			JOIN [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH	
			ON CONCAT(CS.[EXT_ID],':',CS.[EXT_ID_TYPE]) = CONCAT(MMKH.[EXT_ID],':',MMKH.[EXT_ID_TYPE])	
			WHERE MMKH.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT	
			AND CS.[EXT_ID] IS NOT NULL	
			AND MMKH.[EXT_ID] IS NOT NULL
			AND CS.[EXT_ID_TYPE] IS NOT NULL	
			AND MMKH.[EXT_ID_TYPE] IS NOT NULL	
		)
		INSERT INTO [FHPDataMarts].[dbo].[Member_Contacts_Staging]
		(
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]

			,[CONTACT_TYPE]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[RELATION_TO_MEMBER]
			,[PROVIDER_ID_TYPE]
			,[PROVIDER_ID]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		)
		SELECT
			 [MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]

			,[CONTACT_TYPE]
			,[FIRST_NAME]
			,[MIDDLE_NAME]
			,[LAST_NAME]
			,[PREFIX]
			,[SUFFIX]
			,[DATE_OF_BIRTH]
			,[GENDER]
			,[RELATION_TO_MEMBER]
			,[PROVIDER_ID_TYPE]
			,[PROVIDER_ID]

			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[LOB_VENDOR]
			,[LOB_TYPE]
		FROM All_Matching_Staged_Contacts;





		/********************************************************************************************************************************************************************

			Copy the Merged ID records from the KeyHistory table back into the KeyHistory table with the new Primary ID.   Then flag the Merged ID records as being deleted.

		*********************************************************************************************************************************************************************/



		INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		(
			 [MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
		)
		SELECT DISTINCT
			 @MEMBER_MASTER_ROW_ID_PRIMARY_CURSOR_CURRENT  --[MEMBER_MASTER_ROW_ID]
			,[MHK_INTERNAL_ID]
			,[MEDHOK_ID]
			,[SSN]
			,[HICN]
			,[CLAIM_SUBSCRIBER_ID]
			,[MBI]
			,[MEDICAID_NO]
			,[MRN]
			,[EXT_ID]
			,[EXT_ID_TYPE]
			,[EXT_ID_2]
			,[EXT_ID_TYPE_2]
			,[EXT_ID_3]
			,[EXT_ID_TYPE_3]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT



		-- [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		UPDATE TheTarget
		SET
			 ROW_DELETED = 'Y'
			,ROW_DELETED_DATE = GETDATE()
			,ROW_DELETED_REASON = 'Member merged due to incorrect split.'
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] TheTarget
		WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT




		/********************************************************************************************************************************************************************

			Update the control table to flag this row as being processed

		*********************************************************************************************************************************************************************/

		UPDATE TheTarget
		SET
			  TheTarget.[ROW_PROCESSED] = 'Y'
			 ,TheTarget.[ROW_PROCESSED_DATE] = GETDATE()
		FROM [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] TheTarget
		WHERE MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID = @MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID_CURSOR_CURRENT



		-- Grab the next member for the following loop, if there is one
		FETCH NEXT FROM MEMBER_CURSOR INTO @MEMBER_UTILITY_MERGEMEMBER_KEYS_ROW_ID_CURSOR_CURRENT, @MEMBER_MASTER_ROW_ID_PRIMARY_CURSOR_CURRENT, @MEMBER_MASTER_ROW_ID_MERGE_CURSOR_CURRENT







		/********************************************************************************************************************************************************************

			Finally, update the Membership Export tables to keep everything in sync

		*********************************************************************************************************************************************************************/		


		--------------------------------------- PATIENT EVENTS ----------------------------------------


		-- Update MM Patient Events
		UPDATE MPE
			SET MPE.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDW].[dbo].[Member_PatientEvents] MPE
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MPE.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Update MembershipExport Patient Events
		UPDATE MEPE
			SET MEPE.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDW].[dbo].[Membership_Export_PatientEvents] MEPE
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MEPE.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Update Patient Events Historical
		UPDATE MPEH
			SET MPEH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Historical] MPEH
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MPEH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Update Patient Events Staging
		UPDATE MPES
			SET MPES.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDataMarts].[dbo].[Member_PatientEvents_Staging] MPES
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MPES.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO




		--------------------------------------- LABS ----------------------------------------

		-- Update MM Labs
		UPDATE ML
			SET ML.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDW].[dbo].[Member_Labs] ML
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON ML.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Update MembershipExport Labs
		UPDATE MEL
			SET MEL.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDW].[dbo].[Membership_Export_Labs] MEL
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MEL.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Update Labs Historical
		UPDATE MLH
			SET MLH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDataMarts].[dbo].[Member_Labs_Historical] MLH
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MLH.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Update Labs Staging
		UPDATE MLS
			SET MLS.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDataMarts].[dbo].[Member_Labs_Staging] MLS
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MLS.[MEMBER_MASTER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO




		--------------------------------------- PROVIDER SCHEMA ----------------------------------------


		-- Update Member Provider
		UPDATE MEMP
			SET MEMP.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_PRIMARY]
		FROM [FHPDW].[dbo].[Membership_Export_Member_Provider] MEMP
		JOIN [FHPDataMarts].[dbo].[Member_Utility_MergeMember_Keys] MUMK
		ON MEMP.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = MUMK.[MEMBER_MASTER_ROW_ID_MERGE]
		GO


		-- Note:  The remaining Provider schema tables do not rely on MM ID's, so they can be left alone. 



	-- END CURSOR:  MEMBER_CURSOR
	END

	CLOSE MEMBER_CURSOR
	DEALLOCATE MEMBER_CURSOR



END
GO

