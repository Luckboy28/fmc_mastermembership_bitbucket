

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Member_Provider_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Member_Provider_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Member_Provider_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: September 25th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT

	;WITH NewRecords AS
	(

		SELECT DISTINCT

			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
			,[SPECIALITY_UTILIZED]

		FROM [FHPDataMarts].[dbo].[Membership_Export_Member_Provider_SOURCE]
	
		EXCEPT
		
		SELECT DISTINCT

			 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
			,[SPECIALITY_UTILIZED]

		FROM [FHPDW].[dbo].[Membership_Export_Member_Provider] TheTable

	)
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Member_Provider]
	(
		-- [MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]

		 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,[SPECIALITY_UTILIZED]

		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]

		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]

		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		-- TheSource.[MEMBERSHIP_EXPORT_MEMBER_PROVIDER_ROW_ID]

		 NewRecords.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,NewRecords.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
		,NewRecords.[SPECIALITY_UTILIZED]

		,'[FHPDataMarts].[dbo].[Membership_Export_Member_Provider_SOURCE]' [ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]

		----,TheSource.[ROW_PROBLEM]
		----,TheSource.[ROW_PROBLEM_DATE]
		----,TheSource.[ROW_PROBLEM_REASON]
		----,TheSource.[ROW_DELETED]
		----,TheSource.[ROW_DELETED_DATE]
		----,TheSource.[ROW_DELETED_REASON]

		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM NewRecords

	-- NOTE:  There is no update statement.  Records are simply added if they represent a unique key-pair.


	-- Return success
	RETURN 0


END
GO

