

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Facility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Facility_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Facility_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Update date: April 10th 2020
	-- Description:	
	-- ==========================================================================================
	/*
		NOTE:  Need to add UPDATE logic once business rules are available.

		NOTE:  This proc loads both the staging and the final tabels.
	*/
	-- ==========================================================================================


	-- Static GETDATE() for all data adjustments/inserts.  Makes auditing easier.
	DECLARE @GETDATE DATETIME2 = GETDATE();



	-- ==================================================================================================================================
	--
	--              The first step is to add new records to [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging]
	--
	-- ==================================================================================================================================


	WITH KCNGX AS
	(

		SELECT DISTINCT * FROM OPENQUERY([KCNGX_PROVIDERS],
				'

				SELECT
					HCP.HCP_NPI
					--,HCP.HCP_FRST_NM
					--,HCP.HCP_LAST_NM
					--,FD.FAC_ID
					--,FM.HCP_DL_ROLE_NM
					--,FM.MBR_EFF_DT
					--,FM.MBR_EXP_DT
					,FD.*
				FROM PERS_DIAL.FAC_MEMBER FM
				JOIN PERS_DIAL.HCP HCP
					ON FM.HCP_ID = HCP.HCP_ID
				JOIN ORG_DIAL.FAC_DIAL FD
					ON FM.FAC_DIAL_ID = FD.FAC_DIAL_ID
				WHERE HCP.HCP_STS_CD = ''ACTV''
				AND FM.MBR_EXP_DT = ''9999-12-31'' /*Remove this if you want to see the history of where HCPs were affiliated*/
				AND HCP.HCP_NPI IS NOT NULL
				AND FD.FAC_ID IS NOT NULL
				ORDER BY
					 HCP.HCP_ID
					,FD.FAC_ID
					,FM.MBR_EFF_DT
					,FM.MBR_EXP_DT
	
				')
	)
	,RELAVENT_NPI AS
	(
		SELECT DISTINCT
			[NPI]
		FROM [FHPDW].[dbo].[Membership_Export_Provider] MEP
		WHERE MEP.[ROW_DELETED] = 'N'
	)
	,FULL_DATA AS
	(
		-- ==================================================================================================================================
		--
		--                                                         KCNGX Data
		--
		-- ==================================================================================================================================


		-- ADDRESS_TYPE: Physical
		-- FACILITY_TYPE: FKC Clinic
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'FKC Clinic' [FACILITY_TYPE]   --USED LATER
			,KCNGX.[FAC_OTHER_LOCAL_NM] [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_ADDR_LN1])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_ADDR_LN2])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_CITY_NM])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_STATE_CD])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(CASE WHEN LEN(LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD]))) > 0 AND LEN(LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD4]))) > 0 THEN CONCAT(LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD])),'-',LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD4]))) ELSE LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD])) END)),'') [ZIP]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_COUNTY_NM])),'') [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_TEL_NBR],'-',''))),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_FAX_NBR],'-',''))),'') [FAX]
			,NULL [EMAIL]
			,NULLIF(LTRIM(RTRIM(KCNGX.[FAC_ID])),'') [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'KCNGX.[HCP_NPI]' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM KCNGX
		JOIN RELAVENT_NPI
			ON KCNGX.[HCP_NPI] = RELAVENT_NPI.[NPI]
		--LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
		--	ON KCNGX.[HCP_NPI] = NPIData.[NPI]
		WHERE KCNGX.[HCP_NPI] IS NOT NULL
		AND NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_ADDR_LN1])),'') IS NOT NULL

		UNION ALL

		-- ADDRESS_TYPE: Mailing
		-- FACILITY_TYPE: FKC Clinic
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [NPI] --Used for reference later
			--[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'FKC Clinic' [FACILITY_TYPE]   --USED LATER
			,KCNGX.[FAC_OTHER_LOCAL_NM] [FACILITY_NAME]
			,'MAILING' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_ADDR_LN1])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_ADDR_LN2])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_CITY_NM])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_STATE_CD])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(CASE WHEN LEN(LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD]))) > 0 AND LEN(LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD4]))) > 0 THEN CONCAT(LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD])),'-',LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD4]))) ELSE LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD])) END)),'') [ZIP]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_COUNTY_NM])),'') [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_TEL_NBR],'-',''))),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_FAX_NBR],'-',''))),'') [FAX]
			,NULL [EMAIL]
			,NULLIF(LTRIM(RTRIM(KCNGX.[FAC_ID])),'') [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'KCNGX.[HCP_NPI]' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM KCNGX
		JOIN RELAVENT_NPI
			ON KCNGX.[HCP_NPI] = RELAVENT_NPI.[NPI]
		--LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
		--	ON KCNGX.[HCP_NPI] = NPIData.[NPI]
		WHERE KCNGX.[HCP_NPI] IS NOT NULL
		AND NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_ADDR_LN1])),'') IS NOT NULL


		-- ==================================================================================================================================
		--
		--                                                         NPI TABLE DATA
		--
		-- ==================================================================================================================================


		UNION ALL

		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Physical
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'PROVIDER' [FACILITY_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Organization Name])),'') [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Practice Location Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Practice Location Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address State Name])),'') [STATE]
			,CASE WHEN LEN(NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),'')) = 9 THEN CONCAT(LEFT(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),5),'-',RIGHT(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),4)) ELSE NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),'') END [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[NPIData] NPIData
		JOIN RELAVENT_NPI
			ON NPIData.[NPI] = RELAVENT_NPI.[NPI]
		WHERE NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Practice Location Address])),'') IS NOT NULL


		UNION ALL


		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Mailing
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'PROVIDER' [FACILITY_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Organization Name])),'') [FACILITY_NAME]
			,'MAILING' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Mailing Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Mailing Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address State Name])),'') [STATE]
			,CASE WHEN LEN(NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),'')) = 9 THEN CONCAT(LEFT(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),5),'-',RIGHT(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),4)) ELSE NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),'') END [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[NPIData] NPIData
		JOIN RELAVENT_NPI
		ON NPIData.[NPI] = RELAVENT_NPI.[NPI]
		WHERE NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Mailing Address])),'') IS NOT NULL
	)
	INSERT INTO [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging]
	(
		 [NPI]
		,[FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 [NPI]
		,[FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM FULL_DATA

	EXCEPT 

	SELECT
		 [NPI]
		,[FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging]





	-- ==========================================================================================
	-- KCNGX Upsert
	--
	-- Upsert Key:  INTERNAL_FACILITY_ID / FACILITY_TYPE = 'FKC Clinic' / ADDRESS_TYPE  
	-- ==========================================================================================

	INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
	(
		 [FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	)
	SELECT DISTINCT
		-- MEFS.[MEMBERSHIP_EXPORT_FACILITY_STAGING_ROW_ID]
		-- MEFS.[NPI]
		 MEFS.[FACILITY_TYPE]
		,MEFS.[FACILITY_NAME]
		,MEFS.[ADDRESS_TYPE]
		,MEFS.[ADDRESS_1]
		,MEFS.[ADDRESS_2]
		,MEFS.[ADDRESS_3]
		,MEFS.[CITY]
		,MEFS.[STATE]
		,MEFS.[ZIP]
		,MEFS.[COUNTY]
		,MEFS.[COUNTRY]
		,MEFS.[PHONE]
		,MEFS.[ALTERNATE_PHONE]
		,MEFS.[EVENING_PHONE]
		,MEFS.[EMERGENCY_PHONE]
		,MEFS.[FAX]
		,MEFS.[EMAIL]
		,MEFS.[INTERNAL_FACILITY_ID]
		,MEFS.[EXT_ID]
		,MEFS.[EXT_ID_TYPE]
		,MEFS.[STATUS]
		,MEFS.[ROW_SOURCE]
		,MEFS.[ROW_SOURCE_ID]
		--,[ROW_PROCESSED]
		--,[ROW_PROCESSED_DATE]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging] MEFS
	LEFT JOIN [FHPDW].[dbo].[Membership_Export_Facility] MEF
	--ON MEFS.[NPI] = MEF.[NPI]
		ON MEFS.[FACILITY_TYPE] = MEF.[FACILITY_TYPE]
		AND MEFS.[ADDRESS_TYPE] = MEF.[ADDRESS_TYPE]
		AND ISNULL(MEFS.[INTERNAL_FACILITY_ID],'<NULL>') = ISNULL(MEF.[INTERNAL_FACILITY_ID] ,'<NULL>')
		AND MEFS.[ROW_DELETED] = 'N'
		AND MEF.[ROW_DELETED] = 'N'
	WHERE MEFS.[FACILITY_TYPE] = 'FKC Clinic'
	AND MEFS.[INTERNAL_FACILITY_ID] IS NOT NULL
	AND MEF.[MEMBERSHIP_EXPORT_FACILITY_ROW_ID] IS NULL  --New records only (did not join to existing record)



	
	UPDATE MEF
	SET 
		 MEF.[FACILITY_TYPE] = MEFS.[FACILITY_TYPE]
		,MEF.[FACILITY_NAME] = MEFS.[FACILITY_NAME]
		,MEF.[ADDRESS_TYPE] = MEFS.[ADDRESS_TYPE]
		,MEF.[ADDRESS_1] = MEFS.[ADDRESS_1]
		,MEF.[ADDRESS_2] = MEFS.[ADDRESS_2]
		,MEF.[ADDRESS_3] = MEFS.[ADDRESS_3]
		,MEF.[CITY] = MEFS.[CITY]
		,MEF.[STATE] = MEFS.[STATE]
		,MEF.[ZIP] = MEFS.[ZIP]
		,MEF.[COUNTY] = MEFS.[COUNTY]
		,MEF.[COUNTRY] = MEFS.[COUNTRY]
		,MEF.[PHONE] = MEFS.[PHONE]
		,MEF.[ALTERNATE_PHONE] = MEFS.[ALTERNATE_PHONE]
		,MEF.[EVENING_PHONE] = MEFS.[EVENING_PHONE]
		,MEF.[EMERGENCY_PHONE] = MEFS.[EMERGENCY_PHONE]
		,MEF.[FAX] = MEFS.[FAX]
		,MEF.[EMAIL] = MEFS.[EMAIL]
		,MEF.[INTERNAL_FACILITY_ID] = MEFS.[INTERNAL_FACILITY_ID]
		,MEF.[EXT_ID] = MEFS.[EXT_ID]
		,MEF.[EXT_ID_TYPE] = MEFS.[EXT_ID_TYPE]
		,MEF.[STATUS] = MEFS.[STATUS]
		,MEF.[ROW_SOURCE] = MEFS.[ROW_SOURCE]
		,MEF.[ROW_SOURCE_ID] = MEFS.[ROW_SOURCE_ID]
		--,[ROW_PROCESSED]
		--,[ROW_PROCESSED_DATE]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		,MEF.[ROW_UPDATE_DATE] = @GETDATE
	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging] MEFS
	JOIN [FHPDW].[dbo].[Membership_Export_Facility] MEF
		ON MEFS.[FACILITY_TYPE] = MEF.[FACILITY_TYPE]
		AND MEFS.[ADDRESS_TYPE] = MEF.[ADDRESS_TYPE]
		AND MEFS.[INTERNAL_FACILITY_ID] = MEF.[INTERNAL_FACILITY_ID]
	WHERE MEFS.[FACILITY_TYPE] = 'FKC Clinic'
	AND MEFS.[INTERNAL_FACILITY_ID] IS NOT NULL
	AND MEFS.[ROW_DELETED] = 'N'
	AND MEF.[ROW_DELETED] = 'N'
	AND
	(
		   ISNULL(MEF.[FACILITY_NAME],'<NULL>') <> ISNULL(MEFS.[FACILITY_NAME],'<NULL>')
		OR ISNULL(MEF.[ADDRESS_TYPE],'<NULL>') <> ISNULL(MEFS.[ADDRESS_TYPE],'<NULL>')
		OR ISNULL(MEF.[ADDRESS_1],'<NULL>') <> ISNULL(MEFS.[ADDRESS_1],'<NULL>')
		OR ISNULL(MEF.[ADDRESS_2],'<NULL>') <> ISNULL(MEFS.[ADDRESS_2],'<NULL>')
		OR ISNULL(MEF.[ADDRESS_3],'<NULL>') <> ISNULL(MEFS.[ADDRESS_3],'<NULL>')
		OR ISNULL(MEF.[CITY],'<NULL>') <> ISNULL(MEFS.[CITY],'<NULL>')
		OR ISNULL(MEF.[STATE],'<NULL>') <> ISNULL(MEFS.[STATE],'<NULL>')
		OR ISNULL(MEF.[ZIP],'<NULL>') <> ISNULL(MEFS.[ZIP],'<NULL>')
		OR ISNULL(MEF.[COUNTY],'<NULL>') <> ISNULL(MEFS.[COUNTY],'<NULL>')
		OR ISNULL(MEF.[COUNTRY],'<NULL>') <> ISNULL(MEFS.[COUNTRY],'<NULL>')
		OR ISNULL(MEF.[PHONE],'<NULL>') <> ISNULL(MEFS.[PHONE],'<NULL>')
		OR ISNULL(MEF.[ALTERNATE_PHONE],'<NULL>') <> ISNULL(MEFS.[ALTERNATE_PHONE],'<NULL>')
		OR ISNULL(MEF.[EVENING_PHONE],'<NULL>') <> ISNULL(MEFS.[EVENING_PHONE],'<NULL>')
		OR ISNULL(MEF.[EMERGENCY_PHONE],'<NULL>') <> ISNULL(MEFS.[EMERGENCY_PHONE],'<NULL>')
		OR ISNULL(MEF.[FAX],'<NULL>') <> ISNULL(MEFS.[FAX],'<NULL>')
		OR ISNULL(MEF.[EMAIL],'<NULL>') <> ISNULL(MEFS.[EMAIL],'<NULL>')
		OR ISNULL(MEF.[EXT_ID],'<NULL>') <> ISNULL(MEFS.[EXT_ID],'<NULL>')
		OR ISNULL(MEF.[EXT_ID_TYPE],'<NULL>') <> ISNULL(MEFS.[EXT_ID_TYPE],'<NULL>')
		OR ISNULL(MEF.[STATUS],'<NULL>') <> ISNULL(MEFS.[STATUS],'<NULL>')
		OR ISNULL(MEF.[ROW_SOURCE],'<NULL>') <> ISNULL(MEFS.[ROW_SOURCE],'<NULL>')
		OR ISNULL(MEF.[ROW_SOURCE_ID],'<NULL>') <> ISNULL(MEFS.[ROW_SOURCE_ID],'<NULL>')
	)



	-- ==========================================================================================
	-- NPIData Insert
	--
	-- Insert Key:  FACILITY_TYPE = 'PROVIDER' / ADDRESS_TYPE / ALL ADDRESS FIELDS + PHONE
	-- ==========================================================================================

	INSERT INTO [FHPDW].[dbo].[Membership_Export_Facility]
	(
		 [FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	)
	SELECT DISTINCT
		 [FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging]
	WHERE [FACILITY_TYPE] = 'PROVIDER'
	AND [ROW_DELETED] = 'N'

	EXCEPT 

	SELECT
		 [FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
	FROM [FHPDW].[dbo].[Membership_Export_Facility]
	WHERE [ROW_DELETED] = 'N'



	-- Return success
	RETURN 0


END
GO

