

USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Master_StatusUpdate', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Master_StatusUpdate;
GO


CREATE PROCEDURE [dbo].[Member_Master_StatusUpdate]
	-- No parameters needed
AS
BEGIN


-- ==========================================================================================
-- Author:		David M. Wilson
-- Create date: December 26th 2017
-- Description:	
-- ==========================================================================================
/*

	This stored procedure updates the Master Member table with the member's latest LOB_TYPE,
	LOB_VENDOR, and STATUS with their highest priority record from the Member_Eligibility
	table.

*/
-- ==========================================================================================


	-- Update the Member Master record, if needed.
	;WITH LatestRecords AS
	(
		SELECT
			 ROW_NUMBER() OVER (PARTITION BY ME.[MEMBER_MASTER_ROW_ID] ORDER BY ISNULL(ME.[TERM_DATE],CAST('9999-12-31' AS DATE)) DESC, ME.[START_DATE] DESC, LH.[LOB_RANK] ASC) AS [RANK]
			,ME.*
		FROM [FHPDW].[DBO].[Member_Eligibility] ME
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LH
			ON ME.[LOB_TYPE] = LH.[LOB_TYPE]
			AND ME.[LOB_VENDOR] = LH.[LOB_VENDOR]
		WHERE ME.[ROW_DELETED] = 'N'
		AND ME.[START_DATE] <> ISNULL(ME.[TERM_DATE],CAST('9999-12-31' AS DATE)) --One method for soft-deletion
	)
	UPDATE MM
		SET
			[STATUS] = LR.[STATUS],
			[LOB_VENDOR] = LR.[LOB_VENDOR],
			[LOB_TYPE] = LR.[LOB_TYPE],
			[ROW_PROBLEM] = CASE WHEN MM.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN 'N' ELSE MM.[ROW_PROBLEM] END,
			[ROW_PROBLEM_REASON] = CASE WHEN MM.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE MM.[ROW_PROBLEM_REASON] END,
			[ROW_PROBLEM_DATE] = CASE WHEN MM.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE MM.[ROW_PROBLEM_DATE] END
	FROM [FHPDW].[dbo].[Member_Master] MM
	JOIN LatestRecords LR
	ON LR.MEMBER_MASTER_ROW_ID = MM.MEMBER_MASTER_ROW_ID
	WHERE 
	(
		MM.[STATUS] <> LR.[STATUS]
		OR MM.[LOB_VENDOR] <> LR.[LOB_VENDOR]
		OR MM.[LOB_TYPE] <> LR.[LOB_TYPE]
		OR MM.[ROW_PROBLEM] <> CASE WHEN MM.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN 'N' ELSE MM.[ROW_PROBLEM] END
		OR MM.[ROW_PROBLEM_REASON] <> CASE WHEN MM.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE MM.[ROW_PROBLEM_REASON] END
		OR MM.[ROW_PROBLEM_DATE] <> CASE WHEN MM.[ROW_PROBLEM_REASON] = 'Placeholder. Member data not yet loaded.' THEN NULL ELSE MM.[ROW_PROBLEM_DATE] END
	)
	AND
	(
		LR.[RANK] = 1
	)

END
GO


