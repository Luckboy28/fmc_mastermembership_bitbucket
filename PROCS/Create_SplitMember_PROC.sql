USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Member_Utility_SplitMembers', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Member_Utility_SplitMembers;
GO


CREATE PROCEDURE [dbo].[Member_Utility_SplitMembers]
-- No parameters
AS
BEGIN

	DECLARE @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT AS INT;



	DECLARE MEMBER_MASTER_ROW_ID_CURSOR CURSOR FOR
		SELECT DISTINCT
			MEMBER_MASTER_ROW_ID
		FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys]
		WHERE [ROW_PROCESSED] = 'N'
		AND [ROW_DELETED] = 'N'


	OPEN MEMBER_MASTER_ROW_ID_CURSOR
	FETCH NEXT FROM MEMBER_MASTER_ROW_ID_CURSOR INTO @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


	-- Begin main cursor through MEMBER_MASTER_ROW_ID's
	WHILE @@FETCH_STATUS = 0
	BEGIN

		
		/********************************************************************************************************************************************************************

			Make sure the SplitMember keys don't overlap other members (It never should, but good to check)

		*********************************************************************************************************************************************************************/


		-- Make a table variable to hold the results of the data overlap check
		DECLARE @Distinct_KeyHistory_Matches AS TABLE
		(
			MEMBER_MASTER_ROW_ID INT NOT NULL
		);


		-- Empty the table, just to be cautious
		DELETE FROM @Distinct_KeyHistory_Matches;


		-- Gather together all of the matching MEMBER_MASTER_ROW_ID's
		WITH KeyHistory_Matches AS
		(
			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.MHK_INTERNAL_ID = SMEK.KEY_VALUE
			WHERE MMKH.MHK_INTERNAL_ID IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'MHK_INTERNAL_ID'
			
			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.MEDHOK_ID = SMEK.KEY_VALUE
			WHERE MMKH.MEDHOK_ID IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'MEDHOK_ID'

			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.SSN = SMEK.KEY_VALUE
			WHERE MMKH.SSN IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'SSN'

			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.HICN = SMEK.KEY_VALUE
			WHERE MMKH.HICN IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'HICN'

			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.CLAIM_SUBSCRIBER_ID = SMEK.KEY_VALUE
			WHERE MMKH.CLAIM_SUBSCRIBER_ID IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'CLAIM_SUBSCRIBER_ID'

			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.MBI = SMEK.KEY_VALUE
			WHERE MMKH.MBI IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'MBI'

			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.MEDICAID_NO = SMEK.KEY_VALUE
			WHERE MMKH.MEDICAID_NO IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'MEDICAID_NO'

			UNION ALL


			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON MMKH.MRN = SMEK.KEY_VALUE
			WHERE MMKH.MRN IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'MRN'

			UNION ALL

			SELECT
				MMKH.MEMBER_MASTER_ROW_ID
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] MMKH
			JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
			ON CONCAT(MMKH.[EXT_ID],':',MMKH.[EXT_ID_TYPE]) = SMEK.KEY_VALUE
			WHERE MMKH.[EXT_ID] IS NOT NULL
			AND MMKH.[EXT_ID_TYPE] IS NOT NULL
			AND SMEK.MEMBER_MASTER_ROW_ID = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT  --Cursor value
			AND SMEK.[KEY_NAME] = 'EXT_ID_AND_TYPE'

		)
		INSERT INTO @Distinct_KeyHistory_Matches (MEMBER_MASTER_ROW_ID)
		SELECT DISTINCT
			MEMBER_MASTER_ROW_ID
		FROM KeyHistory_Matches;


		-- If the SplitMember/KeyHistory check only produces one distinct MEMBER_MASTER_ROW_ID, and it matches the MEMBER_MASTER_ROW_ID from the SplitMember table that we're currently processing,
		-- then the process is good to go and logic can continue.   Otherwise, write an error to the SplitMember table and move on to the next member.
		IF ((SELECT COUNT(1) FROM @Distinct_KeyHistory_Matches) = 1) AND ((SELECT TOP 1 MEMBER_MASTER_ROW_ID FROM @Distinct_KeyHistory_Matches) = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT)
		BEGIN
			


			/********************************************************************************************************************************************************************

				Begin nested cursor (ewwwwwww) for the MEMBER_SPLITMEMBER_TEMP_MEMBER_ID's.    If Member A is being split into members B, C, and D -- then this cursor
				would loop through B, C, and D.

			*********************************************************************************************************************************************************************/
			
			-- Variable to hold the current loop's value
			DECLARE @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT AS INT;

			-- Declare/setup the cursor
			DECLARE MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR CURSOR FOR
				SELECT DISTINCT
					MEMBER_SPLITMEMBER_TEMP_MEMBER_ID
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys]
				WHERE [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT
				AND [ROW_PROCESSED] = 'N'
				AND [ROW_DELETED] = 'N'
				ORDER BY MEMBER_SPLITMEMBER_TEMP_MEMBER_ID ASC
				

			OPEN MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR
			FETCH NEXT FROM MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR INTO @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT


			-- Begin main cursor through MEMBER_SPLITMEMBER_TEMP_MEMBER_ID's
			WHILE @@FETCH_STATUS = 0
			BEGIN


				-- Create table variable to hold the primary key for the record we're about to create, after it's inserted.
				DECLARE @MEMBER_MASTER_INSERTED AS TABLE
				(
					[MEMBER_MASTER_ROW_ID] INT NOT NULL
				)

				-- Empty the table that we just created, because Dave is a suspicious untrusting bastard
				DELETE FROM @MEMBER_MASTER_INSERTED

				-- Insert the placeholder record.  This placeholder won't have any keys on the [Member_Master] table, but all the relavent keys
				-- will be inserted into the [Member_Master_KeyHistory] table after this insert creates a new MEMBER_MASTER_ROW_ID.
				INSERT INTO [FHPDW].[dbo].[Member_Master]
				(
					 [STATUS]
					,[LOB_VENDOR]
					,[LOB_TYPE]

					,[ROW_SOURCE]
					,[ROW_SOURCE_ID]
					,[ROW_PROBLEM]
					,[ROW_PROBLEM_DATE]
					,[ROW_PROBLEM_REASON]
				)
				OUTPUT INSERTED.[MEMBER_MASTER_ROW_ID] INTO @MEMBER_MASTER_INSERTED
				VALUES
				(
					 'INACTIVE'  --[STATUS]
					,'UNKNOWN'  --[LOB_VENDOR]
					,'UNKNOWN'  --[LOB_TYPE]

					,'PROC: Member_Utility_SplitMembers'  --[ROW_SOURCE]
					,CONCAT(@MEMBER_MASTER_ROW_ID_CURSOR_CURRENT,':',@MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT)  --[ROW_SOURCE_ID]
					,'Y'  --[ROW_PROBLEM]
					,GETDATE()  --[ROW_PROBLEM_DATE]
					,'Placeholder. Member data not yet loaded.'  --[ROW_PROBLEM_REASON]
				)	


				-- Insert each of the provided keys (if they exist) for this temp-sub-member into the KeyHistory table with their new MEMBER_MASTER_ROW_ID
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[MHK_INTERNAL_ID]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,CAST(SMEK.[KEY_VALUE] AS INT) AS [MHK_INTERNAL_ID]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'MHK_INTERNAL_ID'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[MEDHOK_ID]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [MEDHOK_ID]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'MEDHOK_ID'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[SSN]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [SSN]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'SSN'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[HICN]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [HICN]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'HICN'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[CLAIM_SUBSCRIBER_ID]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [CLAIM_SUBSCRIBER_ID]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'CLAIM_SUBSCRIBER_ID'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[MBI]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [MBI]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'MBI'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[MEDICAID_NO]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [MEDICAID_NO]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'MEDICAID_NO'								
												
 												
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[MRN]							
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,SMEK.[KEY_VALUE] AS [MRN]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'MRN'								
												
 				
				-- Special case, since it's a compound key				
				INSERT INTO [FHPDataMarts].[dbo].[Member_Master_KeyHistory]								
				(								
					 [MEMBER_MASTER_ROW_ID]							
					,[EXT_ID]
					,[EXT_ID_TYPE]				
				)								
				SELECT								
					(SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED) AS [MEMBER_MASTER_ROW_ID]							
					,LEFT(SMEK.[KEY_VALUE],CHARINDEX(':',SMEK.[KEY_VALUE]) - 1) AS [EXT_ID]
					,RIGHT(SMEK.[KEY_VALUE], LEN(SMEK.[KEY_VALUE]) - CHARINDEX(':',SMEK.[KEY_VALUE])) AS [EXT_ID_TYPE]
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT								
				AND SMEK.[KEY_NAME] = 'EXT_ID_AND_TYPE'
												
 												

				-- Update the current each [MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] with their new [MEMBER_MASTER_ROW_ID] (stored in [MEMBER_SPLITMEMBER_NEW_PERMANENT_MEMBER_ID]),
				-- and flag them as being processed.
				UPDATE SMEK
				SET
					 MEMBER_SPLITMEMBER_NEW_PERMANENT_MEMBER_ID = (SELECT TOP 1 [MEMBER_MASTER_ROW_ID] FROM @MEMBER_MASTER_INSERTED)
					,ROW_PROCESSED = 'Y'
					,ROW_PROCESSED_DATE = GETDATE()
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT								
				AND SMEK.[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID] = @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT



				-- Grab the next SplitMember Temp ID for the following loop, if there is one
				FETCH NEXT FROM MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR INTO @MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR_CURRENT

			-- END CURSOR:  MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR
			END

			CLOSE MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR
			DEALLOCATE MEMBER_SPLITMEMBER_TEMP_MEMBER_ID_CURSOR


			/********************************************************************************************************************************************************************

				Now that the member has been split, and all the relavent keys loaded into the KeyHistory table, it's time to soft-delete the original member
				(who was incorrectly merged), and hard-delete any references to this member in other non-SOX tables.  This data will then be regenerated
				once the member data is reloaded, per the usual process for creating/updating members.

			*********************************************************************************************************************************************************************/

			-- [FHPDW].[dbo].[Member_Master]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDW].[dbo].[Member_Master] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDW].[dbo].[Member_Eligibility]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDW].[dbo].[Member_Eligibility] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDW].[dbo].[Member_Address]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDW].[dbo].[Member_Address] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDW].[dbo].[Member_Contacts]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDW].[dbo].[Member_Contacts] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDataMarts].[dbo].[Member_Address_Historical]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDataMarts].[dbo].[Member_Address_Historical] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT



			-- [FHPDataMarts].[dbo].[Member_Contacts_Historical]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDataMarts].[dbo].[Member_Contacts_Historical] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDataMarts].[dbo].[Member_Eligibility_Historical]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDataMarts].[dbo].[Member_Eligibility_Historical] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDataMarts].[dbo].[Member_Master_Historical]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDataMarts].[dbo].[Member_Master_Historical] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				--,ROW_PROBLEM = 'Y'
				--,ROW_PROBLEM_DATE = GETDATE()
				--,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDataMarts].[dbo].[Member_Master_Historical]
			UPDATE TheTarget
			SET
				 ROW_DELETED = 'Y'
				,ROW_DELETED_DATE = GETDATE()
				,ROW_DELETED_REASON = 'Member split/removed due to incorrect merge.'
				,ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = 'Deleted.  Member split/removed due to incorrect merge.'
			FROM [FHPDataMarts].[dbo].[Member_Master_Historical] TheTarget
			WHERE TheTarget.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


			-- [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
			DELETE FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk]
			WHERE [MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT
			OR [MEDHOK_ID] IN
			( 
				SELECT														
					 SMEK.[KEY_VALUE] AS [MEDHOK_ID]							
				FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK								
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT															
				AND SMEK.[KEY_NAME] = 'MEDHOK_ID'
			);



			/********************************************************************************************************************************************************************

				Now that the member has been split, we need to force reload any data in the staging tables that match up to the keys found in the 
				[FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] table.

			*********************************************************************************************************************************************************************/

			-- Members
			WITH All_Matching_Staged_Members AS
			(

				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MHK_INTERNAL_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MHK_INTERNAL_ID'	
				AND MMCS.[MHK_INTERNAL_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDHOK_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDHOK_ID'	
				AND MMCS.[MEDHOK_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[SSN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'SSN'	
				AND MMCS.[SSN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[HICN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'HICN'	
				AND MMCS.[HICN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[CLAIM_SUBSCRIBER_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'CLAIM_SUBSCRIBER_ID'	
				AND MMCS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MBI] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MBI'	
				AND MMCS.[MBI] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDICAID_NO] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDICAID_NO'	
				AND MMCS.[MEDICAID_NO] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MRN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MRN'	
				AND MMCS.[MRN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Master_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON CONCAT(MMCS.[EXT_ID],':',MMCS.[EXT_ID_TYPE]) = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'EXT_ID_AND_TYPE'	
				AND MMCS.[EXT_ID] IS NOT NULL
				AND MMCS.[EXT_ID_TYPE] IS NOT NULL
			)
			INSERT INTO [FHPDataMarts].[dbo].[Member_Master_Staging]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
				,[BENEFIT_STATUS]
				,[FIRST_NAME]
				,[MIDDLE_NAME]
				,[LAST_NAME]
				,[PREFIX]
				,[SUFFIX]
				,[DATE_OF_BIRTH]
				,[GENDER]
				,[COMPANY_DESCRIPTION]
				,[LOB_CODE]
				,[FAMILY_ID]
				,[PERSON_NUMBER]
				,[RACE]
				,[ETHNICITY]
				,[PRIMARY_LANGUAGE]
				,[PRIMARY_LANGUAGE_SOURCE]
				,[SPOKEN_LANGUAGE]
				,[SPOKEN_LANGUAGE_SOURCE]
				,[WRITTEN_LANGUAGE]
				,[WRITTEN_LANGUAGE_SOURCE]
				,[OTHER_LANGUAGE]
				,[OTHER_LANGUAGE_SOURCE]
				,[EMPLOYEE]
				,[PBP_NUMBER]
				,[CURRENT_LIS]
				,[IPA_GROUP_EXT_ID]
				,[MEDICARE_PLAN_CODE]
				,[MEDICARE_TYPE]
				,[DUPLICATE_MEDICAID_ID]
				,[PREGNANCY_DUE_DATE]
				,[PREGNANCY_INDICATOR]
				,[BOARD_NUMBER]
				,[DEPENDENT_CODE]
				,[LEGACY_SUBSCRIBER_ID]
				,[GROUP_NUMBER]
				,[SOURCE]
				,[ESCO_ID]
				,[CLIENT_SPECIFIC_DATA]
				,[RELATIONSHIP_CODE]
				,[TIME_ZONE]
				,[DATE_OF_DEATH]
				,[FOSTER_CARE_FLAG]
				,[VIP]
				,[CLINIC_NUMBER]
				,[MODALITY]
				,[PAYER_NAME]
				,[PAYER_ID_TYPE]
				,[PAYER_ID]
				,[DIALYSIS_START_DATE]
				,[KIDNEY_TRANSPLANT_DATE]
				,[LOB_VENDOR]
				,[LOB_TYPE]
				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				--,[ROW_PROCESSED]
				--,[ROW_PROCESSED_DATE]
				--,[ROW_DELETED]
				--,[ROW_DELETED_DATE]
				--,[ROW_DELETED_REASON]
				--,[ROW_CREATE_DATE]
				--,[ROW_UPDATE_DATE]
			)
			SELECT DISTINCT
				 NULL  --[MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
				,[BENEFIT_STATUS]
				,[FIRST_NAME]
				,[MIDDLE_NAME]
				,[LAST_NAME]
				,[PREFIX]
				,[SUFFIX]
				,[DATE_OF_BIRTH]
				,[GENDER]
				,[COMPANY_DESCRIPTION]
				,[LOB_CODE]
				,[FAMILY_ID]
				,[PERSON_NUMBER]
				,[RACE]
				,[ETHNICITY]
				,[PRIMARY_LANGUAGE]
				,[PRIMARY_LANGUAGE_SOURCE]
				,[SPOKEN_LANGUAGE]
				,[SPOKEN_LANGUAGE_SOURCE]
				,[WRITTEN_LANGUAGE]
				,[WRITTEN_LANGUAGE_SOURCE]
				,[OTHER_LANGUAGE]
				,[OTHER_LANGUAGE_SOURCE]
				,[EMPLOYEE]
				,[PBP_NUMBER]
				,[CURRENT_LIS]
				,[IPA_GROUP_EXT_ID]
				,[MEDICARE_PLAN_CODE]
				,[MEDICARE_TYPE]
				,[DUPLICATE_MEDICAID_ID]
				,[PREGNANCY_DUE_DATE]
				,[PREGNANCY_INDICATOR]
				,[BOARD_NUMBER]
				,[DEPENDENT_CODE]
				,[LEGACY_SUBSCRIBER_ID]
				,[GROUP_NUMBER]
				,[SOURCE]
				,[ESCO_ID]
				,[CLIENT_SPECIFIC_DATA]
				,[RELATIONSHIP_CODE]
				,[TIME_ZONE]
				,[DATE_OF_DEATH]
				,[FOSTER_CARE_FLAG]
				,[VIP]
				,[CLINIC_NUMBER]
				,[MODALITY]
				,[PAYER_NAME]
				,[PAYER_ID_TYPE]
				,[PAYER_ID]
				,[DIALYSIS_START_DATE]
				,[KIDNEY_TRANSPLANT_DATE]
				,[LOB_VENDOR]
				,[LOB_TYPE]
				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				--,[CREATE_DATE]
				--,[UPDATE_DATE]
			FROM All_Matching_Staged_Members;



			-- Eligibility
			WITH All_Matching_Staged_Eligibility AS
			(
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MHK_INTERNAL_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MHK_INTERNAL_ID'	
				AND MMCS.[MHK_INTERNAL_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDHOK_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDHOK_ID'	
				AND MMCS.[MEDHOK_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[SSN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'SSN'	
				AND MMCS.[SSN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[HICN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'HICN'	
				AND MMCS.[HICN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[CLAIM_SUBSCRIBER_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'CLAIM_SUBSCRIBER_ID'	
				AND MMCS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MBI] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MBI'	
				AND MMCS.[MBI] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDICAID_NO] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDICAID_NO'	
				AND MMCS.[MEDICAID_NO] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MRN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MRN'	
				AND MMCS.[MRN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Eligibility_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON CONCAT(MMCS.[EXT_ID],':',MMCS.[EXT_ID_TYPE]) = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'EXT_ID_AND_TYPE'	
				AND MMCS.[EXT_ID] IS NOT NULL
				AND MMCS.[EXT_ID_TYPE] IS NOT NULL
			)
			INSERT INTO [FHPDataMarts].[dbo].[Member_Eligibility_Staging]
			(
				 [MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
				,[ESCO_ID]
				,[HLTH_PLN_SYSID]
				,[HLTH_PLN_PROD_LINE]
				,[HLTH_PLN_RPT_GRP]
				,[HLTH_PLN_STD_CARRIER_CD]
				,[PLAN_NAME]
				,[START_DATE]
				,[TERM_DATE]
				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				,[LOB_VENDOR]
				,[LOB_TYPE]
			)
			SELECT DISTINCT
				 NULL  --[MEMBER_MASTER_ROW_ID]
				,[MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
				,[ESCO_ID]
				,[HLTH_PLN_SYSID]
				,[HLTH_PLN_PROD_LINE]
				,[HLTH_PLN_RPT_GRP]
				,[HLTH_PLN_STD_CARRIER_CD]
				,[PLAN_NAME]
				,[START_DATE]
				,[TERM_DATE]
				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				,[LOB_VENDOR]
				,[LOB_TYPE]
			FROM All_Matching_Staged_Eligibility;


			
			-- Address
			WITH All_Matching_Staged_Address AS
			(
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MHK_INTERNAL_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MHK_INTERNAL_ID'	
				AND MMCS.[MHK_INTERNAL_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDHOK_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDHOK_ID'	
				AND MMCS.[MEDHOK_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[SSN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'SSN'	
				AND MMCS.[SSN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[HICN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'HICN'	
				AND MMCS.[HICN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[CLAIM_SUBSCRIBER_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'CLAIM_SUBSCRIBER_ID'	
				AND MMCS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MBI] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MBI'	
				AND MMCS.[MBI] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDICAID_NO] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDICAID_NO'	
				AND MMCS.[MEDICAID_NO] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MRN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MRN'	
				AND MMCS.[MRN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Address_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON CONCAT(MMCS.[EXT_ID],':',MMCS.[EXT_ID_TYPE]) = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'EXT_ID_AND_TYPE'	
				AND MMCS.[EXT_ID] IS NOT NULL
				AND MMCS.[EXT_ID_TYPE] IS NOT NULL
			)
			INSERT INTO [FHPDataMarts].[dbo].[Member_Address_Staging]
			(
				 [MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
				,[ADDRESS_TYPE]
				,[ADDRESS_1]
				,[ADDRESS_2]
				,[ADDRESS_3]
				,[CITY]
				,[STATE]
				,[ZIP]
				,[COUNTY]
				,[ISLAND]
				,[COUNTRY]
				,[EFFECTIVE_DATE]
				,[TERM_DATE]
				,[ADDRESS_STATUS]
				,[PHONE]
				,[ALTERNATE_PHONE]
				,[EVENING_PHONE]
				,[EMERGENCY_PHONE]
				,[FAX]
				,[EMAIL]
				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				,[LOB_VENDOR]
				,[LOB_TYPE]
			)
			SELECT DISTINCT
				 [MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]
				,[ADDRESS_TYPE]
				,[ADDRESS_1]
				,[ADDRESS_2]
				,[ADDRESS_3]
				,[CITY]
				,[STATE]
				,[ZIP]
				,[COUNTY]
				,[ISLAND]
				,[COUNTRY]
				,[EFFECTIVE_DATE]
				,[TERM_DATE]
				,[ADDRESS_STATUS]
				,[PHONE]
				,[ALTERNATE_PHONE]
				,[EVENING_PHONE]
				,[EMERGENCY_PHONE]
				,[FAX]
				,[EMAIL]
				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				,[LOB_VENDOR]
				,[LOB_TYPE]
			FROM All_Matching_Staged_Address;


			-- Contacts
			WITH All_Matching_Staged_Contacts AS
			(
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MHK_INTERNAL_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MHK_INTERNAL_ID'	
				AND MMCS.[MHK_INTERNAL_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDHOK_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDHOK_ID'	
				AND MMCS.[MEDHOK_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[SSN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'SSN'	
				AND MMCS.[SSN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[HICN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'HICN'	
				AND MMCS.[HICN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[CLAIM_SUBSCRIBER_ID] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'CLAIM_SUBSCRIBER_ID'	
				AND MMCS.[CLAIM_SUBSCRIBER_ID] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MBI] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MBI'	
				AND MMCS.[MBI] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MEDICAID_NO] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MEDICAID_NO'	
				AND MMCS.[MEDICAID_NO] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON MMCS.[MRN] = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'MRN'	
				AND MMCS.[MRN] IS NOT NULL	
	
				UNION ALL	
	
				SELECT	
					MMCS.*
				FROM [FHPDataMarts].[dbo].[Member_Contacts_Combined_Sources] MMCS	
				JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK	
				ON CONCAT(MMCS.[EXT_ID],':',MMCS.[EXT_ID_TYPE]) = SMEK.[KEY_VALUE]	
				WHERE SMEK.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT	
				AND SMEK.[KEY_NAME] = 'EXT_ID_AND_TYPE'	
				AND MMCS.[EXT_ID] IS NOT NULL
				AND MMCS.[EXT_ID_TYPE] IS NOT NULL
			)
			INSERT INTO [FHPDataMarts].[dbo].[Member_Contacts_Staging]
			(
				 [MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]

				,[CONTACT_TYPE]
				,[FIRST_NAME]
				,[MIDDLE_NAME]
				,[LAST_NAME]
				,[PREFIX]
				,[SUFFIX]
				,[DATE_OF_BIRTH]
				,[GENDER]
				,[RELATION_TO_MEMBER]
				,[PROVIDER_ID_TYPE]
				,[PROVIDER_ID]

				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				,[LOB_VENDOR]
				,[LOB_TYPE]
			)
			SELECT
				 [MHK_INTERNAL_ID]
				,[MEDHOK_ID]
				,[SSN]
				,[HICN]
				,[CLAIM_SUBSCRIBER_ID]
				,[MBI]
				,[MEDICAID_NO]
				,[MRN]
				,[EXT_ID]
				,[EXT_ID_TYPE]
				,[EXT_ID_2]
				,[EXT_ID_TYPE_2]
				,[EXT_ID_3]
				,[EXT_ID_TYPE_3]

				,[CONTACT_TYPE]
				,[FIRST_NAME]
				,[MIDDLE_NAME]
				,[LAST_NAME]
				,[PREFIX]
				,[SUFFIX]
				,[DATE_OF_BIRTH]
				,[GENDER]
				,[RELATION_TO_MEMBER]
				,[PROVIDER_ID_TYPE]
				,[PROVIDER_ID]

				,[ROW_SOURCE]
				,[ROW_SOURCE_ID]
				,[LOB_VENDOR]
				,[LOB_TYPE]
			FROM All_Matching_Staged_Contacts;


		END
		ELSE
		BEGIN

			-- Log an error for the current member
			UPDATE [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys]
			SET
				 ROW_PROBLEM = 'Y'
				,ROW_PROBLEM_DATE = GETDATE()
				,ROW_PROBLEM_REASON = CONCAT('Failed KeyHistory validation.   Distinct_KeyHistory_Matches = ',(SELECT COUNT(1) FROM @Distinct_KeyHistory_Matches))

		END

		-- Grab the next member for the following loop, if there is one
		FETCH NEXT FROM MEMBER_MASTER_ROW_ID_CURSOR INTO @MEMBER_MASTER_ROW_ID_CURSOR_CURRENT


	-- END CURSOR:  MEMBER_MASTER_ROW_ID_CURSOR
	END

	CLOSE MEMBER_MASTER_ROW_ID_CURSOR
	DEALLOCATE MEMBER_MASTER_ROW_ID_CURSOR


	---- Finally, update any existing MedHOK ID's in the [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] table to their new MEMBER_MASTER_ROW_ID's
	---- REMOVED:  The secondary split member would need a "Primary" ID.
	--UPDATE XW
	--SET
	--	XW.[MEMBER_MASTER_ROW_ID] = SMEK.[MEMBER_SPLITMEMBER_NEW_PERMANENT_MEMBER_ID]
	--FROM [FHPDataMarts].[dbo].[Member_MedHOK_Crosswalk] XW
	--JOIN [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] SMEK
	--ON XW.[MEDHOK_ID] = SMEK.[KEY_VALUE]
	--AND XW.[MEMBER_MASTER_ROW_ID] = SMEK.[MEMBER_MASTER_ROW_ID]  -- Join on the previous key before the split
	--WHERE SMEK.[KEY_NAME] = 'MEDHOK_ID'
	--AND SMEK.[MEMBER_SPLITMEMBER_NEW_PERMANENT_MEMBER_ID] IS NOT NULL


END
GO



--EXEC [FHPDataMarts].[dbo].[Member_Utility_SplitMembers]


--SELECT * FROM [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys]

--UPDATE [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys]
--SET
--	 ROW_PROBLEM = 'N'
--	,ROW_PROBLEM_DATE = NULL
--	,ROW_PROBLEM_REASON = NULL




--INSERT INTO [FHPDataMarts].[dbo].[Member_Utility_SplitMember_ExclusiveKeys] ([MEMBER_MASTER_ROW_ID],[MEMBER_SPLITMEMBER_TEMP_MEMBER_ID],[KEY_VALUE],[KEY_NAME])
--VALUES
-- (53250,1,'451-02-5556','SSN')
--,(53250,1,'451025556A','HICN')
--,(53250,1,'5000169192','MRN')
--,(53250,1,'211689','MHK_INTERNAL_ID')
--,(53250,1,'13-MR-5000169192','MEDHOK_ID')
--,(53250,2,'456-46-6106','SSN')
--,(53250,2,'482602','MRN')
--,(53250,2,'211693','MHK_INTERNAL_ID')
--,(53250,2,'13-MR-482602','MEDHOK_ID')




--SELECT * FROM [FHPDataMarts].[dbo].[Member_Master_Staging]
--ORDER BY ROW_CREATE_DATE DESC