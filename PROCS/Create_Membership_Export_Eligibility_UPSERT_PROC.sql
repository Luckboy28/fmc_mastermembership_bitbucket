
-- Remove from FHPDW if found
USE [FHPDW]
GO


IF OBJECT_ID('dbo.Membership_Export_Eligibility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Eligibility_UPSERT;
GO





USE [FHPDataMarts]
GO


IF OBJECT_ID('dbo.Membership_Export_Eligibility_UPSERT', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Membership_Export_Eligibility_UPSERT;
GO



CREATE PROCEDURE [dbo].[Membership_Export_Eligibility_UPSERT]
	-- No parameters
AS
BEGIN

	-- ==========================================================================================
	-- Author:		David M. Wilson
	-- Create date: March 14th 2019
	-- Description:	
	-- ==========================================================================================
	/*

	*/
	-- ==========================================================================================

	DECLARE @GETDATE DATETIME2 = GETDATE();

	-- INSERT
	INSERT INTO [FHPDW].[dbo].[Membership_Export_Eligibility]
	(
		 [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
		,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,[START_DATE]
		,[TERM_DATE]
		,[TERM_REASON]
		,[STATUS]
		,[LOB_VENDOR]
		,[LOB_TYPE]
		,[HC_LOB]
		,[HC_GROUP]
		,[HC_PROGRAM]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		,[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON]
		,[ROW_DELETED]
		,[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON]
		,[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 TheSource.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
		,TheSource.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
		,TheSource.[START_DATE]
		,TheSource.[TERM_DATE]
		,TheSource.[TERM_REASON]
		,TheSource.[STATUS]
		,TheSource.[LOB_VENDOR]
		,TheSource.[LOB_TYPE]
		,TheSource.[HC_LOB]
		,TheSource.[HC_GROUP]
		,TheSource.[HC_PROGRAM]
		,TheSource.[ROW_SOURCE]
		,TheSource.[ROW_SOURCE_ID]
		,TheSource.[ROW_PROBLEM]
		,TheSource.[ROW_PROBLEM_DATE]
		,TheSource.[ROW_PROBLEM_REASON]
		,TheSource.[ROW_DELETED]
		,TheSource.[ROW_DELETED_DATE]
		,TheSource.[ROW_DELETED_REASON]
		,@GETDATE AS [ROW_CREATE_DATE]
		,@GETDATE AS [ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Eligibility_SOURCE] TheSource
	LEFT OUTER JOIN [FHPDW].[dbo].[Membership_Export_Eligibility] TheTable
	ON TheTable.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] = TheSource.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
	WHERE TheTable.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] IS NULL  -- Only new records



	-- UPDATE
	UPDATE MEM
	SET
		 [START_DATE] = Delta.[START_DATE]
		,[TERM_DATE] = Delta.[TERM_DATE]
		,[TERM_REASON] = Delta.[TERM_REASON]
		,[STATUS] = Delta.[STATUS]
		,[LOB_VENDOR] = Delta.[LOB_VENDOR]
		,[LOB_TYPE] = Delta.[LOB_TYPE]
		,[HC_LOB] = Delta.[HC_LOB]
		,[HC_GROUP] = Delta.[HC_GROUP]
		,[HC_PROGRAM] = Delta.[HC_PROGRAM]
		,[ROW_SOURCE] = Delta.[ROW_SOURCE]
		,[ROW_SOURCE_ID] = Delta.[ROW_SOURCE_ID]
		,[ROW_PROBLEM] = Delta.[ROW_PROBLEM]
		,[ROW_PROBLEM_DATE] = Delta.[ROW_PROBLEM_DATE]
		,[ROW_PROBLEM_REASON] = Delta.[ROW_PROBLEM_REASON]
		,[ROW_DELETED] = Delta.[ROW_DELETED]
		,[ROW_DELETED_DATE] = Delta.[ROW_DELETED_DATE]
		,[ROW_DELETED_REASON] = Delta.[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE] = Delta.[ROW_CREATE_DATE]
		,[ROW_UPDATE_DATE] = @GETDATE
	FROM [FHPDW].[dbo].[Membership_Export_Eligibility] MEM
	JOIN
	(
		SELECT
			 [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,[START_DATE]
			,[TERM_DATE]
			,[TERM_REASON]
			,[STATUS]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[HC_LOB]
			,[HC_GROUP]
			,[HC_PROGRAM]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDataMarts].[dbo].[Membership_Export_Eligibility_SOURCE]
	
		EXCEPT

		SELECT
			 [MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
			,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
			,[START_DATE]
			,[TERM_DATE]
			,[TERM_REASON]
			,[STATUS]
			,[LOB_VENDOR]
			,[LOB_TYPE]
			,[HC_LOB]
			,[HC_GROUP]
			,[HC_PROGRAM]
			,[ROW_SOURCE]
			,[ROW_SOURCE_ID]
			,[ROW_PROBLEM]
			,[ROW_PROBLEM_DATE]
			,[ROW_PROBLEM_REASON]
			,[ROW_DELETED]
			,[ROW_DELETED_DATE]
			,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			,@GETDATE AS [ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[Membership_Export_Eligibility]
	) Delta
	ON MEM.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_ELIGIBILITY_ROW_ID]
	AND MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] = Delta.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]


	-- Return success
	RETURN 0


END
GO





