
USE [FHPDW]
GO

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Membership_Export_PatientEvents', 'U') IS NOT NULL
	DROP TABLE dbo.Membership_Export_PatientEvents;
GO


-- Comments
CREATE TABLE [dbo].[Membership_Export_PatientEvents]
(
	 [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID] INT NOT NULL IDENTITY(1,1)
	,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] INT NOT NULL

	,[EVENT_TYPE] [VARCHAR](255) NULL
	,[EVENT_DESCRIPTION] [VARCHAR](255) NULL
	,[EVENT_START_DATE] [DATE] NULL
	,[EVENT_END_DATE] [DATE] NULL
	,[EVENT_NOTES] [VARCHAR](4000) NULL

	,[ROW_SOURCE] [VARCHAR](500) NOT NULL
	,[ROW_SOURCE_ID] [VARCHAR](50) NULL
	,[ROW_PROBLEM] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_PROBLEM_DATE] [DATETIME2] NULL
	,[ROW_PROBLEM_REASON] [VARCHAR](500) NULL
	,[ROW_DELETED] [VARCHAR](1) NOT NULL DEFAULT 'N'
	,[ROW_DELETED_DATE] [DATETIME2] NULL
	,[ROW_DELETED_REASON] [VARCHAR](500) NULL
	,[ROW_CREATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,[ROW_UPDATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,CONSTRAINT PK_DBO_MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID PRIMARY KEY ([MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID])
	,CONSTRAINT CHK_MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_PROBLEM CHECK ([ROW_PROBLEM] IN ('Y','N'))
	,CONSTRAINT CHK_MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_DELETED CHECK ([ROW_DELETED] IN ('Y','N'))
) ON [PRIMARY]
GO


USE [FHPDW]
GO

GRANT SELECT ON [dbo].[Membership_Export_PatientEvents] TO [FHP_DW_READ_SPECIFIC];
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_PatientEvents] TO [FHP_DW_READ_SPECIFIC];
GO

GRANT SELECT ON [dbo].[Membership_Export_PatientEvents] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_PatientEvents] TO [FHP_DW_READ_SPECIFIC_DATA_TEAM];
GO

GRANT SELECT ON [dbo].[Membership_Export_PatientEvents] TO fkcIT_read_only
GO
GRANT VIEW DEFINITION ON [dbo].[Membership_Export_PatientEvents] TO fkcIT_read_only
GO