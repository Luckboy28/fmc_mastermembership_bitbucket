

USE [FHPDataMarts]
GO

-- Drop the table if it already exists
IF OBJECT_ID('dbo.Member_Master_FDOD', 'U') IS NOT NULL
	DROP TABLE dbo.Member_Master_FDOD;
GO


-- Comments
CREATE TABLE [dbo].[Member_Master_FDOD]
(
	 [ROW_ID] INT NOT NULL IDENTITY(1,1)

	,[MRN] [VARCHAR](50) NOT NULL
	,[FDOCD_DT] [DATE] NULL
	,[PT_FRST_EVER_DIAL_DT] [DATE] NULL
	,[FDOD] [DATE] NULL

	,[ROW_CREATE_DATE] [DATETIME2] NOT NULL DEFAULT GETDATE()
	,CONSTRAINT PK_MEMBER_MASTER_FDOD_CL_DIAL_ROW_ID PRIMARY KEY ([ROW_ID])
) ON [PRIMARY]
GO




