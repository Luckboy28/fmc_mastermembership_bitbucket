/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP 1000 [MEMBERSHIP_EXPORT_PATIENTEVENTS_ROW_ID]
      ,[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
      ,[EVENT_TYPE]
      ,[EVENT_START_DATE]
      ,[EVENT_END_DATE]
      ,[EVENT_NOTES]
      ,[ROW_SOURCE]
      ,[ROW_SOURCE_ID]
      ,[ROW_PROBLEM]
      ,[ROW_PROBLEM_DATE]
      ,[ROW_PROBLEM_REASON]
      ,[ROW_DELETED]
      ,[ROW_DELETED_DATE]
      ,[ROW_DELETED_REASON]
      ,[ROW_CREATE_DATE]
      ,[ROW_UPDATE_DATE]
  FROM [FHPDW].[dbo].[Membership_Export_PatientEvents]



 INSERT INTO [FHPDW].[dbo].[Membership_Export_PatientEvents]
 (
       [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
      ,[EVENT_TYPE]
	  ,[EVENT_DESCRIPTION]
      ,[EVENT_START_DATE]
      ,[EVENT_END_DATE]
      ,[EVENT_NOTES]
      ,[ROW_SOURCE]
      ,[ROW_SOURCE_ID]
)
VALUES
(
       CAST(1 + RAND()*9 AS INT)  --[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
      ,CASE WHEN CAST(1 + RAND()*100 AS INT) > 80 THEN 'HOSPICE' ELSE 'COMORBIDITY' END  --[EVENT_TYPE]
      ,NULL  --[EVENT_DESCRIPTION]
	  ,CAST(CAST(CAST(1 + RAND()*11 AS INT) AS VARCHAR(2)) + '-' + CAST(CAST(1 + RAND()*27 AS INT) AS VARCHAR(2)) + '-' + CAST(CAST(1990 + RAND()*16 AS INT) AS VARCHAR(4)) AS DATE)  --[EVENT_START_DATE]
      ,CASE WHEN CAST(1 + RAND()*2 AS INT) = 1 THEN CAST(CAST(CAST(1 + RAND()*11 AS INT) AS VARCHAR(2)) + '-' + CAST(CAST(1 + RAND()*27 AS INT) AS VARCHAR(2)) + '-' + CAST(CAST(2007 + RAND()*11 AS INT) AS VARCHAR(4)) AS DATE) ELSE NULL END  --[EVENT_END_DATE]
      ,NULL  --[EVENT_NOTES]
      ,'N/A'  --[ROW_SOURCE]
      ,'N/A'  --[ROW_SOURCE_ID]
)




with cte as (
   select *, (ABS(CHECKSUM(NewId())) % 3) + 1 as n
   from contacts
   where city = 'NY'
)
update cte
set firstname = choose(n, 'Bill','Steve','Jack')



;WITH CTE AS
(
	SELECT
		*
		,(ABS(CHECKSUM(NewId())) % 17) + 1 AS N
	FROM [FHPDW].[dbo].[Membership_Export_PatientEvents] PE
	WHERE [EVENT_TYPE] = 'COMORBIDITY'
)
UPDATE CTE
	SET [EVENT_DESCRIPTION] = CHOOSE(n
	,'Arnold-Chiari malformations'
	,'Anxiety'
	,'Autoiommune disease'
	,'Cardiovascular Risks'
	,'Cutaneous Allodynia'
	,'Depression'
	,'Diabetes'
	,'Dystonia'
	,'Fibromyalgia'
	,'Hemicrania continua'
	,'Idiopathic Intracranial Hypertension'
	,'Lupus'
	,'Raynaud’s'
	,'Seizures'
	,'Sjogren’s syndrome'
	,'Thyroid dysfunction'
	,'Trigeminal neuralgia')



SELECT * FROM [FHPDW].[dbo].[Membership_Export_PatientEvents]



[FHPDW].[dbo].[Membership_Export_Eligibility]
[FHPDW].[dbo].[Membership_Export_Labs]
[FHPDW].[dbo].[Membership_Export_Member]
[FHPDW].[dbo].[Membership_Export_PatientEvents]