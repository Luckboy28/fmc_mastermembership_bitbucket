

USE [FHPDev]
GO

IF OBJECT_ID('tempdb..#DISK_USAGE_REPORT') IS NOT NULL
	DROP TABLE #DISK_USAGE_REPORT

CREATE TABLE #DISK_USAGE_REPORT
(
	 [Database Name] VARCHAR(1000)
	,[File Name] VARCHAR(1000)
	,[File Path] VARCHAR(1000)
	,[File Type] VARCHAR(1000)
	,[Space Allocated (MB)] DECIMAL(15,2)
	,[Space Used (MB)] DECIMAL(15,2)
	,[Space Available (%)] NUMERIC(20,2)
	,[file_id] VARCHAR(1000)
	,[Filegroup Name] VARCHAR(1000)
);

EXEC sp_MSforeachdb 'USE [?]

INSERT INTO [FHPDev].#DISK_USAGE_REPORT([Database Name],[File Name],[File Path],[File Type],[Space Allocated (MB)],[Space Used (MB)],[Space Available (%)],[file_id],[Filegroup Name]) 
SELECT
	 db_name() AS [Database Name]
	,DBF.name AS [File Name]
	,DBF.physical_name AS [File Path]
	,DBF.type_desc AS [File Type]
	,CAST((DBF.size/128.0) AS DECIMAL(15,2)) AS [Space Allocated (MB)]
	,CAST(CAST(FILEPROPERTY(DBF.name, ''SpaceUsed'') AS int)/128.0 AS DECIMAL(15,2)) AS [Space Used (MB)]
	,CAST(((CAST(DBF.size/128.0 - CAST(FILEPROPERTY(DBF.name, ''SpaceUsed'') AS int)/128.0 AS DECIMAL(15,2))) / (CAST((DBF.size/128.0) AS DECIMAL(15,2)))) * CAST(''100.00'' AS NUMERIC(20,2)) AS NUMERIC(20,2)) AS [Space Available (%)]
	,[file_id]
	,DS.name AS [Filegroup Name]
FROM sys.database_files AS DBF WITH (NOLOCK) 
LEFT OUTER JOIN sys.data_spaces AS DS WITH (NOLOCK)
ON DBF.data_space_id = DS.data_space_id OPTION (RECOMPILE)'


SELECT * FROM #DISK_USAGE_REPORT



IF OBJECT_ID('tempdb..#DISK_USAGE_TOTALS_REPORT') IS NOT NULL
	DROP TABLE #DISK_USAGE_TOTALS_REPORT

CREATE TABLE #DISK_USAGE_TOTALS_REPORT
(
	 [Description] VARCHAR(1000)
	,[MB] NUMERIC(20,2)
	,[GB] NUMERIC(20,2)
	,[TB] NUMERIC(20,2)
);

INSERT INTO #DISK_USAGE_TOTALS_REPORT([Description],[MB],[GB],[TB])
SELECT
		'Total Space Used'
	,SUM([Space Used (MB)]) AS [Total Space Used (MB)]
	,CAST(SUM([Space Used (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Used (GB)]
	,CAST(SUM([Space Used (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Used (TB)]
FROM #DISK_USAGE_REPORT

INSERT INTO #DISK_USAGE_TOTALS_REPORT([Description],[MB],[GB],[TB])
SELECT
		'Total Space Allocated'
	,SUM([Space Allocated (MB)]) AS [Total Space Allocated (MB)]
	,CAST(SUM([Space Allocated (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Allocated (GB)]
	,CAST(SUM([Space Allocated (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Allocated (TB)]
FROM #DISK_USAGE_REPORT

INSERT INTO #DISK_USAGE_TOTALS_REPORT([Description],[MB],[GB],[TB])
SELECT
		'Total Space Allocated for Data'
	,SUM([Space Allocated (MB)]) AS [Total Space Allocated for Data (MB)]
	,CAST(SUM([Space Allocated (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Data (GB)]
	,CAST(SUM([Space Allocated (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Data (TB)]
FROM #DISK_USAGE_REPORT
WHERE [File Type] = 'ROWS'

INSERT INTO #DISK_USAGE_TOTALS_REPORT([Description],[MB],[GB],[TB])
SELECT
		'Total Space Allocated for Logs'
	,SUM([Space Allocated (MB)]) AS [Total Space Allocated for Logs (MB)]
	,CAST(SUM([Space Allocated (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Logs (GB)]
	,CAST(SUM([Space Allocated (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Logs (TB)]
FROM #DISK_USAGE_REPORT
WHERE [File Type] = 'LOG'


SELECT * FROM #DISK_USAGE_TOTALS_REPORT