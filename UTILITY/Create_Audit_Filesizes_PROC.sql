

USE [FHPDev]
GO



-- Drop the table if it already exists
IF OBJECT_ID('dbo.DISK_USAGE_REPORT', 'U') IS NOT NULL
	DROP TABLE dbo.DISK_USAGE_REPORT;
GO


CREATE TABLE [dbo].[DISK_USAGE_REPORT]
(
	 [Database Name] VARCHAR(1000)
	,[File Name] VARCHAR(1000)
	,[File Path] VARCHAR(1000)
	,[File Type] VARCHAR(1000)
	,[Space Allocated (MB)] DECIMAL(15,2)
	,[Space Used (MB)] DECIMAL(15,2)
	,[Space Available (%)] NUMERIC(20,2)
	,[file_id] VARCHAR(1000)
	,[Filegroup Name] VARCHAR(1000)
	,[ROW_CREATE_DATE] DATE
) ON [PRIMARY]
GO



-- Drop the table if it already exists
IF OBJECT_ID('dbo.DISK_USAGE_REPORT_TOTALS', 'U') IS NOT NULL
	DROP TABLE dbo.DISK_USAGE_REPORT_TOTALS;
GO


CREATE TABLE [dbo].[DISK_USAGE_REPORT_TOTALS]
(
	 [Description] VARCHAR(1000)
	,[MB] NUMERIC(20,2)
	,[GB] NUMERIC(20,2)
	,[TB] NUMERIC(20,2)
	,[ROW_CREATE_DATE] DATE
) ON [PRIMARY]
GO


IF OBJECT_ID('dbo.Audit_Filesizes', 'P') IS NOT NULL
	DROP PROCEDURE dbo.Audit_Filesizes;
GO


CREATE PROCEDURE [dbo].[Audit_Filesizes]
	-- No parameters needed
AS
BEGIN


	EXEC sp_MSforeachdb 'USE [?]

	INSERT INTO [FHPDev].[dbo].[DISK_USAGE_REPORT]([Database Name],[File Name],[File Path],[File Type],[Space Allocated (MB)],[Space Used (MB)],[Space Available (%)],[file_id],[Filegroup Name],[ROW_CREATE_DATE]) 
	SELECT
		 db_name() AS [Database Name]
		,DBF.name AS [File Name]
		,DBF.physical_name AS [File Path]
		,DBF.type_desc AS [File Type]
		,CAST((DBF.size/128.0) AS DECIMAL(15,2)) AS [Space Allocated (MB)]
		,CAST(CAST(FILEPROPERTY(DBF.name, ''SpaceUsed'') AS int)/128.0 AS DECIMAL(15,2)) AS [Space Used (MB)]
		,CAST(((CAST(DBF.size/128.0 - CAST(FILEPROPERTY(DBF.name, ''SpaceUsed'') AS int)/128.0 AS DECIMAL(15,2))) / (CAST((DBF.size/128.0) AS DECIMAL(15,2)))) * CAST(''100.00'' AS NUMERIC(20,2)) AS NUMERIC(20,2)) AS [Space Available (%)]
		,[file_id]
		,DS.name AS [Filegroup Name]
		,CAST(GETDATE() AS DATE) AS [ROW_CREATE_DATE]
	FROM sys.database_files AS DBF WITH (NOLOCK) 
	LEFT OUTER JOIN sys.data_spaces AS DS WITH (NOLOCK)
	ON DBF.data_space_id = DS.data_space_id OPTION (RECOMPILE)'



	INSERT INTO [FHPDev].[dbo].[DISK_USAGE_REPORT_TOTALS]([Description],[MB],[GB],[TB],[ROW_CREATE_DATE])
	SELECT
		 'Total Space Used'
		,SUM([Space Used (MB)]) AS [Total Space Used (MB)]
		,CAST(SUM([Space Used (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Used (GB)]
		,CAST(SUM([Space Used (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Used (TB)]
		,CAST(GETDATE() AS DATE) AS [ROW_CREATE_DATE]
	FROM [FHPDev].[dbo].[DISK_USAGE_REPORT]

	INSERT INTO [FHPDev].[dbo].[DISK_USAGE_REPORT_TOTALS]([Description],[MB],[GB],[TB],[ROW_CREATE_DATE])
	SELECT
		 'Total Space Allocated'
		,SUM([Space Allocated (MB)]) AS [Total Space Allocated (MB)]
		,CAST(SUM([Space Allocated (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Allocated (GB)]
		,CAST(SUM([Space Allocated (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Allocated (TB)]
		,CAST(GETDATE() AS DATE) AS [ROW_CREATE_DATE]
	FROM [FHPDev].[dbo].[DISK_USAGE_REPORT]

	INSERT INTO [FHPDev].[dbo].[DISK_USAGE_REPORT_TOTALS]([Description],[MB],[GB],[TB],[ROW_CREATE_DATE])
	SELECT
		 'Total Space Allocated for Data'
		,SUM([Space Allocated (MB)]) AS [Total Space Allocated for Data (MB)]
		,CAST(SUM([Space Allocated (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Data (GB)]
		,CAST(SUM([Space Allocated (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Data (TB)]
		,CAST(GETDATE() AS DATE) AS [ROW_CREATE_DATE]
	FROM [FHPDev].[dbo].[DISK_USAGE_REPORT]
	WHERE [File Type] = 'ROWS'

	INSERT INTO [FHPDev].[dbo].[DISK_USAGE_REPORT_TOTALS]([Description],[MB],[GB],[TB],[ROW_CREATE_DATE])
	SELECT
		 'Total Space Allocated for Logs'
		,SUM([Space Allocated (MB)]) AS [Total Space Allocated for Logs (MB)]
		,CAST(SUM([Space Allocated (MB)])/1000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Logs (GB)]
		,CAST(SUM([Space Allocated (MB)])/1000000.0 AS NUMERIC(20,2)) AS [Total Space Allocated for Logs (TB)]
		,CAST(GETDATE() AS DATE) AS [ROW_CREATE_DATE]
	FROM [FHPDev].[dbo].[DISK_USAGE_REPORT]
	WHERE [File Type] = 'LOG'


END
GO