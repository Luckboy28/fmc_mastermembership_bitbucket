%include '/export/home/icg/users/kumashan/pwd.txt';
/**********************************************************************************************************************************************************************
                                                       STEP 1 -- MEMBERSHIP
**********************************************************************************************************************************************************************/

/**********************************************************************
 STEP 1a -- 1)All active patients from mastermembership
**********************************************************************/

PROC SQL; 
CONNECT TO ODBC (DSN=VH2SQL01 USER=medspring\&user_sql PW=&PASS_SQL);
CREATE TABLE WORK.MEMBERSHIP AS
SELECT * FROM CONNECTION TO ODBC
	(	
		SELECT DISTINCT  
			M.MRN,
			M.LOB_VENDOR,
			M.LOB_TYPE,
			M.STATUS
		FROM FHPDW.dbo.Member_Master M
			WHERE 1 = 1
				AND M.STATUS = 'ACTIVE'
				AND M.MRN IS NOT NULL 
	)
;
QUIT;

/**********************************************************************************************************************************************************************
                                                       STEP 2 -- FDOD
**********************************************************************************************************************************************************************/

/**********************************************************************
 STEP 2a -- 1)Getting FDOD from the source Carly created and this 
			  will be the source of truth of dialysis.
**********************************************************************/

PROC SQL; 
CONNECT TO ORACLE (user=%superq(user_ora) pw=%superq(pass_ora) path=KCNGX);
CREATE TABLE MEMBERS_FDOD_CL_DIAL AS	
SELECT 
	INPUT(X.MRN,20.) AS MRN,
	DATEPART(X.FDOCD_DT) AS FDOD FORMAT MMDDYY10.
FROM CONNECTION TO ORACLE
	(	
		WITH Patient_Details AS
			(
				SELECT DISTINCT  
					P.MRN,
					P.PT_PHI_ID 
				FROM PERS_DIAL_PHI.PT_PHI P
				WHERE 1 = 1
			)
			SELECT DISTINCT
				P.MRN,
				P.PT_PHI_ID,
				D.FDOCD_DT,
				D.CHIEF_ICD_CAT_CD,
				D.CHIEF_ICD_NM,
				D.CHIEF_ICD_CD
			FROM Patient_Details P 
				LEFT JOIN CL_DIAL.PT_AKI_ESRD_HIST D
					ON P.PT_PHI_ID = D.PT_PHI_ID
				WHERE 1 = 1
					AND D.CHIEF_ICD_CAT_CD = 'ESRD'
	) X
INNER JOIN WORK.MEMBERSHIP R
	ON INPUT(X.MRN,20.) = INPUT(R.MRN,20.) 
;

	DISCONNECT FROM ORACLE;
QUIT;




/**********************************************************************
 STEP 2b -- 1)USING PT_INS_SNAPSHOT TO FIND FDOD FOR THE MEMBERS MISSING 
			  FDOD FROM Carlys Source    
			2)GETTING MAX FDOD BECAUSE SOME MEMBER MIGHT HAVE HAD A KIDNEY 
			   TRANSPLANT AND WHEN THEN THEY COME BACK FOR DIALYSIS.
			3)AT THIS SCENARIO THEY WILL HAVE A NEW FDOD SO WE WANT TO 
			  CAPTURE THE NEW FDOD AFTER TRANSPLANT AND THAT IS WHY WE 
 			  GET THE MAX FDOD
**********************************************************************/

PROC SQL; 
CONNECT TO ORACLE (user=%superq(user_ora) pw=%superq(pass_ora) path=KCNGX);
CREATE TABLE PT_INS_FDOD AS
SELECT DISTINCT
	INPUT(X.PT_MRN,20.) AS MRN,
	MAX(DATEPART(X.PT_FRST_EVER_DIAL_DT)) AS FDOD FORMAT MMDDYY10.  
FROM CONNECTION TO ORACLE
	(	
		SELECT 
			PT_MRN,
			PT_FRST_EVER_DIAL_DT 
		FROM FN_DIAL.PT_INS_SNAPSHOT
			WHERE 1 = 1
	) X

 INNER JOIN WORK.MEMBERSHIP R
     ON INPUT(X.PT_MRN,20.) = INPUT(R.MRN,20.)
 WHERE 1 = 1
 GROUP BY 
     INPUT(X.PT_MRN,20.)
;

	DISCONNECT FROM ORACLE;
QUIT;



/**********************************************************************
 STEP 2c -- 1)MIN TREATMENT IF FDOD IS MISSING IN PT_INS
**********************************************************************/

PROC SQL; 
CONNECT TO ORACLE (user=%superq(user_ora) pw=%superq(pass_ora) path=datw);
CREATE TABLE MEMBERS_FDOD1 AS
SELECT 
	M.*,
	DATEPART(X.FDOD) AS FDOD_TXT FORMAT MMDDYY10. 
FROM CONNECTION TO ORACLE
		(	
			WITH HEMO_DATE AS
			(
			    SELECT
			        PATIENT_ID,
			        MIN(HEMO_POST_DATE) AS Min_TXT_Date 
			    FROM KC.HEMO_TREATMENT_POST
			        GROUP BY
			            PATIENT_ID
			)
			,
			HOME_DATE AS 
			(
			    SELECT 
			        PATIENT_ID,
			        MIN(HOME_TREAT_COUNT_START_DATE) AS Min_TXT_Date
			    FROM KC.HOME_TREATMENT_COUNT
			        GROUP BY 
			            PATIENT_ID
			)
			,
			Combined_TXT_DATE AS 
			(
			    SELECT * FROM  HEMO_DATE
			    UNION ALL
			    SELECT * FROM HOME_DATE
			)
			    SELECT 
			        PATIENT_ID AS MRN,
			        MIN(Min_TXT_Date) AS FDOD
			    FROM Combined_TXT_DATE
			        GROUP BY
			            PATIENT_ID
		) X
INNER JOIN WORK.MEMBERSHIP M 
	ON X.MRN = INPUT(M.MRN,20.)
;
	DISCONNECT FROM ORACLE;
QUIT;



/**********************************************************************
 STEP 2d -- 1)If patient missing FDOD in PT_INS then we use the first
			  treatment date 
**********************************************************************/

PROC SQL;
CREATE TABLE FDOD_PT_TXT AS 
SELECT DISTINCT
	M.MRN,
	CASE WHEN P.FDOD IS NULL THEN F.FDOD_TXT ELSE P.FDOD END AS FDOD_TXT_PT FORMAT MMDDYY10.
FROM WORK.MEMBERSHIP M 
	LEFT JOIN WORK.PT_INS_FDOD P
		ON INPUT(M.MRN,20.) = P.MRN
	LEFT JOIN WORK.MEMBERS_FDOD1 F
		ON INPUT(M.MRN,20.) = INPUT(F.MRN,20.)
	WHERE 1 = 1
;
QUIT;



/**********************************************************************
 STEP 2e -- 1)This is our final FDOD
			2)FDOD with membership
			3)If FDOD is missing from CL_DIAL then we use PT_INS and if
			  still missing we use the first treatment date. 
**********************************************************************/

PROC SQL;
CREATE TABLE FDOD_FINAL AS 
SELECT DISTINCT
	M.MRN,
	CASE WHEN C.FDOD IS NULL THEN F.FDOD_TXT_PT ELSE C.FDOD END AS FDOD FORMAT MMDDYY10.
FROM WORK.MEMBERSHIP M
	LEFT JOIN MEMBERS_FDOD_CL_DIAL C
		ON INPUT(M.MRN,20.) = C.MRN
	LEFT JOIN FDOD_PT_TXT F
		ON INPUT(M.MRN,20.) = INPUT(F.MRN,20.)
	WHERE 1 = 1
;
QUIT;
