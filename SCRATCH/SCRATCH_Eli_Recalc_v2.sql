	---- Terminate members whose eligibilities have lapsed (TERM_DATE < TODAY)
	--UPDATE Eli
	--SET
	--	 Eli.[TERM_DATE] = GETDATE()
	--	,Eli.[TERM_REASON] = 'LOB Expired'
	--	,Eli.[STATUS] = 'INACTIVE'
	--FROM [FHPDW].[dbo].[Member_Eligibility] Eli
	--WHERE Eli.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
	--AND Eli.[TERM_DATE] IS NOT NULL
	--AND Eli.[TERM_DATE] < GETDATE()
	--AND Eli.[STATUS] = 'ACTIVE'
	--AND Eli.[ROW_DELETED] = 'N'   -- Only operate on active data, to avoid confusion


	--;WITH STATUS_ActiveRecords AS
	--(

		---- Expired Members to Terminate
		--SELECT
		--	 Eli.[MEMBER_ELIGIBILITY_ROW_ID]
		--	,Eli.[MEMBER_MASTER_ROW_ID]
		--	,Eli.[START_DATE]
		--	,GETDATE() AS [TERM_DATE]
		--	,'LOB Expired' AS [TERM_REASON]
		--	,'INACTIVE' AS [STATUS]
		--	,2147483647 AS [Active_Rank]  -- Max SQL integer
		--	,1 AS [Type_Rank]
		--FROM [FHPDW].[dbo].[Member_Eligibility] Eli
		--JOIN [FHPDW].[dbo].[Member_Master] Mem
		--	ON MEM.MEMBER_MASTER_ROW_ID = ELI.MEMBER_MASTER_ROW_ID
		--WHERE 1=1
		----AND Eli.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		--AND Eli.[TERM_DATE] IS NOT NULL
		--AND Eli.[TERM_DATE] < GETDATE()
		--AND Eli.[STATUS] = 'ACTIVE'
		--AND Mem.ROW_DELETED = 'N'   -- Member should be valid
		--AND Eli.[ROW_DELETED] = 'N'  -- Eligibility should be valid

		--UNION ALL



		;WITH ActiveMembers_Ranked AS
		(
		SELECT
			 [MEMBER_ELIGIBILITY_ROW_ID]
			,ELI.[MEMBER_MASTER_ROW_ID]
			,ELI.[START_DATE]
			,ELI.[TERM_DATE]
			,ELI.[TERM_REASON]
			,ELI.[STATUS]
			,ROW_NUMBER() OVER (PARTITION BY ELI.[MEMBER_MASTER_ROW_ID] ORDER BY LOBH.[LOB_RANK] ASC, [START_DATE] DESC, ISNULL([TERM_DATE],CAST('9999-12-31' AS DATETIME2)) DESC, ELI.[MEMBER_ELIGIBILITY_ROW_ID] DESC) [Rank]   -- NOTE:  For sorting reasons, NULL is assigned the latest date that SQL supports, because it has higher priority over normal dates.
		FROM [FHPDW].[dbo].[Member_Eligibility] ELI
		JOIN [FHPDataMarts].[dbo].[Member_LOB_Hierarchy] LOBH
			ON ELI.[LOB_TYPE] = LOBH.[LOB_TYPE]
			AND ELI.[LOB_VENDOR] = LOBH.[LOB_VENDOR]
		JOIN [FHPDW].[dbo].[Member_Master] MEM
			ON MEM.MEMBER_MASTER_ROW_ID = ELI.MEMBER_MASTER_ROW_ID
		WHERE 1=1
		AND ELI.[MEMBER_MASTER_ROW_ID] = @MEMBER_MASTER_ROW_ID
		AND [START_DATE] <= GETDATE() AND ([TERM_DATE] IS NULL OR [TERM_DATE] >= GETDATE())
		AND Mem.ROW_DELETED = 'N'   -- Member should be valid
		AND Eli.[ROW_DELETED] = 'N'  -- Eligibility should be valid
		)
		,HighestRankedActive AS
		(
		SELECT
			 [MEMBER_ELIGIBILITY_ROW_ID]
			,[MEMBER_MASTER_ROW_ID]
			,[START_DATE]
			,[TERM_DATE]
			,[TERM_REASON]
			,[STATUS]
			,[Rank]
		FROM ActiveMembers_Ranked AMR
		WHERE AMR.[Rank] = 1
		)
		,FINAL_DATA AS
		(
		SELECT
			 AMR.[MEMBER_ELIGIBILITY_ROW_ID]
			,AMR.[MEMBER_MASTER_ROW_ID]
			,AMR.[START_DATE]
			,CASE
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] <> AMR.[MEMBER_ELIGIBILITY_ROW_ID] AND DATEADD(day,-1,HRA.[START_DATE]) < AMR.[START_DATE] -- If not the highest ranked active eligibility   -- Prevent the new TERM_DATE from being less than it's own START_DATE
					THEN AMR.[START_DATE]
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] <> AMR.[MEMBER_ELIGIBILITY_ROW_ID] AND DATEADD(day,-1,HRA.[START_DATE]) >= AMR.[START_DATE]
					THEN DATEADD(day,-1,HRA.[START_DATE])
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] = AMR.[MEMBER_ELIGIBILITY_ROW_ID] 
					THEN HRA.[TERM_DATE]  -- This could be a future date, and that would be valid.  Does not need to be null.
			 END AS [TERM_DATE]

			,CASE
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] <> AMR.[MEMBER_ELIGIBILITY_ROW_ID]
					THEN 'Replaced by another LOB'
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] = AMR.[MEMBER_ELIGIBILITY_ROW_ID]
					THEN NULL
			 END AS [TERM_REASON]

			,CASE
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] <> AMR.[MEMBER_ELIGIBILITY_ROW_ID]
					THEN 'INACTIVE'
				WHEN AMR.[MEMBER_ELIGIBILITY_ROW_ID] = AMR.[MEMBER_ELIGIBILITY_ROW_ID]
					THEN 'ACTIVE'
			 END AS [STATUS]

			,HRA.[Rank]
		FROM ActiveMembers_Ranked AMR
		JOIN HighestRankedActive HRA
			ON AMR.MEMBER_MASTER_ROW_ID = HRA.MEMBER_MASTER_ROW_ID  --Expose the winning record to every record, including the losers, so that date caluclations can be done
		)
		SELECT * FROM FINAL_DATA



		--SELECT DISTINCT [TERM_REASON] FROM [FHPDW].[dbo].[Member_Eligibility]


