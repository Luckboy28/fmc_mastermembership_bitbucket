




--SELECT * INTO #TestData
--FROM OPENQUERY([KCNGX_PROVIDERS],
--'

--SELECT * FROM ORG_DIAL.FAC_DIAL

	
--')


--SELECT * FROM #TestData


--SELECT DISTINCT
--	 TD.[LOC_PHYS_ADDR_LN1]
--	,H.[PCP_ADDR_LINE1]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_ADDR_LN1],H.[PCP_ADDR_LINE1],0,0.85,0) [ADDR_SIM]
--	,TD.[LOC_PHYS_CITY_NM]
--	,H.[PCP_CITY_NAME]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_CITY_NM],H.[PCP_CITY_NAME],0,0.85,0) [CITY_SIM]
--	,TD.[LOC_PHYS_STATE_CD]
--	,H.[PCP_STATE_CD]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_STATE_CD],H.[PCP_STATE_CD],0,0.85,0) [STATE_SIM]
--	,TD.[LOC_PHYS_POSTAL_CD]
--	,H.[PCP_ZIP_CD]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_POSTAL_CD],H.[PCP_ZIP_CD],0,0.85,0) [ZIP_SIM]
--FROM #TestData TD
--JOIN [FHPDataMarts].[dbo].[Humana_CKD_Member] H
--ON 1=1
--WHERE [mds].[mdq].[Similarity](TD.[LOC_PHYS_ADDR_LN1],H.[PCP_ADDR_LINE1],0,0.85,0) > 0.7
--AND [mds].[mdq].[Similarity](TD.[LOC_PHYS_CITY_NM],H.[PCP_CITY_NAME],0,0.85,0) > 0.7
--AND [mds].[mdq].[Similarity](TD.[LOC_PHYS_STATE_CD],H.[PCP_STATE_CD],0,0.85,0) > 0.7
--AND [mds].[mdq].[Similarity](TD.[LOC_PHYS_POSTAL_CD],H.[PCP_ZIP_CD],0,0.85,0) > 0.7
--ORDER BY
--[mds].[mdq].[Similarity](TD.[LOC_PHYS_ADDR_LN1],H.[PCP_ADDR_LINE1],0,0.85,0) DESC
--,[mds].[mdq].[Similarity](TD.[LOC_PHYS_CITY_NM],H.[PCP_CITY_NAME],0,0.85,0) DESC
--,[mds].[mdq].[Similarity](TD.[LOC_PHYS_STATE_CD],H.[PCP_STATE_CD],0,0.85,0) DESC
--,[mds].[mdq].[Similarity](TD.[LOC_PHYS_POSTAL_CD],H.[PCP_ZIP_CD],0,0.85,0) DESC








--SELECT DISTINCT
--	 [HCP_NPI] AS [NPI]
--	,[FAC_ID] AS [FACILITY_ID]
--	,[HCP_DL_ROLE_NM] AS [ROLE]
--	,CAST([MBR_EFF_DT] AS DATE) AS [START_DATE]
--	,CASE WHEN CAST([MBR_EFF_DT] AS DATE) = CAST('9999-12-31' AS DATE) THEN NULL ELSE CAST([MBR_EFF_DT] AS DATE) END AS [END_DATE]
--FROM OPENQUERY([KCNGX_PROVIDERS],
--'

--SELECT
--	 HCP.HCP_NPI
--	--,HCP.HCP_FRST_NM
--	--,HCP.HCP_LAST_NM
--	,FD.FAC_ID
--	,FM.HCP_DL_ROLE_NM
--	,FM.MBR_EFF_DT
--	,FM.MBR_EXP_DT
--FROM PERS_DIAL.FAC_MEMBER FM
--JOIN PERS_DIAL.HCP HCP
--	ON FM.HCP_ID = HCP.HCP_ID
--JOIN ORG_DIAL.FAC_DIAL FD
--	ON FM.FAC_DIAL_ID = FD.FAC_DIAL_ID
--WHERE HCP.HCP_STS_CD = ''ACTV''
----AND FM.MBR_EXP_DT = ''9999-12-31'' /*Remove this if you want to see the history of where HCPs were affiliated*/
--AND HCP.HCP_NPI IS NOT NULL
--AND FD.FAC_ID IS NOT NULL
--ORDER BY
--	 HCP.HCP_ID
--	,FD.FAC_ID
--	,FM.MBR_EFF_DT
--	,FM.MBR_EXP_DT
	
--')









--SELECT DISTINCT
--	 TD.[LOC_PHYS_ADDR_LN1]
--	,H.[PCP_ADDR_LINE1]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_ADDR_LN1],H.[PCP_ADDR_LINE1],0,0.85,0) [ADDR_SIM]
--	,TD.[LOC_PHYS_CITY_NM]
--	,H.[PCP_CITY_NAME]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_CITY_NM],H.[PCP_CITY_NAME],0,0.85,0) [CITY_SIM]
--	,TD.[LOC_PHYS_STATE_CD]
--	,H.[PCP_STATE_CD]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_STATE_CD],H.[PCP_STATE_CD],0,0.85,0) [STATE_SIM]
--	,TD.[LOC_PHYS_POSTAL_CD]
--	,H.[PCP_ZIP_CD]
--	,[mds].[mdq].[Similarity](TD.[LOC_PHYS_POSTAL_CD],H.[PCP_ZIP_CD],0,0.85,0) [ZIP_SIM]
--FROM #TestData TD
--JOIN [FHPDataMarts].[dbo].[Humana_CKD_Member] H
--ON 1=1
--WHERE [mds].[mdq].[Similarity](TD.[LOC_PHYS_ADDR_LN1],H.[PCP_ADDR_LINE1],0,0.85,0) > 0.7
--AND [mds].[mdq].[Similarity](TD.[LOC_PHYS_CITY_NM],H.[PCP_CITY_NAME],0,0.85,0) > 0.7
--AND [mds].[mdq].[Similarity](TD.[LOC_PHYS_STATE_CD],H.[PCP_STATE_CD],0,0.85,0) > 0.7
--AND [mds].[mdq].[Similarity](TD.[LOC_PHYS_POSTAL_CD],H.[PCP_ZIP_CD],0,0.85,0) > 0.7
--ORDER BY
--[mds].[mdq].[Similarity](TD.[LOC_PHYS_ADDR_LN1],H.[PCP_ADDR_LINE1],0,0.85,0) DESC
--,[mds].[mdq].[Similarity](TD.[LOC_PHYS_CITY_NM],H.[PCP_CITY_NAME],0,0.85,0) DESC
--,[mds].[mdq].[Similarity](TD.[LOC_PHYS_STATE_CD],H.[PCP_STATE_CD],0,0.85,0) DESC
--,[mds].[mdq].[Similarity](TD.[LOC_PHYS_POSTAL_CD],H.[PCP_ZIP_CD],0,0.85,0) DESC



--SELECT * FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] 



--SELECT
--	 [FIRST_NAME]
--	,[LAST_NAME]
--	,[DATE_OF_BIRTH]
--	,[SEX_CD]
--	,[ADDRESS_LINE1]
--	,[ADDRESS_LINE2]
--	,[CITY]
--	,[STATE_CD]
--	,[ZIP_CD]
--	,[HOME_PHONE_NBR]
--FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]
--JOIN 



WITH MM AS
(
	SELECT DISTINCT
		 MM.[MEMBER_MASTER_ROW_ID]
		,MM.[FIRST_NAME]
		,MM.[LAST_NAME]
		,MM.[DATE_OF_BIRTH]
		,MM.[GENDER]
		,AH.[ADDRESS_1]
		,AH.[ADDRESS_2]
		,AH.[CITY]
		,AH.[STATE]
		,AH.[ZIP]
		,AH.[PHONE]
	FROM [FHPDW].[dbo].[Member_Master] MM
	JOIN [FHPDataMarts].[dbo].[Member_Address_Historical] AH
	ON MM.[MEMBER_MASTER_ROW_ID] = AH.[MEMBER_MASTER_ROW_ID]
	WHERE AH.ADDRESS_TYPE = 'PERMANENT'
	AND MM.[ROW_DELETED] = 'N'
	AND AH.[ROW_DELETED] = 'N'
)
,CKD AS
(
	SELECT DISTINCT
		 [MBR_ID]
		,[FIRST_NAME]
		,[LAST_NAME]
		,[DATE_OF_BIRTH]
		,[SEX_CD] [GENDER]
		,[ADDRESS_LINE1] [ADDRESS_1]
		,[ADDRESS_LINE2] [ADDRESS_2]
		,[CITY]
		,[STATE_CD] [STATE]
		,[ZIP_CD] [ZIP]
		,[HOME_PHONE_NBR] [PHONE]
	FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]
	WHERE [ROW_SOURCE] = 'Fresenius Referral File 202001.csv'
)
SELECT
	 CAST((ISNULL([mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](CAST(MM.[DATE_OF_BIRTH] AS VARCHAR(10)),CAST(CKD.[DATE_OF_BIRTH] AS VARCHAR(10)),0,0.85,0),CAST('0.0' AS FLOAT))) AS FLOAT) [PROB_TOTAL]


	,[mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0) [PROB_FIRST_NAME]
	,[mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0) [PROB_LAST_NAME]
	,[mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0) [PROB_ADDRESS_1]
	,[mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0) [PROB_ADDRESS_2]
	,[mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0) [PROB_GENDER]
	,[mds].[mdq].[Similarity](CAST(MM.[DATE_OF_BIRTH] AS VARCHAR(10)),CAST(CKD.[DATE_OF_BIRTH] AS VARCHAR(10)),0,0.85,0) [PROB_DATE_OF_BIRTH]
	,MM.[MEMBER_MASTER_ROW_ID]
	,CKD.[MBR_ID]
	,1 [ORDER]
	,MM.[FIRST_NAME]
	,MM.[LAST_NAME]
	,MM.[DATE_OF_BIRTH]
	,MM.[GENDER]
	,MM.[ADDRESS_1]
	,MM.[ADDRESS_2]
	,MM.[CITY]
	,MM.[STATE]
	,MM.[ZIP]
	,MM.[PHONE]
	--,CKD.[FIRST_NAME]
	--,CKD.[LAST_NAME]
	--,CKD.[DATE_OF_BIRTH]
	--,CKD.[GENDER]
	--,CKD.[ADDRESS_1]
	--,CKD.[ADDRESS_2]
	--,CKD.[CITY]
	--,CKD.[STATE]
	--,CKD.[ZIP]
	--,CKD.[PHONE]
FROM CKD
JOIN MM
--ON ISNULL(MM.[FIRST_NAME],'<NULL>') = ISNULL(CKD.[FIRST_NAME],'<NULL>')
--AND ISNULL(MM.[LAST_NAME],'<NULL>') = ISNULL(CKD.[LAST_NAME],'<NULL>')
--AND ISNULL(MM.[DATE_OF_BIRTH],CAST('9999-12-31' AS DATE)) = ISNULL(CKD.[DATE_OF_BIRTH],CAST('9999-12-31' AS DATE))
--AND ISNULL(MM.[GENDER],'<NULL>') = ISNULL(CKD.[GENDER],'<NULL>')
--AND ISNULL(MM.[ADDRESS_1],'<NULL>') = ISNULL(CKD.[ADDRESS_1],'<NULL>')
--AND ISNULL(MM.[ADDRESS_2],'<NULL>') = ISNULL(CKD.[ADDRESS_2],'<NULL>')
ON ISNULL(MM.[CITY],'<NULL>') = ISNULL(CKD.[CITY],'<NULL>')
--AND ISNULL(MM.[STATE],'<NULL>') = ISNULL(CKD.[STATE],'<NULL>')
AND ISNULL(LEFT(MM.[ZIP],5),'<NULL>') = ISNULL(LEFT(CKD.[ZIP],5),'<NULL>')
--AND ISNULL(MM.[PHONE],'<NULL>') = ISNULL(CKD.[PHONE],'<NULL>')
--ORDER BY 
--	 ISNULL([mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
--	 ISNULL([mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
--	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0),CAST('0.0' AS FLOAT)) +
--	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0),CAST('0.0' AS FLOAT)) +
--	 ISNULL([mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0),CAST('0.0' AS FLOAT)) DESC
WHERE CAST((ISNULL([mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](CAST(MM.[DATE_OF_BIRTH] AS VARCHAR(10)),CAST(CKD.[DATE_OF_BIRTH] AS VARCHAR(10)),0,0.85,0),CAST('0.0' AS FLOAT))) AS FLOAT) > CAST('3.0' AS FLOAT)


UNION ALL


SELECT
	 CAST((ISNULL([mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](CAST(MM.[DATE_OF_BIRTH] AS VARCHAR(10)),CAST(CKD.[DATE_OF_BIRTH] AS VARCHAR(10)),0,0.85,0),CAST('0.0' AS FLOAT))) AS FLOAT) [PROB_TOTAL]


	,[mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0) [PROB_FIRST_NAME]
	,[mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0) [PROB_LAST_NAME]
	,[mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0) [PROB_ADDRESS_1]
	,[mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0) [PROB_ADDRESS_2]
	,[mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0) [PROB_GENDER]
	,[mds].[mdq].[Similarity](CAST(MM.[DATE_OF_BIRTH] AS VARCHAR(10)),CAST(CKD.[DATE_OF_BIRTH] AS VARCHAR(10)),0,0.85,0) [PROB_DATE_OF_BIRTH]
	,MM.[MEMBER_MASTER_ROW_ID]
	,CKD.[MBR_ID]
	,2 [ORDER]
	--,MM.[FIRST_NAME]
	--,MM.[LAST_NAME]
	--,MM.[DATE_OF_BIRTH]
	--,MM.[GENDER]
	--,MM.[ADDRESS_1]
	--,MM.[ADDRESS_2]
	--,MM.[CITY]
	--,MM.[STATE]
	--,MM.[ZIP]
	--,MM.[PHONE]
	,CKD.[FIRST_NAME]
	,CKD.[LAST_NAME]
	,CKD.[DATE_OF_BIRTH]
	,CKD.[GENDER]
	,CKD.[ADDRESS_1]
	,CKD.[ADDRESS_2]
	,CKD.[CITY]
	,CKD.[STATE]
	,CKD.[ZIP]
	,CKD.[PHONE]
FROM CKD
JOIN MM
--ON ISNULL(MM.[FIRST_NAME],'<NULL>') = ISNULL(CKD.[FIRST_NAME],'<NULL>')
--AND ISNULL(MM.[LAST_NAME],'<NULL>') = ISNULL(CKD.[LAST_NAME],'<NULL>')
--AND ISNULL(MM.[DATE_OF_BIRTH],CAST('9999-12-31' AS DATE)) = ISNULL(CKD.[DATE_OF_BIRTH],CAST('9999-12-31' AS DATE))
--AND ISNULL(MM.[GENDER],'<NULL>') = ISNULL(CKD.[GENDER],'<NULL>')
--AND ISNULL(MM.[ADDRESS_1],'<NULL>') = ISNULL(CKD.[ADDRESS_1],'<NULL>')
--AND ISNULL(MM.[ADDRESS_2],'<NULL>') = ISNULL(CKD.[ADDRESS_2],'<NULL>')
ON ISNULL(MM.[CITY],'<NULL>') = ISNULL(CKD.[CITY],'<NULL>')
--AND ISNULL(MM.[STATE],'<NULL>') = ISNULL(CKD.[STATE],'<NULL>')
AND ISNULL(LEFT(MM.[ZIP],5),'<NULL>') = ISNULL(LEFT(CKD.[ZIP],5),'<NULL>')
--AND ISNULL(MM.[PHONE],'<NULL>') = ISNULL(CKD.[PHONE],'<NULL>')
--ORDER BY 8 DESC, 1 DESC
WHERE CAST((ISNULL([mds].[mdq].[Similarity](MM.[FIRST_NAME],CKD.[FIRST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[LAST_NAME],CKD.[LAST_NAME],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_1],CKD.[ADDRESS_1],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[ADDRESS_2],CKD.[ADDRESS_2],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](MM.[GENDER],CKD.[GENDER],0,0.85,0),CAST('0.0' AS FLOAT)) +
	 ISNULL([mds].[mdq].[Similarity](CAST(MM.[DATE_OF_BIRTH] AS VARCHAR(10)),CAST(CKD.[DATE_OF_BIRTH] AS VARCHAR(10)),0,0.85,0),CAST('0.0' AS FLOAT))) AS FLOAT) > CAST('3.0' AS FLOAT)
ORDER BY 8 DESC, 9 DESC, 10 ASC, 1 DESC



--SELECT * FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]
--WHERE [MBR_ID] = 'H5745712200'

--SELECT * FROM [FHPDW].[dbo].[Member_Master]
--WHERE [MEMBER_MASTER_ROW_ID] = 127984