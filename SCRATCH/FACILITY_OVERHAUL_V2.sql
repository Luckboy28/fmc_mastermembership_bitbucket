
	WITH KCNGX AS
	(

		SELECT DISTINCT * FROM OPENQUERY([KCNGX_PROVIDERS],
				'

				SELECT
					HCP.HCP_NPI
					--,HCP.HCP_FRST_NM
					--,HCP.HCP_LAST_NM
					--,FD.FAC_ID
					--,FM.HCP_DL_ROLE_NM
					--,FM.MBR_EFF_DT
					--,FM.MBR_EXP_DT
					,FD.*
				FROM PERS_DIAL.FAC_MEMBER FM
				JOIN PERS_DIAL.HCP HCP
					ON FM.HCP_ID = HCP.HCP_ID
				JOIN ORG_DIAL.FAC_DIAL FD
					ON FM.FAC_DIAL_ID = FD.FAC_DIAL_ID
				WHERE HCP.HCP_STS_CD = ''ACTV''
				AND FM.MBR_EXP_DT = ''9999-12-31'' /*Remove this if you want to see the history of where HCPs were affiliated*/
				AND HCP.HCP_NPI IS NOT NULL
				AND FD.FAC_ID IS NOT NULL
				ORDER BY
					 HCP.HCP_ID
					,FD.FAC_ID
					,FM.MBR_EFF_DT
					,FM.MBR_EXP_DT
	
				')
	)
	,RELAVENT_NPI AS
	(

		SELECT
			HCKDM.[PCP_NPI_ID] [NPI]
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		WHERE HCKDM.[ROW_DELETED] = 'N'
		AND HCKDM.[PCP_NPI_ID] IS NOT NULL
		AND HCKDM.[PCP_NPI_ID] <> 0
		AND HCKDM.[PCP_NPI_ID] <> ''

		UNION ALL

		SELECT
			HCKDM.[Neph_NPI_ID] [NPI]
		FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
		WHERE HCKDM.[ROW_DELETED] = 'N'
		AND HCKDM.[Neph_NPI_ID] IS NOT NULL
		AND HCKDM.[Neph_NPI_ID] <> 0
		AND HCKDM.[Neph_NPI_ID] <> ''
	)
	,DISTINCT_NPI AS
	(
		SELECT DISTINCT
			[NPI]
		FROM RELAVENT_NPI
	)
	,FULL_DATA AS
	(
		-- ==================================================================================================================================
		--
		--                                                         KCNGX Data
		--
		-- ==================================================================================================================================


		-- ADDRESS_TYPE: Physical
		-- FACILITY_TYPE: FKC Clinic
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'FKC Clinic' [FACILITY_TYPE]   --USED LATER
			,KCNGX.[FAC_OTHER_LOCAL_NM] [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_ADDR_LN1])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_ADDR_LN2])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_CITY_NM])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_STATE_CD])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(CASE WHEN LEN(LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD]))) > 0 AND LEN(LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD4]))) > 0 THEN CONCAT(LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD])),'-',LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD4]))) ELSE LTRIM(RTRIM(KCNGX.[LOC_PHYS_POSTAL_CD])) END)),'') [ZIP]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_COUNTY_NM])),'') [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_TEL_NBR],'-',''))),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_FAX_NBR],'-',''))),'') [FAX]
			,NULL [EMAIL]
			,NULLIF(LTRIM(RTRIM(KCNGX.[FAC_ID])),'') [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'KCNGX.[HCP_NPI]' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM KCNGX
		JOIN RELAVENT_NPI
			ON KCNGX.[HCP_NPI] = RELAVENT_NPI.[NPI]
		--LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
		--	ON KCNGX.[HCP_NPI] = NPIData.[NPI]
		WHERE KCNGX.[HCP_NPI] IS NOT NULL
		AND NULLIF(LTRIM(RTRIM(KCNGX.[LOC_PHYS_ADDR_LN1])),'') IS NOT NULL

		UNION ALL

		-- ADDRESS_TYPE: Mailing
		-- FACILITY_TYPE: FKC Clinic
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [NPI] --Used for reference later
			--[MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'FKC Clinic' [FACILITY_TYPE]   --USED LATER
			,KCNGX.[FAC_OTHER_LOCAL_NM] [FACILITY_NAME]
			,'MAILING' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_ADDR_LN1])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_ADDR_LN2])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_CITY_NM])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_STATE_CD])),'') [STATE]
			,NULLIF(LTRIM(RTRIM(CASE WHEN LEN(LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD]))) > 0 AND LEN(LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD4]))) > 0 THEN CONCAT(LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD])),'-',LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD4]))) ELSE LTRIM(RTRIM(KCNGX.[LOC_MAIL_POSTAL_CD])) END)),'') [ZIP]
			,NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_COUNTY_NM])),'') [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_TEL_NBR],'-',''))),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(REPLACE(KCNGX.[LOC_FAX_NBR],'-',''))),'') [FAX]
			,NULL [EMAIL]
			,NULLIF(LTRIM(RTRIM(KCNGX.[FAC_ID])),'') [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'KCNGX.[HCP_NPI]' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(KCNGX.[HCP_NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM KCNGX
		JOIN RELAVENT_NPI
			ON KCNGX.[HCP_NPI] = RELAVENT_NPI.[NPI]
		--LEFT JOIN [FHPDW].[dbo].[NPIData] NPIData
		--	ON KCNGX.[HCP_NPI] = NPIData.[NPI]
		WHERE KCNGX.[HCP_NPI] IS NOT NULL
		AND NULLIF(LTRIM(RTRIM(KCNGX.[LOC_MAIL_ADDR_LN1])),'') IS NOT NULL


		-- ==================================================================================================================================
		--
		--                                                         NPI TABLE DATA
		--
		-- ==================================================================================================================================


		UNION ALL

		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Physical
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'PROVIDER' [FACILITY_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Organization Name])),'') [FACILITY_NAME]
			,'PHYSICAL' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Practice Location Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Practice Location Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address State Name])),'') [STATE]
			,CASE WHEN LEN(NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),'')) = 9 THEN CONCAT(LEFT(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),5),'-',RIGHT(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),4)) ELSE NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Postal Code])),'') END [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Practice Location Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[NPIData] NPIData
		JOIN RELAVENT_NPI
			ON NPIData.[NPI] = RELAVENT_NPI.[NPI]
		WHERE NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Practice Location Address])),'') IS NOT NULL


		UNION ALL


		-- ADDRESS_SOURCE: NPIData
		-- ADDRESS_TYPE: Mailing
		SELECT DISTINCT
			 NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [NPI] --Used for reference later
			-- [MEMBERSHIP_EXPORT_FACILITY_ROW_ID]
			,'PROVIDER' [FACILITY_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Organization Name])),'') [FACILITY_NAME]
			,'MAILING' [ADDRESS_TYPE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Mailing Address])),'') [ADDRESS_1]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Second Line Business Mailing Address])),'') [ADDRESS_2]
			,NULL [ADDRESS_3]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address City Name])),'') [CITY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address State Name])),'') [STATE]
			,CASE WHEN LEN(NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),'')) = 9 THEN CONCAT(LEFT(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),5),'-',RIGHT(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),4)) ELSE NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Postal Code])),'') END [ZIP]
			,NULL [COUNTY]
			,'USA' [COUNTRY]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Telephone Number])),'') [PHONE]
			,NULL [ALTERNATE_PHONE]
			,NULL [EVENING_PHONE]
			,NULL [EMERGENCY_PHONE]
			,NULLIF(LTRIM(RTRIM(NPIData.[Provider Business Mailing Address Fax Number])),'') [FAX]
			,NULL [EMAIL]
			,NULL [INTERNAL_FACILITY_ID]
			,NULL [EXT_ID]
			,NULL [EXT_ID_TYPE]
			,'ACTIVE' [STATUS]
			,'NPIData' [ROW_SOURCE]
			,NULLIF(LTRIM(RTRIM(NPIData.[NPI])),'') [ROW_SOURCE_ID]
			--,[ROW_PROBLEM]
			--,[ROW_PROBLEM_DATE]
			--,[ROW_PROBLEM_REASON]
			--,[ROW_DELETED]
			--,[ROW_DELETED_DATE]
			--,[ROW_DELETED_REASON]
			--,[ROW_CREATE_DATE]
			--,[ROW_UPDATE_DATE]
		FROM [FHPDW].[dbo].[NPIData] NPIData
		JOIN RELAVENT_NPI
		ON NPIData.[NPI] = RELAVENT_NPI.[NPI]
		WHERE NULLIF(LTRIM(RTRIM(NPIData.[Provider First Line Business Mailing Address])),'') IS NOT NULL
	)
	INSERT INTO [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging]
	(
		 [NPI]
		,[FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,[ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	)
	SELECT DISTINCT
		 [NPI]
		,[FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM FULL_DATA

	EXCEPT 

	SELECT
		 [NPI]
		,[FACILITY_TYPE]
		,[FACILITY_NAME]
		,[ADDRESS_TYPE]
		,[ADDRESS_1]
		,[ADDRESS_2]
		,[ADDRESS_3]
		,[CITY]
		,[STATE]
		,[ZIP]
		,[COUNTY]
		,[COUNTRY]
		,[PHONE]
		,[ALTERNATE_PHONE]
		,[EVENING_PHONE]
		,[EMERGENCY_PHONE]
		,[FAX]
		,[EMAIL]
		,[INTERNAL_FACILITY_ID]
		,[EXT_ID]
		,[EXT_ID_TYPE]
		,[STATUS]
		,[ROW_SOURCE]
		,NULL [ROW_SOURCE_ID]
		--,[ROW_PROBLEM]
		--,[ROW_PROBLEM_DATE]
		--,[ROW_PROBLEM_REASON]
		--,[ROW_DELETED]
		--,[ROW_DELETED_DATE]
		--,[ROW_DELETED_REASON]
		--,[ROW_CREATE_DATE]
		--,[ROW_UPDATE_DATE]
	FROM [FHPDataMarts].[dbo].[Membership_Export_Facility_Staging]







------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--,CASE WHEN NPIData.[Entity Type Code] = 1 THEN 'PROVIDER' WHEN NPIData.[Entity Type Code] = 2 THEN 'ORGANIZATION' ELSE 'UNKNOWN' END AS [PROVIDER_TYPE]   --PROVIDER TYPE (NEW FIELD)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
