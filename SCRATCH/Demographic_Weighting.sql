



/*

--- Notes / Thought Processes ---

The idea here is to weight biographical information based on uniqueness and the odds of it changing over time.

For example, somebody's birthday should never change, but somebody's last name might change if they get married, so birthday is weighted more heavily.

In this arbtitrary weighting scheme, 1.0 represents the baseline.  Anything greater is considered more reliable, and anything lower is considered less reliable.

Weighting is only calculated in cases where both fields are NOT NULL.  If one of the values is missing, then the two are not weighted.  However, this will negatively effect the "coverage" factor.

*/

SELECT 
	 [MEMBER_MASTER_ROW_ID]
	,[MHK_INTERNAL_ID]
	,[MEDHOK_ID]
	,[SSN]
	,[HICN]
	,[CLAIM_SUBSCRIBER_ID]
	,[MBI]
	,[MEDICAID_NO]
	,[MRN]
	,[EXT_ID]
	,[EXT_ID_TYPE]
	,[EXT_ID_2]
	,[EXT_ID_TYPE_2]
	,[EXT_ID_3]
	,[EXT_ID_TYPE_3]

	,[FIRST_NAME] -- 1.0
	,[MIDDLE_NAME] -- 1.0
	,[LAST_NAME] --Men 1.0, Women 0.6.    >> Rough napkin math to decide the odds of a women's last name changing:  ~50% get married, 80% take husbands last name = 40% change name = 60% confidence that names has stayed the same
	,[PREFIX] 
	,[SUFFIX]
	,[DATE_OF_BIRTH] --2.0
	,[GENDER] --2.0
	,[RACE] --0.5
	,[ETHNICITY] --0.5
FROM [FHPDW].[dbo].[Member_Master]

-- Total weight: 70%




SELECT 

	 [ADDRESS_TYPE]
	,[ADDRESS_1] -- 3.0
	,[ADDRESS_2] -- 0.5
	,[ADDRESS_3] -- 0.5

	,[CITY] -- 1.0
	,[STATE] -- 1.0
	,[ZIP] -- 2.0

	,[PHONE] -- 4.0  (Should have almost exact match / Low Error Threshold)
	,[ALTERNATE_PHONE] -- 4.0  (Should have almost exact match / Low Error Threshold)
	,[FAX] -- 1.0
	,[EMAIL] -- 4.0  (Should have almost exact match / Low Error Threshold)

FROM [FHPDW].[dbo].[Member_Address]
WHERE [PHONE] IS NOT NULL




SELECT * FROM [FHPDataMarts].[dbo].[Member_Address_Staging] MAS
JOIN 
WHERE MAS.[ADDRESS_TYPE] = 'PERMANENT'


SELECT MEMBER_MASTER_ROW_ID, COUNT(1) FROM [FHPDataMarts].[dbo].[Member_Address_Historical] MAH
GROUP BY MEMBER_MASTER_ROW_ID
ORDER BY 2 DESC



SELECT * FROM [FHPDataMarts].[dbo].[Member_Address_Historical] MAH
WHERE MEMBER_MASTER_ROW_ID = 86467


-- Total weight: 30%

-- Missing data doesn't effect the total percent.


--[mds].[mdq].[Similarity](ST1.[STRING],ST2.[STRING],0,0.85,0.0) [Similarity]






--SELECT * FROM [FHPDW].[dbo].[Member_Master] MM
--LEFT OUTER JOIN [FHPDW].[dbo].[Member_Address] MA
--ON MM.MEMBER_MASTER_ROW_ID = MA.MEMBER_MASTER_ROW_ID
--WHERE MA.[ADDRESS_TYPE] = 'PERMANENT'
--AND MM.[STATUS] = 'ACTIVE'