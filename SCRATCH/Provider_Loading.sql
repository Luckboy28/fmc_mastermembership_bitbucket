


--TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Provider]


--;WITH Source_Ranked AS
--(

--	SELECT DISTINCT

--		 [PCP_NPI_ID]  [NPI]
--		,'Primary Care Physician' [TYPE]
--		,[PCP_FIRST_NAME] [FIRST_NAME]
--		,NULL [MIDDLE_NAME]
--		,[PCP_LAST_NAME] [LAST_NAME]
--		,NULL [GENDER]
--		,[PCP_ORG_NAME] [TITLE]
--		,[PCP_GROUPER_NAME]  [GROUP_NAME]
--		,'ACTIVE' [STATUS]
--		,'[FHPDataMarts].[dbo].[Humana_CKD_Member]' [ROW_SOURCE]
--		,[HUMANA_CKD_MEMBER_ROW_ID] [ROW_SOURCE_ID]
--		,ROW_NUMBER() OVER (PARTITION BY [PCP_NPI_ID] ORDER BY [HUMANA_CKD_MEMBER_ROW_ID] ASC) [RANK]

--	FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]
--	WHERE [PCP_NPI_ID] IS NOT NULL


--)
--INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider]
--(
--	 [NPI]
--	,[TYPE]
--	,[FIRST_NAME]
--	,[MIDDLE_NAME]
--	,[LAST_NAME]
--	,[GENDER]
--	,[TITLE]
--	,[GROUP_NAME]
--	,[STATUS]
--	,[ROW_SOURCE]
--	,[ROW_SOURCE_ID]
--)
--SELECT DISTINCT

--	 [NPI]
--	,[TYPE]
--	,[FIRST_NAME]
--	,[MIDDLE_NAME]
--	,[LAST_NAME]
--	,[GENDER]
--	,[TITLE]
--	,[GROUP_NAME]
--	,[STATUS]
--	,[ROW_SOURCE]
--	,[ROW_SOURCE_ID]

--FROM Source_Ranked
--WHERE [RANK] = 1









--TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Provider_Address]


--;WITH Source_Ranked AS
--(
--	SELECT DISTINCT
--		 MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--		,'PRIMARY' [ADDRESS_TYPE]
--		,[PCP_ADDR_LINE1] [ADDRESS_1]
--		,[PCP_ADDR_LINE2] [ADDRESS_2]
--		,NULL [ADDRESS_3]
--		,[PCP_CITY_NAME] [CITY]
--		,[PCP_STATE_CD] [STATE]
--		,[PCP_ZIP_CD] [ZIP]
--		,NULL [COUNTY]
--		,'USA' [COUNTRY]
--		,[PCP_PHONE_NBR] [PHONE]
--		,NULL [ALTERNATE_PHONE]
--		,NULL [EVENING_PHONE]
--		,NULL [EMERGENCY_PHONE]
--		,NULL [FAX]
--		,NULL [EMAIL]
--		,'ACTIVE' [STATUS]
--		,'[FHPDataMarts].[dbo].[Humana_CKD_Member]' [ROW_SOURCE]
--		,[HUMANA_CKD_MEMBER_ROW_ID] [ROW_SOURCE_ID]
--		,ROW_NUMBER() OVER (PARTITION BY MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] ORDER BY [HUMANA_CKD_MEMBER_ROW_ID] ASC) [RANK]
--	FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
--	JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP
--	ON HCKDM.[PCP_NPI_ID] = MEP.[NPI]
--	WHERE HCKDM.[PCP_NPI_ID] IS NOT NULL

--)
--INSERT INTO [FHPDW].[dbo].[Membership_Export_Provider_Address]
--(
--	 [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--	,[ADDRESS_TYPE]
--	,[ADDRESS_1]
--	,[ADDRESS_2]
--	,[ADDRESS_3]
--	,[CITY]
--	,[STATE]
--	,[ZIP]
--	,[COUNTY]
--	,[COUNTRY]
--	,[PHONE]
--	,[ALTERNATE_PHONE]
--	,[EVENING_PHONE]
--	,[EMERGENCY_PHONE]
--	,[FAX]
--	,[EMAIL]
--	,[STATUS]
--	,[ROW_SOURCE]
--	,[ROW_SOURCE_ID]
--)
--SELECT DISTINCT
--	 [MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
--	,[ADDRESS_TYPE]
--	,[ADDRESS_1]
--	,[ADDRESS_2]
--	,[ADDRESS_3]
--	,[CITY]
--	,[STATE]
--	,[ZIP]
--	,[COUNTY]
--	,[COUNTRY]
--	,[PHONE]
--	,[ALTERNATE_PHONE]
--	,[EVENING_PHONE]
--	,[EMERGENCY_PHONE]
--	,[FAX]
--	,[EMAIL]
--	,[STATUS]
--	,[ROW_SOURCE]
--	,[ROW_SOURCE_ID]
--FROM Source_Ranked
--WHERE [RANK] = 1



TRUNCATE TABLE [FHPDW].[dbo].[Membership_Export_Member_Providers]


INSERT INTO [FHPDW].[dbo].[Membership_Export_Member_Providers]
(
	 [MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
	,[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
	,[ROW_SOURCE]
	,[ROW_SOURCE_ID]
)
SELECT DISTINCT
	 MEM.[MEMBERSHIP_EXPORT_MEMBER_ROW_ID] --[MEMBERSHIP_EXPORT_MEMBER_ROW_ID]
	,MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] --[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID]
	,'[FHPDataMarts].[dbo].[Humana_CKD_Member]' --[ROW_SOURCE]
	,[HUMANA_CKD_MEMBER_ROW_ID] --[ROW_SOURCE_ID]
FROM [FHPDataMarts].[dbo].[Humana_CKD_Member] HCKDM
JOIN [FHPDW].[dbo].[Membership_Export_Provider] MEP
ON HCKDM.[PCP_NPI_ID] = MEP.[NPI]
JOIN [FHPDW].[dbo].[Membership_Export_Member] MEM
ON HCKDM.[MBR_ID] = MEM.[ROW_SOURCE_ID]
WHERE MEM.LOB_TYPE = 'CKD'
AND MEM.LOB_VENDOR = 'HUMANA'



AND MEP.[MEMBERSHIP_EXPORT_PROVIDER_ROW_ID] = 100






SELECT * FROM [FHPDW].[dbo].[Membership_Export_Member_Providers]


[FHPDW].[dbo].[Membership_Export_Provider]


SELECT DISTINCT * FROM [FHPDW].[dbo].[Membership_Export_Provider_Address] A
LEFT JOIN [FHPDataMarts].[dbo].[Humana_CKD_Member] B
ON A.[ADDRESS_1] = B.[PCP_ADDR_LINE1]
WHERE B.[PCP_ADDR_LINE1] IS NULL



SELECT [ADDRESS_1], COUNT(1) FROM [FHPDW].[dbo].[Membership_Export_Provider_Address] A
GROUP BY [ADDRESS_1]
ORDER BY 2 DESC

SELECT * FROM [FHPDW].[dbo].[Membership_Export_Provider_Address] A
WHERE [ADDRESS_1] = '101 MEMORIAL HOSPITAL DR'


SELECT * FROM [FHPDataMarts].[dbo].[Humana_CKD_Member]
WHERE [PCP_ADDR_LINE1] = '101 MEMORIAL HOSPITAL DR'