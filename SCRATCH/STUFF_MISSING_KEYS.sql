;WITH HICN_MBI AS
	(
		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[HICN] AS [HICN_MBI]
			,[ROW_CREATE_DATE]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE HICN IS NOT NULL
		AND ROW_DELETED = 'N'

		UNION ALL

		SELECT
			 [MEMBER_MASTER_ROW_ID]
			,[MBI] AS [HICN_MBI]
			,[ROW_CREATE_DATE]
		FROM [FHPDataMarts].[dbo].[Member_Master_KeyHistory]
		WHERE MBI IS NOT NULL
		AND ROW_DELETED = 'N'
	)
	,STUFF_DATA AS
	(
		SELECT DISTINCT
			--ImportedData.MMR_LOADING_v2_ImportedDataID as [ImportedDataID]
			CAST(LTRIM(RTRIM(SUBSTRING(ImportedData.[ImportData],20,12))) AS VARCHAR(12)) AS [HICN_MBI]
			--,HICN_MBI.MEMBER_MASTER_ROW_ID
		FROM [FHPDataMarts].[dbo].[MMR_LOADING_v2_ImportedData] ImportedData
		LEFT JOIN HICN_MBI
		ON HICN_MBI.[HICN_MBI] = CAST(LTRIM(RTRIM(SUBSTRING(ImportedData.[ImportData],20,12))) AS VARCHAR(12))
		WHERE 1=1
		AND HICN_MBI.MEMBER_MASTER_ROW_ID IS NULL
		AND ImportedData.[RowDeleted] = 'N'
	)
	,STUFFED AS
	(
		SELECT TOP 1
			UnknownKeys = STUFF
			(
				(SELECT ',' + [HICN_MBI] FROM STUFF_DATA FOR XML PATH ('')), 1, 1, ''
			)
		FROM STUFF_DATA
	)
	SELECT
		*
	FROM STUFFED