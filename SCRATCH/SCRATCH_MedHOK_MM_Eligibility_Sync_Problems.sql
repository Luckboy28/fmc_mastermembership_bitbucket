


SELECT  
	 MM.[MEMBER_MASTER_ROW_ID]
	,MH.[Member ID]
	,ME.[START_DATE]
	,MHE.[Effective Date]
	,ME.[TERM_DATE]
	,MHE.[Term Date]
	,MHE.[Updated At]
	,ME.[ROW_UPDATE_DATE]
	,MHE.[Line of Business]
	,ME.[LOB_VENDOR]
	,MHE.[Company]
	,ME.[LOB_TYPE]
FROM [MedHok Nightly].[dbo].[Member] MH
JOIN [FHPDW].[dbo].[Member_Master] MM
	ON MM.[MEDHOK_ID] = MH.[Member ID]
JOIN [MedHok Nightly].[dbo].[Eligibility] MHE
	ON MHE.[MHK Member Internal ID] = MH.[MHK Member Internal ID]
JOIN [FHPDW].[dbo].[Member_Eligibility] ME
	ON MM.[MEMBER_MASTER_ROW_ID] = ME.[MEMBER_MASTER_ROW_ID]
WHERE ME.[START_DATE] = MHE.[Effective Date]
AND ISNULL(ME.[TERM_DATE],CAST('9999-12-31' AS DATE)) <> ISNULL(MHE.[Term Date],CAST('9999-12-31' AS DATE))
AND ((ME.[TERM_DATE] IS NULL AND MHE.[Term Date] IS NOT NULL) OR (ME.[TERM_DATE] IS NOT NULL AND MHE.[Term Date] IS NULL))
ORDER BY 1


SELECT * FROM [FHPDev].[dbo].[Member_Eligibility_MedHOK_Extracts] MME
WHERE [MEMBER_MASTER_ROW_ID] = 357
AND [START_DATE] = CAST('2016-12-01' AS DATE)


SELECT * FROM [FHPDW].[dbo].[Member_Master]
WHERE [MEMBER_MASTER_ROW_ID] = 357



--OR (ME.[TERM_DATE] IS NULL) OR (MHE.[Term Date] IS NULL))
