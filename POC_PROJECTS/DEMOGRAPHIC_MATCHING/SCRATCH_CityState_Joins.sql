
--TRUNCATE TABLE [FHPDev].[dbo].[Dave_MM_Demographic_Summary]

--INSERT INTO [FHPDev].[dbo].[Dave_MM_Demographic_Summary]
--(
--	 [MEMBER_MASTER_ROW_ID]
--	,[FIRST_NAME]
--	,[MIDDLE_NAME]
--	,[LAST_NAME]
--	,[DATE_OF_BIRTH]
--	,[MEMBER_ADDRESS_HISTORICAL_ROW_ID]
--	,[ADDRESS_1]
--	,[CITY]
--	,[STATE]
--	,[ZIP]
--)
--SELECT DISTINCT
--	 MM.[MEMBER_MASTER_ROW_ID]
--	,MM.[FIRST_NAME]
--	,MM.[MIDDLE_NAME]
--	,MM.[LAST_NAME]
--	,MM.[DATE_OF_BIRTH]
--	,MAH.[MEMBER_ADDRESS_HISTORICAL_ROW_ID]
--	,MAH.[ADDRESS_1]
--	,MAH.[CITY]
--	,MAH.[STATE]
--	,MAH.[ZIP]
--FROM [VH2-SQL-01].[FHPDataMarts].[dbo].[Member_Address_Historical] MAH
--JOIN [VH2-SQL-01].[FHPDW].[dbo].[Member_Master] MM
--	ON MAH.[MEMBER_MASTER_ROW_ID] = MM.[MEMBER_MASTER_ROW_ID]
--WHERE MAH.[ADDRESS_1] IS NOT NULL




---- UPDATE STATES
--;WITH States AS
--(
--	SELECT DISTINCT
--		 [STATE_FULL]
--		,[STATE_ABBREVIATION]
--	FROM [FHPDev].[dbo].[US_City_State_Zip_County]
--)
--UPDATE TheTable
--SET
--	[STATE] = ISNULL(States.[STATE_ABBREVIATION],TheTable.[STATE])

--FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary] TheTable
--LEFT JOIN States
--	ON TheTable.[STATE] = States.[STATE_FULL]
--WHERE TheTable.[STATE] <> ISNULL(States.[STATE_ABBREVIATION],TheTable.[STATE])





--SELECT DISTINCT
--	 TheTable.[CITY]
--	,TheLookup.[CITY]
--	,TheTable.[STATE]
--	,TheLookup.[STATE_ABBREVIATION]
--FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary] TheTable
--JOIN [FHPDev].[dbo].[US_City_State_Zip_County] TheLookup
--ON TheTable.[CITY] = TheLookup.[CITY]
--AND TheTable.[STATE] = TheLookup.[STATE_ABBREVIATION]



---- UPDATE STATE KEY FIELD
--;WITH States_Ranked AS
--(
--	SELECT DISTINCT
--		[STATE]
--		,DENSE_RANK() OVER (ORDER BY [STATE] ASC) AS [RANK]
--	FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary]
--	WHERE [STATE] IS NOT NULL
--	AND LEN([STATE]) = 2
--	--ORDER BY [STATE] ASC
--)
--UPDATE TheTable
--SET
--	TheTable.[STATE_ID] = States_Ranked.[RANK]
--FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary] TheTable
--JOIN States_Ranked
--	ON TheTable.[STATE] = States_Ranked.[STATE]
--WHERE TheTable.[STATE] IS NOT NULL
--AND LEN(TheTable.[STATE]) = 2



---- UPDATE CITY KEY FIELD
--;WITH Cities_Ranked AS
--(
--	SELECT DISTINCT
--		[CITY]
--		,DENSE_RANK() OVER (ORDER BY [CITY] ASC) AS [RANK]
--	FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary]
--	WHERE [CITY] IS NOT NULL
--	--ORDER BY [STATE] ASC
--)
--UPDATE TheTable
--SET
--	TheTable.[CITY_ID] = Cities_Ranked.[RANK]
--FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary] TheTable
--JOIN Cities_Ranked
--	ON TheTable.[CITY] = Cities_Ranked.[CITY]
--WHERE TheTable.[CITY] IS NOT NULL



---- UPDATE ZIP KEY FIELD
--UPDATE [FHPDev].[dbo].[Dave_MM_Demographic_Summary]
--SET
--	[ZIP_ID] = TRY_CAST(LEFT([ZIP],5) AS INT)


SELECT * FROM [FHPDev].[dbo].[Dave_MM_Demographic_Summary]